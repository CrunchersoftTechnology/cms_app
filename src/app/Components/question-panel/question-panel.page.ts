import { Storage } from '@ionic/storage';
import { ModalController, AlertController, PopoverController } from '@ionic/angular';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-question-panel',
  templateUrl: './question-panel.page.html',
  styleUrls: ['./question-panel.page.scss'],
})
export class QuestionPanelPage implements OnInit {
  selectedOption: {
    questionNo: number,
    option: string
  };

  questionArray: {
    index: number,
    questionNo: number,
    answered: string;
  }[] = [];

  page: string = '';


  @Input() index: number;
  @Input() totalQuestions: {
    questionNo: number,
    index: number,
    questionType: string
  }[] = [];
  @Input() answeredQuestions: {
    questionNo: number,
    option: string,
    userNumericalAns: string;
    userUnit: string;
  }[] = [];
  
  constructor(private modalController: ModalController,
              private storage: Storage,
              private popoverController: PopoverController) { }

  ngOnInit() {
    this.storage.ready().then(ready => {
      if(ready) {
        this.storage.get('page').then(result => {
          this.page = result;
        }) 
      }
    })  
    // console.log('in question panel');
    this.questionArray = [];
    // console.log('total questions',this.totalQuestions)
    // console.log('answered questions', this.answeredQuestions)
    this.totalQuestions.map(question => {
      this.selectedOption = this.answeredQuestions.find(({questionNo}) => questionNo == question.questionNo);
      if( this.selectedOption != undefined) {
        this.questionArray.push({
          index: question.index,
          questionNo: question.questionNo,
          answered: "true"
        })
      }
     else {
      this.questionArray.push({
        index: question.index,
        questionNo: question.questionNo,
        answered: ''
      })
     }
    })
    // console.log('questionsArray', this.questionArray)
  }

  selectQuestion(index) {
    this.storage.set('index', index).then(_ => {
      this.popoverController.dismiss();
    })
  }

  closeQuestionPanel() {
    this.popoverController.dismiss();
  }
}
