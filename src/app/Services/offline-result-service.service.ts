import { Injectable } from "@angular/core";
import { DatabaseService } from "./database.service";

@Injectable({
  providedIn: "root",
})
export class OfflineResultServiceService {
  constructor(private databaseService: DatabaseService) {}

  insertIntoCMSResults(
    SId,
    ObtainedMarks,
    TotalMarks,
    Timing,
    Duration,
    Title,
    Percentage,
    TestDate,
    Category,
    RStatus
  ) {
    return this.databaseService
      .getDataBase()
      .executeSql(
        `INSERT INTO CMSResults(SId, ObtainedMarks, TotalMarks, Timing, Duration, Title, Percentage, TestDate, Category, Status) values (?,?,?,?,?,?,?,?,?,?)`,
        [
          SId,
          ObtainedMarks,
          TotalMarks,
          Timing,
          Duration,
          Title,
          Percentage,
          TestDate,
          Category,
          RStatus,
        ]
      );
    // .then((_) =>
    //   this.databaseService
    //     .getDataBase()
    //     .executeSql(`select * from CMSResults`, [])
    //     .then((data) => console.log("offline result database", data))
    // );
  }

  deleteFromCMSResults() {
    return this.databaseService
      .getDataBase()
      .executeSql(`delete from CMSResults`, []);
  }
}
