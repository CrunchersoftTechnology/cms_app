import { Injectable } from "@angular/core";
import { DatabaseService } from "./database.service";
import { StorageServiceService } from "./storage-service.service";
import { OfflineTestSchedule } from "../Models/offlineTestSchedule";

@Injectable({
  providedIn: "root",
})
export class OfflineExamScheduleServiceService {
  constructor(private databaseService: DatabaseService) {}

  insertIntoDatabase(testDetails: OfflineTestSchedule) {
    return this.databaseService
      .getDataBase()
      .executeSql(
        `insert into OfflineExamSchedule(NotificationId, SubjectId, InTime, OutTime, TotalMark, Title, TestDate, Category) values (?,?,?,?,?,?,?,?)`,
        [
          testDetails.NotificationId,
          testDetails.SubjectId,
          testDetails.InTime,
          testDetails.OutTime,
          testDetails.TotalMark,
          testDetails.Title,
          testDetails.TestDate,
          testDetails.Category,
        ]
      );
  }

  createTable() {
    return this.databaseService
      .getDataBase()
      .executeSql(
        `create table OfflineExamSchedule (NotificationId text, SubjectId integer, InTime text, OutTime text, TotalMark integer, Title text, TestDate text, Category text)`,
        []
      );
  }

  getTestDetails() {
    return this.databaseService
      .getDataBase()
      .executeSql(`select * from OfflineExamSchedule`, [])
      .then((data) => {
        let testDetails: OfflineTestSchedule[] = [];
        for (let i = 0; i < data.rows.length; i++) {
          testDetails.push({
            Category: data.rows.item(i).Category,
            InTime: data.rows.item(i).InTime,
            NotificationId: data.rows.item(i).NotificationId,
            OutTime: data.rows.item(i).OutTime,
            SubjectId: data.rows.item(i).SubjectId,
            TestDate: data.rows.item(i).TestDate,
            Title: data.rows.item(i).Title,
            TotalMark: data.rows.item(i).TotalMark,
          });
        }
        return testDetails;
      });
  }

  checkTableExists() {
    return this.databaseService
      .getDataBase()
      .executeSql(
        `SELECT name FROM sqlite_master WHERE type='table' AND name='OfflineExamSchedule'`,
        []
      )
      .then((data) => {
        if (data.rows.length > 0) return true;
        else return false;
      });
  }
}
