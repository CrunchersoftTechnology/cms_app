import { Injectable } from "@angular/core";
import { DatabaseService } from "./database.service";
import { feesHistoryDetails, feesDetails } from "../Models/feesinfo";

@Injectable({
  providedIn: "root",
})
export class FeesServiceService {
  constructor(private databaseService: DatabaseService) {}

  deleteFromCMSFeesInfo() {
    return this.databaseService
      .getDataBase()
      .executeSql(`DELETE FROM CMSFeesInfo`, []);
  }

  insertIntoCMSFeesInfo(totalFees, paidFees, remainingFees) {
    return this.databaseService
      .getDataBase()
      .executeSql(
        `INSERT INTO CMSFeesInfo(totalFees, paidFees, remFees) values(?,?,?)`,
        [totalFees, paidFees, remainingFees]
      );
    // .then((_) =>
    //   this.databaseService
    //     .getDataBase()
    //     .executeSql(`Select * from CMSFeesInfo`, [])
    //     .then((data) => console.log(data))
    // );
  }

  deleteFromCMSFeesHistory() {
    return this.databaseService
      .getDataBase()
      .executeSql(`DELETE FROM CMSFeesHistory`, []);
  }

  insertIntoCMSFeesHistory(paidFees, paidDate) {
    return this.databaseService
      .getDataBase()
      .executeSql(
        `INSERT INTO CMSFeesHistory(paidFees, paidDate) values(?,?)`,
        [paidFees, paidDate]
      );
    // .then((_) =>
    //   this.databaseService
    //     .getDataBase()
    //     .executeSql(`Select * from CMSFeesHistory`, [])
    //     .then((data) => console.log(data))
    // );
  }

  getStudentFeesHistory() {
    return this.databaseService
      .getDataBase()
      .executeSql(`SELECT paidFees, paidDate FROM CMSFeesHistory`, [])
      .then((data) => {
        let feesDetails: feesHistoryDetails[] = [];
        for (let i = 0; i < data.rows.length; i++) {
          feesDetails.push({
            paidDate: data.rows.item(i).paidDate,
            paidFees: data.rows.item(i).paidFees,
          });
        }
        return feesDetails;
      });
  }

  getStudentFeesData() {
    return this.databaseService
      .getDataBase()
      .executeSql(`SELECT totalFees, paidFees, remFees FROM CMSFeesInfo`, [])
      .then((data) => {
        let feesDetails: feesDetails;
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            feesDetails = {
              paidFees: data.rows.item(i).paidFees,
              remainingFees: data.rows.item(i).remFees,
              totalFees: data.rows.item(i).totalFees,
            };
          }
        } else {
          feesDetails = {
            paidFees: "-",
            remainingFees: "-",
            totalFees: "-",
          };
        }
        return feesDetails;
      });
  }
}
