import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertServiceService {

  constructor(private alertController: AlertController) { }

  async createAlert(message) {
    const alert = await this.alertController.create({
      animated: true,
      backdropDismiss: false,
      subHeader: message,
      buttons: [
        {
          text: "Ok",
          role: "cancel"
        }
      ],
      cssClass: "alert-title",
    })
    await alert.present();
  }
}
