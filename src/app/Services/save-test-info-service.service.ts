import { Injectable } from "@angular/core";
import { DatabaseService } from "./database.service";
import { BehaviorSubject } from "rxjs";
import { SavedTestDetails } from "./../Models/savedTestDetails";

@Injectable({
  providedIn: "root",
})
export class SaveTestInfoServiceService {
  testDetailsObj = new BehaviorSubject(null);

  constructor(private databaseService: DatabaseService) {}

  insertIntoTable(testDetailsObj) {
    // console.log("insert into table service");
    let obj = [
      testDetailsObj.testDate,
      testDetailsObj.subjectName,
      testDetailsObj.totalQue,
      testDetailsObj.obtainedMarks,
      testDetailsObj.totalMarks,
      testDetailsObj.totalTime,
      testDetailsObj.attQueCount,
      testDetailsObj.nonAttQueCount,
      testDetailsObj.corrQueCount,
      testDetailsObj.inCorrQueCount,
      testDetailsObj.timeTakenMin,
      testDetailsObj.timeTakenSec,
      testDetailsObj.testType,
      testDetailsObj.questions,
    ];
    return this.databaseService
      .getDataBase()
      .executeSql(
        `INSERT INTO save_test_info (TEST_DATE, SUBJECT_NAME, TOTAL_QUES, OBTAINED_MARKS, TOTAL_MARKS, TOTAL_TIME, ATTEMPTED, NON_ATTEMPTED,
          CORR_QUE, INCORR_QUE, TIME_TAKEN_MIN, TIME_TAKEN_SEC, TEST_TYPE, QUESTIONS) 
          values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
        obj
      )
      .then((data) => {
        // console.log("in insert table ");
        // console.log('in save test select', data)
        // console.log('in save test select')
        // this.databaseService
        //   .getDataBase()
        //   .executeSql(`SELECT * FROM save_test_info `, [])
        //   .then((data) => {
        //     console.log("in save test select", data);
        //   });
        this.getTestDetails();
      });
  }

  getTestDetails() {
    // console.log('in get test details service')
    return this.databaseService
      .getDataBase()
      .executeSql(
        `SELECT TEST_ID, TEST_DATE, SUBJECT_NAME, OBTAINED_MARKS, TOTAL_MARKS FROM save_test_info order by TEST_ID desc`,
        []
      )
      .then((data) => {
        let testDetails = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            testDetails.push({
              testId: data.rows.item(i).TEST_ID,
              testDate: data.rows.item(i).TEST_DATE,
              subjectName: data.rows.item(i).SUBJECT_NAME,
              obtainedMarks: data.rows.item(i).OBTAINED_MARKS,
              totalMarks: data.rows.item(i).TOTAL_MARKS,
            });
          }
          this.testDetailsObj.next(testDetails);
        } else {
          this.testDetailsObj.next(testDetails);
        }
      });
  }

  getTest() {
    // console.log('in get test service')
    this.getTestDetails();
    return this.testDetailsObj.asObservable();
  }

  getTestDetailsForTest(testId) {
    return this.databaseService
      .getDataBase()
      .executeSql(`SELECT * FROM save_test_info WHERE TEST_ID = ?`, [testId])
      .then((data) => {
        let testDetails: SavedTestDetails;
        if (data.rows.length > 0) {
          testDetails = {
            testDate: data.rows.item(0).TEST_DATE,
            subjectName: data.rows.item(0).SUBJECT_NAME,
            totalQue: data.rows.item(0).TOTAL_QUES,

            obtainedMarks: data.rows.item(0).OBTAINED_MARKS,
            totalMarks: data.rows.item(0).TOTAL_MARKS,
            totalTime: data.rows.item(0).TOTAL_TIME,
            attQueCount: data.rows.item(0).ATTEMPTED,
            nonAttQueCount: data.rows.item(0).NON_ATTEMPTED,
            corrQueCount: data.rows.item(0).CORR_QUE,
            inCorrQueCount: data.rows.item(0).INCORR_QUE,
            timeTakenMin: data.rows.item(0).TIME_TAKEN_MIN,
            timeTakenSec: data.rows.item(0).TIME_TAKEN_SEC,
            testType: data.rows.item(0).TEST_TYPE,
            questions: data.rows.item(0).QUESTIONS,
          };
        }
        return testDetails;
      });
  }

  getSavedTestsCount() {
    return this.databaseService
      .getDataBase()
      .executeSql(`select count(*) as testsCount from save_test_info`, [])
      .then((data) => {
        return data.rows.item(0).testsCount;
      });
  }

  deleteFromSaveTestInfo() {
    return this.databaseService
      .getDataBase()
      .executeSql(`delete from save_test_info`, []);
  }
}
