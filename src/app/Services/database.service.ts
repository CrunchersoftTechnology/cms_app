import { Injectable } from "@angular/core";
import { SQLite, SQLiteObject } from "@ionic-native/sqlite/ngx";
import { SQLitePorter } from "@ionic-native/sqlite-porter/ngx";
import { Platform } from '@ionic/angular';
import { BehaviorSubject } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class DatabaseService {
  private dataBase: SQLiteObject;
  private dbready: BehaviorSubject<boolean> = new BehaviorSubject(false);
  constructor(
    private plt: Platform,
    private sqlitePorter: SQLitePorter,
    private sqlite: SQLite
  ) {}
  createDataBase() {
    return this.plt.ready().then(() => {
      this.sqlite
        .create({
          name: "CMSDatabase.db",
          location: "default",
          createFromLocation: 1
        })
        .then((db: SQLiteObject) => {
          this.dataBase = db;
          this.dbready.next(true);
        });
    });
  }
  getDatabaseState() {
    return this.dbready.asObservable();
  }

  getDataBase() {
    return this.dataBase;
  }
}
