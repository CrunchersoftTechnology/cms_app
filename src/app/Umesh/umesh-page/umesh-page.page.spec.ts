import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UmeshPagePage } from './umesh-page.page';

describe('UmeshPagePage', () => {
  let component: UmeshPagePage;
  let fixture: ComponentFixture<UmeshPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmeshPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UmeshPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
