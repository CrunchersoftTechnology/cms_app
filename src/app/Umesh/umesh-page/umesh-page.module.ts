import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UmeshPagePageRoutingModule } from './umesh-page-routing.module';

import { UmeshPagePage } from './umesh-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UmeshPagePageRoutingModule
  ],
  declarations: [UmeshPagePage]
})
export class UmeshPagePageModule {}
