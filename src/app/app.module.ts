import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { HTTP } from "@ionic-native/http/ngx";
import { SQLite } from "@ionic-native/sqlite/ngx";
import { SQLitePorter } from "@ionic-native/sqlite-porter/ngx";
import { IonicStorageModule } from "@ionic/storage";
import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Network } from "@ionic-native/network/ngx";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic/ngx";
import { PreviewAnyFile } from "@ionic-native/preview-any-file/ngx";
import { PhotoViewer } from "@ionic-native/photo-viewer/ngx";
import { StreamingMedia } from "@ionic-native/streaming-media/ngx";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { Keyboard } from "@ionic-native/keyboard/ngx";
import { Device } from "@ionic-native/device/ngx";
import { AppAvailability } from '@ionic-native/app-availability/ngx';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
import { CalenderComponentModule } from "./Components/calender-component/calender-component.module";

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    CalenderComponentModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    HTTP,
    SQLite,
    SQLitePorter,
    Network,
    FCM,
    PreviewAnyFile,
    PhotoViewer,
    StreamingMedia,
    InAppBrowser,
    AppAvailability,
    Keyboard,
    Device,
    YoutubeVideoPlayer
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
