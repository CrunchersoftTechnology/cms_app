import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClassTimeTablePagePage } from './class-time-table-page.page';

const routes: Routes = [
  {
    path: '',
    component: ClassTimeTablePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClassTimeTablePagePageRoutingModule {}
