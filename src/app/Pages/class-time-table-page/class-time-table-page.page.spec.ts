import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClassTimeTablePagePage } from './class-time-table-page.page';

describe('ClassTimeTablePagePage', () => {
  let component: ClassTimeTablePagePage;
  let fixture: ComponentFixture<ClassTimeTablePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassTimeTablePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClassTimeTablePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
