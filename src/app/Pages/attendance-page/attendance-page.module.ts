import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AttendancePagePageRoutingModule } from './attendance-page-routing.module';

import { AttendancePagePage } from './attendance-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AttendancePagePageRoutingModule
  ],
  declarations: [AttendancePagePage]
})
export class AttendancePagePageModule {}
