import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LiveTestPagePage } from './live-test-page.page';

describe('LiveTestPagePage', () => {
  let component: LiveTestPagePage;
  let fixture: ComponentFixture<LiveTestPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveTestPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LiveTestPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
