import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LiveTestPagePageRoutingModule } from './live-test-page-routing.module';

import { LiveTestPagePage } from './live-test-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LiveTestPagePageRoutingModule
  ],
  declarations: [LiveTestPagePage]
})
export class LiveTestPagePageModule {}
