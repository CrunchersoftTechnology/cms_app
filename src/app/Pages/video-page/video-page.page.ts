import { Component, OnInit } from "@angular/core";
import {
  StreamingMedia,
  StreamingVideoOptions,
} from "@ionic-native/streaming-media/ngx";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { ApiServiceService } from "src/app/Services/api-service.service";
import { VideoInfo } from "src/app/Models/fileInfo";
import { AppAvailability } from "@ionic-native/app-availability/ngx";
import {
  InAppBrowser,
  InAppBrowserObject,
} from "@ionic-native/in-app-browser/ngx";
import { YoutubeVideoPlayer } from "@ionic-native/youtube-video-player/ngx";

@Component({
  selector: "app-video-page",
  templateUrl: "./video-page.page.html",
  styleUrls: ["./video-page.page.scss"],
})
export class VideoPagePage implements OnInit {
  subjects: string[] = [];
  selectedSubjects: string[] = [];
  subjectDetails: {
    subjectId: string;
    subjectName: string;
  }[] = [];
  videoInfo: VideoInfo[] = [];
  selectedSubject: string = "0";
  branchId: string = "";
  classId: string = "";
  // apiKey = "AIzaSyB1IxCfQeRjZOYn744r3Zdl3w0grnITHz8"

  constructor(
    public storageService: StorageServiceService,
    private apiService: ApiServiceService,
    private streamingMedia: StreamingMedia,
    private youtube: YoutubeVideoPlayer,
    private appAvailability: AppAvailability,
    private inAppBrowser: InAppBrowser
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    this.branchId = this.storageService.userDetails.branchId;
    this.classId = this.storageService.userDetails.classId;
    this.subjects = this.storageService.userDetails.subjectName.split(",");
    this.selectedSubjects = this.storageService.userDetails.selectedSubjects.split(
      ","
    );
    this.subjectDetails = [];
    for (let i = 0; i < this.subjects.length; i++) {
      this.subjectDetails.push({
        subjectId: this.selectedSubjects[i],
        subjectName: this.subjects[i],
      });
    }
    this.selectedSubject = this.subjectDetails[0].subjectId;
    this.getVideoList(this.branchId, this.classId, this.selectedSubject);
  }

  onSelectChange() {
    this.getVideoList(this.branchId, this.classId, this.selectedSubject);
  }

  getVideoList(branchId, classId, subjectId) {
    this.apiService.getVideo(branchId, classId, subjectId).then((data) => {
      console.log(JSON.parse(data.data));
      this.videoInfo = JSON.parse(data.data).d;
    });
  }

  onVideoClick(vidoeUrl) {
    // let options: StreamingVideoOptions = {
    //   orientation: "landscape",
    //   controls: true,
    //   successCallback: () => console.log("successfully played"),
    //   errorCallback: (e) => console.log("Error", e),
    // };

    // this.streamingMedia.playVideo(link, options);
    // this.youtube.openVideo(link);

    const browser: InAppBrowserObject = this.inAppBrowser.create(
      vidoeUrl,
      "_system"
    );
  }

  doRefresh(event) {
    this.getVideoList(this.branchId, this.classId, this.selectedSubject);
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  // onClick(video) {
  //   this.youtube.openVideo(video)
  // }
}
