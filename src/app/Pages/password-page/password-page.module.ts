import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PasswordPagePageRoutingModule } from './password-page-routing.module';

import { PasswordPagePage } from './password-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasswordPagePageRoutingModule
  ],
  declarations: [PasswordPagePage]
})
export class PasswordPagePageModule {}
