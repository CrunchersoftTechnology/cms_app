import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { AlertServiceService } from "./../../Services/alert-service.service";
import { HTTP } from "@ionic-native/http/ngx";
import { ToastServiceService } from "./../../Services/toast-service.service";
import { OtpServiceService } from "./../../Services/otp-service.service";
import { ApiServiceService } from "./../../Services/api-service.service";
import { UserDetailsService } from "src/app/Services/user-details.service";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-password-page",
  templateUrl: "./password-page.page.html",
  styleUrls: ["./password-page.page.scss"],
})
export class PasswordPagePage implements OnInit {
  password: string = "";
  randomNumber: number;
  enteredOTP: string = "";
  inputType: string = "password";
  iconName: string = "eye";
  showErrorMessage: boolean = false;
  constructor(
    private router: Router,
    private storage: Storage,
    private navController: NavController,
    private storageService: StorageServiceService,
    private alertService: AlertServiceService,
    private otpService: OtpServiceService,
    private apiService: ApiServiceService,
    private userDetailsService: UserDetailsService
  ) {}

  ngOnInit() {}

  onLogin() {
    if (this.password == this.storageService.userInfo.AppPassword) {
      this.password = "";
      this.userDetailsService.getStudentAccess(
        this.storageService.userInfo.UserId
      );
      this.showErrorMessage = false;
    } else this.showErrorMessage = true;
  }

  onForgotPassClick() {
    this.router.navigateByUrl("/forgot-password-page");
    this.showErrorMessage = false;
  }

  changeInputType() {
    if (this.inputType == "password") {
      this.inputType = "text";
      this.iconName = "eye-off";
    } else {
      this.inputType = "password";
      this.iconName = "eye";
    }
  }

  onBackClick() {
    this.navController.back();
  }
}
