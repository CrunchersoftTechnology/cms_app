import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PasswordPagePage } from './password-page.page';

const routes: Routes = [
  {
    path: '',
    component: PasswordPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PasswordPagePageRoutingModule {}
