import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExamTimeTablePagePage } from './exam-time-table-page.page';

const routes: Routes = [
  {
    path: '',
    component: ExamTimeTablePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExamTimeTablePagePageRoutingModule {}
