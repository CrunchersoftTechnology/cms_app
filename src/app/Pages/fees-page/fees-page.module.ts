import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FeesPagePageRoutingModule } from './fees-page-routing.module';

import { FeesPagePage } from './fees-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FeesPagePageRoutingModule
  ],
  declarations: [FeesPagePage]
})
export class FeesPagePageModule {}
