import { Component, OnInit, ViewChild } from "@angular/core";
import { Chart } from "chart.js";
import { ApiServiceService } from "./../../Services/api-service.service";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { FeesServiceService } from "./../../Services/fees-service.service";
import { feesHistoryDetails, feesDetails } from "src/app/Models/feesinfo";
import { InternetServiceService } from "./../../Services/internet-service.service";
import { ToastServiceService } from "./../../Services/toast-service.service";

@Component({
  selector: "app-fees-page",
  templateUrl: "./fees-page.page.html",
  styleUrls: ["./fees-page.page.scss"],
})
export class FeesPagePage implements OnInit {
  @ViewChild("pieChart") pieChart1;
  pie: any;
  userId: string = "";
  feesHistoryDetails: feesHistoryDetails[] = [];
  feesDetails: feesDetails = {
    paidFees: "",
    remainingFees: "",
    totalFees: "",
  };
  constructor(
    private apiService: ApiServiceService,
    public storageService: StorageServiceService,
    private feesService: FeesServiceService,
    private internetService: InternetServiceService,
    private toastService: ToastServiceService
  ) {}

  ngOnInit() {
    this.userId = this.storageService.userDetails.userId;
    if (this.internetService.networkConnected) this.getFeesData(this.userId);
    else {
      this.getStudentFeesData();
      this.getStudentFeesHistory();
      this.toastService.createToast(
        "Check internet connection to update details",
        2000
      );
    }
  }

  getFeesData(userId) {
    this.apiService.getFeesData(userId).then(
      (result) => {
        // console.log(JSON.parse(result.data));
        let data = JSON.parse(result.data);
        var length = data.d.length;
        // console.log('length', length)
        if (length > 0) {
          this.feesService.deleteFromCMSFeesInfo().then((_) => {
            for (var i = 0; i < data.d.length; i++) {
              var total_fees = data.d[i].total_fee;
              var paid_fees = data.d[i].paid_fee;
              var rem_fees = parseInt(total_fees) - parseInt(paid_fees);
              this.feesService
                .insertIntoCMSFeesInfo(total_fees, paid_fees, rem_fees)
                .then((_) => this.getFeesHistory(userId));
            }
          });
        } else if (length == 0) {
          this.getFeesHistory(userId);
        }
      },
      (err) => {
        this.getFeesHistory(userId);
      }
    );
  }

  getFeesHistory(userId) {
    this.apiService.getFeesHistory(userId).then(
      (result) => {
        let data = JSON.parse(result.data);
        // console.log('history',data);
        var length = data.d.length;
        if (length > 0) {
          var dataLength: number = 0;
          this.feesService.deleteFromCMSFeesHistory().then((_) => {
            for (var i = 0; i < data.d.length; i++) {
              var paid_fees = data.d[i].payment;
              var paid_date = data.d[i].paid_date;
              dataLength = i + 1;
              this.feesService.insertIntoCMSFeesHistory(paid_fees, paid_date);
            }
            if (dataLength == data.d.length) {
              this.getStudentFeesData();
              this.getStudentFeesHistory();
            }
          });
        } else if (length == 0) {
          this.getStudentFeesData();
          this.getStudentFeesHistory();
        }
      },
      (err) => {
        this.getStudentFeesData();
        this.getStudentFeesHistory();
      }
    );
  }

  getStudentFeesData() {
    this.feesService.getStudentFeesData().then((data) => {
      this.feesDetails = data;
      console.log(this.feesDetails);
      this.pie = new Chart(this.pieChart1.nativeElement, {
        type: "pie",
        data: {
          labels: ["Paid", "Remaining"],
          datasets: [
            {
              label: "Fees Details",
              data: [this.feesDetails.paidFees, this.feesDetails.remainingFees],
              backgroundColor: ["#2dd36f", "#eb445a"],
            },
          ],
        },
        options: {
          tooltips: {
            enabled: true,
          },
        },
      });
    });
  }

  getStudentFeesHistory() {
    this.feesService.getStudentFeesHistory().then((data) => {
      this.feesHistoryDetails = data;
      console.log(this.feesHistoryDetails);
    });
  }

  doRefresh(event) {
    if (this.internetService.networkConnected) this.getFeesData(this.userId);
    else {
      this.getStudentFeesData();
      this.getStudentFeesHistory();
      this.toastService.createToast(
        "Check internet connection to update details",
        3000
      );
    }
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}
