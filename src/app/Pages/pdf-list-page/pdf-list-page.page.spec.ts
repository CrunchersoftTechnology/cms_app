import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PdfListPagePage } from './pdf-list-page.page';

describe('PdfListPagePage', () => {
  let component: PdfListPagePage;
  let fixture: ComponentFixture<PdfListPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfListPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PdfListPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
