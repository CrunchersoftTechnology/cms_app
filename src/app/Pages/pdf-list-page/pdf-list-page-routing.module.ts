import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PdfListPagePage } from './pdf-list-page.page';

const routes: Routes = [
  {
    path: '',
    component: PdfListPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PdfListPagePageRoutingModule {}
