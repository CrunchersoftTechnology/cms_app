import { Component, OnInit } from "@angular/core";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { ApiServiceService } from "src/app/Services/api-service.service";
import { OfflineTestSchedule } from "src/app/Models/offlineTestSchedule";
import { OfflineExamScheduleServiceService } from "./../../Services/offline-exam-schedule-service.service";
import { InternetServiceService } from "./../../Services/internet-service.service";
import { ToastServiceService } from "./../../Services/toast-service.service";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-offline-exam-schedule-page",
  templateUrl: "./offline-exam-schedule-page.page.html",
  styleUrls: ["./offline-exam-schedule-page.page.scss"],
})
export class OfflineExamSchedulePagePage implements OnInit {
  classId: string = "";
  branchId: string = "";
  clientId: string = "";
  selectedBatches: string = "";
  selectedSubjects: string = "";
  userDetails: any;
  offlineTestData: OfflineTestSchedule[] = [];
 
  constructor(
    private storage: Storage,
    public storageService: StorageServiceService,
    private apiService: ApiServiceService,
    private offlineExamScheduleService: OfflineExamScheduleServiceService,
    private internetService: InternetServiceService,
    private toastService: ToastServiceService
  ) {}

  ngOnInit() {
    this.userDetails = {
      classId: this.storageService.userDetails.classId,
      clientId: this.storageService.userDetails.clientId,
      branchId: this.storageService.userDetails.branchId,
      selectedBatches: this.storageService.userDetails.batchId,
      selectedSubjects: this.storageService.userDetails.selectedSubjects,
    };
    this.offlineExamScheduleService.checkTableExists().then((exists) => {
      console.log("exists", exists)
      if (exists == true) {
        this.saveTestData();
      } else {
        this.offlineExamScheduleService.createTable().then((_) => {
          this.saveTestData();
        });
      }
    });
  }

  saveTestData() {
    if (this.internetService.networkConnected) {
      this.storage.get("offlineTestScheduleId").then((data) => {
        console.log("offlineTestScheduleId", data);
        if (data){ this.getOfflineTestSchedule(this.userDetails, data);
        }
        else
        { 
          console.log("umesh...............");
          this.getOfflineTestSchedule(this.userDetails, "0");
      }
      });
    } else {
      this.getOfflineTestScheduleDatabase();
      this.toastService.createToast(
        "Check internet connection to update details",
        3000
      );
    }
  }

  getOfflineTestSchedule(userDetails, notificationId) {
    this.apiService
      .getCMSOfflineTestNotification(userDetails, notificationId)
      .then((result) => {
        console.log("Offline Test Detailssssssss", JSON.parse(result.data).d);
        let data = JSON.parse(result.data);
        let length = data.d.length;
        if (length > 0) {
          let offlineTestData: OfflineTestSchedule;
          let iteration: number = 0;
          for (let i = 0; i < length; i++) {
            iteration += 1;


        

           

            offlineTestData = {
              Category: data.d[i].Category,
              InTime: data.d[i].InTime,
              NotificationId: data.d[i].NotificationId,
              OutTime: data.d[i].OutTime,
              SubjectId: data.d[i].SubjectId,
              TestDate: data.d[i].TestDate,
              Title: data.d[i].Title,
              TotalMark: data.d[i].TotalMark,
            };
            this.offlineExamScheduleService
              .insertIntoDatabase(offlineTestData)
              .then((_) => {
                console.log("Added sucessfully", offlineTestData);
                if (iteration == length) this.getOfflineTestScheduleDatabase();
              });
            // .catch((err) => {
            //   console.log("error", err);
            //   this.offlineExamScheduleService.createTable().then((_) =>
            //     this.offlineExamScheduleService
            //       .insertIntoDatabase(offlineTestData)
            //       .then((_) => {
            //         console.log("Added sucessfully 1", offlineTestData);
            //         if (iteration == length)
            //           this.getOfflineTestScheduleDatabase();
            //       })
            //   );
            // });
          }
        } else {
          this.getOfflineTestScheduleDatabase();
        }
      });
  }

  getOfflineTestScheduleDatabase() {
    this.offlineExamScheduleService
      .getTestDetails()
      .then((data: OfflineTestSchedule[]) => {
        this.offlineTestData = data;
        if (this.offlineTestData.length > 0) {
          this.storage.set(
            "offlineTestScheduleId",
            this.offlineTestData[this.offlineTestData.length - 1].NotificationId
          );


          
          console.log(
            "offline Test Data",
            this.offlineTestData,
            "offlineTestScheduleId",
            this.offlineTestData[this.offlineTestData.length - 1].NotificationId,
            this.offlineTestData[this.offlineTestData.length - 1].InTime,

          );



        }
      });
  }


  dateConvert(date){

    console.log("umesh test datessss",
      date  
    );
    let start=0;
    let inDateString="";
    for(let i=0;i<date.length;i++){

if( date[i] =='('){
start=1;
continue;

}else if(date[i] ==')'){

break;
}

if(start==1){

  inDateString=inDateString+date[i];

}
    
    }



let d=new Date(parseInt(inDateString));

let hours=d.getHours();
let seconds=d.getSeconds();
let finalHours=0;
let ampm="";
if(hours>12){
ampm="PM"
finalHours=hours/12;

}else{
  ampm="AM"
}

let dateS=finalHours.toString().split(".")[0]+" : "+seconds.toString()+" "+ampm;




return dateS;
  




  }


}
