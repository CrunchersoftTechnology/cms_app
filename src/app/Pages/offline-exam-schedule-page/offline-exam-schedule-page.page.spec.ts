import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OfflineExamSchedulePagePage } from './offline-exam-schedule-page.page';

describe('OfflineExamSchedulePagePage', () => {
  let component: OfflineExamSchedulePagePage;
  let fixture: ComponentFixture<OfflineExamSchedulePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfflineExamSchedulePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OfflineExamSchedulePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
