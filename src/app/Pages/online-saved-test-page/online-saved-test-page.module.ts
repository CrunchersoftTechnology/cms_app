import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OnlineSavedTestPagePageRoutingModule } from './online-saved-test-page-routing.module';

import { OnlineSavedTestPagePage } from './online-saved-test-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OnlineSavedTestPagePageRoutingModule
  ],
  declarations: [OnlineSavedTestPagePage]
})
export class OnlineSavedTestPagePageModule {}
