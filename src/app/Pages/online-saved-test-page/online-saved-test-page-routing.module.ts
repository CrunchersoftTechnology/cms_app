import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnlineSavedTestPagePage } from './online-saved-test-page.page';

const routes: Routes = [
  {
    path: '',
    component: OnlineSavedTestPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OnlineSavedTestPagePageRoutingModule {}
