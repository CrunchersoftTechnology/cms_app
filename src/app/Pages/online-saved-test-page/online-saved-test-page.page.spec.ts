import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OnlineSavedTestPagePage } from './online-saved-test-page.page';

describe('OnlineSavedTestPagePage', () => {
  let component: OnlineSavedTestPagePage;
  let fixture: ComponentFixture<OnlineSavedTestPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlineSavedTestPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OnlineSavedTestPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
