import { Component, OnInit } from "@angular/core";
import { OfflineResultServiceService } from "./../../Services/offline-result-service.service";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { InternetServiceService } from "./../../Services/internet-service.service";
import { ToastServiceService } from "./../../Services/toast-service.service";
import { ApiServiceService } from "./../../Services/api-service.service";
import { OfflineResult } from "src/app/Models/offlineResult";

@Component({
  selector: "app-offline-result-page",
  templateUrl: "./offline-result-page.page.html",
  styleUrls: ["./offline-result-page.page.scss"],
})
export class OfflineResultPagePage implements OnInit {
  userId: string = "";
  sid: string = "";
  offLineResult: OfflineResult[] = [];
  constructor(
    private offlineResult: OfflineResultServiceService,
    public storageService: StorageServiceService,
    private internetService: InternetServiceService,
    private toastService: ToastServiceService,
    private apiService: ApiServiceService
  ) {}

  ngOnInit() {
    this.userId = this.storageService.userDetails.userId;
    this.sid = this.storageService.userDetails.sId;
    if (this.internetService.networkConnected) this.getResult();
    else {
      this.getStudentResult();
      this.toastService.createToast(
        "Check internet connection to update details",
        3000
      );
    }
  }

  getResult() {
    this.offLineResult = [];
    this.apiService.getResult(this.userId).then(
      (result) => {
        let data = JSON.parse(result.data);
        // this.offLineResult = data.d;
        data.d.map((result) => {
          if (result.Category == "Offline") this.offLineResult.push(result);
        });
        console.log("offlineResult", this.offLineResult);
        let length = data.d.length;
        if (length > 0) {
          let dataLength = 0;
          this.offlineResult.deleteFromCMSResults().then((_) => {
            for (let i = 0; i < data.d.length; i++) {
              let ObtainedMarks = data.d[i].ObtainedMarks;
              let TotalMarks = data.d[i].TotalMarks;
              let Timing = data.d[i].Timing;
              let Duration = data.d[i].Duration;
              let Title = data.d[i].Title;
              let Percentage = data.d[i].Percentage;
              let TestDate = data.d[i].TestDate;
              let Category = data.d[i].Category;
              let RStatus = data.d[i].Status;
              dataLength = i + 1;
              this.offlineResult.insertIntoCMSResults(
                this.sid,
                ObtainedMarks,
                TotalMarks,
                Timing,
                Duration,
                Title,
                Percentage,
                TestDate,
                Category,
                RStatus
              );
            }
            if (dataLength == data.d.length) this.getStudentResult();
          });
        } else if (length == 0)
          this.offlineResult
            .deleteFromCMSResults()
            .then((_) => this.getStudentResult());
      },
      (err) => this.getStudentResult()
    );
  }

  getStudentResult() {}

  doRefresh(event) {
    if (this.internetService.networkConnected) this.getResult();
    else {
      this.getStudentResult();
      this.toastService.createToast(
        "Check internet connection to update details",
        3000
      );
    }
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}
