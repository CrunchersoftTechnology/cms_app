import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OfflineResultPagePageRoutingModule } from './offline-result-page-routing.module';

import { OfflineResultPagePage } from './offline-result-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OfflineResultPagePageRoutingModule
  ],
  declarations: [OfflineResultPagePage]
})
export class OfflineResultPagePageModule {}
