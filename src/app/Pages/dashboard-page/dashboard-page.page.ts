import { Component, OnInit } from "@angular/core";
import { Router, RouterEvent } from "@angular/router";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Storage } from "@ionic/storage";
import { StorageServiceService } from "src/app/Services/storage-service.service";
import {
  MenuController,
  LoadingController,
  AlertController,
} from "@ionic/angular";
import { LoadingServiceService } from "./../../Services/loading-service.service";
import { DatabaseService } from "./../../Services/database.service";
import { UserDetailsService } from "./../../Services/user-details.service";
import { AttendanceServiceService } from "./../../Services/attendance-service.service";
import { FeesServiceService } from "./../../Services/fees-service.service";
import { NotificationServiceService } from "src/app/Services/notification-service.service";
import { InternetServiceService } from "src/app/Services/internet-service.service";
import { LastNotification } from "src/app/Models/notificationInfo";
import { ApiServiceService } from "./../../Services/api-service.service";
import { SaveTestInfoServiceService } from "./../../Services/save-test-info-service.service";
import { OfflineResultServiceService } from "./../../Services/offline-result-service.service";
import { DailyPracticePaperServiceService } from "./../../Services/daily-practice-paper-service.service";
import { TimeTableServiceService } from "./../../Services/time-table-service.service";
import { UserDetails } from "src/app/Models/userInfo";

@Component({
  selector: "app-dashboard-page",
  templateUrl: "./dashboard-page.page.html",
  styleUrls: ["./dashboard-page.page.scss"],
})
export class DashboardPagePage implements OnInit {
  pages = [
    {
      title: "HOME",
      url: "/dashboard-page/home-page",
      icon: "home",
    },
    {
      title: "PROFILE",
      url: "/dashboard-page/profile",
      icon: "person",
    },
    {
      title: "ATTENDANCE",
      url: "/dashboard-page/attendance-page",
      icon: "clipboard",
    },

    {
      title: "FEES",
      url: "/dashboard-page/fees-page",
      icon: "newspaper",
    },
    {
      title: "LIVE TESTS",
      url: "/dashboard-page/live-test-page",
      icon: "newspaper",
    },
    {
      title: "OFFLINE EXAM SCHEDULE",
      url: "/dashboard-page/offline-exam-schedule-page",
      icon: "newspaper",
    },
    {
      title: "NOTIFICATIONS",
      url: "/dashboard-page/notifications-page",
      icon: "medal",
    },
    {
      title: "ONLINE RESULT",
      url: "/dashboard-page/online-saved-test-page",
      icon: "browsers",
    },
    {
      title: "OFFINE RESULT",
      url: "/dashboard-page/offline-result-page",
      icon: "copy",
    },
    {
      title: "STUDY MATERIAL",
      url: "/dashboard-page/pdf-page",
      icon: "bookmark",
    },
    {
      title: "EXAM TIMETABLE",
      url: "/dashboard-page/class-time-table-page/2",
      icon: "bar-chart",
    },
    {
      title: "EXPERT LECTURE TIMETABLE",
      url: "/dashboard-page/class-time-table-page/1",
      icon: "notifications",
    },
    {
      title: "DPP",
      url: "/dashboard-page/daily-practice-paper-page",
      icon: "desktop",
    },
    {
      title: "VIDEOS",
      url: "/dashboard-page/video-page",
      icon: "settings",
    },

    {
      title: "ABOUT US",
      url: "/dashboard-page/about-us-page",
      icon: "desktop",
    },
    {
      title: "LOGOUT",
      url: "",
      icon: "log-out",
    },
  ];
  selectedPath = "";

  lastNotification: LastNotification;
  sid: string;
  notificationsCount: number = 0;
  constructor(
    private router: Router,
    private storage: Storage,
    private statusBar: StatusBar,
    private loadingController: LoadingController,
    private menuController: MenuController,
    private alertController: AlertController,
    public storageService: StorageServiceService,
    private loadingService: LoadingServiceService,
    private databaseService: DatabaseService,
    private userDetailsService: UserDetailsService,
    private attendanceService: AttendanceServiceService,
    private feesService: FeesServiceService,
    private notificationService: NotificationServiceService,
    private internetService: InternetServiceService,
    private apiServiceService: ApiServiceService,
    private saveTestInfoServiceService: SaveTestInfoServiceService,
    private offlineService: OfflineResultServiceService,
    private dailyPracticePaperService: DailyPracticePaperServiceService,
    private timeTableService: TimeTableServiceService,
    private apiService: ApiServiceService
  ) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
  }

  ngOnInit() {
    // console.log("user info", this.storageService.userInfo.UserId);
    // this.databaseService.getDatabaseState().subscribe((ready) => {
    //   if (ready)
    //     this.userDetailsService
    //       .selectFromCMSRegistrationDetails()
    //       .then((data: UserDetails) => {
    //         this.storageService.userDetails = data;
    //         console.log(
    //           "user details home page",
    //           this.storageService.userDetails
    //         );
    //         this.apiService
    //           .checkActiveStatus(this.storageService.userDetails.userId)
    //           .then((data) => {
    //             console.log(
    //               "Activation Status",
    //               JSON.parse(data.data).d[0].IsActive
    //             );
    //             if (JSON.parse(data.data).d[0].IsActive == "True") {
    //               this.saveTestInfoServiceService
    //                 .getSavedTestsCount()
    //                 .then((data) => {
    //                   if (data == 0) {
    //                     // this.loadingService.createLoading('Fetching your tests...')

    //                     setTimeout(
    //                       (_) =>
    //                         this.userDetailsService.getSavedTests(
    //                           this.storageService.userInfo.UserId,
    //                           this.storageService.userInfo.ClientId
    //                         ),
    //                       3000
    //                     );
    //                   }
    //                 });
    //             } else {
    //               this.onAccoutDeactivated();
    //             }
    //           });
    //       });
    // });

    // this.storage
    //   .get("token")
    //   .then((token) => (this.storageService.tokenId = token));
  }

  onClose() {
    this.menuController.close();
  }

  async onAccoutDeactivated() {
      const alert = await this.alertController.create({
        header: "Accout Deactivated",
        subHeader: "Your accout is deactivated",
        cssClass: "alert-title",
        buttons: [
          {
            text: "Ok",
            handler: () => {
              this.handleLogout();
            },
          },
        ],
      });
      await alert.present();
  }

  async onLogout() {
    this.menuController.toggle().then(async () => {
      const alert = await this.alertController.create({
        header: "Logout",
        subHeader: "Do you want to logout ?",
        cssClass: "alert-title",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
          },
          {
            text: "Logout",
            handler: () => {
              this.handleLogout();
            },
          },
        ],
      });
      await alert.present();
    });
  }

  handleLogout() {
    this.loadingService.createLoading("Loging Out...").then((_) => {
      this.databaseService.getDatabaseState().subscribe((ready) => {
        if (ready)
          this.userDetailsService.deleteFromCMSLogStatus().then((_) =>
            this.userDetailsService
              .deleteFromCMSRegistrationDetails()
              .then((_) =>
                this.attendanceService.deleteFromCMSAttendance().then((_) =>
                  this.attendanceService
                    .deleteFromCMSAttendanceCount()
                    .then((_) =>
                      this.feesService.deleteFromCMSFeesHistory().then((_) =>
                        this.feesService.deleteFromCMSFeesInfo().then((_) =>
                          this.notificationService
                            .deleteFromCMSNotificationId()
                            .then((_) =>
                              this.notificationService
                                .deleteFromCMSNotificationMessage()
                                .then((_) =>
                                  this.notificationService
                                    .deleteFromCMSNotificationMessageTest()
                                    .then((_) =>
                                      this.notificationService
                                        .deleteFromNotification()
                                        .then((_) =>
                                          this.notificationService
                                            .deleteFromOnlineTestNotifications()
                                            .then((_) =>
                                              this.saveTestInfoServiceService
                                                .deleteFromSaveTestInfo()
                                                .then((_) =>
                                                  this.offlineService
                                                    .deleteFromCMSResults()
                                                    .then((_) =>
                                                      this.dailyPracticePaperService
                                                        .deleteFromCMSDailyPracticePaper()
                                                        .then((_) =>
                                                          this.timeTableService
                                                            .deleteFromCMSTimeTable()
                                                            .then((_) => {
                                                              this.storage
                                                                .ready()
                                                                .then(
                                                                  (ready) => {
                                                                    if (ready)
                                                                      this.storage.set(
                                                                        "userLoggedIn",
                                                                        false
                                                                      );
                                                                  }
                                                                );
                                                              this.apiServiceService.setToken(
                                                                this
                                                                  .storageService
                                                                  .userDetails
                                                                  .userId,
                                                                ""
                                                              );

                                                              this.apiServiceService
                                                                .removeSecurityCode(
                                                                  this
                                                                    .storageService
                                                                    .userDetails
                                                                    .clientId,
                                                                  this
                                                                    .storageService
                                                                    .userDetails
                                                                    .userId
                                                                )
                                                                .then((_) =>
                                                                  this.loadingController
                                                                    .dismiss()
                                                                    .then((_) =>
                                                                      this.router
                                                                        .navigateByUrl(
                                                                          "/login-page"
                                                                        )
                                                                        .then(
                                                                          () => {
                                                                            this.statusBar.backgroundColorByHexString(
                                                                              "#ffffff"
                                                                            );
                                                                            this.statusBar.styleDefault();
                                                                          }
                                                                        )
                                                                    )
                                                                );
                                                            })
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                      )
                    )
                )
              )
          );
      });
    });
  }

  onClick(title) {
    if (title == "LOGOUT") {
      this.menuController.close();
      this.onLogout();
    }
  }
}
