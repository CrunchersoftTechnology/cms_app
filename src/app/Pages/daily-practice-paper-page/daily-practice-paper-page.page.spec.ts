import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DailyPracticePaperPagePage } from './daily-practice-paper-page.page';

describe('DailyPracticePaperPagePage', () => {
  let component: DailyPracticePaperPagePage;
  let fixture: ComponentFixture<DailyPracticePaperPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyPracticePaperPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DailyPracticePaperPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
