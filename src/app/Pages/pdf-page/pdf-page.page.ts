import { Component, OnInit } from "@angular/core";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-pdf-page",
  templateUrl: "./pdf-page.page.html",
  styleUrls: ["./pdf-page.page.scss"],
})
export class PdfPagePage implements OnInit {
  constructor(
    private router: Router,
    public storageService: StorageServiceService
  ) {}

  ngOnInit() {}

  onPDFCategoryClick(pdfType, pdfTypeGet, type) {
    this.storageService.pdftype = pdfType;
    this.storageService.pdftypeget = pdfTypeGet;
    this.router.navigate(['/pdf-list-page', type]);
  }
}
