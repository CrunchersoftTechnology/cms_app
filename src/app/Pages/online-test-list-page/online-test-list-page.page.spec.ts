import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OnlineTestListPagePage } from './online-test-list-page.page';

describe('OnlineTestListPagePage', () => {
  let component: OnlineTestListPagePage;
  let fixture: ComponentFixture<OnlineTestListPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlineTestListPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OnlineTestListPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
