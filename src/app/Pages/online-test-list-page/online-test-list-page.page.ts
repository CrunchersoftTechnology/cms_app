import { Component, OnInit } from "@angular/core";
import { NotificationServiceService } from "src/app/Services/notification-service.service";
import { Notifications } from "src/app/Models/notificationInfo";
import { ApiServiceService } from "./../../Services/api-service.service";
import { LoadingServiceService } from "./../../Services/loading-service.service";
import { ToastServiceService } from "./../../Services/toast-service.service";
import {
  LoadingController,
  PopoverController,
  NavController,
} from "@ionic/angular";
import { TestDetailsComponentComponent } from "./../../Components/test-details-component/test-details-component.component";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { TestDetails } from "./../../Models/testDetails";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-online-test-list-page",
  templateUrl: "./online-test-list-page.page.html",
  styleUrls: ["./online-test-list-page.page.scss"],
})
export class OnlineTestListPagePage implements OnInit {
  notifications: Notifications[] = [];
  constructor(
    private notificationService: NotificationServiceService,
    private apiService: ApiServiceService,
    private loadingService: LoadingServiceService,
    private toastService: ToastServiceService,
    public storageService: StorageServiceService,
    private loadingController: LoadingController,
    private popoverController: PopoverController,
    private navController: NavController,
    private storage: Storage
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    this.getNotifications();
  }

  getNotifications() {
    this.notificationService.getNotifications().then((data) => {
      this.notifications = data;
      console.log("notifications", this.notifications);
    });
  }

  onTestClick(notifications: Notifications) {
    // console.log(notifications);
    if (notifications.Status == "Unread") {
      if (notifications.TestObj == "TestObj") {
        this.loadingService.createLoading("Downloading test").then((_) => {
          console.log("in if");
          this.apiService.getTestObj(notifications.TestPaperId).then(
            (result) => {
              // console.log(result.data);
              let testObj = result.data;
              this.notifications[
                this.notifications.indexOf(notifications)
              ].TestObj = testObj;
              this.notificationService
                .updateOnlineTestNotifications(testObj, notifications.ID)
                .then((_) => {
                  this.loadingController
                    .dismiss()
                    .then((_) => this.createPopover(notifications));
                });
            },
            (err) => {
              this.loadingController
                .dismiss()
                .then((_) =>
                  this.toastService.createToast(
                    "Error while downloading test",
                    3000
                  )
                );
            }
          );
        });
      } else {
        this.createPopover(notifications);
      }
      this.GetCMReceivedTestNotification(
        notifications.ArrengeTestId,
        this.storageService.userDetails.userId,
        this.storageService.userDetails.clientId,
        "received"
      );
    } else this.toastService.createToast("Test already given", 3000);
  }

  GetCMReceivedTestNotification(arrangeTestId, userId, clientId, recivedCode) {
    this.apiService
      .GetCMReceivedTestNotification(
        arrangeTestId,
        userId,
        clientId,
        recivedCode
      )
      .then((data) => console.log("recivedTestApi", JSON.parse(data.data).d));
  }

  async createPopover(notification) {
    // console.log(notification)
    const popover = await this.popoverController.create({
      component: TestDetailsComponentComponent,
      animated: true,
      cssClass: "myPopOver1",
      componentProps: { notification: notification },
    });
    await popover.present();
  }

  doRefresh(event) {
    // this.notificationService.getMaxArrengeTestId().then((data) => {
    this.storage.get("maxTestPaperId").then((data) => {
      console.log("maxTestPaperId", data);
      this.apiService
        .getCMSLatestOnlineTestRecived(
          this.storageService.userDetails.userId,
          this.storageService.userDetails.clientId,
          data
        )
        .then((data) => {
          let recivedTests: any[] = [];
          recivedTests = JSON.parse(data.data).d;
          console.log("revivedTests", recivedTests);
          if (recivedTests.length > 0) {
            let maxPaperId: string =
              recivedTests[recivedTests.length - 1].ArrengeTestId;
            this.storage.set("maxTestPaperId", maxPaperId);
            recivedTests.map((recivedTest: any) => {
              var Message = recivedTest.Message;
              var title = Message.split("$^$")[0];
              var TestType = Message.split("$^$")[1].split("TestType:")[1];
              var CetCorrect = Message.split("$^$")[2].split("CetCorrect:")[1];
              var CetInCorrect = Message.split("$^$")[3].split(
                "CetInCorrect:"
              )[1];
              var NeetCorrect = Message.split("$^$")[4].split(
                "NeetCorrect:"
              )[1];
              var NeetInCorrect = Message.split("$^$")[5].split(
                "NeetInCorrect:"
              )[1];
              var JeeCorrect = Message.split("$^$")[6].split("JeeCorrect:")[1];
              var JeeInCorrect = Message.split("$^$")[7].split(
                "JeeInCorrect:"
              )[1];
              var JeeNewCorrect = Message.split("$^$")[8].split(
                "JeeNewCorrect:"
              )[1];
              var JeeNewInCorrect = Message.split("$^$")[9].split(
                "JeeNewInCorrect:"
              )[1];
              var testDate = Message.split("$^$")[10].split("Date:")[1];
              var EndDate = Message.split("$^$")[11].split("EndDate:")[1];
              var startTime = Message.split("$^$")[12].split("Start Time:")[1];
              var endTime = Message.split("$^$")[13].split("End Time:")[1];
              var duration = Message.split("$^$")[14].split("Duration:")[1];
              var testPaperId = Message.split("$^$")[15].split(
                "TestPaperId:"
              )[1];
              let testDetails: TestDetails;
              testDetails = {
                CetCorrect: CetCorrect,
                CetInCorrect: CetInCorrect,
                EndDate: EndDate,
                JeeCorrect: JeeCorrect,
                JeeInCorrect: JeeInCorrect,
                JeeNewCorrect: JeeNewCorrect,
                JeeNewInCorrect: JeeNewInCorrect,
                NeetCorrect: NeetCorrect,
                NeetInCorrect: NeetInCorrect,
                TestType: TestType,
                arrengeTestId: recivedTest.ArrengeTestId,
                duration: duration,
                endTime: endTime,
                sid: this.storageService.userDetails.sId,
                startTime: startTime,
                status: "",
                testDate: testDate,
                testPaperId: testPaperId,
                title: title,
              };
              this.notificationService
                .insertIntoOnlineTestNotifications(testDetails, null)
                .then((_) => {
                  this.notificationService.getNotifications().then((data) => {
                    this.notifications = data;
                    console.log("notifications", this.notifications);
                    event.target.complete();
                  });
                });
            });
          } else {
            event.target.complete();
            this.toastService.createToast("You recived all your tests", 3000);
          }
        });
    });
    // event.target.complete()
  }

  onBackClick() {
    this.navController.back();
  }
}
