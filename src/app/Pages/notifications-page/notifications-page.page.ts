import { Component, OnInit } from "@angular/core";
import { NotificationServiceService } from "./../../Services/notification-service.service";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { ToastServiceService } from "src/app/Services/toast-service.service";
import { ApiServiceService } from "src/app/Services/api-service.service";
import { Notice, LastNotification } from "src/app/Models/notificationInfo";
import { Router } from "@angular/router";

@Component({
  selector: "app-notifications-page",
  templateUrl: "./notifications-page.page.html",
  styleUrls: ["./notifications-page.page.scss"],
})
export class NotificationsPagePage implements OnInit {
  sid: string = "";
  noticeDetails: Notice[] = [];
  lastNotification: LastNotification;
  constructor(
    private router: Router,
    private notificationService: NotificationServiceService,
    public storageService: StorageServiceService,
    private apiServiceService: ApiServiceService,
    private toastService: ToastServiceService
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    // this.storageService.testNotificationsCount = 0;
    // this.storageService.generalNotificationsCount = 0;
    // this.notificationService.getNotificationCount();
    // console.log(
    //   this.storageService.testNotificationsCount,
    //   this.storageService.generalNotificationsCount
    // );
    this.sid = this.storageService.userDetails.sId;
    this.getNotices();
  }

  getNotices() {
    this.notificationService
      .getCMSNotificationMessage(this.sid)
      .then((data) => {
        this.noticeDetails = data;
        console.log("noticeDetails", this.noticeDetails);
      });
  }

  doRefresh(event) {
    this.getLastNotification(event);
  }

  getLastNotification(event) {
    this.sid = this.storageService.userDetails.sId;
    this.notificationService.getLastNotification().then((data) => {
      this.lastNotification = data;
      if (this.lastNotification.ArrengeTestId == null)
        this.lastNotification.ArrengeTestId = this.lastNotification.ROnlineTestNotificationId;
      this.notificationService
        .selectFromCMSNotificationMessage()
        .then((data) => {
          if (data != "") this.lastNotification.ROnlineNotificationId = data;
          this.getNotificationsOnline(
            this.lastNotification.branchId,
            this.lastNotification.classId,
            this.lastNotification.BatchId,
            this.lastNotification.ROnlineNotificationId,
            this.sid,
            this.lastNotification.RPdfUploadId,
            this.lastNotification.ArrengeTestId,
            this.lastNotification.MaxOfflineTestPaperId,
            this.lastNotification.MaxOfflineTestStudentMarksId,
            this.lastNotification.userId,
            this.lastNotification.selectedSubjects,
            this.lastNotification.studentName,
            event
          );
        });
    });
  }

  getNotificationsOnline(
    branchId,
    classId,
    selectedBatches,
    RNotificationId,
    SId,
    pdfUploadId,
    arrengeTestId,
    MaxOfflineTestPaperId,
    MaxOfflineTestStudentMarksId,
    userId,
    selectedSubjects,
    studentName,
    event
  ) {
    if (
      arrengeTestId == "" ||
      arrengeTestId == null ||
      arrengeTestId == undefined
    )
      console.log("test is null");
    this.apiServiceService
      .getNotificationsOnline(
        branchId,
        classId,
        selectedBatches,
        RNotificationId,
        pdfUploadId,
        arrengeTestId,
        MaxOfflineTestPaperId,
        MaxOfflineTestStudentMarksId,
        userId,
        selectedSubjects,
        this.storageService.userDetails.clientId,
        studentName,
        "norecived",
        this.storageService.tokenId
      )
      .then(
        (result) => {
          // console.log("success")
          let data = JSON.parse(result.data);
          console.log("getNotificationsOnline", data);
          if (data.d.length > 0) {
            var dataLength = 0;
            for (var i = 0; i < data.d.length; i++) {
              var NotificationId = data.d[i].NotificationId;
              var Category = data.d[i].Category;
              var Message = data.d[i].Message;
              console.log(data.d[i].Message);
              var NDateOnline = data.d[i].Date.split(" ")[0];
              var NTimeOnline = data.d[i].Date.split(" ")[1];
              dataLength = i + 1;
              this.apiServiceService.getNotificationsOnline(
                branchId,
                classId,
                selectedBatches,
                RNotificationId,
                pdfUploadId,
                NotificationId,
                MaxOfflineTestPaperId,
                MaxOfflineTestStudentMarksId,
                userId,
                selectedSubjects,
                this.storageService.userDetails.clientId,
                studentName,
                "received",
                this.storageService.tokenId
              );
              this.exists(
                NotificationId,
                Category,
                Message,
                NDateOnline,
                NTimeOnline,
                SId,
                event
              );
            }
            // if (dataLength == data.d.length) {
            //   // localStorage.setItem("connectionStatus", "0");
            //   this.getNotificationCount();
            // }
          }
          // else if (length == 0) {
          //   this.getNotificationCount();
          // }
          else {
            event.target.complete();
            this.toastService.createToast("You recived all your notices", 3000);
          }
        },
        (err) => console.log("Server Error", err)
      );
  }

  exists(
    NotificationId,
    Category,
    Message,
    NDateOnline,
    NTimeOnline,
    SId,
    event
  ) {
    if (Category == "Notice" || Category == "PDF") {
      this.notificationService
        .insertIntoCMSNotificationMessage(
          Message,
          NDateOnline,
          NTimeOnline,
          SId,
          NotificationId,
          Category
        )
        .then((_) =>
          this.notificationService
            .getCMSNotificationMessage(this.sid)
            .then((data) => {
              this.noticeDetails = data;
              event.target.complete();
              console.log("noticeDetails", this.noticeDetails);
            })
        );
    }
  }
  onNotificationClick(notificationId) {
    this.noticeDetails[this.noticeDetails.length - notificationId].NStatus = "Read";
    this.notificationService
      .updateCMSNotificationMessage(notificationId)
      .then((_) =>
        this.router.navigate(["/notification-detail-page", notificationId])
      );
      console.log('notification', this.noticeDetails)
  }
}
