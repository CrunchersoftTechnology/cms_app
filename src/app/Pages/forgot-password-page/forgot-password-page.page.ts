import { Component, OnInit } from "@angular/core";
import { AlertServiceService } from "./../../Services/alert-service.service";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { OtpServiceService } from "./../../Services/otp-service.service";
import { Router } from "@angular/router";
import { ApiServiceService } from "./../../Services/api-service.service";
import { ToastServiceService } from "./../../Services/toast-service.service";
import { UserDetailsService } from "./../../Services/user-details.service";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-forgot-password-page",
  templateUrl: "./forgot-password-page.page.html",
  styleUrls: ["./forgot-password-page.page.scss"],
})
export class ForgotPasswordPagePage implements OnInit {
  randomNumber: number;
  password: string = "";
  reEnterPAssword: string = "";
  EnteredOtp: string = "";
  inputType: string = "password";
  iconName: string = "eye";
  showErrorMessage: boolean = false;

  inputTypeRePass: string = "password";
  iconNameRePass: string = "eye";

  constructor(
    private router: Router,
    private navController: NavController,
    private apiService: ApiServiceService,
    private alertService: AlertServiceService,
    public storageService: StorageServiceService,
    private otpService: OtpServiceService,
    private toastService: ToastServiceService,
    private userDetailsService: UserDetailsService
  ) {}

  ngOnInit() {}

  onCreatePass() {
    if (this.password == this.reEnterPAssword) {
      // this.randomNumber = Math.floor(1000 + Math.random() * 9000);
      // console.log(this.randomNumber);
      // // console.log(this.number);
      // // console.log(this.storageService.userInfo.StudentContact);
      // this.otpService.sendOtp(
      //   this.storageService.userInfo.StudentContact,
      //   this.randomNumber
      // );
      this.router.navigateByUrl("/otp-page");
      this.showErrorMessage = false;
      this.storageService.password = this.password;
      this.storageService.forgotPassword = true;
    } else {
      this.showErrorMessage = true;
    }
  }

  changeInputType() {
    if (this.inputType == "password") {
      this.inputType = "text";
      this.iconName = "eye-off";
    } else {
      this.inputType = "password";
      this.iconName = "eye";
    }
  }

  changeInputTypeRePass() {
    if (this.inputTypeRePass == "password") {
      this.inputTypeRePass = "text";
      this.iconNameRePass = "eye-off";
    } else {
      this.inputTypeRePass = "password";
      this.iconNameRePass = "eye";
    }
  }

  onBackClick() {
    this.navController.back();
  }
}
