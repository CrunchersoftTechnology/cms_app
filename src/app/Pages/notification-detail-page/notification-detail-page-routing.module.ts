import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotificationDetailPagePage } from './notification-detail-page.page';

const routes: Routes = [
  {
    path: '',
    component: NotificationDetailPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationDetailPagePageRoutingModule {}
