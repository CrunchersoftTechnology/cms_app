import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotificationDetailPagePageRoutingModule } from './notification-detail-page-routing.module';

import { NotificationDetailPagePage } from './notification-detail-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotificationDetailPagePageRoutingModule
  ],
  declarations: [NotificationDetailPagePage]
})
export class NotificationDetailPagePageModule {}
