import { Component, OnInit } from "@angular/core";
import { StorageServiceService } from "src/app/Services/storage-service.service";
import { NavController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";
import { Notice } from "./../../Models/notificationInfo";
import { NotificationServiceService } from "src/app/Services/notification-service.service";

@Component({
  selector: "app-notification-detail-page",
  templateUrl: "./notification-detail-page.page.html",
  styleUrls: ["./notification-detail-page.page.scss"],
})
export class NotificationDetailPagePage implements OnInit {
  notificationId: number;
  notificationDetail: Notice;
  constructor(
    private navController: NavController,
    private activatedRoute: ActivatedRoute,
    public storageService: StorageServiceService,
    private notificationService: NotificationServiceService
  ) {}

  ngOnInit() {
    this.notificationDetail = {
      Category: '',
      ID: 0,
      NBody: '',
      NDate: '',
      NOnlineNotificationId: '',
      NStatus: '',
      NTime: '',
      sId: ''
    }
    this.notificationId = this.activatedRoute.snapshot.params["id"];
    this.notificationService
      .getCMSNotification(this.notificationId)
      .then((data) => (this.notificationDetail = data));
  }

  onBackClick() {
    this.navController.back();
  }
}
