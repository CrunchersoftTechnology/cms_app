import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnlineTestPagePage } from './online-test-page.page';

const routes: Routes = [
  {
    path: '',
    component: OnlineTestPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OnlineTestPagePageRoutingModule {}
