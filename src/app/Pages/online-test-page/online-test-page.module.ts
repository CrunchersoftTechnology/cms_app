import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OnlineTestPagePageRoutingModule } from './online-test-page-routing.module';

import { OnlineTestPagePage } from './online-test-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OnlineTestPagePageRoutingModule
  ],
  declarations: [OnlineTestPagePage]
})
export class OnlineTestPagePageModule {}
