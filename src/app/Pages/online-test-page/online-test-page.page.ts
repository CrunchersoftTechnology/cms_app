import { Component, OnInit, ViewChild } from "@angular/core";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { QuestionInfo } from "src/app/Models/questionInfo";
import { AlertServiceService } from "./../../Services/alert-service.service";
import { ToastServiceService } from "./../../Services/toast-service.service";
import {
  PopoverController,
  NavController,
  Platform,
  AlertController,
} from "@ionic/angular";
import { Chart } from "chart.js";
import { Storage } from "@ionic/storage";
import { Router, ActivatedRoute } from "@angular/router";
import { QuestionPanelPage } from "../../Components/question-panel/question-panel.page";
import { Notifications } from "src/app/Models/notificationInfo";
import { SavedTestDetails } from "src/app/Models/savedTestDetails";
import { ApiServiceService } from "./../../Services/api-service.service";
import { SaveTestInfoServiceService } from "./../../Services/save-test-info-service.service";

@Component({
  selector: "app-online-test-page",
  templateUrl: "./online-test-page.page.html",
  styleUrls: ["./online-test-page.page.scss"],
})
export class OnlineTestPagePage implements OnInit {
  @ViewChild("pieChart1") pieChart1;
  @ViewChild("pieChart2") pieChart2;
  @ViewChild("pieChart3") pieChart3;
  pie1: any;
  pie2: any;
  pie3: any;

  questionInfo: QuestionInfo[] = [];
  questionsList: QuestionInfo[] = [];
  savedTestDetails: SavedTestDetails;
  testDetails: Notifications;
  questionCount: number = 0;
  index: number = 0;
  testSubmitted: boolean = false;
  testView: boolean = true;
  page: string = "";
  bookmarkIcon: string = "bookmark-outline";
  questionImage: boolean = true;
  questionImagePath: string = "";

  optionImage: boolean = true;
  optionImagePath: string = "";

  hintImage: boolean = true;
  hintImagePath: string = "";
  iconCount: number = 0;
  showInputBox: boolean = false;
  option: string;
  inCorrOption: string;
  answer: string = "";
  unit: string = "";
  selectionCleared: boolean = false;
  questionStatus: string;
  testId: number;
  inGo: boolean = false;
  testTitle: string = "";
  count = 0;
  totalQuestions = [];
  stopClockInterval;
  secs = 0;
  fmins;
  fsecs;
  totalMins;
  totalSecs;
  testGivenDate: string;

  totalMarks: number = 0;
  outOfMarks: number = 0;
  corrMarks: number = 0;
  inCorrMarks: number = 0;

  correctQuestions: number = 0;
  inCorrectQuestions: number = 0;
  skippedQuestions: number = 0;

  accuracy: number;
  score: number;
  correctPer: number;
  inCorrectPer: number;
  skippedPer: number;

  selectedOptionArr: {
    questionNo: number;
    option: string;
    userNumericalAns: string;
    userUnit: string;
  }[] = [];

  selectedOption: {
    questionNo: number;
    option: string;
    userNumericalAns: string;
    userUnit: string;
  };

  SubjectTestId: number;
  testType: string = "";
  constructor(
    private platform: Platform,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private popoverController: PopoverController,
    private alertController: AlertController,
    private navController: NavController,
    private storage: Storage,
    public storageService: StorageServiceService,
    private alertService: AlertServiceService,
    private toastService: ToastServiceService,
    private apiService: ApiServiceService,
    private saveTestInfoService: SaveTestInfoServiceService
  ) {
    this.platform.backButton.subscribeWithPriority(0, (processNextHandler) => {
      // console.log("in test back");
      if (this.router.url.startsWith("/online-test-page")) {
        if (this.page == "unit-test" || this.page == "saved-test") {
          if (this.testView == true && this.testSubmitted == false) {
          } else if (this.testView == true && this.testSubmitted == true) {
            this.testView = false;
          } else if (this.testView == false && this.testSubmitted == true) {
            this.navController.back();
          }
        } else if (this.page == "bookmark") {
          if (this.inGo == true) {
            popoverController.dismiss();
          } else {
            this.navController.back();
          }
        }
        this.popoverController.dismiss();
      } else {
        processNextHandler();
      }
      // else if(this.router.url.startsWith('/dashboard')){
      //   navigator["app"].exitApp();
      // }
    });
  }

  ngOnInit() {
    this.selectedOption = {
      questionNo: null,
      option: "",
      userNumericalAns: "",
      userUnit: "",
    };
    this.page = this.storageService.page;

    if (this.page == "unit-test") {
      this.testDetails = this.storageService.notification;
      this.testTitle = this.testDetails.Title;
      this.questionInfo = JSON.parse(this.testDetails.TestObj).QuestionDetails;
      this.questionInfo.map((question) => {
        this.questionsList.push({
          Answer: question.Answer,
          ChapterId: question.ChapterId,
          ChapterName: question.ChapterName,
          ClassId: question.ClassId,
          ClassName: question.ClassName,
          Hint: question.Hint,
          HintImageFile: question.HintImageFile,
          HintImagePath: question.HintImagePath,
          IsHintAsImage: question.IsHintAsImage,
          IsOptionAsImage: question.IsOptionAsImage,
          IsQuestionAsImage: question.IsQuestionAsImage,
          Numerical_Answer: question.Numerical_Answer,
          Option1: question.Option1,
          Option2: question.Option2,
          Option3: question.Option3,
          Option4: question.Option4,
          OptionImageFile: question.OptionImageFile,
          OptionImagePath: question.OptionImagePath,
          QuestionId: question.QuestionId,
          QuestionImageFile: question.QuestionImageFile,
          QuestionImagePath: question.QuestionImagePath,
          QuestionInfo: question.QuestionInfo,
          QuestionLevel: question.QuestionLevel,
          QuestionType: question.QuestionType,
          QuestionYear: question.QuestionYear,
          SubjectName: question.SubjectName,
          SubjectTestId: question.SubjectTestId,
          Unit: question.Unit,
          userAns: "",
          userNumericalAns: "",
          userUnit: "",
        });
      });
      console.log(this.questionsList);
      this.questionCount = this.questionsList.length;
      this.showTest();
      this.onReady();
    } else if (this.page == "saved-test") {
      this.testView = false;
      this.testSubmitted = true;
      this.testId = parseInt(this.activatedRoute.snapshot.paramMap.get("id"));
      this.saveTestInfoService
        .getTestDetailsForTest(this.testId)
        .then((data) => {
          this.savedTestDetails = data;
          console.log("Saved test", this.savedTestDetails);
          this.testTitle = this.savedTestDetails.subjectName;
          this.totalMarks = this.savedTestDetails.obtainedMarks;
          this.outOfMarks = this.savedTestDetails.totalMarks;
          this.totalMins = this.savedTestDetails.timeTakenMin;
          this.totalSecs = this.savedTestDetails.timeTakenSec;
          this.correctQuestions = this.savedTestDetails.corrQueCount;
          this.inCorrectQuestions = this.savedTestDetails.inCorrQueCount;
          this.skippedQuestions =
            this.savedTestDetails.totalQue -
            (this.correctQuestions + this.inCorrectQuestions);
          this.testGivenDate =
            this.savedTestDetails.testDate.slice(0, 2) +
            "-" +
            this.savedTestDetails.testDate.slice(2, 4) +
            "-" +
            this.savedTestDetails.testDate.slice(4);
          this.questionsList = JSON.parse(this.savedTestDetails.questions);
          this.questionCount = this.questionsList.length;
          this.testType = this.savedTestDetails.testType;
          this.createPieChart();
        });
    }
  }

  next() {
    if (this.index < this.questionCount - 1) {
      this.index = this.index + 1;
      this.showTest();
      if (this.questionsList[this.index].QuestionType != 3) {
        if (
          (this.page == "unit-test" ||
            this.page == "paused-test" ||
            this.page == "question-papers") &&
          this.testSubmitted == false
        ) {
          this.funSelectedOption();
        } else {
          this.inCorrOption = "";
          this.option = this.questionsList[this.index].Answer;
          if (
            this.questionsList[this.index].Answer !=
            this.questionsList[this.index].userAns
          ) {
            this.inCorrOption = this.questionsList[this.index].userAns;
          }
          if (
            (this.page == "saved-test" ||
              this.page == "unit-test" ||
              this.page == "paused-test" ||
              this.page == "question-papers") &&
            this.testSubmitted == true
          ) {
            this.getQuestionStatus();
          }
        }
      } else {
        this.selectionCleared = false;
        this.answer = this.questionsList[this.index].userNumericalAns;
        this.unit = this.questionsList[this.index].userUnit;
        if (
          (this.page == "saved-test" ||
            this.page == "unit-test" ||
            this.page == "paused-test") &&
          this.testSubmitted == true
        ) {
          this.getQuestionStatus();
        }
      }
    } else {
      this.alertService.createAlert("You are on the last question");
    }
  }

  previous() {
    if (this.index > 0) {
      this.index = this.index - 1;
      this.showTest();
      if (this.questionsList[this.index].QuestionType != 3) {
        if (
          (this.page == "unit-test" ||
            this.page == "paused-test" ||
            this.page == "question-papers") &&
          this.testSubmitted == false
        ) {
          this.funSelectedOption();
        } else {
          this.inCorrOption = "";
          this.option = this.questionsList[this.index].Answer;
          if (
            this.questionsList[this.index].Answer !=
            this.questionsList[this.index].userAns
          ) {
            this.inCorrOption = this.questionsList[this.index].userAns;
          }
          if (
            (this.page == "saved-test" ||
              this.page == "unit-test" ||
              this.page == "paused-test" ||
              this.page == "question-papers") &&
            this.testSubmitted == true
          ) {
            this.getQuestionStatus();
          }
        }
      } else {
        this.selectionCleared = false;
        this.answer = this.questionsList[this.index].userNumericalAns;
        this.unit = this.questionsList[this.index].userUnit;
        if (
          (this.page == "saved-test" ||
            this.page == "unit-test" ||
            this.page == "paused-test") &&
          this.testSubmitted == true
        ) {
          this.getQuestionStatus();
        }
      }
    } else {
      this.alertService.createAlert("You are on the first question");
    }
  }

  funSelectedOption() {
    // this.hintShown = false;
    this.option = "";
    // document.getElementById('hint').innerHTML = ''
    this.selectedOption = this.selectedOptionArr.find(
      ({ questionNo }) =>
        questionNo === this.questionsList[this.index].QuestionId
    );
    // console.log("selectedOptions", this.selectedOption);
    if (this.selectedOption == null) {
      this.option = "";
      this.selectionCleared = false;
    } else {
      this.option = this.selectedOption.option;
      this.selectionCleared = true;
    }
    // this.selectedOptionArr.find(({questionNo}) => questionNo == this.index).option;
    // console.log(this.selectedOptionArr.find(({questionNo}) => questionNo == this.index).option);
    // console.log(this.questionsList[this.index].ANSWER);
  }

  getQuestionStatus() {
    if (this.questionsList[this.index].QuestionType != 3) {
      if (
        this.questionsList[this.index].userAns == "" ||
        this.questionsList[this.index].userAns == "Unsolved"
      ) {
        this.questionStatus = "SKIPPED";
      } else if (
        this.questionsList[this.index].Answer ==
        this.questionsList[this.index].userAns
      ) {
        this.questionStatus = "CORRRECT";
      } else {
        this.questionStatus = "INCORRECT";
      }
    } else if (this.questionsList[this.index].QuestionType == 3) {
      if (this.questionsList[this.index].userNumericalAns == "") {
        this.questionStatus = "SKIPPED";
      } else if (
        this.questionsList[this.index].userNumericalAns ==
          this.questionsList[this.index].Numerical_Answer &&
        this.questionsList[this.index].userUnit ==
          this.questionsList[this.index].Unit
      ) {
        this.questionStatus = "CORRRECT";
      } else {
        this.questionStatus = "INCORRECT";
      }
    }
  }

  showTest() {
    this.iconCount = this.questionsList[this.index].QuestionLevel;

    // if (
    //   this.bookmarkQustions.find(
    //     ({ questionId }) =>
    //       questionId == this.questionsList[this.index].QUESTION_ID
    //   ) == null
    // ) {
    //   this.bookmarkIcon = "bookmark-outline";
    //   // console.log("in show test if");
    // } else {
    //   this.bookmarkIcon = "bookmark";
    //   // console.log("in show test else");
    // }
    document.getElementById("question").innerHTML =
      "$" + this.questionsList[this.index].QuestionInfo + "$";
    document.getElementById("opt1").innerHTML =
      "$" + this.questionsList[this.index].Option1 + "$";
    document.getElementById("opt2").innerHTML =
      "$" + this.questionsList[this.index].Option2 + "$";
    document.getElementById("opt3").innerHTML =
      "$" + this.questionsList[this.index].Option3 + "$";
    document.getElementById("opt4").innerHTML =
      "$" + this.questionsList[this.index].Option4 + "$";
    eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
    // if(this.questionsList[this.index].QUESTION_YEAR != '') {
    //   document.getElementById('prevAsk').innerHTML = this.questionsList[this.index].QUESTION_YEAR;
    // }
    if (this.page == "bookmark") {
      document.getElementById("hint").innerHTML =
        "$" + this.questionsList[this.index].Hint + "$";
      eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
      this.option = this.questionsList[this.index].Answer;
      this.answer = this.questionsList[this.index].Numerical_Answer;
      this.unit = this.questionsList[this.index].Unit;
    }
    if (this.testSubmitted == true) {
      document.getElementById("hint").innerHTML =
        "$" + this.questionsList[this.index].Hint + "$";
      eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
    }
    this.questionsList[this.index].QuestionType == 3
      ? (this.showInputBox = true)
      : (this.showInputBox = false);

    this.SubjectTestId = this.questionsList[this.index].SubjectTestId;

    if (this.questionsList[this.index].IsQuestionAsImage == 1) {
      this.questionImage = false;
      this.questionImagePath = `assets/${
        this.questionsList[this.index].QuestionImagePath
      }`;
    } else {
      this.questionImage = true;
      this.questionImagePath = "";
    }

    if (this.questionsList[this.index].IsOptionAsImage == 1) {
      this.optionImage = false;
      this.optionImagePath = `assets/${
        this.questionsList[this.index].OptionImagePath
      }`;
    } else {
      this.optionImage = true;
      this.optionImagePath = "";
    }

    if (
      this.testSubmitted == true &&
      this.questionsList[this.index].IsHintAsImage == 1
    ) {
      this.hintImage = false;
      this.hintImagePath = `assets/${
        this.questionsList[this.index].HintImagePath
      }`;
    } else {
      this.hintImage = true;
      this.hintImagePath = "";
    }
  }

  onReady() {
    this.secs = Number.parseInt(this.testDetails.Duration) * 60;
    this.stopClockInterval = setInterval((_) => this.countTime(), 1000);
  }

  async countTime() {
    this.secs--;
    this.fmins = Math.floor(this.secs / 60);
    this.fsecs = Math.floor(this.secs % 60);
    if (this.fmins <= 9) {
      this.fmins = "0" + this.fmins;
    }
    if (this.fsecs <= 9) {
      this.fsecs = "0" + this.fsecs;
    }
    if (this.secs >= 0) {
      document.getElementById("timer").innerHTML =
        this.fmins + ":" + this.fsecs;
    } else {
      clearTimeout(this.stopClockInterval);
      const alert = await this.alertController.create({
        subHeader: "Your test time exceeds",
        cssClass: "alert-title",
        animated: true,
        buttons: [
          {
            text: "Ok",
            handler: () => {
              this.popoverController.dismiss();
              this.submit();
            },
          },
        ],
        backdropDismiss: false,
      });
      await alert.present();
    }
  }

  clickOpt1() {
    if (this.testSubmitted == false) {
      if (
        (this.page == "unit-test" ||
          this.page == "paused-test" ||
          this.page == "question-papers") &&
        this.selectionCleared == false
      ) {
        this.selectOption("A");
      } else if (
        this.page == "unit-test" ||
        this.page == "paused-test" ||
        this.page == "question-papers"
      ) {
        this.toastService.createToast("you cannot select another option", 1000);
      }
    }
  }

  clickOpt2() {
    if (this.testSubmitted == false) {
      if (
        (this.page == "unit-test" ||
          this.page == "paused-test" ||
          this.page == "question-papers") &&
        this.selectionCleared == false
      ) {
        this.selectOption("B");
      } else if (
        this.page == "unit-test" ||
        this.page == "paused-test" ||
        this.page == "question-papers"
      ) {
        this.toastService.createToast("you cannot select another option", 1000);
      }
    }
  }

  clickOpt3() {
    if (this.testSubmitted == false) {
      if (
        (this.page == "unit-test" ||
          this.page == "paused-test" ||
          this.page == "question-papers") &&
        this.selectionCleared == false
      ) {
        this.selectOption("C");
      } else if (
        this.page == "unit-test" ||
        this.page == "paused-test" ||
        this.page == "question-papers"
      ) {
        this.toastService.createToast("you cannot select another option", 1000);
      }
    }
  }

  clickOpt4() {
    if (this.testSubmitted == false) {
      if (
        (this.page == "unit-test" ||
          this.page == "paused-test" ||
          this.page == "question-papers") &&
        this.selectionCleared == false
      ) {
        this.selectOption("D");
      } else if (
        this.page == "unit-test" ||
        this.page == "paused-test" ||
        this.page == "question-papers"
      ) {
        this.toastService.createToast("you cannot select another option", 1000);
      }
    }
  }

  selectOption(option: string) {
    this.selectedOption = this.selectedOptionArr.find(
      ({ questionNo }) =>
        questionNo === this.questionsList[this.index].QuestionId
    );
    // console.log("selected in opt1", this.selectedOption);
    if (this.selectedOptionArr.includes(this.selectedOption)) {
      // console.log("in opt4 if");
      this.selectedOptionArr.splice(
        this.selectedOptionArr.indexOf(this.selectedOption),
        1
      );
      this.questionsList[this.index].userAns = "";
    }
    this.selectedOption = {
      questionNo: this.questionsList[this.index].QuestionId,
      option: option,
      userNumericalAns: "",
      userUnit: "",
    };
    this.option = option;
    this.questionsList[this.index].userAns = option;
    // console.log("user ans", this.questionsList[this.index].userAns);
    this.selectedOptionArr.push(this.selectedOption);
    this.selectionCleared = true;
    // console.log("selectedArr", this.selectedOptionArr);
  }

  onBackClick() {
    if (this.page == "unit-test" || this.page == "saved-test")
      if (this.testSubmitted == true) this.testView = false;
    // console.log("test view", this.testView);

    // } else if (this.page == "bookmark") {
    //   this.navController.back();
    // }
  }

  bookmark() {}

  addRemoveBookmark() {}

  async go() {
    this.inGo = true;
    if (this.count == 0) {
      this.totalQuestions = [];
      let index = 0;
      this.questionsList.map((questions) => {
        this.totalQuestions.push({
          index: index,
          questionNo: questions.QuestionId,
          questionType: questions.QuestionType,
        });
        index++;
      });
      this.count++;
    }
    const modal = await this.popoverController.create({
      component: QuestionPanelPage,
      backdropDismiss: false,
      animated: true,
      cssClass: "myPopOver3",
      componentProps: {
        totalQuestions: this.totalQuestions,
        answeredQuestions: this.selectedOptionArr,
        index: this.index,
      },
    });
    await modal.present();
    modal.onDidDismiss().then((_) => {
      this.inGo = false;
      this.storage.ready().then((ready) => {
        if (ready) {
          this.storage.get("index").then((index) => {
            // console.log("index in modal", index);
            if (index == null) {
            } else this.index = index;
            this.showTest();
            this.storage.remove("index");
            if (this.questionsList[this.index].QuestionType != 3) {
              if (
                this.page == "unit-test" ||
                this.page == "paused-test" ||
                this.page == "question-papers"
              ) {
                this.funSelectedOption();
              }
            } else {
              this.selectionCleared = false;
              this.answer = this.questionsList[this.index].userNumericalAns;
              this.unit = this.questionsList[this.index].userUnit;
            }
          });
        }
      });
    });
  }

  clear() {
    if (this.questionsList[this.index].QuestionType != 3) {
      this.selectionCleared = false;
      this.option = "";
      this.questionsList[this.index].userAns = "";
      this.selectedOption = this.selectedOptionArr.find(
        ({ questionNo }) =>
          questionNo === this.questionsList[this.index].QuestionId
      );
      // console.log("selected in opt1", this.selectedOption);
      if (this.selectedOptionArr.includes(this.selectedOption)) {
        // console.log("in opt4 if");
        this.selectedOptionArr.splice(
          this.selectedOptionArr.indexOf(this.selectedOption),
          1
        );
      }
    } else if (this.questionsList[this.index].QuestionType == 3) {
      this.answer = "";
      this.unit = "";
    }
  }

  onAnswerChange() {
    this.questionsList[this.index].userNumericalAns = this.answer;
    this.selectedOption = this.selectedOptionArr.find(
      ({ questionNo }) =>
        questionNo === this.questionsList[this.index].QuestionId
    );
    if (this.answer.length == 0) {
      this.unit = "";
      if (this.selectedOptionArr.includes(this.selectedOption)) {
        this.selectedOptionArr.splice(
          this.selectedOptionArr.indexOf(this.selectedOption),
          1
        );
      }
    } else {
      if (this.selectedOptionArr.includes(this.selectedOption)) {
        this.selectedOptionArr[
          this.selectedOptionArr.indexOf(this.selectedOption)
        ].userNumericalAns = this.answer;
      } else {
        this.selectedOptionArr.push({
          questionNo: this.questionsList[this.index].QuestionId,
          option: "",
          userNumericalAns: this.answer,
          userUnit: this.unit,
        });
      }
    }
  }

  onUnitChange() {
    this.questionsList[this.index].userUnit = this.unit;
    this.selectedOption = this.selectedOptionArr.find(
      ({ questionNo }) =>
        questionNo === this.questionsList[this.index].QuestionId
    );
    if (this.unit.length == 0 && this.answer.length != 0) {
      if (this.selectedOptionArr.includes(this.selectedOption)) {
        this.selectedOptionArr[
          this.selectedOptionArr.indexOf(this.selectedOption)
        ].userUnit = "";
      }
    } else if (this.unit.length > 0 && this.answer.length > 0) {
      if (this.selectedOptionArr.includes(this.selectedOption)) {
        this.selectedOptionArr[
          this.selectedOptionArr.indexOf(this.selectedOption)
        ].userUnit = this.unit;
      }
    }
  }

  async onSubmit() {
    const alert = await this.alertController.create({
      subHeader: "Do you want to submit test ?",
      cssClass: "alert-title",
      animated: true,
      backdropDismiss: false,
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
        },
        {
          text: "Confirm",
          handler: () => {
            clearTimeout(this.stopClockInterval);
            this.submit();
          },
        },
      ],
    });
    await alert.present();
  }

  submit() {
    console.log("After Submit", this.questionsList);
    console.log("Selected option array", this.selectedOptionArr);
    this.testView = false;
    this.testSubmitted = true;

    if (this.testDetails.TestType == "1")
      this.outOfMarks =
        this.questionsList.length *
        Number.parseInt(this.testDetails.NeetCorrect);
    else if (
      this.testDetails.TestType == "2" ||
      this.testDetails.TestType == "4"
    ) {
      this.questionsList.map((question) => {
        if (question.QuestionType == 3)
          this.outOfMarks += Number.parseInt(this.testDetails.JeeNewCorrect);
        else this.outOfMarks += Number.parseInt(this.testDetails.JeeCorrect);
      });
    } else if (
      this.testDetails.TestType == "3" ||
      this.testDetails.TestType == "5"
    ) {
      this.questionsList.map((question) => {
        if (
          question.SubjectTestId == 1 ||
          question.SubjectTestId == 2 ||
          question.SubjectTestId == 4
        )
          this.outOfMarks += 1;
        else this.outOfMarks += 2;
      });
    }

    // this.corrMarks = this.settings.correctMarks;
    // this.inCorrMarks = this.settings.inCorrectMarks;
    this.getMarks();
    this.getTime();
    this.saveTest();
    this.sendTestDetails();
  }

  getMarks() {
    if (this.testDetails.TestType == "1") {
      this.questionsList.map((question) => {
        if (question.userAns != "") {
          if (question.Answer == question.userAns) {
            this.totalMarks += Number.parseInt(this.testDetails.NeetCorrect);
            this.correctQuestions++;
          } else {
            this.totalMarks -= Number.parseInt(this.testDetails.NeetInCorrect);
            this.inCorrectQuestions++;
          }
        }
      });
    } else if (
      this.testDetails.TestType == "2" ||
      this.testDetails.TestType == "4"
    ) {
      this.questionsList.map((question) => {
        if (question.QuestionType == 3) {
          if (question.userNumericalAns != "") {
            if (
              question.userNumericalAns == question.Numerical_Answer &&
              question.userUnit == question.Unit
            ) {
              this.totalMarks += Number.parseInt(
                this.testDetails.JeeNewCorrect
              );
              this.correctQuestions++;
            } else {
              this.totalMarks -= Number.parseInt(
                this.testDetails.JeeNewInCorrect
              );
              this.inCorrectQuestions++;
            }
          }
        } else {
          if (question.userAns != "") {
            if (question.Answer == question.userAns) {
              this.totalMarks += Number.parseInt(this.testDetails.JeeCorrect);
              this.correctQuestions++;
            } else {
              this.totalMarks -= Number.parseInt(this.testDetails.JeeInCorrect);
              this.inCorrectQuestions++;
            }
          }
        }
      });
    } else if (
      this.testDetails.TestType == "3" ||
      this.testDetails.TestType == "5"
    ) {
      this.questionsList.map((question) => {
        if (question.userAns != "") {
          if (question.Answer == question.userAns) {
            if (
              question.SubjectTestId == 1 ||
              question.SubjectTestId == 2 ||
              question.SubjectTestId == 4
            )
              this.totalMarks += 1;
            else this.totalMarks += 2;
            this.correctQuestions++;
          } else this.inCorrectQuestions++;
        }
      });
    }
    this.skippedQuestions =
      this.questionsList.length -
      (this.correctQuestions + this.inCorrectQuestions);
    // console.log("skipped", this.skippedQuestions);
    // console.log("correct", this.correctQuestions);
    // console.log("incorrect", this.inCorrectQuestions);
    // console.log("total marks", this.totalMarks);
    // console.log("out of marks", this.outOfMarks);
    this.createPieChart();
  }

  getTime() {
    if (this.secs < 0) {
      this.totalMins = Number.parseInt(this.testDetails.Duration);
      this.totalSecs = "00";
    } else {
      let min = Number.parseInt(this.testDetails.Duration) - 1;
      let sec = 60;
      this.totalMins = min - this.fmins;
      this.totalSecs = sec - this.fsecs;
      if (this.totalSecs == 60) {
        this.totalMins = this.totalMins + 1;
        this.totalSecs = 0;
      }
      if (this.totalMins < 10) {
        this.totalMins = "0" + this.totalMins;
      }

      if (this.totalSecs < 10) {
        this.totalSecs = "0" + this.totalSecs;
      }
    }
  }

  saveTest() {
    let day = new Date().getDate().toString();
    if (day.length == 1) day = "0" + day;
    let month = (new Date().getMonth() + 1).toString();
    if (month.length == 1) month = "0" + month;
    let year = new Date().getFullYear().toString();
    let submitDate = day + month + year;
    this.savedTestDetails = {
      testDate: submitDate,
      subjectName: this.testDetails.Title,
      totalQue: this.questionsList.length,
      obtainedMarks: this.totalMarks,
      totalMarks: this.outOfMarks,
      totalTime: Number.parseInt(this.testDetails.Duration),
      attQueCount: this.correctQuestions + this.inCorrectQuestions,
      nonAttQueCount: this.skippedQuestions,
      corrQueCount: this.correctQuestions,
      inCorrQueCount: this.inCorrectQuestions,
      timeTakenMin: this.totalMins,
      timeTakenSec: this.totalSecs,
      testType: this.testDetails.TestType,
      questions: JSON.stringify(this.questionsList),
    };
    console.log("save test obj", this.savedTestDetails);

    this.saveTestInfoService.insertIntoTable(this.savedTestDetails);
    // let savedTest = { ...this.savedTestDetails };
    this.apiService
      .APPPostResultOfOnlineTest(
        this.savedTestDetails,
        this.storageService.userDetails.userId,
        this.storageService.userDetails.clientId
      )
      .then((data) => console.log("savedTestApi", JSON.parse(data.data)));
  }

  sendTestDetails() {
    let questionInfo = [];
    this.questionsList.map((question) => {
      if (question.QuestionType != 3) {
        questionInfo.push({
          QuestionId: question.QuestionId.toString(),
          CorrectAnswer: question.Answer,
          StudentAnswer: question.userAns != "" ? question.userAns : "Unsolved",
          Status:
            question.Answer == question.userAns
              ? "Correct"
              : question.userAns == ""
              ? "Unsolved"
              : "Incorrect",
        });
      } else {
        questionInfo.push({
          QuestionId: question.QuestionId.toString(),
          CorrectAnswer: `${question.Numerical_Answer}, ${question.Unit}`,
          StudentAnswer:
            question.userNumericalAns != ""
              ? `${question.userNumericalAns}, ${question.userUnit}`
              : "Unsolved",
          Status:
            question.Numerical_Answer == question.userNumericalAns &&
            question.Unit == question.userUnit
              ? "Correct"
              : question.userNumericalAns == ""
              ? "Unsolved"
              : "Incorrect",
        });
      }
    });

    let testDetails = {
      ArrengedTestId: this.testDetails.ArrengeTestId,
      UserId: this.storageService.userDetails.userId,
      TestPaperId: this.testDetails.TestPaperId,
      TestDate:
        this.testDetails.TestDate.split("-")[1] +
        "-" +
        this.testDetails.TestDate.split("-")[0] +
        "-" +
        this.testDetails.TestDate.split("-")[2],
      TimeDuration: this.testDetails.Duration,
      StartTime: this.testDetails.StartTime,
      ObtainedMarks: this.totalMarks,
      OutOfMarks: this.outOfMarks,
      Questions: questionInfo,
      BranchId: this.storageService.userDetails.branchId,
      ClientId: this.storageService.userDetails.clientId,
      timeTakenMin: this.totalMins,
      timeTakenSec: this.totalSecs,
    };
    console.log("test details", testDetails);
    this.apiService.sendTestDetails(testDetails).then(
      (data) => console.log(data.data),
      (err) => console.log(err)
    );
  }
  createPieChart() {
    if (this.page == "saved-test") {
      this.correctPer = Math.round(
        (this.savedTestDetails.corrQueCount / this.savedTestDetails.totalQue) *
          100
      );
      this.inCorrectPer = Math.round(
        (this.savedTestDetails.inCorrQueCount /
          this.savedTestDetails.totalQue) *
          100
      );
      this.skippedPer = 100 - (this.correctPer + this.inCorrectPer);
    } else if (this.page == "unit-test") {
      this.correctPer = Math.round(
        (this.correctQuestions / this.questionsList.length) * 100
      );
      this.inCorrectPer = Math.round(
        (this.inCorrectQuestions / this.questionsList.length) * 100
      );
      this.skippedPer = 100 - (this.correctPer + this.inCorrectPer);
    }
    this.pie1 = new Chart(this.pieChart1.nativeElement, {
      type: "pie",
      data: {
        datasets: [
          {
            data: [this.correctPer, this.inCorrectPer, this.skippedPer],
            backgroundColor: ["#2dd36f", "#eb445a", "#3880ff"],
          },
        ],
      },
      options: {
        tooltips: {
          enabled: false,
        },
      },
    });
    if (this.page == "unit-test") {
      if (this.selectedOptionArr.length == 0) {
        this.accuracy = 0;
      } else {
        this.accuracy = Math.round(
          (this.correctQuestions /
            (this.correctQuestions + this.inCorrectQuestions)) *
            100
        );
      }
    } else if (this.page == "saved-test") {
      if (this.savedTestDetails.attQueCount == 0) {
        this.accuracy = 0;
      } else {
        this.accuracy = Math.round(
          (this.savedTestDetails.corrQueCount /
            (this.savedTestDetails.corrQueCount +
              this.savedTestDetails.inCorrQueCount)) *
            100
        );
      }
    }
    this.pie2 = new Chart(this.pieChart2.nativeElement, {
      type: "pie",
      data: {
        datasets: [
          {
            data: [this.accuracy, 100 - this.accuracy],
            backgroundColor: ["#2dd36f", "#eb445a"],
          },
        ],
      },
      options: {
        tooltips: {
          enabled: false,
        },
      },
    });
    if (this.page == "unit-test") {
      this.score = Math.round((this.totalMarks / this.outOfMarks) * 100);
    } else if (this.page == "saved-test") {
      this.score = Math.round(
        (this.savedTestDetails.obtainedMarks /
          this.savedTestDetails.totalMarks) *
          100
      );
    }
    this.pie3 = new Chart(this.pieChart3.nativeElement, {
      type: "pie",
      data: {
        datasets: [
          {
            data: [this.score, 100 - this.score],
            backgroundColor: ["#2dd36f", "#eb445a"],
          },
        ],
      },
      options: {
        tooltips: {
          enabled: false,
        },
      },
    });
  }

  solution() {
    this.inCorrOption = "";
    this.testView = true;
    this.index = 0;
    this.showTest();
    this.option = this.questionsList[0].Answer;
    if (
      this.questionsList[this.index].Answer !=
      this.questionsList[this.index].userAns
    ) {
      this.inCorrOption = this.questionsList[this.index].userAns;
    }
    if (
      (this.page == "saved-test" || this.page == "unit-test") &&
      this.testSubmitted == true
    ) {
      this.getQuestionStatus();
    }
  }

  selectQuestion(index) {
    this.inCorrOption = "";
    this.testView = true;
    this.index = index;
    this.showTest();
    this.answer = this.questionsList[index].userNumericalAns;
    this.unit = this.questionsList[index].userUnit;
    this.option = this.questionsList[index].Answer;
    if (
      this.questionsList[this.index].Answer !=
      this.questionsList[this.index].userAns
    ) {
      this.inCorrOption = this.questionsList[this.index].userAns;
    }
    if (
      (this.page == "saved-test" || this.page == "unit-test") &&
      this.testSubmitted == true
    ) {
      this.getQuestionStatus();
    }
  }

  exitTest() {
    this.navController.back();
    // if (this.page == "unit-test")
    //   this.router.navigateByUrl("/dashboard/unit-test");

    // else {
    //   this.navController.back();
    // }
  }
}
