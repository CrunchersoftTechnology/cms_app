export interface FileInfo {
  Id: number;
  Title: string;
  FileName: string;
  Date: string;
  SubjectId: number;
  SubjectName: string;
}

export interface FileInfoTimeTable {
  Description: string;
  FileName: string;
  Date: string;
  Category: string;
  AttachmentDescription: string;
}

export interface FileInfoDPP {
  Description: string;
  FileName: string;
  Date: string;
  AttachmentDescription: string;
}

export interface VideoInfo {
  Id: number;
  Description: string;
  Link: string;
  Date: string;
  SubjectId: number;
  SubjectName: string;
  FileName: string;
}
