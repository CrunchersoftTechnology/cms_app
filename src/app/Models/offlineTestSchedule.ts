export interface OfflineTestSchedule {
  NotificationId: String;
  SubjectId: Number;
  InTime: String;
  OutTime: String;
  TotalMark: Number;
  Title: String;
  TestDate: String;
  Category: String;
}
