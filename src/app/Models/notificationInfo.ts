export interface LastNotification {
  ArrengeTestId: string;
  BatchId: string;
  MaxOfflineTestPaperId: string;
  MaxOfflineTestStudentMarksId: string;
  NBody: string;
  NDate: string;
  NOnlineNotificationId: string;
  NTime: string;
  ROnlineNotificationId: string;
  ROnlineTestNotificationId: string;
  RPdfUploadId: string;
  branchId: string;
  classId: string;
  selectedSubjects: string;
  studentName: string;
  userId: string;
}

export interface Notifications {
  ArrengeTestId: string;
  CetCorrect: string;
  CetInCorrect: string;
  Duration: string;
  EndDate: string;
  EndTime: string;
  ID: number;
  JeeCorrect: string;
  JeeInCorrect: string;
  JeeNewCorrect: string;
  JeeNewInCorrect: string;
  NeetCorrect: string;
  NeetInCorrect: string;
  SId: string;
  StartTime: string;
  Status: string;
  TestDate: string;
  TestObj: string;
  TestPaperId: string;
  TestType: string;
  Title: string;
}

export interface Notice {
  Category: string;
  ID: number;
  NBody: string;
  NDate: string;
  NOnlineNotificationId: string;
  NStatus: string;
  NTime: string;
  sId: string;
}
