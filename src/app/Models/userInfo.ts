export interface UserInfo {
  userName: string;
  resultCode: string;
  UserId: string;
  ParentContact: string;
  StudentName: string;
  ClassName: string;
  SID: string;
  ClassId: string;
  SelectedSubjects: string;
  StudentContact: string;
  IsActive: string;
  Email: string;
  BranchName: string;
  BranchId: string;
  ClientId: string;
  BatchId: string;
  NotificationId: string;
  OnlineTestId: string;
  SubjectName: string;
  BatchName: string;
  PdfUploadId: string;
  MaxOfflineTestPaperI: string;
  MaxOfflineTestStudentMarksId: string;
  AppPassword: string;
  LoginStatus: string;
  RegistrationDate: string;
  SecurityCode: number;
  DeviceId: string
}

export interface profileInfo {
  studentName: string;
  className: string;
  studentContact: string;
  parentContact: string;
  subjectName: string;
  email: string;
  branchName: string;
  batchName: string;
}

export interface UserDetails {
    parentName: string;
    studentName: string;
    parentContact: string;
    className: string;
    sId: string;
    birthDate: string;
    classId: string;
    selectedSubjects: string;
    studentContact: string;
    branchName: string;
    email: string;
    branchId: string;
    onlineNotificationId: string;
    onlineTestId: string;
    batchId: string;
    subjectName: string;
    batchName: string;
    pdfUploadId: string;
    maxOfflineTestPaperId: string;
    maxOfflineTestStudentMarksId: string;
    clientId: string;
    userId: string;
    status: string;
    schoolName: string;
    doj: string;
    photoPath: string
    // RegistrationDate: string
}
