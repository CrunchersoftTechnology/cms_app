export interface OfflineResult {
  ObtainedMarks: string;
  TotalMarks: string;
  Timing: string;
  Duration: string;
  Title: string;
  Percentage: string;
  TestDate: string;
  Category: string;
  Status: string;
}
