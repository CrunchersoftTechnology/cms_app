export interface feesHistoryDetails {
  paidFees: string;
  paidDate: string;
}

export interface feesDetails {
  totalFees: string;
  paidFees: string;
  remainingFees: string;
}
