export interface SavedTestDetails {
    testDate: string;
    subjectName: string;
    totalQue: number;
    obtainedMarks: number;
    totalMarks: number;
    totalTime: number;
    attQueCount: number;
    nonAttQueCount: number;
    corrQueCount: number;
    inCorrQueCount: number;
    timeTakenMin: string;
    timeTakenSec: string;
    testType: string;
    questions: string;
  };