(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Pages-notice-page-notice-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/notice-page/notice-page.page.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/notice-page/notice-page.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesNoticePageNoticePagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\" mode=\"ios\"> </ion-toolbar>\n</ion-header>\n\n<ion-content> </ion-content>\n";
      /***/
    },

    /***/
    "./src/app/Pages/notice-page/notice-page-routing.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/Pages/notice-page/notice-page-routing.module.ts ***!
      \*****************************************************************/

    /*! exports provided: NoticePagePageRoutingModule */

    /***/
    function srcAppPagesNoticePageNoticePageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NoticePagePageRoutingModule", function () {
        return NoticePagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _notice_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./notice-page.page */
      "./src/app/Pages/notice-page/notice-page.page.ts");

      var routes = [{
        path: '',
        component: _notice_page_page__WEBPACK_IMPORTED_MODULE_3__["NoticePagePage"]
      }];

      var NoticePagePageRoutingModule = function NoticePagePageRoutingModule() {
        _classCallCheck(this, NoticePagePageRoutingModule);
      };

      NoticePagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], NoticePagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/Pages/notice-page/notice-page.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/Pages/notice-page/notice-page.module.ts ***!
      \*********************************************************/

    /*! exports provided: NoticePagePageModule */

    /***/
    function srcAppPagesNoticePageNoticePageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NoticePagePageModule", function () {
        return NoticePagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _notice_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./notice-page-routing.module */
      "./src/app/Pages/notice-page/notice-page-routing.module.ts");
      /* harmony import */


      var _notice_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./notice-page.page */
      "./src/app/Pages/notice-page/notice-page.page.ts");

      var NoticePagePageModule = function NoticePagePageModule() {
        _classCallCheck(this, NoticePagePageModule);
      };

      NoticePagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _notice_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["NoticePagePageRoutingModule"]],
        declarations: [_notice_page_page__WEBPACK_IMPORTED_MODULE_6__["NoticePagePage"]]
      })], NoticePagePageModule);
      /***/
    },

    /***/
    "./src/app/Pages/notice-page/notice-page.page.scss":
    /*!*********************************************************!*\
      !*** ./src/app/Pages/notice-page/notice-page.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesNoticePageNoticePagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL1BhZ2VzL25vdGljZS1wYWdlL25vdGljZS1wYWdlLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/Pages/notice-page/notice-page.page.ts":
    /*!*******************************************************!*\
      !*** ./src/app/Pages/notice-page/notice-page.page.ts ***!
      \*******************************************************/

    /*! exports provided: NoticePagePage */

    /***/
    function srcAppPagesNoticePageNoticePagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NoticePagePage", function () {
        return NoticePagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var NoticePagePage = /*#__PURE__*/function () {
        function NoticePagePage() {
          _classCallCheck(this, NoticePagePage);

          this.sid = "";
          this.noticeDetails = [];
        }

        _createClass(NoticePagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return NoticePagePage;
      }();

      NoticePagePage.ctorParameters = function () {
        return [];
      };

      NoticePagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-notice-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./notice-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/notice-page/notice-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./notice-page.page.scss */
        "./src/app/Pages/notice-page/notice-page.page.scss"))["default"]]
      })], NoticePagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=Pages-notice-page-notice-page-module-es5.js.map