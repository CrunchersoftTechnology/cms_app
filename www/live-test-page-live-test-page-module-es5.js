(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["live-test-page-live-test-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/live-test-page/live-test-page.page.html":
    /*!*****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/live-test-page/live-test-page.page.html ***!
      \*****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesLiveTestPageLiveTestPagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar\n  color=\"primary\"\n  mode=\"ios\"\n  *ngIf=\"storageService.instituteName.length > 0\"\n>\n  <ion-title>{{storageService.instituteName}}</ion-title>\n</ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons class=\"menuButton\" slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Tests</ion-title>\n  </ion-toolbar>\n  <ion-toolbar>\n    <ion-segment\n      color=\"primary\"\n      [(ngModel)]=\"selectedView\"\n      class=\"ion-no-padding item\"\n      (ionChange)=\"onViewChange($event)\"\n    >\n      <ion-segment-button value=\"0\">\n        <ion-label style=\"font-size: 3vw\">Live Tests</ion-label>\n      </ion-segment-button>\n      <ion-segment-button value=\"1\">\n        <ion-label style=\"font-size: 3vw\">Recent Tests</ion-label>\n      </ion-segment-button>\n    </ion-segment>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\">\n  <ion-refresher (ionRefresh)=\"doRefresh($event)\" slot=\"fixed\">\n    <ion-refresher-content\n      pullingIcon=\"chevron-down-circle-outline\"\n      pullingText=\"Pull to refresh\"\n      refreshingSpinner=\"circles\"\n      refreshingText=\"Refreshing...\"\n    >\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-slides\n    #slides\n    mode=\"md\"\n    pager=\"false\"\n    class=\"slides\"\n    [options]=\"sliderOptions\"\n    (ionSlideDidChange)=\"slideChanged(slides)\"\n    (ionSlidesDidLoad)=\"slideChanged(slides)\"\n  >\n    <ion-slide class=\"slide\">\n      <ion-grid class=\"ion-no-padding ion-no-margin\">\n        <ion-row>\n          <ion-col size=\"12\">\n            <div *ngFor=\"let notification of notifications\">\n              <ion-card\n                *ngIf=\"notification.Status == 'Unread'\"\n                class=\"ion-padding\"\n                (click)=\"onTestClick(notification)\"\n              >\n                <ion-card-title>{{notification.Title}}</ion-card-title>\n                <ion-card-content>\n                  <ion-item lines=\"none\" class=\"ion-no-margin ion-no-padding\">\n                    <ion-label>{{notification.TestDate}}</ion-label> <br />\n                    <ion-label>{{notification.StartTime}}</ion-label>\n                    <ion-icon\n                      name=\"checkmark-circle\"\n                      slot=\"end\"\n                      color=\"success\"\n                    ></ion-icon>\n                  </ion-item>\n                </ion-card-content>\n              </ion-card>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-slide>\n    <ion-slide class=\"slide\">\n      <ion-grid class=\"ion-no-padding ion-no-margin\">\n        <ion-row>\n          <ion-col size=\"12\">\n            <div *ngFor=\"let notification of notifications\">\n              <ion-card\n                *ngIf=\"notification.Status != 'Unread'\"\n                class=\"ion-padding\"\n              >\n                <ion-card-title>{{notification.Title}}</ion-card-title>\n                <ion-card-content>\n                  <ion-item lines=\"none\" class=\"ion-no-margin ion-no-padding\">\n                    <ion-label>{{notification.TestDate}}</ion-label> <br />\n                    <ion-label>{{notification.StartTime}}</ion-label>\n                    <ion-icon\n                      name=\"checkmark-circle\"\n                      slot=\"end\"\n                      color=\"danger\"\n                    ></ion-icon>\n                  </ion-item>\n                </ion-card-content>\n              </ion-card>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/live-test-page/live-test-page-routing.module.ts":
    /*!***********************************************************************!*\
      !*** ./src/app/pages/live-test-page/live-test-page-routing.module.ts ***!
      \***********************************************************************/

    /*! exports provided: LiveTestPagePageRoutingModule */

    /***/
    function srcAppPagesLiveTestPageLiveTestPageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LiveTestPagePageRoutingModule", function () {
        return LiveTestPagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _live_test_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./live-test-page.page */
      "./src/app/pages/live-test-page/live-test-page.page.ts");

      var routes = [{
        path: '',
        component: _live_test_page_page__WEBPACK_IMPORTED_MODULE_3__["LiveTestPagePage"]
      }];

      var LiveTestPagePageRoutingModule = function LiveTestPagePageRoutingModule() {
        _classCallCheck(this, LiveTestPagePageRoutingModule);
      };

      LiveTestPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LiveTestPagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/live-test-page/live-test-page.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/live-test-page/live-test-page.module.ts ***!
      \***************************************************************/

    /*! exports provided: LiveTestPagePageModule */

    /***/
    function srcAppPagesLiveTestPageLiveTestPageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LiveTestPagePageModule", function () {
        return LiveTestPagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _live_test_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./live-test-page-routing.module */
      "./src/app/pages/live-test-page/live-test-page-routing.module.ts");
      /* harmony import */


      var _live_test_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./live-test-page.page */
      "./src/app/pages/live-test-page/live-test-page.page.ts");

      var LiveTestPagePageModule = function LiveTestPagePageModule() {
        _classCallCheck(this, LiveTestPagePageModule);
      };

      LiveTestPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _live_test_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["LiveTestPagePageRoutingModule"]],
        declarations: [_live_test_page_page__WEBPACK_IMPORTED_MODULE_6__["LiveTestPagePage"]]
      })], LiveTestPagePageModule);
      /***/
    },

    /***/
    "./src/app/pages/live-test-page/live-test-page.page.scss":
    /*!***************************************************************!*\
      !*** ./src/app/pages/live-test-page/live-test-page.page.scss ***!
      \***************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesLiveTestPageLiveTestPagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".slides {\n  height: 100%;\n}\n\n.slide {\n  width: 412px;\n  overflow-y: auto;\n  align-items: start;\n}\n\n.slides-div {\n  position: absolute;\n  top: 0%;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGl2ZS10ZXN0LXBhZ2UvbGl2ZS10ZXN0LXBhZ2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xpdmUtdGVzdC1wYWdlL2xpdmUtdGVzdC1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zbGlkZXMge1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4uc2xpZGUge1xyXG4gICAgd2lkdGg6IDQxMnB4O1xyXG4gICAgb3ZlcmZsb3cteTogYXV0bztcclxuICAgIGFsaWduLWl0ZW1zOiBzdGFydDtcclxufVxyXG5cclxuLnNsaWRlcy1kaXYge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/live-test-page/live-test-page.page.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/live-test-page/live-test-page.page.ts ***!
      \*************************************************************/

    /*! exports provided: LiveTestPagePage */

    /***/
    function srcAppPagesLiveTestPageLiveTestPagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LiveTestPagePage", function () {
        return LiveTestPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/Services/notification-service.service */
      "./src/app/Services/notification-service.service.ts");
      /* harmony import */


      var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../../Services/api-service.service */
      "./src/app/Services/api-service.service.ts");
      /* harmony import */


      var _Services_loading_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./../../Services/loading-service.service */
      "./src/app/Services/loading-service.service.ts");
      /* harmony import */


      var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./../../Services/toast-service.service */
      "./src/app/Services/toast-service.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _Components_test_details_component_test_details_component_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./../../Components/test-details-component/test-details-component.component */
      "./src/app/Components/test-details-component/test-details-component.component.ts");
      /* harmony import */


      var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./../../Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var LiveTestPagePage = /*#__PURE__*/function () {
        function LiveTestPagePage(notificationService, apiService, loadingService, toastService, storageService, loadingController, popoverController, storage, router) {
          _classCallCheck(this, LiveTestPagePage);

          this.notificationService = notificationService;
          this.apiService = apiService;
          this.loadingService = loadingService;
          this.toastService = toastService;
          this.storageService = storageService;
          this.loadingController = loadingController;
          this.popoverController = popoverController;
          this.storage = storage;
          this.router = router;
          this.sliderOptions = {
            initialSlide: 0,
            speed: 500,
            slidesPerView: 1
          };
          this.selectedView = 0;
          this.notifications = [];
          this.liveTests = [];
          this.recentTests = [];
        }

        _createClass(LiveTestPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.getNotifications();
          }
        }, {
          key: "getNotifications",
          value: function getNotifications() {
            var _this = this;

            this.notificationService.getNotifications().then(function (data) {
              _this.notifications = data; // this.notifications.map((notification) => {
              //   if(notification.Status == 'Unread') {
              //     if(notification.EndDate == '09-11-2020') {
              //     }
              //   }
              // });

              console.log("notifications", _this.notifications);
            });
          }
        }, {
          key: "onTestClick",
          value: function onTestClick(notifications) {
            var _this2 = this;

            // console.log(notifications);
            if (notifications.Status == "Unread") {
              if (notifications.TestObj == "TestObj") {
                this.loadingService.createLoading("Downloading test").then(function (_) {
                  console.log("in if");

                  _this2.apiService.getTestObj(notifications.TestPaperId).then(function (result) {
                    // console.log(result.data);
                    var testObj = result.data;
                    _this2.notifications[_this2.notifications.indexOf(notifications)].TestObj = testObj;

                    _this2.notificationService.updateOnlineTestNotifications(testObj, notifications.ID).then(function (_) {
                      _this2.loadingController.dismiss().then(function (_) {
                        return _this2.createPopover(notifications);
                      });
                    });
                  }, function (err) {
                    _this2.loadingController.dismiss().then(function (_) {
                      return _this2.toastService.createToast("Error while downloading test", 3000);
                    });
                  });
                });
              } else {
                this.createPopover(notifications);
              }

              this.GetCMReceivedTestNotification(notifications.ArrengeTestId, this.storageService.userDetails.userId, this.storageService.userDetails.clientId, "received");
            } else this.toastService.createToast("Test already given", 3000);
          }
        }, {
          key: "GetCMReceivedTestNotification",
          value: function GetCMReceivedTestNotification(arrangeTestId, userId, clientId, recivedCode) {
            this.apiService.GetCMReceivedTestNotification(arrangeTestId, userId, clientId, recivedCode).then(function (data) {
              return console.log("recivedTestApi", JSON.parse(data.data).d);
            });
          }
        }, {
          key: "createPopover",
          value: function createPopover(notification) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this3 = this;

              var popover;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.storageService.testExpired = ""; // console.log(notification)

                      _context.next = 3;
                      return this.popoverController.create({
                        component: _Components_test_details_component_test_details_component_component__WEBPACK_IMPORTED_MODULE_7__["TestDetailsComponentComponent"],
                        animated: true,
                        cssClass: "myPopOver1",
                        componentProps: {
                          notification: notification
                        }
                      });

                    case 3:
                      popover = _context.sent;
                      _context.next = 6;
                      return popover.present();

                    case 6:
                      popover.onDidDismiss().then(function (_) {
                        console.log("testExpired", _this3.storageService.testExpired);
                        if (_this3.storageService.testExpired == "true") notification.Status = "expired";else if (_this3.storageService.testExpired == "false") notification.Status = "read";
                      });

                    case 7:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "doRefresh",
          value: function doRefresh(event) {
            var _this4 = this;

            // this.notificationService.getMaxArrengeTestId().then((data) => {
            this.storage.get("maxTestPaperId").then(function (data) {
              console.log("maxTestPaperId", data);

              _this4.apiService.getCMSLatestOnlineTestRecived(_this4.storageService.userDetails.userId, _this4.storageService.userDetails.clientId, data).then(function (data) {
                var recivedTests = [];
                recivedTests = JSON.parse(data.data).d;
                console.log("revivedTests", recivedTests);

                if (recivedTests.length > 0) {
                  var maxPaperId = recivedTests[recivedTests.length - 1].ArrengeTestId;

                  _this4.storage.set("maxTestPaperId", maxPaperId);

                  recivedTests.map(function (recivedTest) {
                    var Message = recivedTest.Message;
                    var title = Message.split("$^$")[0];
                    var TestType = Message.split("$^$")[1].split("TestType:")[1];
                    var CetCorrect = Message.split("$^$")[2].split("CetCorrect:")[1];
                    var CetInCorrect = Message.split("$^$")[3].split("CetInCorrect:")[1];
                    var NeetCorrect = Message.split("$^$")[4].split("NeetCorrect:")[1];
                    var NeetInCorrect = Message.split("$^$")[5].split("NeetInCorrect:")[1];
                    var JeeCorrect = Message.split("$^$")[6].split("JeeCorrect:")[1];
                    var JeeInCorrect = Message.split("$^$")[7].split("JeeInCorrect:")[1];
                    var JeeNewCorrect = Message.split("$^$")[8].split("JeeNewCorrect:")[1];
                    var JeeNewInCorrect = Message.split("$^$")[9].split("JeeNewInCorrect:")[1];
                    var testDate = Message.split("$^$")[10].split("Date:")[1];
                    var EndDate = Message.split("$^$")[11].split("EndDate:")[1];
                    var startTime = Message.split("$^$")[12].split("Start Time:")[1];
                    var endTime = Message.split("$^$")[13].split("End Time:")[1];
                    var duration = Message.split("$^$")[14].split("Duration:")[1];
                    var testPaperId = Message.split("$^$")[15].split("TestPaperId:")[1];
                    var testDetails;
                    testDetails = {
                      CetCorrect: CetCorrect,
                      CetInCorrect: CetInCorrect,
                      EndDate: EndDate,
                      JeeCorrect: JeeCorrect,
                      JeeInCorrect: JeeInCorrect,
                      JeeNewCorrect: JeeNewCorrect,
                      JeeNewInCorrect: JeeNewInCorrect,
                      NeetCorrect: NeetCorrect,
                      NeetInCorrect: NeetInCorrect,
                      TestType: TestType,
                      arrengeTestId: recivedTest.ArrengeTestId,
                      duration: duration,
                      endTime: endTime,
                      sid: _this4.storageService.userDetails.sId,
                      startTime: startTime,
                      status: "",
                      testDate: testDate,
                      testPaperId: testPaperId,
                      title: title
                    };

                    _this4.notificationService.insertIntoOnlineTestNotifications(testDetails, null).then(function (_) {
                      _this4.getNotifications();

                      event.target.complete();
                    });
                  });
                } else {
                  event.target.complete();

                  _this4.toastService.createToast("You recived all your tests", 3000);
                }
              });
            }); // event.target.complete()
          }
        }, {
          key: "onViewChange",
          value: function onViewChange(event) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.selectedSlide.slideTo(this.selectedView);

                    case 2:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "slideChanged",
          value: function slideChanged(slides) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this5 = this;

              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      // console.log("in slide change");
                      //  this.slider.getActiveIndex().then(index => {
                      //   this.selectedView = index
                      //   })
                      //   console.log(this.selectedView)
                      this.selectedSlide = slides;
                      slides.getActiveIndex().then(function (index) {
                        _this5.selectedView = index;
                      });

                    case 2:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }]);

        return LiveTestPagePage;
      }();

      LiveTestPagePage.ctorParameters = function () {
        return [{
          type: src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_2__["NotificationServiceService"]
        }, {
          type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_3__["ApiServiceService"]
        }, {
          type: _Services_loading_service_service__WEBPACK_IMPORTED_MODULE_4__["LoadingServiceService"]
        }, {
          type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_5__["ToastServiceService"]
        }, {
          type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_8__["StorageServiceService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["PopoverController"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["Storage"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"]
        }];
      };

      LiveTestPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-live-test-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./live-test-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/live-test-page/live-test-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./live-test-page.page.scss */
        "./src/app/pages/live-test-page/live-test-page.page.scss"))["default"]]
      })], LiveTestPagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=live-test-page-live-test-page-module-es5.js.map