(function () {
  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["attendance-page-attendance-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/attendance-page/attendance-page.page.html":
    /*!*******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/attendance-page/attendance-page.page.html ***!
      \*******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesAttendancePageAttendancePagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar\n    color=\"primary\"\n    mode=\"ios\"\n    *ngIf=\"storageService.instituteName.length > 0\"\n  >\n    <ion-title>{{storageService.instituteName}}</ion-title>\n  </ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Attendance</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content\n      pullingIcon=\"chevron-down-circle-outline\"\n      pullingText=\"Pull to refresh\"\n      refreshingSpinner=\"circles\"\n      refreshingText=\"Refreshing...\"\n    >\n    </ion-refresher-content>\n  </ion-refresher>\n  <canvas [hidden]=\"attendanceDataLocal.length == 0\" #pieChart></canvas>\n\n  <ion-item>\n    <ion-grid fixed>\n      <ion-row>\n        <ion-col [size]=\"subjects.length > 0 ? 4 : 6\">\n          <ion-select\n            [(ngModel)]=\"selectedMonth\"\n            interface=\"popover\"\n            (ionChange)=\"onSelectChange()\"\n          >\n            <ion-select-option value=\"0\" [disabled]=\"true\"\n              >Month</ion-select-option\n            >\n            <ion-select-option value=\"13\">All</ion-select-option>\n            <ion-select-option value=\"1\">January</ion-select-option>\n            <ion-select-option value=\"2\">February</ion-select-option>\n            <ion-select-option value=\"3\">March</ion-select-option>\n            <ion-select-option value=\"4\">April</ion-select-option>\n            <ion-select-option value=\"5\">May</ion-select-option>\n            <ion-select-option value=\"6\">June</ion-select-option>\n            <ion-select-option value=\"7\">July</ion-select-option>\n            <ion-select-option value=\"8\">August</ion-select-option>\n            <ion-select-option value=\"9\">September</ion-select-option>\n            <ion-select-option value=\"10\">October</ion-select-option>\n            <ion-select-option value=\"11\">November</ion-select-option>\n            <ion-select-option value=\"12\">December</ion-select-option>\n          </ion-select>\n        </ion-col>\n        <ion-col [size]=\"subjects.length > 0 ? 4 : 6\">\n          <ion-select\n            [(ngModel)]=\"selectedStatus\"\n            interface=\"popover\"\n            (ionChange)=\"onSelectChange()\"\n          >\n            <ion-select-option value=\"0\" [disabled]=\"true\"\n              >Status</ion-select-option\n            >\n            <ion-select-option value=\"Both\">Both</ion-select-option>\n            <ion-select-option value=\"Present\">Present</ion-select-option>\n            <ion-select-option value=\"Absent\">Absent</ion-select-option>\n          </ion-select></ion-col\n        >\n        <ion-col *ngIf=\"subjects.length > 0\" size=\"4\">\n          <ion-select\n            [(ngModel)]=\"selectedSubject\"\n            interface=\"popover\"\n            (ionChange)=\"onSelectChange()\"\n          >\n            <ion-select-option value=\"0\" [disabled]=\"true\"\n              >Subject</ion-select-option\n            >\n            <ion-select-option value=\"All\">All</ion-select-option>\n            <ion-select-option *ngFor=\"let sub of subjects\" [value]=\"sub\"\n              >{{sub}}</ion-select-option\n            >\n          </ion-select>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-item>\n  <ion-list *ngIf=\"attendanceData.length > 0; else elseBlock\">\n    <ion-item *ngFor=\"let data of attendanceData\">\n      <ion-icon\n        *ngIf=\"data.status == 'Present' \"\n        class=\"ion-margin-start\"\n        slot=\"start\"\n        name=\"checkmark\"\n        color=\"success\"\n      ></ion-icon>\n      <ion-icon\n        *ngIf=\"data.status == 'Absent' \"\n        class=\"ion-margin-start\"\n        slot=\"start\"\n        name=\"close-circle\"\n        color=\"danger\"\n      ></ion-icon>\n      <ion-label>{{data.date}}</ion-label>\n    </ion-item>\n  </ion-list>\n  <ng-template #elseBlock>\n    <ion-item>\n      <ion-label>No records found</ion-label>\n    </ion-item>\n  </ng-template>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/attendance-page/attendance-page-routing.module.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/pages/attendance-page/attendance-page-routing.module.ts ***!
      \*************************************************************************/

    /*! exports provided: AttendancePagePageRoutingModule */

    /***/
    function srcAppPagesAttendancePageAttendancePageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AttendancePagePageRoutingModule", function () {
        return AttendancePagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _attendance_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./attendance-page.page */
      "./src/app/pages/attendance-page/attendance-page.page.ts");

      var routes = [{
        path: '',
        component: _attendance_page_page__WEBPACK_IMPORTED_MODULE_3__["AttendancePagePage"]
      }];

      var AttendancePagePageRoutingModule = function AttendancePagePageRoutingModule() {
        _classCallCheck(this, AttendancePagePageRoutingModule);
      };

      AttendancePagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AttendancePagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/attendance-page/attendance-page.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/attendance-page/attendance-page.module.ts ***!
      \*****************************************************************/

    /*! exports provided: AttendancePagePageModule */

    /***/
    function srcAppPagesAttendancePageAttendancePageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AttendancePagePageModule", function () {
        return AttendancePagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _attendance_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./attendance-page-routing.module */
      "./src/app/pages/attendance-page/attendance-page-routing.module.ts");
      /* harmony import */


      var _attendance_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./attendance-page.page */
      "./src/app/pages/attendance-page/attendance-page.page.ts");

      var AttendancePagePageModule = function AttendancePagePageModule() {
        _classCallCheck(this, AttendancePagePageModule);
      };

      AttendancePagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _attendance_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["AttendancePagePageRoutingModule"]],
        declarations: [_attendance_page_page__WEBPACK_IMPORTED_MODULE_6__["AttendancePagePage"]]
      })], AttendancePagePageModule);
      /***/
    },

    /***/
    "./src/app/pages/attendance-page/attendance-page.page.scss":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/attendance-page/attendance-page.page.scss ***!
      \*****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAttendancePageAttendancePagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2F0dGVuZGFuY2UtcGFnZS9hdHRlbmRhbmNlLXBhZ2UucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/pages/attendance-page/attendance-page.page.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/attendance-page/attendance-page.page.ts ***!
      \***************************************************************/

    /*! exports provided: AttendancePagePage */

    /***/
    function srcAppPagesAttendancePageAttendancePagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AttendancePagePage", function () {
        return AttendancePagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var chart_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! chart.js */
      "./node_modules/chart.js/dist/Chart.js");
      /* harmony import */


      var chart_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var _Services_database_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../../Services/database.service */
      "./src/app/Services/database.service.ts");
      /* harmony import */


      var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./../../Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");
      /* harmony import */


      var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./../../Services/api-service.service */
      "./src/app/Services/api-service.service.ts");
      /* harmony import */


      var _Services_attendance_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./../../Services/attendance-service.service */
      "./src/app/Services/attendance-service.service.ts");
      /* harmony import */


      var _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./../../Services/internet-service.service */
      "./src/app/Services/internet-service.service.ts");
      /* harmony import */


      var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./../../Services/toast-service.service */
      "./src/app/Services/toast-service.service.ts");

      var AttendancePagePage = /*#__PURE__*/function () {
        function AttendancePagePage(databaseService, storageService, apiService, attendanceService, internetService, toastService) {
          _classCallCheck(this, AttendancePagePage);

          this.databaseService = databaseService;
          this.storageService = storageService;
          this.apiService = apiService;
          this.attendanceService = attendanceService;
          this.internetService = internetService;
          this.toastService = toastService;
          this.userId = "";
          this.sid = "";
          this.classId = "";
          this.selectedBatches = "";
          this.branchId = "";
          this.selectedMonth = "0";
          this.selectedSubject = "0";
          this.selectedStatus = "0";
          this.attendanceData = [];
          this.attendanceDataLocal = [];
          this.statusWiseAttendanceData = [];
          this.subjectWiseAttendanceData = [];
          this.subjects = [];
        }

        _createClass(AttendancePagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.userId = this.storageService.userDetails.userId;
            this.sid = this.storageService.userDetails.sId;
            this.classId = this.storageService.userDetails.classId;
            this.selectedBatches = this.storageService.userDetails.batchId;
            this.branchId = this.storageService.userDetails.branchId;
            if (this.internetService.networkConnected) this.getAttendanceData();else {
              this.getMoreStudAttendanceData();
              this.getDistinctSubjects();
              this.toastService.createToast("Check internet connection to update details", 3000);
            }
          }
        }, {
          key: "getAttendanceData",
          value: function getAttendanceData() {
            var _this = this;

            this.apiService.getAttendanceData().then(function (result) {
              console.log(JSON.parse(result.data).d[0]);
              var data = JSON.parse(result.data);
              var dataLength = 0;

              if (data.d.length > 0) {
                _this.attendanceService.deleteFromCMSAttendance().then(function (_) {
                  for (var i = 0; i < data.d.length; i++) {
                    var adate = data.d[i].ADate;
                    var SubjectName = data.d[i].SubjectName;
                    var BatchName = data.d[i].BatchName;
                    var InTime = data.d[i].InTime;
                    var OutTime = data.d[i].OutTime;
                    var MonthOfDate = data.d[i].Month;
                    var attStatus = data.d[i].Status;
                    dataLength = i + 1;

                    _this.attendanceService.insertIntoCMSAttendance(adate, attStatus, SubjectName, BatchName, InTime, OutTime, MonthOfDate);
                  }

                  if (dataLength == data.d.length) {
                    _this.getAttendanceCount(_this.sid, _this.classId, _this.userId, _this.selectedBatches, _this.branchId);
                  }
                });
              } else if (data.d.length == 0) {
                _this.attendanceService.deleteFromCMSAttendance().then(function (_) {
                  _this.getAttendanceCount(_this.sid, _this.classId, _this.userId, _this.selectedBatches, _this.branchId);
                });
              }
            }, function (err) {
              _this.getAttendanceCount(_this.sid, _this.classId, _this.userId, _this.selectedBatches, _this.branchId);
            });
          }
        }, {
          key: "getAttendanceCount",
          value: function getAttendanceCount(sid, classId, userId, selectedBatches, branchId) {
            var _this2 = this;

            this.apiService.getAttendanceCount(sid, classId, selectedBatches, branchId).then(function (result) {
              // console.log(JSON.parse(result.data));
              var data = JSON.parse(result.data);
              var dataLength = 0;

              if (data.d.length > 0) {
                var totalCount = data.d[0].TotalCount;
                var presentCount = data.d[0].PresentCount;

                _this2.attendanceService.deleteFromCMSAttendanceCount().then(function (_) {
                  for (var i = 0; i < totalCount.length; i++) {
                    dataLength = i + 1;
                    var monthNm = "";
                    if (dataLength == 1) monthNm = "Jan";else if (dataLength == 2) monthNm = "Feb";else if (dataLength == 3) monthNm = "Mar";else if (dataLength == 4) monthNm = "Apr";else if (dataLength == 5) monthNm = "May";else if (dataLength == 6) monthNm = "Jun";else if (dataLength == 7) monthNm = "Jul";else if (dataLength == 8) monthNm = "Aug";else if (dataLength == 9) monthNm = "Sept";else if (dataLength == 10) monthNm = "Oct";else if (dataLength == 11) monthNm = "Nov";else if (dataLength == 12) monthNm = "Dec";

                    _this2.attendanceService.insertIntoCMSAttendanceCount(dataLength, monthNm, totalCount[i], presentCount[i]);
                  }

                  _this2.getMoreStudAttendanceData();

                  _this2.getDistinctSubjects(); // this.getTopFiveMonthAttendance();

                });
              } else if (data.d.length == 0) {
                _this2.getMoreStudAttendanceData();

                _this2.getDistinctSubjects(); // this.getTopFiveMonthAttendance();

              }
            }, function (err) {
              _this2.getMoreStudAttendanceData();

              _this2.getDistinctSubjects(); // this.getTopFiveMonthAttendance();

            });
          }
        }, {
          key: "getMoreStudAttendanceData",
          value: function getMoreStudAttendanceData() {
            var _this3 = this;

            this.attendanceService.getAttendance().then(function (data) {
              _this3.attendanceDataLocal = data;
              _this3.attendanceData = _toConsumableArray(_this3.attendanceDataLocal);

              _this3.getTopFiveMonthAttendance();

              console.log("attendance data", _this3.attendanceData);
            });
          }
        }, {
          key: "getDistinctSubjects",
          value: function getDistinctSubjects() {
            var _this4 = this;

            this.attendanceService.getDistinctSubjects().then(function (data) {
              _this4.subjects = data.filter(function (d) {
                return d != null;
              });
              console.log("subjects", _this4.subjects);
            });
          }
        }, {
          key: "onSelectChange",
          value: function onSelectChange() {
            // console.log("selected status", this.selectedStatus);
            // console.log("Selected Month", this.selectedMonth);
            if (this.selectedMonth == "0" && this.selectedStatus != "0" && this.selectedSubject == "0") this.getOnlyStatusWiseAttendance(this.selectedStatus);else if (this.selectedMonth != "0" && this.selectedStatus == "0" && this.selectedSubject == "0") this.getOnlyMonthWiseAttendance(this.selectedMonth);else if (this.selectedMonth == "0" && this.selectedStatus == "0" && this.selectedSubject != "0") this.getOnlySubjectWiseAttendance(this.selectedSubject);else if (this.selectedMonth != "0" && this.selectedStatus != "0" && this.selectedSubject == "0") this.getMonthAndStatusWiseAttendance(this.selectedMonth, this.selectedStatus);else if (this.selectedMonth != "0" && this.selectedStatus == "0" && this.selectedSubject != "0") this.getMonthAndSubjectWiseAttendance(this.selectedMonth, this.selectedSubject);else if (this.selectedMonth == "0" && this.selectedStatus != "0" && this.selectedSubject != "0") this.getStatusAndSubjectWiseAttendance(this.selectedStatus, this.selectedSubject);else this.getStatusSubjectMonthWiseAttendance(this.selectedStatus, this.selectedSubject, this.selectedMonth);
          }
        }, {
          key: "getOnlyStatusWiseAttendance",
          value: function getOnlyStatusWiseAttendance(selectedStatus) {
            this.attendanceData = [];
            if (selectedStatus == "Both") this.attendanceData = _toConsumableArray(this.attendanceDataLocal);else this.attendanceData = this.attendanceDataLocal.filter(function (data) {
              return data.status == selectedStatus;
            });
          }
        }, {
          key: "getOnlyMonthWiseAttendance",
          value: function getOnlyMonthWiseAttendance(selectedMonth) {
            this.attendanceData = [];
            if (selectedMonth == "13") this.attendanceData = _toConsumableArray(this.attendanceDataLocal);else this.attendanceData = this.attendanceDataLocal.filter(function (data) {
              return data.monthOfDate == selectedMonth;
            });
          }
        }, {
          key: "getOnlySubjectWiseAttendance",
          value: function getOnlySubjectWiseAttendance(selectedSubject) {
            this.attendanceData = [];
            if (selectedSubject == "All") this.attendanceData = _toConsumableArray(this.attendanceDataLocal);else this.attendanceData = this.attendanceDataLocal.filter(function (data) {
              return data.subjectName == selectedSubject;
            });
          }
        }, {
          key: "getMonthAndStatusWiseAttendance",
          value: function getMonthAndStatusWiseAttendance(selectedMonth, selectedStatus) {
            this.attendanceData = [];
            this.statusWiseAttendanceData = [];
            if (selectedStatus == "Both") this.statusWiseAttendanceData = _toConsumableArray(this.attendanceDataLocal);else this.statusWiseAttendanceData = this.attendanceDataLocal.filter(function (data) {
              return data.status == selectedStatus;
            });
            if (selectedMonth == "13") this.attendanceData = _toConsumableArray(this.statusWiseAttendanceData);else this.attendanceData = this.statusWiseAttendanceData.filter(function (data) {
              return data.monthOfDate == selectedMonth;
            });
          }
        }, {
          key: "getMonthAndSubjectWiseAttendance",
          value: function getMonthAndSubjectWiseAttendance(selectedMonth, selectedSubject) {
            this.attendanceData = [];
            this.subjectWiseAttendanceData = [];
            if (selectedSubject == "All") this.subjectWiseAttendanceData = _toConsumableArray(this.attendanceDataLocal);else this.subjectWiseAttendanceData = this.attendanceDataLocal.filter(function (data) {
              return data.subjectName == selectedSubject;
            });
            if (selectedMonth == "13") this.attendanceData = _toConsumableArray(this.subjectWiseAttendanceData);else this.attendanceData = this.subjectWiseAttendanceData.filter(function (data) {
              return data.monthOfDate == selectedMonth;
            });
          }
        }, {
          key: "getStatusAndSubjectWiseAttendance",
          value: function getStatusAndSubjectWiseAttendance(selectedStatus, selectedSubject) {
            this.attendanceData = [];
            this.statusWiseAttendanceData = [];
            if (selectedStatus == "Both") this.statusWiseAttendanceData = _toConsumableArray(this.attendanceDataLocal);else this.statusWiseAttendanceData = this.attendanceDataLocal.filter(function (data) {
              return data.status == selectedStatus;
            });
            if (selectedSubject == "All") this.attendanceData = _toConsumableArray(this.statusWiseAttendanceData);else this.attendanceData = this.statusWiseAttendanceData.filter(function (data) {
              return data.subjectName == selectedSubject;
            });
          }
        }, {
          key: "getStatusSubjectMonthWiseAttendance",
          value: function getStatusSubjectMonthWiseAttendance(selectedStatus, selectedSubject, selectedMonth) {
            this.attendanceData = [];
            this.statusWiseAttendanceData = [];
            this.subjectWiseAttendanceData = [];
            if (selectedStatus == "Both") this.statusWiseAttendanceData = _toConsumableArray(this.attendanceDataLocal);else this.statusWiseAttendanceData = this.attendanceDataLocal.filter(function (data) {
              return data.status == selectedStatus;
            });
            if (selectedSubject == "All") this.subjectWiseAttendanceData = _toConsumableArray(this.statusWiseAttendanceData);else this.subjectWiseAttendanceData = this.statusWiseAttendanceData.filter(function (data) {
              return data.subjectName == selectedSubject;
            });
            if (selectedMonth == "13") this.attendanceData = _toConsumableArray(this.subjectWiseAttendanceData);else this.attendanceData = this.subjectWiseAttendanceData.filter(function (data) {
              return data.monthOfDate == selectedMonth;
            });
          }
        }, {
          key: "getTopFiveMonthAttendance",
          value: function getTopFiveMonthAttendance() {
            var presentCount = 0;
            var absentCount = 0;
            this.attendanceDataLocal.map(function (data) {
              console.log("attendance status", data.status);
              if (data.status == "Present") presentCount++;else if (data.status == "Absent") absentCount++;
            });
            this.pie = new chart_js__WEBPACK_IMPORTED_MODULE_2__["Chart"](this.pieChart1.nativeElement, {
              type: "pie",
              data: {
                labels: ["Present", "Absent"],
                datasets: [{
                  label: "Attendance Details",
                  data: [presentCount, absentCount],
                  backgroundColor: ["#2dd36f", "#eb445a"]
                }]
              },
              options: {
                tooltips: {
                  enabled: true
                }
              }
            });
          }
        }, {
          key: "doRefresh",
          value: function doRefresh(event) {
            if (this.internetService.networkConnected) this.getAttendanceData();else {
              this.getMoreStudAttendanceData();
              this.getDistinctSubjects();
              this.toastService.createToast("Check internet connection to update details", 3000);
            }
            setTimeout(function () {
              event.target.complete();
            }, 2000);
          }
        }]);

        return AttendancePagePage;
      }();

      AttendancePagePage.ctorParameters = function () {
        return [{
          type: _Services_database_service__WEBPACK_IMPORTED_MODULE_3__["DatabaseService"]
        }, {
          type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_4__["StorageServiceService"]
        }, {
          type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_5__["ApiServiceService"]
        }, {
          type: _Services_attendance_service_service__WEBPACK_IMPORTED_MODULE_6__["AttendanceServiceService"]
        }, {
          type: _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_7__["InternetServiceService"]
        }, {
          type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_8__["ToastServiceService"]
        }];
      };

      AttendancePagePage.propDecorators = {
        pieChart1: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ["pieChart"]
        }]
      };
      AttendancePagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-attendance-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./attendance-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/attendance-page/attendance-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./attendance-page.page.scss */
        "./src/app/pages/attendance-page/attendance-page.page.scss"))["default"]]
      })], AttendancePagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=attendance-page-attendance-page-module-es5.js.map