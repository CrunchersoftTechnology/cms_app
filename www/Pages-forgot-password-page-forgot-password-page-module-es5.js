(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Pages-forgot-password-page-forgot-password-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/forgot-password-page/forgot-password-page.page.html":
    /*!*****************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/forgot-password-page/forgot-password-page.page.html ***!
      \*****************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesForgotPasswordPageForgotPasswordPagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Login</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-item>\n    <ion-input placeholder=\"Enter Password\" [(ngModel)]=\"password\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-input placeholder=\"Re-Enter Password\" [(ngModel)]=\"reEnterPAssword\"></ion-input>\n  </ion-item>\n  <ion-button\n    (click)=\"onCreatePass()\"\n    expand=\"block\"\n    fill=\"solid\"\n    color=\"primary\"\n    [disabled]=\"password == '' || reEnterPAssword == ''\"\n  >\n    Create Password\n  </ion-button>\n  <ion-item>\n    <ion-input placeholder=\"Enter OTP\" [(ngModel)]=\"EnteredOtp\"></ion-input>\n  </ion-item>\n  <ion-button\n    expand=\"block\"\n    fill=\"solid\"\n    color=\"primary\"\n    (click)=\"verifyOtp()\"\n    [disabled]=\"EnteredOtp.length != 4 \"\n  >\n    Verify\n  </ion-button>\n</ion-content> -->\n\n<ion-button (click)=\"onBackClick()\" fill=\"clear\" class=\"back-button\">\n  <ion-icon slot=\"icon-only\" name=\"chevron-back-outline\"></ion-icon>\n</ion-button>\n\n<div class=\"login\">\n  <div heading-column-lr *ngIf=\"!storageService.firstTimeLogin\">\n    <h1 big-heading>Forgot</h1>\n    <!-- <div style=\"height: 5px\"></div> -->\n    <p class=\"sub-line\">Password ?</p>\n    <!-- <h2 small-heading (click)=\"onRegisterClick()\">REGISTER</h2> -->\n    <p class=\"welcome\">Enter new password</p>\n  </div>\n\n  <div heading-column-lr *ngIf=\"storageService.firstTimeLogin\">\n    <h1 big-heading>Set up</h1>\n    <!-- <div style=\"height: 5px\"></div> -->\n    <p class=\"sub-line\">Password</p>\n    <!-- <h2 small-heading (click)=\"onRegisterClick()\">REGISTER</h2> -->\n    <p class=\"welcome\">Enter new password</p>\n  </div>\n\n  <form name=\"loginForm\" #loginForm=\"ngForm\">\n    <ion-item transparent>\n      <!-- <ion-icon icon-small name=\"person\" color=\"primary\"></ion-icon> -->\n      <ion-input\n        name=\"emailId\"\n        [(ngModel)]=\"password\"\n        #emailId=\"ngModel\"\n        required\n        [type]=\"inputType\"\n        placeholder=\"Enter Password\"\n      ></ion-input>\n      <ion-icon\n        [name]=\"iconName\"\n        slot=\"end\"\n        (click)=\"changeInputType()\"\n      ></ion-icon>\n    </ion-item>\n    <!-- <div *ngIf=\"emailId.invalid && (emailId.dirty || emailId.touched)\">\n        <ion-text color=\"danger\" *ngIf=\"emailId.errors.required\"\n          >Enter password</ion-text\n        >\n      </div> -->\n\n    <ion-item transparent>\n      <!-- <ion-icon icon-small name=\"person\" color=\"primary\"></ion-icon> -->\n      <ion-input\n        name=\"emailId\"\n        [(ngModel)]=\"reEnterPAssword\"\n        [type]=\"inputTypeRePass\"\n        placeholder=\"Re-Enter Password\"\n      ></ion-input>\n      <ion-icon\n        [name]=\"iconNameRePass\"\n        slot=\"end\"\n        (click)=\"changeInputTypeRePass()\"\n      ></ion-icon>\n    </ion-item>\n    <!-- <div *ngIf=\"emailId.invalid && (emailId.dirty || emailId.touched)\">\n        <ion-text color=\"danger\" *ngIf=\"emailId.errors.required\"\n          >Enter password</ion-text\n        >\n      </div> -->\n    <div *ngIf=\"showErrorMessage\">\n      <ion-text color=\"danger\">Password doesn’t match</ion-text>\n    </div>\n\n    <!-- <ion-label class=\"forgot-password\" (click)=\"onForgotPass()\"\n      >Forgot Password</ion-label\n    > -->\n    <div style=\"height: 30px\"></div>\n\n    <ion-button\n      color=\"primary\"\n      class=\"ion-button-class\"\n      size=\"large\"\n      expand=\"block\"\n      fill=\"solid\"\n      shape=\"round\"\n      (click)=\"onCreatePass()\"\n      [disabled]=\"emailId.invalid\"\n    >\n      Set new password\n    </ion-button>\n  </form>\n</div>\n\n<ion-label class=\"version\"> Version 1.0 </ion-label>\n";
      /***/
    },

    /***/
    "./src/app/Pages/forgot-password-page/forgot-password-page-routing.module.ts":
    /*!***********************************************************************************!*\
      !*** ./src/app/Pages/forgot-password-page/forgot-password-page-routing.module.ts ***!
      \***********************************************************************************/

    /*! exports provided: ForgotPasswordPagePageRoutingModule */

    /***/
    function srcAppPagesForgotPasswordPageForgotPasswordPageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ForgotPasswordPagePageRoutingModule", function () {
        return ForgotPasswordPagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _forgot_password_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./forgot-password-page.page */
      "./src/app/Pages/forgot-password-page/forgot-password-page.page.ts");

      var routes = [{
        path: '',
        component: _forgot_password_page_page__WEBPACK_IMPORTED_MODULE_3__["ForgotPasswordPagePage"]
      }];

      var ForgotPasswordPagePageRoutingModule = function ForgotPasswordPagePageRoutingModule() {
        _classCallCheck(this, ForgotPasswordPagePageRoutingModule);
      };

      ForgotPasswordPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ForgotPasswordPagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/Pages/forgot-password-page/forgot-password-page.module.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/Pages/forgot-password-page/forgot-password-page.module.ts ***!
      \***************************************************************************/

    /*! exports provided: ForgotPasswordPagePageModule */

    /***/
    function srcAppPagesForgotPasswordPageForgotPasswordPageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ForgotPasswordPagePageModule", function () {
        return ForgotPasswordPagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _forgot_password_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./forgot-password-page-routing.module */
      "./src/app/Pages/forgot-password-page/forgot-password-page-routing.module.ts");
      /* harmony import */


      var _forgot_password_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./forgot-password-page.page */
      "./src/app/Pages/forgot-password-page/forgot-password-page.page.ts");

      var ForgotPasswordPagePageModule = function ForgotPasswordPagePageModule() {
        _classCallCheck(this, ForgotPasswordPagePageModule);
      };

      ForgotPasswordPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _forgot_password_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["ForgotPasswordPagePageRoutingModule"]],
        declarations: [_forgot_password_page_page__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordPagePage"]]
      })], ForgotPasswordPagePageModule);
      /***/
    },

    /***/
    "./src/app/Pages/forgot-password-page/forgot-password-page.page.scss":
    /*!***************************************************************************!*\
      !*** ./src/app/Pages/forgot-password-page/forgot-password-page.page.scss ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesForgotPasswordPageForgotPasswordPagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".login {\n  position: absolute;\n  left: 50%;\n  top: 40%;\n  transform: translate(-49%, -49%);\n  width: 90%;\n}\n\n.registration {\n  position: absolute;\n  top: 2%;\n  left: 5%;\n  width: 90%;\n}\n\n.login-header {\n  background: var(--ion-color-primary);\n}\n\n.logo-image {\n  display: block;\n  width: 120px;\n  margin: 0 auto;\n  text-align: center;\n}\n\n[heading-column-lr] {\n  display: flex;\n  align-items: center;\n  margin-bottom: 30vw;\n}\n\n[big-heading] {\n  font-size: 12vw !important;\n  color: var(--ion-color-dark);\n}\n\n[small-heading] {\n  margin-left: auto;\n  padding-right: 40px;\n  font-size: 15px;\n}\n\n[transparent] {\n  background: transparent;\n}\n\nion-input {\n  font-size: 4.5vw;\n  font-weight: 400;\n}\n\n[icon-small] {\n  width: 30px;\n}\n\nion-item {\n  --highlight-color-focused: transparent;\n  --highlight-height: 0;\n  --highlight-color-invalid: #000;\n  --highlight-color-valid: #ddd;\n  --min-height: 65px;\n  --padding-start: 0;\n}\n\nion-label {\n  color: #000;\n  opacity: 0.8;\n  font-size: 13px;\n  display: block;\n}\n\n.forgot-password {\n  color: var(--ion-color-primary);\n  font-size: 13px;\n  display: block;\n  text-align: right;\n  padding: 10px;\n}\n\n.link-button {\n  color: var(--ion-color-primary);\n}\n\n.demo-login {\n  color: var(--ion-color-primary);\n  margin-left: 80px;\n}\n\n.chbox {\n  margin-right: 5px;\n}\n\n.label {\n  margin-top: 10px;\n  position: absolute;\n  left: 30%;\n}\n\n.label-text {\n  font-size: 5vw;\n  font-weight: bold;\n  text-decoration: underline;\n}\n\n.ion-button-class {\n  margin-top: 6vw;\n  width: 98%;\n  font-size: 4vw;\n}\n\n.welcome {\n  position: absolute;\n  top: 26%;\n  font-size: 4.5vw;\n  color: gray;\n  font-weight: 400;\n}\n\n.version {\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  bottom: 4%;\n  font-size: 4vw;\n  color: gray;\n  font-weight: 300;\n}\n\n.sub-line {\n  margin: 0;\n  font-weight: 500;\n  font-size: 12vw;\n  position: absolute;\n  top: 17%;\n}\n\n.back-button {\n  position: absolute;\n  top: 7%;\n  left: -3%;\n  font-size: 6vw;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvUGFnZXMvZm9yZ290LXBhc3N3b3JkLXBhZ2UvZm9yZ290LXBhc3N3b3JkLXBhZ2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUVBLGdDQUFBO0VBQ0EsVUFBQTtBQUpKOztBQU9BO0VBQ0ksa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7QUFKSjs7QUFPQTtFQUNJLG9DQUFBO0FBSko7O0FBT0E7RUFDSSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQUpKOztBQU9BO0VBQ0ksYUFBQTtFQUVBLG1CQUFBO0VBQ0EsbUJBQUE7QUFKSjs7QUFPQTtFQUNJLDBCQUFBO0VBQ0EsNEJBQUE7QUFKSjs7QUFPQTtFQUNJLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBSko7O0FBT0E7RUFDSSx1QkFBQTtBQUpKOztBQU9BO0VBQ0ksZ0JBQUE7RUFDQSxnQkFBQTtBQUpKOztBQU9BO0VBQ0ksV0FBQTtBQUpKOztBQU9BO0VBQ0ksc0NBQUE7RUFDQSxxQkFBQTtFQUNBLCtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FBSko7O0FBT0E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBSko7O0FBT0E7RUFDSSwrQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0FBSko7O0FBT0E7RUFDSSwrQkFBQTtBQUpKOztBQU9BO0VBQ0ksK0JBQUE7RUFDQSxpQkFBQTtBQUpKOztBQU9BO0VBQ0ksaUJBQUE7QUFKSjs7QUFPQTtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0FBSko7O0FBT0E7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQkFBQTtBQUpKOztBQU9BO0VBQ0ksZUFBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0FBSko7O0FBT0E7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQUpKOztBQVNBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQU5KOztBQVNBO0VBQ0ksU0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtBQU5KOztBQVNBO0VBQ0ksa0JBQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0FBTkoiLCJmaWxlIjoic3JjL2FwcC9QYWdlcy9mb3Jnb3QtcGFzc3dvcmQtcGFnZS9mb3Jnb3QtcGFzc3dvcmQtcGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyAuY2hib3gge1xyXG4vLyAgICAgbWFyZ2luOiAzcHg7XHJcbi8vICAgICBwYWRkaW5nOiAwcHg7XHJcbi8vICB9XHJcblxyXG4ubG9naW4ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgdG9wOiA0MCU7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKC00OSUsIC00OSUpO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTQ5JSwgLTQ5JSk7XHJcbiAgICB3aWR0aDogOTAlXHJcbn1cclxuXHJcbi5yZWdpc3RyYXRpb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAyJTtcclxuICAgIGxlZnQ6IDUlO1xyXG4gICAgd2lkdGg6IDkwJVxyXG59XHJcblxyXG4ubG9naW4taGVhZGVyIHtcclxuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxufVxyXG5cclxuLmxvZ28taW1hZ2Uge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuW2hlYWRpbmctY29sdW1uLWxyXSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHZ3O1xyXG59XHJcblxyXG5bYmlnLWhlYWRpbmddIHtcclxuICAgIGZvbnQtc2l6ZTogMTJ2dyAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYXJrKTtcclxufVxyXG5cclxuW3NtYWxsLWhlYWRpbmddIHtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgcGFkZGluZy1yaWdodDogNDBweDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG5cclxuW3RyYW5zcGFyZW50XSB7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxuaW9uLWlucHV0IHtcclxuICAgIGZvbnQtc2l6ZTogNC41dnc7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG5baWNvbi1zbWFsbF0ge1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbn1cclxuXHJcbmlvbi1pdGVtIHtcclxuICAgIC0taGlnaGxpZ2h0LWNvbG9yLWZvY3VzZWQ6IHRyYW5zcGFyZW50O1xyXG4gICAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwO1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItaW52YWxpZDogIzAwMDtcclxuICAgIC0taGlnaGxpZ2h0LWNvbG9yLXZhbGlkOiAjZGRkO1xyXG4gICAgLS1taW4taGVpZ2h0OiA2NXB4O1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xyXG59XHJcblxyXG5pb24tbGFiZWwge1xyXG4gICAgY29sb3I6ICMwMDA7XHJcbiAgICBvcGFjaXR5OiAuODtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uZm9yZ290LXBhc3N3b3JkIHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgcGFkZGluZzogMTBweFxyXG59XHJcblxyXG4ubGluay1idXR0b24ge1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KVxyXG59XHJcblxyXG4uZGVtby1sb2dpbiB7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDgwcHg7XHJcbn1cclxuXHJcbi5jaGJveCB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxufVxyXG5cclxuLmxhYmVsIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiAzMCU7XHJcbn1cclxuXHJcbi5sYWJlbC10ZXh0IHtcclxuICAgIGZvbnQtc2l6ZTogNXZ3O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxufVxyXG5cclxuLmlvbi1idXR0b24tY2xhc3Mge1xyXG4gICAgbWFyZ2luLXRvcDogNnZ3O1xyXG4gICAgd2lkdGg6IDk4JTtcclxuICAgIGZvbnQtc2l6ZTogNHZ3XHJcbn1cclxuXHJcbi53ZWxjb21lIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMjYlO1xyXG4gICAgZm9udC1zaXplOiA0LjV2dztcclxuICAgIGNvbG9yOiBncmF5O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxufVxyXG5cclxuXHJcblxyXG4udmVyc2lvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIGJvdHRvbTogNCU7XHJcbiAgICBmb250LXNpemU6IDR2dztcclxuICAgIGNvbG9yOiBncmF5O1xyXG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcclxufVxyXG5cclxuLnN1Yi1saW5lIHtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBmb250LXNpemU6IDEydnc7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDE3JTtcclxufVxyXG5cclxuLmJhY2stYnV0dG9uIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNyU7XHJcbiAgICBsZWZ0OiAtMyU7XHJcbiAgICBmb250LXNpemU6IDZ2dztcclxuICAgIG1hcmdpbjogMDtcclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/Pages/forgot-password-page/forgot-password-page.page.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/Pages/forgot-password-page/forgot-password-page.page.ts ***!
      \*************************************************************************/

    /*! exports provided: ForgotPasswordPagePage */

    /***/
    function srcAppPagesForgotPasswordPageForgotPasswordPagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ForgotPasswordPagePage", function () {
        return ForgotPasswordPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _Services_alert_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../../Services/alert-service.service */
      "./src/app/Services/alert-service.service.ts");
      /* harmony import */


      var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../../Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");
      /* harmony import */


      var _Services_otp_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./../../Services/otp-service.service */
      "./src/app/Services/otp-service.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./../../Services/api-service.service */
      "./src/app/Services/api-service.service.ts");
      /* harmony import */


      var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./../../Services/toast-service.service */
      "./src/app/Services/toast-service.service.ts");
      /* harmony import */


      var _Services_user_details_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./../../Services/user-details.service */
      "./src/app/Services/user-details.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var ForgotPasswordPagePage = /*#__PURE__*/function () {
        function ForgotPasswordPagePage(router, navController, apiService, alertService, storageService, otpService, toastService, userDetailsService) {
          _classCallCheck(this, ForgotPasswordPagePage);

          this.router = router;
          this.navController = navController;
          this.apiService = apiService;
          this.alertService = alertService;
          this.storageService = storageService;
          this.otpService = otpService;
          this.toastService = toastService;
          this.userDetailsService = userDetailsService;
          this.password = "";
          this.reEnterPAssword = "";
          this.EnteredOtp = "";
          this.inputType = "password";
          this.iconName = "eye";
          this.showErrorMessage = false;
          this.inputTypeRePass = "password";
          this.iconNameRePass = "eye";
        }

        _createClass(ForgotPasswordPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "onCreatePass",
          value: function onCreatePass() {
            if (this.password == this.reEnterPAssword) {
              // this.randomNumber = Math.floor(1000 + Math.random() * 9000);
              // console.log(this.randomNumber);
              // // console.log(this.number);
              // // console.log(this.storageService.userInfo.StudentContact);
              // this.otpService.sendOtp(
              //   this.storageService.userInfo.StudentContact,
              //   this.randomNumber
              // );
              this.router.navigateByUrl("/otp-page");
              this.showErrorMessage = false;
              this.storageService.password = this.password;
              this.storageService.forgotPassword = true;
            } else {
              this.showErrorMessage = true;
            }
          }
        }, {
          key: "changeInputType",
          value: function changeInputType() {
            if (this.inputType == "password") {
              this.inputType = "text";
              this.iconName = "eye-off";
            } else {
              this.inputType = "password";
              this.iconName = "eye";
            }
          }
        }, {
          key: "changeInputTypeRePass",
          value: function changeInputTypeRePass() {
            if (this.inputTypeRePass == "password") {
              this.inputTypeRePass = "text";
              this.iconNameRePass = "eye-off";
            } else {
              this.inputTypeRePass = "password";
              this.iconNameRePass = "eye";
            }
          }
        }, {
          key: "onBackClick",
          value: function onBackClick() {
            this.navController.back();
          }
        }]);

        return ForgotPasswordPagePage;
      }();

      ForgotPasswordPagePage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["NavController"]
        }, {
          type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_6__["ApiServiceService"]
        }, {
          type: _Services_alert_service_service__WEBPACK_IMPORTED_MODULE_2__["AlertServiceService"]
        }, {
          type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__["StorageServiceService"]
        }, {
          type: _Services_otp_service_service__WEBPACK_IMPORTED_MODULE_4__["OtpServiceService"]
        }, {
          type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_7__["ToastServiceService"]
        }, {
          type: _Services_user_details_service__WEBPACK_IMPORTED_MODULE_8__["UserDetailsService"]
        }];
      };

      ForgotPasswordPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-forgot-password-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./forgot-password-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/forgot-password-page/forgot-password-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./forgot-password-page.page.scss */
        "./src/app/Pages/forgot-password-page/forgot-password-page.page.scss"))["default"]]
      })], ForgotPasswordPagePage);
      /***/
    },

    /***/
    "./src/app/Services/toast-service.service.ts":
    /*!***************************************************!*\
      !*** ./src/app/Services/toast-service.service.ts ***!
      \***************************************************/

    /*! exports provided: ToastServiceService */

    /***/
    function srcAppServicesToastServiceServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ToastServiceService", function () {
        return ToastServiceService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var ToastServiceService = /*#__PURE__*/function () {
        function ToastServiceService(toastController) {
          _classCallCheck(this, ToastServiceService);

          this.toastController = toastController;
        }

        _createClass(ToastServiceService, [{
          key: "createToast",
          value: function createToast(message, duration) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        animated: true,
                        duration: duration,
                        position: "bottom",
                        message: message
                      });

                    case 2:
                      toast = _context.sent;
                      _context.next = 5;
                      return toast.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return ToastServiceService;
      }();

      ToastServiceService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      ToastServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ToastServiceService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=Pages-forgot-password-page-forgot-password-page-module-es5.js.map