(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["offline-result-page-offline-result-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/offline-result-page/offline-result-page.page.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/offline-result-page/offline-result-page.page.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar\n  color=\"primary\"\n  mode=\"ios\"\n  *ngIf=\"storageService.instituteName.length > 0\"\n>\n  <ion-title>{{storageService.instituteName}}</ion-title>\n</ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons class=\"menuButton\" slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Offline Result</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content\n      pullingIcon=\"chevron-down-circle-outline\"\n      pullingText=\"Pull to refresh\"\n      refreshingSpinner=\"circles\"\n      refreshingText=\"Refreshing...\"\n    >\n    </ion-refresher-content>\n  </ion-refresher>\n  <table id=\"tblfeesDetails\">\n    <td colspan=\"5\" color=\"primary\" class=\"tableName\">Offline Test Result</td>\n    <tr>\n      <th>Title</th>\n      <th>Date</th>\n      <th>Timing</th>\n      <th>Marks</th>\n      <th>%</th>\n    </tr>\n    <tr *ngFor=\"let result of offLineResult\">\n      <td>{{result.Title}}</td>\n      <td>{{result.TestDate}}</td>\n      <td>{{result.Timing}}</td>\n      <td *ngIf=\"result.Status == 'True'\">\n        {{result.ObtainedMarks+'/'+result.TotalMarks}}\n      </td>\n      <td *ngIf=\"result.Status == 'True'\">{{result.Percentage}}</td>\n      <td *ngIf=\"result.Status == 'False'\" colspan=\"2\">Absent</td>\n    </tr>\n  </table>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/Services/toast-service.service.ts":
/*!***************************************************!*\
  !*** ./src/app/Services/toast-service.service.ts ***!
  \***************************************************/
/*! exports provided: ToastServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastServiceService", function() { return ToastServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let ToastServiceService = class ToastServiceService {
    constructor(toastController) {
        this.toastController = toastController;
    }
    createToast(message, duration) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let toast = yield this.toastController.create({
                animated: true,
                duration: duration,
                position: "bottom",
                message: message
            });
            yield toast.present();
        });
    }
};
ToastServiceService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
ToastServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ToastServiceService);



/***/ }),

/***/ "./src/app/pages/offline-result-page/offline-result-page-routing.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/offline-result-page/offline-result-page-routing.module.ts ***!
  \*********************************************************************************/
/*! exports provided: OfflineResultPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfflineResultPagePageRoutingModule", function() { return OfflineResultPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _offline_result_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./offline-result-page.page */ "./src/app/pages/offline-result-page/offline-result-page.page.ts");




const routes = [
    {
        path: '',
        component: _offline_result_page_page__WEBPACK_IMPORTED_MODULE_3__["OfflineResultPagePage"]
    }
];
let OfflineResultPagePageRoutingModule = class OfflineResultPagePageRoutingModule {
};
OfflineResultPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OfflineResultPagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/offline-result-page/offline-result-page.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/offline-result-page/offline-result-page.module.ts ***!
  \*************************************************************************/
/*! exports provided: OfflineResultPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfflineResultPagePageModule", function() { return OfflineResultPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _offline_result_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./offline-result-page-routing.module */ "./src/app/pages/offline-result-page/offline-result-page-routing.module.ts");
/* harmony import */ var _offline_result_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./offline-result-page.page */ "./src/app/pages/offline-result-page/offline-result-page.page.ts");







let OfflineResultPagePageModule = class OfflineResultPagePageModule {
};
OfflineResultPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _offline_result_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["OfflineResultPagePageRoutingModule"]
        ],
        declarations: [_offline_result_page_page__WEBPACK_IMPORTED_MODULE_6__["OfflineResultPagePage"]]
    })
], OfflineResultPagePageModule);



/***/ }),

/***/ "./src/app/pages/offline-result-page/offline-result-page.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/offline-result-page/offline-result-page.page.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#tblfeesDetails th,\ntd {\n  text-align: center;\n  padding: 4px;\n  border: 1px solid #ccc;\n}\n\n#tblfeesDetails {\n  height: auto;\n  padding: 1px;\n  margin-top: 5px;\n  overflow: scroll;\n  border: 1px solid #ccc;\n  font-size: 4vw;\n  width: 100%;\n}\n\n#tblfeesDetails th {\n  border: 1px solid #ccc;\n}\n\n.tableName {\n  color: white;\n  background: #3880ff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb2ZmbGluZS1yZXN1bHQtcGFnZS9vZmZsaW5lLXJlc3VsdC1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7RUFFRSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtBQUNGOztBQUVBO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxzQkFBQTtBQUVGOztBQUNBO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0FBRUoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9vZmZsaW5lLXJlc3VsdC1wYWdlL29mZmxpbmUtcmVzdWx0LXBhZ2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI3RibGZlZXNEZXRhaWxzIHRoLFxyXG50ZCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmc6IDRweDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG59XHJcblxyXG4jdGJsZmVlc0RldGFpbHMge1xyXG4gIGhlaWdodDogYXV0bztcclxuICBwYWRkaW5nOiAxcHg7XHJcbiAgbWFyZ2luLXRvcDogNXB4O1xyXG4gIG92ZXJmbG93OiBzY3JvbGw7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICBmb250LXNpemU6IDR2dztcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4jdGJsZmVlc0RldGFpbHMgdGgge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbn1cclxuXHJcbi50YWJsZU5hbWUge1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYmFja2dyb3VuZDogIzM4ODBmZjtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/offline-result-page/offline-result-page.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/offline-result-page/offline-result-page.page.ts ***!
  \***********************************************************************/
/*! exports provided: OfflineResultPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfflineResultPagePage", function() { return OfflineResultPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _Services_offline_result_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../Services/offline-result-service.service */ "./src/app/Services/offline-result-service.service.ts");
/* harmony import */ var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");
/* harmony import */ var _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../Services/internet-service.service */ "./src/app/Services/internet-service.service.ts");
/* harmony import */ var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../Services/toast-service.service */ "./src/app/Services/toast-service.service.ts");
/* harmony import */ var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../Services/api-service.service */ "./src/app/Services/api-service.service.ts");







let OfflineResultPagePage = class OfflineResultPagePage {
    constructor(offlineResult, storageService, internetService, toastService, apiService) {
        this.offlineResult = offlineResult;
        this.storageService = storageService;
        this.internetService = internetService;
        this.toastService = toastService;
        this.apiService = apiService;
        this.userId = "";
        this.sid = "";
        this.offLineResult = [];
    }
    ngOnInit() {
        this.userId = this.storageService.userDetails.userId;
        this.sid = this.storageService.userDetails.sId;
        if (this.internetService.networkConnected)
            this.getResult();
        else {
            this.getStudentResult();
            this.toastService.createToast("Check internet connection to update details", 3000);
        }
    }
    getResult() {
        this.offLineResult = [];
        this.apiService.getResult(this.userId).then((result) => {
            let data = JSON.parse(result.data);
            // this.offLineResult = data.d;
            data.d.map((result) => {
                if (result.Category == "Offline")
                    this.offLineResult.push(result);
            });
            console.log("offlineResult", this.offLineResult);
            let length = data.d.length;
            if (length > 0) {
                let dataLength = 0;
                this.offlineResult.deleteFromCMSResults().then((_) => {
                    for (let i = 0; i < data.d.length; i++) {
                        let ObtainedMarks = data.d[i].ObtainedMarks;
                        let TotalMarks = data.d[i].TotalMarks;
                        let Timing = data.d[i].Timing;
                        let Duration = data.d[i].Duration;
                        let Title = data.d[i].Title;
                        let Percentage = data.d[i].Percentage;
                        let TestDate = data.d[i].TestDate;
                        let Category = data.d[i].Category;
                        let RStatus = data.d[i].Status;
                        dataLength = i + 1;
                        this.offlineResult.insertIntoCMSResults(this.sid, ObtainedMarks, TotalMarks, Timing, Duration, Title, Percentage, TestDate, Category, RStatus);
                    }
                    if (dataLength == data.d.length)
                        this.getStudentResult();
                });
            }
            else if (length == 0)
                this.offlineResult
                    .deleteFromCMSResults()
                    .then((_) => this.getStudentResult());
        }, (err) => this.getStudentResult());
    }
    getStudentResult() { }
    doRefresh(event) {
        if (this.internetService.networkConnected)
            this.getResult();
        else {
            this.getStudentResult();
            this.toastService.createToast("Check internet connection to update details", 3000);
        }
        setTimeout(() => {
            event.target.complete();
        }, 2000);
    }
};
OfflineResultPagePage.ctorParameters = () => [
    { type: _Services_offline_result_service_service__WEBPACK_IMPORTED_MODULE_2__["OfflineResultServiceService"] },
    { type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__["StorageServiceService"] },
    { type: _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_4__["InternetServiceService"] },
    { type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_5__["ToastServiceService"] },
    { type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_6__["ApiServiceService"] }
];
OfflineResultPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-offline-result-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./offline-result-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/offline-result-page/offline-result-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./offline-result-page.page.scss */ "./src/app/pages/offline-result-page/offline-result-page.page.scss")).default]
    })
], OfflineResultPagePage);



/***/ })

}]);
//# sourceMappingURL=offline-result-page-offline-result-page-module-es2015.js.map