(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Umesh-umesh-page-umesh-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/Umesh/umesh-page/umesh-page.page.html":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Umesh/umesh-page/umesh-page.page.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUmeshUmeshPageUmeshPagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Umesh-page</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/Umesh/umesh-page/umesh-page-routing.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/Umesh/umesh-page/umesh-page-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: UmeshPagePageRoutingModule */

    /***/
    function srcAppUmeshUmeshPageUmeshPageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UmeshPagePageRoutingModule", function () {
        return UmeshPagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _umesh_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./umesh-page.page */
      "./src/app/Umesh/umesh-page/umesh-page.page.ts");

      var routes = [{
        path: '',
        component: _umesh_page_page__WEBPACK_IMPORTED_MODULE_3__["UmeshPagePage"]
      }];

      var UmeshPagePageRoutingModule = function UmeshPagePageRoutingModule() {
        _classCallCheck(this, UmeshPagePageRoutingModule);
      };

      UmeshPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], UmeshPagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/Umesh/umesh-page/umesh-page.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/Umesh/umesh-page/umesh-page.module.ts ***!
      \*******************************************************/

    /*! exports provided: UmeshPagePageModule */

    /***/
    function srcAppUmeshUmeshPageUmeshPageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UmeshPagePageModule", function () {
        return UmeshPagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _umesh_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./umesh-page-routing.module */
      "./src/app/Umesh/umesh-page/umesh-page-routing.module.ts");
      /* harmony import */


      var _umesh_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./umesh-page.page */
      "./src/app/Umesh/umesh-page/umesh-page.page.ts");

      var UmeshPagePageModule = function UmeshPagePageModule() {
        _classCallCheck(this, UmeshPagePageModule);
      };

      UmeshPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _umesh_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["UmeshPagePageRoutingModule"]],
        declarations: [_umesh_page_page__WEBPACK_IMPORTED_MODULE_6__["UmeshPagePage"]]
      })], UmeshPagePageModule);
      /***/
    },

    /***/
    "./src/app/Umesh/umesh-page/umesh-page.page.scss":
    /*!*******************************************************!*\
      !*** ./src/app/Umesh/umesh-page/umesh-page.page.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUmeshUmeshPageUmeshPagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL1VtZXNoL3VtZXNoLXBhZ2UvdW1lc2gtcGFnZS5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/Umesh/umesh-page/umesh-page.page.ts":
    /*!*****************************************************!*\
      !*** ./src/app/Umesh/umesh-page/umesh-page.page.ts ***!
      \*****************************************************/

    /*! exports provided: UmeshPagePage */

    /***/
    function srcAppUmeshUmeshPageUmeshPagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UmeshPagePage", function () {
        return UmeshPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var UmeshPagePage = /*#__PURE__*/function () {
        function UmeshPagePage() {
          _classCallCheck(this, UmeshPagePage);
        }

        _createClass(UmeshPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return UmeshPagePage;
      }();

      UmeshPagePage.ctorParameters = function () {
        return [];
      };

      UmeshPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-umesh-page',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./umesh-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/Umesh/umesh-page/umesh-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./umesh-page.page.scss */
        "./src/app/Umesh/umesh-page/umesh-page.page.scss"))["default"]]
      })], UmeshPagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=Umesh-umesh-page-umesh-page-module-es5.js.map