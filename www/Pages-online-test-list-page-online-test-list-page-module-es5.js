(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Pages-online-test-list-page-online-test-list-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/online-test-list-page/online-test-list-page.page.html":
    /*!*******************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/online-test-list-page/online-test-list-page.page.html ***!
      \*******************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesOnlineTestListPageOnlineTestListPagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar\n  color=\"primary\"\n  mode=\"ios\"\n  *ngIf=\"storageService.instituteName.length > 0\"\n>\n  <ion-title>{{storageService.instituteName}}</ion-title>\n</ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <!-- <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons> -->\n    <ion-icon\n      style=\"padding-left: 10px; font-size: 6vw\"\n      name=\"arrow-back\"\n      slot=\"start\"\n      (click)=\"onBackClick()\"\n    ></ion-icon>\n    <ion-title>Online Tests</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content\n      pullingIcon=\"chevron-down-circle-outline\"\n      pullingText=\"Pull to refresh\"\n      refreshingSpinner=\"circles\"\n      refreshingText=\"Refreshing...\"\n    >\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-card\n    *ngFor=\"let notification of notifications\"\n    class=\"ion-padding\"\n    (click)=\"onTestClick(notification)\"\n  >\n    <ion-card-title>{{notification.Title}}</ion-card-title>\n    <ion-card-content>\n      <ion-item lines=\"none\" class=\"ion-no-margin ion-no-padding\">\n        <ion-label>{{notification.TestDate}}</ion-label> <br />\n        <ion-label>{{notification.StartTime}}</ion-label>\n        <ion-icon\n          name=\"checkmark-circle\"\n          slot=\"end\"\n          [color]=\"notification.Status == 'Unread' ? 'danger' : 'success'\"\n        ></ion-icon>\n      </ion-item>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/Pages/online-test-list-page/online-test-list-page-routing.module.ts":
    /*!*************************************************************************************!*\
      !*** ./src/app/Pages/online-test-list-page/online-test-list-page-routing.module.ts ***!
      \*************************************************************************************/

    /*! exports provided: OnlineTestListPagePageRoutingModule */

    /***/
    function srcAppPagesOnlineTestListPageOnlineTestListPageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OnlineTestListPagePageRoutingModule", function () {
        return OnlineTestListPagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _online_test_list_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./online-test-list-page.page */
      "./src/app/Pages/online-test-list-page/online-test-list-page.page.ts");

      var routes = [{
        path: '',
        component: _online_test_list_page_page__WEBPACK_IMPORTED_MODULE_3__["OnlineTestListPagePage"]
      }];

      var OnlineTestListPagePageRoutingModule = function OnlineTestListPagePageRoutingModule() {
        _classCallCheck(this, OnlineTestListPagePageRoutingModule);
      };

      OnlineTestListPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], OnlineTestListPagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/Pages/online-test-list-page/online-test-list-page.module.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/Pages/online-test-list-page/online-test-list-page.module.ts ***!
      \*****************************************************************************/

    /*! exports provided: OnlineTestListPagePageModule */

    /***/
    function srcAppPagesOnlineTestListPageOnlineTestListPageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OnlineTestListPagePageModule", function () {
        return OnlineTestListPagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _online_test_list_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./online-test-list-page-routing.module */
      "./src/app/Pages/online-test-list-page/online-test-list-page-routing.module.ts");
      /* harmony import */


      var _online_test_list_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./online-test-list-page.page */
      "./src/app/Pages/online-test-list-page/online-test-list-page.page.ts");
      /* harmony import */


      var _Components_test_details_component_test_details_component_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./../../Components/test-details-component/test-details-component.component */
      "./src/app/Components/test-details-component/test-details-component.component.ts");

      var OnlineTestListPagePageModule = function OnlineTestListPagePageModule() {
        _classCallCheck(this, OnlineTestListPagePageModule);
      };

      OnlineTestListPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _online_test_list_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["OnlineTestListPagePageRoutingModule"]],
        declarations: [_online_test_list_page_page__WEBPACK_IMPORTED_MODULE_6__["OnlineTestListPagePage"], _Components_test_details_component_test_details_component_component__WEBPACK_IMPORTED_MODULE_7__["TestDetailsComponentComponent"]]
      })], OnlineTestListPagePageModule);
      /***/
    },

    /***/
    "./src/app/Pages/online-test-list-page/online-test-list-page.page.scss":
    /*!*****************************************************************************!*\
      !*** ./src/app/Pages/online-test-list-page/online-test-list-page.page.scss ***!
      \*****************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesOnlineTestListPageOnlineTestListPagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL1BhZ2VzL29ubGluZS10ZXN0LWxpc3QtcGFnZS9vbmxpbmUtdGVzdC1saXN0LXBhZ2UucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/Pages/online-test-list-page/online-test-list-page.page.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/Pages/online-test-list-page/online-test-list-page.page.ts ***!
      \***************************************************************************/

    /*! exports provided: OnlineTestListPagePage */

    /***/
    function srcAppPagesOnlineTestListPageOnlineTestListPagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OnlineTestListPagePage", function () {
        return OnlineTestListPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/Services/notification-service.service */
      "./src/app/Services/notification-service.service.ts");
      /* harmony import */


      var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../../Services/api-service.service */
      "./src/app/Services/api-service.service.ts");
      /* harmony import */


      var _Services_loading_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./../../Services/loading-service.service */
      "./src/app/Services/loading-service.service.ts");
      /* harmony import */


      var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./../../Services/toast-service.service */
      "./src/app/Services/toast-service.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _Components_test_details_component_test_details_component_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./../../Components/test-details-component/test-details-component.component */
      "./src/app/Components/test-details-component/test-details-component.component.ts");
      /* harmony import */


      var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./../../Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");

      var OnlineTestListPagePage = /*#__PURE__*/function () {
        function OnlineTestListPagePage(notificationService, apiService, loadingService, toastService, storageService, loadingController, popoverController, navController, storage) {
          _classCallCheck(this, OnlineTestListPagePage);

          this.notificationService = notificationService;
          this.apiService = apiService;
          this.loadingService = loadingService;
          this.toastService = toastService;
          this.storageService = storageService;
          this.loadingController = loadingController;
          this.popoverController = popoverController;
          this.navController = navController;
          this.storage = storage;
          this.notifications = [];
        }

        _createClass(OnlineTestListPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.getNotifications();
          }
        }, {
          key: "getNotifications",
          value: function getNotifications() {
            var _this = this;

            this.notificationService.getNotifications().then(function (data) {
              _this.notifications = data;
              console.log("notifications", _this.notifications);
            });
          }
        }, {
          key: "onTestClick",
          value: function onTestClick(notifications) {
            var _this2 = this;

            // console.log(notifications);
            if (notifications.Status == "Unread") {
              if (notifications.TestObj == "TestObj") {
                this.loadingService.createLoading("Downloading test").then(function (_) {
                  console.log("in if");

                  _this2.apiService.getTestObj(notifications.TestPaperId).then(function (result) {
                    // console.log(result.data);
                    var testObj = result.data;
                    _this2.notifications[_this2.notifications.indexOf(notifications)].TestObj = testObj;

                    _this2.notificationService.updateOnlineTestNotifications(testObj, notifications.ID).then(function (_) {
                      _this2.loadingController.dismiss().then(function (_) {
                        return _this2.createPopover(notifications);
                      });
                    });
                  }, function (err) {
                    _this2.loadingController.dismiss().then(function (_) {
                      return _this2.toastService.createToast("Error while downloading test", 3000);
                    });
                  });
                });
              } else {
                this.createPopover(notifications);
              }

              this.GetCMReceivedTestNotification(notifications.ArrengeTestId, this.storageService.userDetails.userId, this.storageService.userDetails.clientId, "received");
            } else this.toastService.createToast("Test already given", 3000);
          }
        }, {
          key: "GetCMReceivedTestNotification",
          value: function GetCMReceivedTestNotification(arrangeTestId, userId, clientId, recivedCode) {
            this.apiService.GetCMReceivedTestNotification(arrangeTestId, userId, clientId, recivedCode).then(function (data) {
              return console.log("recivedTestApi", JSON.parse(data.data).d);
            });
          }
        }, {
          key: "createPopover",
          value: function createPopover(notification) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var popover;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.popoverController.create({
                        component: _Components_test_details_component_test_details_component_component__WEBPACK_IMPORTED_MODULE_7__["TestDetailsComponentComponent"],
                        animated: true,
                        cssClass: "myPopOver1",
                        componentProps: {
                          notification: notification
                        }
                      });

                    case 2:
                      popover = _context.sent;
                      _context.next = 5;
                      return popover.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "doRefresh",
          value: function doRefresh(event) {
            var _this3 = this;

            // this.notificationService.getMaxArrengeTestId().then((data) => {
            this.storage.get("maxTestPaperId").then(function (data) {
              console.log("maxTestPaperId", data);

              _this3.apiService.getCMSLatestOnlineTestRecived(_this3.storageService.userDetails.userId, _this3.storageService.userDetails.clientId, data).then(function (data) {
                var recivedTests = [];
                recivedTests = JSON.parse(data.data).d;
                console.log("revivedTests", recivedTests);

                if (recivedTests.length > 0) {
                  var maxPaperId = recivedTests[recivedTests.length - 1].ArrengeTestId;

                  _this3.storage.set("maxTestPaperId", maxPaperId);

                  recivedTests.map(function (recivedTest) {
                    var Message = recivedTest.Message;
                    var title = Message.split("$^$")[0];
                    var TestType = Message.split("$^$")[1].split("TestType:")[1];
                    var CetCorrect = Message.split("$^$")[2].split("CetCorrect:")[1];
                    var CetInCorrect = Message.split("$^$")[3].split("CetInCorrect:")[1];
                    var NeetCorrect = Message.split("$^$")[4].split("NeetCorrect:")[1];
                    var NeetInCorrect = Message.split("$^$")[5].split("NeetInCorrect:")[1];
                    var JeeCorrect = Message.split("$^$")[6].split("JeeCorrect:")[1];
                    var JeeInCorrect = Message.split("$^$")[7].split("JeeInCorrect:")[1];
                    var JeeNewCorrect = Message.split("$^$")[8].split("JeeNewCorrect:")[1];
                    var JeeNewInCorrect = Message.split("$^$")[9].split("JeeNewInCorrect:")[1];
                    var testDate = Message.split("$^$")[10].split("Date:")[1];
                    var EndDate = Message.split("$^$")[11].split("EndDate:")[1];
                    var startTime = Message.split("$^$")[12].split("Start Time:")[1];
                    var endTime = Message.split("$^$")[13].split("End Time:")[1];
                    var duration = Message.split("$^$")[14].split("Duration:")[1];
                    var testPaperId = Message.split("$^$")[15].split("TestPaperId:")[1];
                    var testDetails;
                    testDetails = {
                      CetCorrect: CetCorrect,
                      CetInCorrect: CetInCorrect,
                      EndDate: EndDate,
                      JeeCorrect: JeeCorrect,
                      JeeInCorrect: JeeInCorrect,
                      JeeNewCorrect: JeeNewCorrect,
                      JeeNewInCorrect: JeeNewInCorrect,
                      NeetCorrect: NeetCorrect,
                      NeetInCorrect: NeetInCorrect,
                      TestType: TestType,
                      arrengeTestId: recivedTest.ArrengeTestId,
                      duration: duration,
                      endTime: endTime,
                      sid: _this3.storageService.userDetails.sId,
                      startTime: startTime,
                      status: "",
                      testDate: testDate,
                      testPaperId: testPaperId,
                      title: title
                    };

                    _this3.notificationService.insertIntoOnlineTestNotifications(testDetails, null).then(function (_) {
                      _this3.notificationService.getNotifications().then(function (data) {
                        _this3.notifications = data;
                        console.log("notifications", _this3.notifications);
                        event.target.complete();
                      });
                    });
                  });
                } else {
                  event.target.complete();

                  _this3.toastService.createToast("You recived all your tests", 3000);
                }
              });
            }); // event.target.complete()
          }
        }, {
          key: "onBackClick",
          value: function onBackClick() {
            this.navController.back();
          }
        }]);

        return OnlineTestListPagePage;
      }();

      OnlineTestListPagePage.ctorParameters = function () {
        return [{
          type: src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_2__["NotificationServiceService"]
        }, {
          type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_3__["ApiServiceService"]
        }, {
          type: _Services_loading_service_service__WEBPACK_IMPORTED_MODULE_4__["LoadingServiceService"]
        }, {
          type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_5__["ToastServiceService"]
        }, {
          type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_8__["StorageServiceService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["PopoverController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["Storage"]
        }];
      };

      OnlineTestListPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-online-test-list-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./online-test-list-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/online-test-list-page/online-test-list-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./online-test-list-page.page.scss */
        "./src/app/Pages/online-test-list-page/online-test-list-page.page.scss"))["default"]]
      })], OnlineTestListPagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=Pages-online-test-list-page-online-test-list-page-module-es5.js.map