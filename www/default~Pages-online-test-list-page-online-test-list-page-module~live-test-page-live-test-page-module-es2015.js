(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~Pages-online-test-list-page-online-test-list-page-module~live-test-page-live-test-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/Components/test-details-component/test-details-component.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Components/test-details-component/test-details-component.component.html ***!
  \*******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <ion-item>\n    <ion-label> Test title </ion-label>\n    <ion-label slot=\"end\" class=\"ion-text-end ion-text-wrap\">{{\n      notification.Title\n    }}</ion-label>\n  </ion-item>\n  <ion-item>\n    <ion-label> Test Date </ion-label>\n    <ion-label slot=\"end\" class=\"ion-text-end ion-text-wrap\">{{\n      notification.TestDate\n    }}</ion-label>\n  </ion-item>\n  <ion-item>\n    <ion-label> Start Time </ion-label>\n    <ion-label slot=\"end\" class=\"ion-text-end ion-text-wrap\">{{\n      notification.StartTime\n    }}</ion-label>\n  </ion-item>\n  <ion-item *ngIf=\"showEndDate\">\n    <ion-label> End Date </ion-label>\n    <ion-label slot=\"end\" class=\"ion-text-end ion-text-wrap\">{{\n      notification.EndDate\n    }}</ion-label>\n  </ion-item>\n  <ion-item *ngIf=\"showEndTime\">\n    <ion-label> End Time </ion-label>\n    <ion-label slot=\"end\" class=\"ion-text-end ion-text-wrap\">{{\n      notification.EndTime\n    }}</ion-label>\n  </ion-item>\n  <ion-item>\n    <ion-label> Time Duration </ion-label>\n    <ion-label slot=\"end\" class=\"ion-text-end ion-text-wrap\"\n      >{{ notification.Duration }} Minutes</ion-label\n    >\n  </ion-item>\n  <ion-button\n    expand=\"block\"\n    fill=\"solid\"\n    shape=\"round\"\n    color=\"primary\"\n    [disabled]=\"notification.Status == 'read'\"\n    (click)=\"startTest()\"\n  >\n    Start test\n  </ion-button>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/Components/test-details-component/test-details-component.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/Components/test-details-component/test-details-component.component.scss ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL0NvbXBvbmVudHMvdGVzdC1kZXRhaWxzLWNvbXBvbmVudC90ZXN0LWRldGFpbHMtY29tcG9uZW50LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/Components/test-details-component/test-details-component.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/Components/test-details-component/test-details-component.component.ts ***!
  \***************************************************************************************/
/*! exports provided: TestDetailsComponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestDetailsComponentComponent", function() { return TestDetailsComponentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/notification-service.service */ "./src/app/Services/notification-service.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");
/* harmony import */ var src_app_Services_toast_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/Services/toast-service.service */ "./src/app/Services/toast-service.service.ts");







let TestDetailsComponentComponent = class TestDetailsComponentComponent {
    constructor(router, popoverController, notificationService, toastService, storageService) {
        this.router = router;
        this.popoverController = popoverController;
        this.notificationService = notificationService;
        this.toastService = toastService;
        this.storageService = storageService;
        this.showEndDate = false;
        this.showEndTime = false;
    }
    ngOnInit() {
        console.log("in component", this.notification);
        this.checkEndDate(this.notification.EndDate, this.notification.EndTime);
    }
    checkEndDate(testEndDate, endTime) {
        var endDateSplit = testEndDate.split("-");
        var endTimeSplit = endTime.split(":");
        var dayEndDate = endDateSplit[0];
        var monthEndDate = endDateSplit[1];
        var yearEndDate = endDateSplit[2];
        var hourEndTime = endTimeSplit[0];
        var minutEndTime = endTimeSplit[1];
        var endMinutSplit = minutEndTime.split(" ");
        var finalEndMinut = endMinutSplit[0];
        var secEndTime = "00";
        var finalEndDate = yearEndDate +
            "/" +
            monthEndDate +
            "/" +
            dayEndDate +
            " " +
            hourEndTime +
            ":" +
            finalEndMinut +
            ":" +
            secEndTime;
        var testEndDateTime = Math.round(new Date(finalEndDate).getTime() / 1000);
        var currentDateTime = Math.round(+new Date().getTime() / 1000);
        var checkDate = Math.round(new Date("2020/11/09 07:18:00").getTime() / 1000);
        if (checkDate == testEndDateTime) {
            this.showEndDate = false;
            this.showEndTime = false;
        }
        else {
            this.showEndDate = true;
            this.showEndTime = true;
        }
    }
    startTest() {
        var data = this.notification.TestObj;
        var testStartDate = this.notification.TestDate;
        var testEndDate = this.notification.EndDate;
        var startTime = this.notification.StartTime;
        var endTime = this.notification.EndTime;
        var startDateSplit = testStartDate.split("-");
        var startTimeSplit = startTime.split(":");
        var dayStartDate = startDateSplit[0];
        var monthStartDate = startDateSplit[1];
        var yearStartDate = startDateSplit[2];
        var hourStartTime = startTimeSplit[0];
        var minutStartTime = startTimeSplit[1];
        var startMinutSplit = minutStartTime.split(" ");
        var finalStartMinut = startMinutSplit[0];
        var startAMPM = startMinutSplit[1];
        var secStartTime = "00";
        var endDateSplit = testEndDate.split("-");
        var endTimeSplit = endTime.split(":");
        var dayEndDate = endDateSplit[0].split(" ").join("");
        var monthEndDate = endDateSplit[1].split(" ").join("");
        var yearEndDate = endDateSplit[2].split(" ").join("");
        var hourEndTime = endTimeSplit[0].split(" ").join("");
        var minutEndTime = endTimeSplit[1];
        var endMinutSplit = minutEndTime.split(" ");
        var finalEndMinut = endMinutSplit[0].split(" ").join("");
        var endAMPM = endMinutSplit[1].split(" ").join("");
        var secEndTime = "00";
        var finalStartDate = yearStartDate +
            "/" +
            monthStartDate +
            "/" +
            dayStartDate +
            " " +
            this.convertTime12to24(hourStartTime + ":" + finalStartMinut + " " + startAMPM);
        var finalEndDate = yearEndDate +
            "/" +
            monthEndDate +
            "/" +
            dayEndDate +
            " " +
            this.convertTime12to24(hourEndTime + ":" + finalEndMinut + " " + endAMPM);
        var finalEndDate1 = yearEndDate +
            "/" +
            monthEndDate +
            "/" +
            dayEndDate +
            " " +
            hourEndTime +
            ":" +
            finalEndMinut +
            ":" +
            "00";
        var testStartDateTime = new Date(finalStartDate).getTime();
        var testEndDateTime = new Date(finalEndDate).getTime();
        var testEndDateTime1 = new Date(finalEndDate1).getTime();
        // console.log(new Date(finalStartDate));
        // console.log(new Date(finalEndDate));
        var curDays = new Date().getDate();
        var curMonths = new Date().getMonth() + 1;
        var curYesrs = new Date().getFullYear();
        var curHourss = new Date().getHours();
        var curMini = new Date().getMinutes();
        var curSecc = new Date().getSeconds();
        var finalCurr = curYesrs +
            "/" +
            curMonths +
            "/" +
            curDays +
            " " +
            curHourss +
            ":" +
            curMini +
            ":" +
            curSecc;
        var currentDateTime = new Date(finalCurr).getTime();
        // console.log(new Date(finalCurr));
        var checkDate = new Date("2020/11/09 07:18:00").getTime();
        var ID = this.notification.ID;
        if (checkDate == testEndDateTime1) {
            // console.log(data);
            var da = JSON.parse(data);
            // console.log(data);
            // console.log(da.CurrentDateTime);
            var examDate = this.notification.TestDate;
            var ExamDateFormat = examDate.split("-")[2] +
                "-" +
                examDate.split("-")[1] +
                "-" +
                examDate.split("-")[0];
            var result = da.CurrentDateTime;
            var todayDateTime = new Date();
            var serverDate = result.split("T")[0];
            var serverTime = result.split("T")[1].substring(0, 5);
            var month = todayDateTime.getMonth() + 1;
            if (month.toString().length == 1)
                month = "0" + month;
            var tDate = todayDateTime.getDate().toString();
            if (tDate.length == 1)
                tDate = "0" + tDate;
            var todayDate = todayDateTime.getFullYear() + "-" + month + "-" + tDate;
            if (todayDate == serverDate) {
                try {
                    var todayDateFormat = new Date(todayDateTime.getFullYear(), parseInt(month) - 1, todayDateTime.getDate());
                    var examDateFormatDate = new Date(examDate.split("-")[2], parseInt(examDate.split("-")[1]) - 1, examDate.split("-")[0]);
                }
                catch (err) { }
                if (ExamDateFormat == todayDate) {
                    var hours = todayDateTime.getHours();
                    var minutes = todayDateTime.getMinutes();
                    var ampm = hours >= 12 ? "PM" : "AM";
                    hours = hours % 12;
                    hours = hours ? hours : 12;
                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    var strTime = hours + ":" + minutes + " " + ampm;
                    var monthNames = [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December",
                    ];
                    var datedetails = monthNames[todayDateTime.getMonth()] +
                        " " +
                        todayDateTime.getDate() +
                        ", " +
                        todayDateTime.getFullYear() +
                        " ";
                    var stt = new Date(datedetails + strTime);
                    stt = stt.getTime();
                    var endt = new Date(datedetails + this.notification.StartTime);
                    endt = endt.getTime();
                    var beforefiveminutes = endt;
                    var afterfiveminutes = endt + 1800000;
                    var diff = (endt - stt) / 1000 / 60 / 60;
                    if (stt >= beforefiveminutes && stt <= afterfiveminutes) {
                        this.notificationService.startTest(ID, "read").then((_) => {
                            this.storageService.testExpired = "false";
                            this.popoverController.dismiss().then((_) => {
                                this.storageService.notification = this.notification;
                                this.storageService.page = "unit-test";
                                this.router.navigateByUrl("/online-test-page");
                            });
                        });
                    }
                    else if (stt < beforefiveminutes) {
                        this.toastService.createToast("You are early for the test.", 3000);
                    }
                    else if (stt > afterfiveminutes) {
                        this.notificationService.startTest(ID, "expired").then((_) => {
                            this.storageService.testExpired = "true";
                            this.popoverController.dismiss().then((_) => {
                                this.toastService.createToast("Sorry your test time is out.", 3000);
                            });
                        });
                    }
                }
                else if (examDateFormatDate > todayDateFormat) {
                    this.toastService.createToast("You are early for the test.", 3000);
                }
                else if (examDateFormatDate < todayDateFormat) {
                    this.notificationService.startTest(ID, "expired").then((_) => {
                        this.storageService.testExpired = "true";
                        this.popoverController.dismiss().then((_) => {
                            this.toastService.createToast("Sorry your test time is out.", 3000);
                        });
                    });
                }
            }
            else {
                this.toastService.createToast("Device date is incorrect.", 3000);
            }
        }
        else {
            if (currentDateTime < testStartDateTime) {
                this.toastService.createToast("please Start Test On Time", 3000);
            }
            else if (currentDateTime <= testEndDateTime &&
                currentDateTime >= testStartDateTime) {
                this.notificationService.startTest(ID, "read").then((_) => {
                    this.storageService.testExpired = "false";
                    this.popoverController.dismiss().then((_) => {
                        this.storageService.notification = this.notification;
                        this.storageService.page = "unit-test";
                        this.router.navigateByUrl("/online-test-page");
                    });
                });
            }
            else if (currentDateTime > testEndDateTime) {
                this.notificationService.startTest(ID, "expired").then((_) => {
                    this.storageService.testExpired = "true";
                    this.popoverController.dismiss().then((_) => {
                        this.toastService.createToast("Sorry your test time is out.", 3000);
                    });
                });
            }
        }
    }
    convertTime12to24(time12h) {
        const [time, modifier] = time12h.split(" ");
        let [hours, minutes] = time.split(":");
        if (hours === "12") {
            hours = "00";
        }
        if (modifier === "PM") {
            hours = parseInt(hours, 10) + 12;
        }
        return `${hours}:${minutes}`;
    }
};
TestDetailsComponentComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["PopoverController"] },
    { type: src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_2__["NotificationServiceService"] },
    { type: src_app_Services_toast_service_service__WEBPACK_IMPORTED_MODULE_6__["ToastServiceService"] },
    { type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_5__["StorageServiceService"] }
];
TestDetailsComponentComponent.propDecorators = {
    notification: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }]
};
TestDetailsComponentComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-test-details-component",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./test-details-component.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/Components/test-details-component/test-details-component.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./test-details-component.component.scss */ "./src/app/Components/test-details-component/test-details-component.component.scss")).default]
    })
], TestDetailsComponentComponent);



/***/ }),

/***/ "./src/app/Services/toast-service.service.ts":
/*!***************************************************!*\
  !*** ./src/app/Services/toast-service.service.ts ***!
  \***************************************************/
/*! exports provided: ToastServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastServiceService", function() { return ToastServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let ToastServiceService = class ToastServiceService {
    constructor(toastController) {
        this.toastController = toastController;
    }
    createToast(message, duration) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let toast = yield this.toastController.create({
                animated: true,
                duration: duration,
                position: "bottom",
                message: message
            });
            yield toast.present();
        });
    }
};
ToastServiceService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
ToastServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ToastServiceService);



/***/ })

}]);
//# sourceMappingURL=default~Pages-online-test-list-page-online-test-list-page-module~live-test-page-live-test-page-module-es2015.js.map