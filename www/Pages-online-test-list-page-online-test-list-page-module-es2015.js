(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Pages-online-test-list-page-online-test-list-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/online-test-list-page/online-test-list-page.page.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/online-test-list-page/online-test-list-page.page.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar\n  color=\"primary\"\n  mode=\"ios\"\n  *ngIf=\"storageService.instituteName.length > 0\"\n>\n  <ion-title>{{storageService.instituteName}}</ion-title>\n</ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <!-- <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons> -->\n    <ion-icon\n      style=\"padding-left: 10px; font-size: 6vw\"\n      name=\"arrow-back\"\n      slot=\"start\"\n      (click)=\"onBackClick()\"\n    ></ion-icon>\n    <ion-title>Online Tests</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content\n      pullingIcon=\"chevron-down-circle-outline\"\n      pullingText=\"Pull to refresh\"\n      refreshingSpinner=\"circles\"\n      refreshingText=\"Refreshing...\"\n    >\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-card\n    *ngFor=\"let notification of notifications\"\n    class=\"ion-padding\"\n    (click)=\"onTestClick(notification)\"\n  >\n    <ion-card-title>{{notification.Title}}</ion-card-title>\n    <ion-card-content>\n      <ion-item lines=\"none\" class=\"ion-no-margin ion-no-padding\">\n        <ion-label>{{notification.TestDate}}</ion-label> <br />\n        <ion-label>{{notification.StartTime}}</ion-label>\n        <ion-icon\n          name=\"checkmark-circle\"\n          slot=\"end\"\n          [color]=\"notification.Status == 'Unread' ? 'danger' : 'success'\"\n        ></ion-icon>\n      </ion-item>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/Pages/online-test-list-page/online-test-list-page-routing.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/Pages/online-test-list-page/online-test-list-page-routing.module.ts ***!
  \*************************************************************************************/
/*! exports provided: OnlineTestListPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnlineTestListPagePageRoutingModule", function() { return OnlineTestListPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _online_test_list_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./online-test-list-page.page */ "./src/app/Pages/online-test-list-page/online-test-list-page.page.ts");




const routes = [
    {
        path: '',
        component: _online_test_list_page_page__WEBPACK_IMPORTED_MODULE_3__["OnlineTestListPagePage"]
    }
];
let OnlineTestListPagePageRoutingModule = class OnlineTestListPagePageRoutingModule {
};
OnlineTestListPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OnlineTestListPagePageRoutingModule);



/***/ }),

/***/ "./src/app/Pages/online-test-list-page/online-test-list-page.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/Pages/online-test-list-page/online-test-list-page.module.ts ***!
  \*****************************************************************************/
/*! exports provided: OnlineTestListPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnlineTestListPagePageModule", function() { return OnlineTestListPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _online_test_list_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./online-test-list-page-routing.module */ "./src/app/Pages/online-test-list-page/online-test-list-page-routing.module.ts");
/* harmony import */ var _online_test_list_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./online-test-list-page.page */ "./src/app/Pages/online-test-list-page/online-test-list-page.page.ts");
/* harmony import */ var _Components_test_details_component_test_details_component_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../../Components/test-details-component/test-details-component.component */ "./src/app/Components/test-details-component/test-details-component.component.ts");








let OnlineTestListPagePageModule = class OnlineTestListPagePageModule {
};
OnlineTestListPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _online_test_list_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["OnlineTestListPagePageRoutingModule"]
        ],
        declarations: [_online_test_list_page_page__WEBPACK_IMPORTED_MODULE_6__["OnlineTestListPagePage"], _Components_test_details_component_test_details_component_component__WEBPACK_IMPORTED_MODULE_7__["TestDetailsComponentComponent"]]
    })
], OnlineTestListPagePageModule);



/***/ }),

/***/ "./src/app/Pages/online-test-list-page/online-test-list-page.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/Pages/online-test-list-page/online-test-list-page.page.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL1BhZ2VzL29ubGluZS10ZXN0LWxpc3QtcGFnZS9vbmxpbmUtdGVzdC1saXN0LXBhZ2UucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/Pages/online-test-list-page/online-test-list-page.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/Pages/online-test-list-page/online-test-list-page.page.ts ***!
  \***************************************************************************/
/*! exports provided: OnlineTestListPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnlineTestListPagePage", function() { return OnlineTestListPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/notification-service.service */ "./src/app/Services/notification-service.service.ts");
/* harmony import */ var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../Services/api-service.service */ "./src/app/Services/api-service.service.ts");
/* harmony import */ var _Services_loading_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../Services/loading-service.service */ "./src/app/Services/loading-service.service.ts");
/* harmony import */ var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../Services/toast-service.service */ "./src/app/Services/toast-service.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _Components_test_details_component_test_details_component_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../../Components/test-details-component/test-details-component.component */ "./src/app/Components/test-details-component/test-details-component.component.ts");
/* harmony import */ var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./../../Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");










let OnlineTestListPagePage = class OnlineTestListPagePage {
    constructor(notificationService, apiService, loadingService, toastService, storageService, loadingController, popoverController, navController, storage) {
        this.notificationService = notificationService;
        this.apiService = apiService;
        this.loadingService = loadingService;
        this.toastService = toastService;
        this.storageService = storageService;
        this.loadingController = loadingController;
        this.popoverController = popoverController;
        this.navController = navController;
        this.storage = storage;
        this.notifications = [];
    }
    ngOnInit() { }
    ionViewDidEnter() {
        this.getNotifications();
    }
    getNotifications() {
        this.notificationService.getNotifications().then((data) => {
            this.notifications = data;
            console.log("notifications", this.notifications);
        });
    }
    onTestClick(notifications) {
        // console.log(notifications);
        if (notifications.Status == "Unread") {
            if (notifications.TestObj == "TestObj") {
                this.loadingService.createLoading("Downloading test").then((_) => {
                    console.log("in if");
                    this.apiService.getTestObj(notifications.TestPaperId).then((result) => {
                        // console.log(result.data);
                        let testObj = result.data;
                        this.notifications[this.notifications.indexOf(notifications)].TestObj = testObj;
                        this.notificationService
                            .updateOnlineTestNotifications(testObj, notifications.ID)
                            .then((_) => {
                            this.loadingController
                                .dismiss()
                                .then((_) => this.createPopover(notifications));
                        });
                    }, (err) => {
                        this.loadingController
                            .dismiss()
                            .then((_) => this.toastService.createToast("Error while downloading test", 3000));
                    });
                });
            }
            else {
                this.createPopover(notifications);
            }
            this.GetCMReceivedTestNotification(notifications.ArrengeTestId, this.storageService.userDetails.userId, this.storageService.userDetails.clientId, "received");
        }
        else
            this.toastService.createToast("Test already given", 3000);
    }
    GetCMReceivedTestNotification(arrangeTestId, userId, clientId, recivedCode) {
        this.apiService
            .GetCMReceivedTestNotification(arrangeTestId, userId, clientId, recivedCode)
            .then((data) => console.log("recivedTestApi", JSON.parse(data.data).d));
    }
    createPopover(notification) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // console.log(notification)
            const popover = yield this.popoverController.create({
                component: _Components_test_details_component_test_details_component_component__WEBPACK_IMPORTED_MODULE_7__["TestDetailsComponentComponent"],
                animated: true,
                cssClass: "myPopOver1",
                componentProps: { notification: notification },
            });
            yield popover.present();
        });
    }
    doRefresh(event) {
        // this.notificationService.getMaxArrengeTestId().then((data) => {
        this.storage.get("maxTestPaperId").then((data) => {
            console.log("maxTestPaperId", data);
            this.apiService
                .getCMSLatestOnlineTestRecived(this.storageService.userDetails.userId, this.storageService.userDetails.clientId, data)
                .then((data) => {
                let recivedTests = [];
                recivedTests = JSON.parse(data.data).d;
                console.log("revivedTests", recivedTests);
                if (recivedTests.length > 0) {
                    let maxPaperId = recivedTests[recivedTests.length - 1].ArrengeTestId;
                    this.storage.set("maxTestPaperId", maxPaperId);
                    recivedTests.map((recivedTest) => {
                        var Message = recivedTest.Message;
                        var title = Message.split("$^$")[0];
                        var TestType = Message.split("$^$")[1].split("TestType:")[1];
                        var CetCorrect = Message.split("$^$")[2].split("CetCorrect:")[1];
                        var CetInCorrect = Message.split("$^$")[3].split("CetInCorrect:")[1];
                        var NeetCorrect = Message.split("$^$")[4].split("NeetCorrect:")[1];
                        var NeetInCorrect = Message.split("$^$")[5].split("NeetInCorrect:")[1];
                        var JeeCorrect = Message.split("$^$")[6].split("JeeCorrect:")[1];
                        var JeeInCorrect = Message.split("$^$")[7].split("JeeInCorrect:")[1];
                        var JeeNewCorrect = Message.split("$^$")[8].split("JeeNewCorrect:")[1];
                        var JeeNewInCorrect = Message.split("$^$")[9].split("JeeNewInCorrect:")[1];
                        var testDate = Message.split("$^$")[10].split("Date:")[1];
                        var EndDate = Message.split("$^$")[11].split("EndDate:")[1];
                        var startTime = Message.split("$^$")[12].split("Start Time:")[1];
                        var endTime = Message.split("$^$")[13].split("End Time:")[1];
                        var duration = Message.split("$^$")[14].split("Duration:")[1];
                        var testPaperId = Message.split("$^$")[15].split("TestPaperId:")[1];
                        let testDetails;
                        testDetails = {
                            CetCorrect: CetCorrect,
                            CetInCorrect: CetInCorrect,
                            EndDate: EndDate,
                            JeeCorrect: JeeCorrect,
                            JeeInCorrect: JeeInCorrect,
                            JeeNewCorrect: JeeNewCorrect,
                            JeeNewInCorrect: JeeNewInCorrect,
                            NeetCorrect: NeetCorrect,
                            NeetInCorrect: NeetInCorrect,
                            TestType: TestType,
                            arrengeTestId: recivedTest.ArrengeTestId,
                            duration: duration,
                            endTime: endTime,
                            sid: this.storageService.userDetails.sId,
                            startTime: startTime,
                            status: "",
                            testDate: testDate,
                            testPaperId: testPaperId,
                            title: title,
                        };
                        this.notificationService
                            .insertIntoOnlineTestNotifications(testDetails, null)
                            .then((_) => {
                            this.notificationService.getNotifications().then((data) => {
                                this.notifications = data;
                                console.log("notifications", this.notifications);
                                event.target.complete();
                            });
                        });
                    });
                }
                else {
                    event.target.complete();
                    this.toastService.createToast("You recived all your tests", 3000);
                }
            });
        });
        // event.target.complete()
    }
    onBackClick() {
        this.navController.back();
    }
};
OnlineTestListPagePage.ctorParameters = () => [
    { type: src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_2__["NotificationServiceService"] },
    { type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_3__["ApiServiceService"] },
    { type: _Services_loading_service_service__WEBPACK_IMPORTED_MODULE_4__["LoadingServiceService"] },
    { type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_5__["ToastServiceService"] },
    { type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_8__["StorageServiceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["Storage"] }
];
OnlineTestListPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-online-test-list-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./online-test-list-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/online-test-list-page/online-test-list-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./online-test-list-page.page.scss */ "./src/app/Pages/online-test-list-page/online-test-list-page.page.scss")).default]
    })
], OnlineTestListPagePage);



/***/ })

}]);
//# sourceMappingURL=Pages-online-test-list-page-online-test-list-page-module-es2015.js.map