(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["about-us-page-about-us-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/about-us-page/about-us-page.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/about-us-page/about-us-page.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <!-- <ion-toolbar\n  color=\"primary\"\n  mode=\"ios\"\n  *ngIf=\"storageService.instituteName.length > 0\"\n>\n  <ion-title>{{storageService.instituteName}}</ion-title>\n</ion-toolbar> -->\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title (click)=\"onTitleClick()\">{{details.name}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-item >\n    <ion-label id=\"about-us\" class=\"ion-text-wrap ion-text-justify\"></ion-label>\n  </ion-item>\n\n  <ion-label>{{details.address}}</ion-label>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/about-us-page/about-us-page-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/about-us-page/about-us-page-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: AboutUsPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutUsPagePageRoutingModule", function() { return AboutUsPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _about_us_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./about-us-page.page */ "./src/app/pages/about-us-page/about-us-page.page.ts");




const routes = [
    {
        path: '',
        component: _about_us_page_page__WEBPACK_IMPORTED_MODULE_3__["AboutUsPagePage"]
    }
];
let AboutUsPagePageRoutingModule = class AboutUsPagePageRoutingModule {
};
AboutUsPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AboutUsPagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/about-us-page/about-us-page.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/about-us-page/about-us-page.module.ts ***!
  \*************************************************************/
/*! exports provided: AboutUsPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutUsPagePageModule", function() { return AboutUsPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _about_us_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./about-us-page-routing.module */ "./src/app/pages/about-us-page/about-us-page-routing.module.ts");
/* harmony import */ var _about_us_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./about-us-page.page */ "./src/app/pages/about-us-page/about-us-page.page.ts");







let AboutUsPagePageModule = class AboutUsPagePageModule {
};
AboutUsPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _about_us_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["AboutUsPagePageRoutingModule"]
        ],
        declarations: [_about_us_page_page__WEBPACK_IMPORTED_MODULE_6__["AboutUsPagePage"]]
    })
], AboutUsPagePageModule);



/***/ }),

/***/ "./src/app/pages/about-us-page/about-us-page.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/about-us-page/about-us-page.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Fib3V0LXVzLXBhZ2UvYWJvdXQtdXMtcGFnZS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/about-us-page/about-us-page.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/about-us-page/about-us-page.page.ts ***!
  \***********************************************************/
/*! exports provided: AboutUsPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutUsPagePage", function() { return AboutUsPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../Services/api-service.service */ "./src/app/Services/api-service.service.ts");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var src_app_Services_storage_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");





let AboutUsPagePage = class AboutUsPagePage {
    constructor(inAppBrowser, apiService, storageService) {
        this.inAppBrowser = inAppBrowser;
        this.apiService = apiService;
        this.storageService = storageService;
        this.details = {
            ClientId: 0,
            aboutus: "",
            address: "",
            email_id: "",
            name: "",
            websiteURL: "",
        };
    }
    ngOnInit() { }
    ionViewDidEnter() {
        this.apiService.getAboutUsDetails().then((data) => {
            // console.log(JSON.parse(data.data));
            this.details = JSON.parse(data.data).d[0];
            console.log("aboutus", this.details);
            document.getElementById("about-us").innerHTML = this.details.aboutus;
        });
    }
    onTitleClick() {
        if (this.details.websiteURL != null || this.details.websiteURL != "")
            this.inAppBrowser.create(this.details.websiteURL, "_self", {
                location: "no",
            });
    }
};
AboutUsPagePage.ctorParameters = () => [
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__["InAppBrowser"] },
    { type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_2__["ApiServiceService"] },
    { type: src_app_Services_storage_service_service__WEBPACK_IMPORTED_MODULE_4__["StorageServiceService"] }
];
AboutUsPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-about-us-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./about-us-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/about-us-page/about-us-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./about-us-page.page.scss */ "./src/app/pages/about-us-page/about-us-page.page.scss")).default]
    })
], AboutUsPagePage);



/***/ })

}]);
//# sourceMappingURL=about-us-page-about-us-page-module-es2015.js.map