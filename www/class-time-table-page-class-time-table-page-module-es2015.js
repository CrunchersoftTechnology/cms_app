(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["class-time-table-page-class-time-table-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/class-time-table-page/class-time-table-page.page.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/class-time-table-page/class-time-table-page.page.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar\n    color=\"primary\"\n    mode=\"ios\"\n    *ngIf=\"storageService.instituteName.length > 0\"\n  >\n    <ion-title>{{storageService.instituteName}}</ion-title>\n  </ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons class=\"menuButton\" slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title\n      >{{category == '1' ?'Expert Lecture TimeTable': 'Exam TimeTable'}}</ion-title\n    >\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content\n      pullingIcon=\"chevron-down-circle-outline\"\n      pullingText=\"Pull to refresh\"\n      refreshingSpinner=\"circles\"\n      refreshingText=\"Refreshing...\"\n    >\n    </ion-refresher-content>\n  </ion-refresher>\n  <app-calender-component\n    *ngIf=\"eventSource.length > 0; else elseBlock\"\n    [eventSource]=\"eventSource\"\n    (onEventClicked)=\"handleEventClicked($event)\"\n  ></app-calender-component>\n\n  <ng-template class=\"ion-no-margin ion-no-padding\" #elseBlock>\n    <ion-row>\n      <ion-col size=\"2\">\n        <ion-button fill=\"clear\" (click)=\"back()\">\n          <ion-icon name=\"arrow-back\" slot=\"icon-only\"></ion-icon>\n        </ion-button>\n      </ion-col>\n\n      <ion-col size=\"8\" class=\"ion-text-center\">\n        <h5>{{ viewTitle }}</h5>\n      </ion-col>\n\n      <ion-col size=\"2\">\n        <ion-button fill=\"clear\" (click)=\"next()\">\n          <ion-icon name=\"arrow-forward\" slot=\"icon-only\"></ion-icon>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n\n    <calendar\n      [eventSource]=\"eventSource\"\n      [calendarMode]=\"calendar.mode\"\n      [currentDate]=\"calendar.currentDate\"\n      (onTitleChanged)=\"onViewTitleChanged($event)\"\n      startHour=\"6\"\n      endHour=\"20\"\n      step=\"30\"\n      startingDayWeek=\"1\"\n    >\n    </calendar>\n  </ng-template>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/Services/toast-service.service.ts":
/*!***************************************************!*\
  !*** ./src/app/Services/toast-service.service.ts ***!
  \***************************************************/
/*! exports provided: ToastServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastServiceService", function() { return ToastServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let ToastServiceService = class ToastServiceService {
    constructor(toastController) {
        this.toastController = toastController;
    }
    createToast(message, duration) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let toast = yield this.toastController.create({
                animated: true,
                duration: duration,
                position: "bottom",
                message: message
            });
            yield toast.present();
        });
    }
};
ToastServiceService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
ToastServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ToastServiceService);



/***/ }),

/***/ "./src/app/pages/class-time-table-page/class-time-table-page-routing.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/class-time-table-page/class-time-table-page-routing.module.ts ***!
  \*************************************************************************************/
/*! exports provided: ClassTimeTablePagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassTimeTablePagePageRoutingModule", function() { return ClassTimeTablePagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _class_time_table_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./class-time-table-page.page */ "./src/app/pages/class-time-table-page/class-time-table-page.page.ts");




const routes = [
    {
        path: '',
        component: _class_time_table_page_page__WEBPACK_IMPORTED_MODULE_3__["ClassTimeTablePagePage"]
    }
];
let ClassTimeTablePagePageRoutingModule = class ClassTimeTablePagePageRoutingModule {
};
ClassTimeTablePagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ClassTimeTablePagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/class-time-table-page/class-time-table-page.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/class-time-table-page/class-time-table-page.module.ts ***!
  \*****************************************************************************/
/*! exports provided: ClassTimeTablePagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassTimeTablePagePageModule", function() { return ClassTimeTablePagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _Components_calender_component_calender_component_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../Components/calender-component/calender-component.module */ "./src/app/Components/calender-component/calender-component.module.ts");
/* harmony import */ var _class_time_table_page_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./class-time-table-page-routing.module */ "./src/app/pages/class-time-table-page/class-time-table-page-routing.module.ts");
/* harmony import */ var _class_time_table_page_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./class-time-table-page.page */ "./src/app/pages/class-time-table-page/class-time-table-page.page.ts");
/* harmony import */ var ionic2_calendar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ionic2-calendar */ "./node_modules/ionic2-calendar/__ivy_ngcc__/fesm2015/ionic2-calendar.js");









let ClassTimeTablePagePageModule = class ClassTimeTablePagePageModule {
};
ClassTimeTablePagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _Components_calender_component_calender_component_module__WEBPACK_IMPORTED_MODULE_5__["CalenderComponentModule"],
            ionic2_calendar__WEBPACK_IMPORTED_MODULE_8__["NgCalendarModule"],
            _class_time_table_page_routing_module__WEBPACK_IMPORTED_MODULE_6__["ClassTimeTablePagePageRoutingModule"]
        ],
        declarations: [_class_time_table_page_page__WEBPACK_IMPORTED_MODULE_7__["ClassTimeTablePagePage"]],
    })
], ClassTimeTablePagePageModule);



/***/ }),

/***/ "./src/app/pages/class-time-table-page/class-time-table-page.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/class-time-table-page/class-time-table-page.page.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ::ng-deep .monthview-eventdetail-timecolumn {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY2xhc3MtdGltZS10YWJsZS1wYWdlL2NsYXNzLXRpbWUtdGFibGUtcGFnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxhQUFBO0FBQVIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9jbGFzcy10aW1lLXRhYmxlLXBhZ2UvY2xhc3MtdGltZS10YWJsZS1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IDo6bmctZGVlcCB7XHJcbiAgICAubW9udGh2aWV3LWV2ZW50ZGV0YWlsLXRpbWVjb2x1bW4ge1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/class-time-table-page/class-time-table-page.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/class-time-table-page/class-time-table-page.page.ts ***!
  \***************************************************************************/
/*! exports provided: ClassTimeTablePagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassTimeTablePagePage", function() { return ClassTimeTablePagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../Services/internet-service.service */ "./src/app/Services/internet-service.service.ts");
/* harmony import */ var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");
/* harmony import */ var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../Services/api-service.service */ "./src/app/Services/api-service.service.ts");
/* harmony import */ var _Services_time_table_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../Services/time-table-service.service */ "./src/app/Services/time-table-service.service.ts");
/* harmony import */ var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../Services/toast-service.service */ "./src/app/Services/toast-service.service.ts");
/* harmony import */ var _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/photo-viewer/ngx */ "./node_modules/@ionic-native/photo-viewer/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_native_preview_any_file_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/preview-any-file/ngx */ "./node_modules/@ionic-native/preview-any-file/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var ionic2_calendar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ionic2-calendar */ "./node_modules/ionic2-calendar/__ivy_ngcc__/fesm2015/ionic2-calendar.js");












let ClassTimeTablePagePage = class ClassTimeTablePagePage {
    constructor(photoViewer, previewAnyFile, activatedRoute, internetService, storageService, apiService, timeTableService, toastService, inAppBrowser) {
        this.photoViewer = photoViewer;
        this.previewAnyFile = previewAnyFile;
        this.activatedRoute = activatedRoute;
        this.internetService = internetService;
        this.storageService = storageService;
        this.apiService = apiService;
        this.timeTableService = timeTableService;
        this.toastService = toastService;
        this.inAppBrowser = inAppBrowser;
        this.fileInfo = [];
        this.fileInfoDatabase = [];
        this.eventSource = [];
        this.userId = "";
        this.branchId = "";
        this.classId = "";
        this.batchId = "";
        this.category = "";
        this.calendar = {
            mode: "month",
            currentDate: new Date(),
        };
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.category = this.activatedRoute.snapshot.params["id"];
        console.log("category", this.category);
        this.eventSource = [];
        this.userId = this.storageService.userDetails.userId;
        this.branchId = this.storageService.userDetails.branchId;
        this.classId = this.storageService.userDetails.classId;
        this.batchId = this.storageService.userDetails.batchId;
        if (this.internetService.networkConnected) {
            this.getTimeTable(this.userId, this.branchId, this.classId, this.batchId);
        }
        else {
            this.toastService.createToast("Check internet connection to update details", 3000);
            this.showDatePickerWithClassTTDate();
        }
    }
    getTimeTable(userId, branchId, classId, batchId) {
        this.apiService.getTimeTable(branchId, classId, batchId).then((result) => {
            let data = JSON.parse(result.data);
            let length = data.d.length;
            if (length > 0) {
                this.timeTableService.deleteFromCMSTimeTable().then((_) => {
                    this.fileInfo = data.d;
                    console.log("file info", this.fileInfo);
                    this.fileInfo.map((file) => {
                        this.timeTableService.insertIntoCMSTimeTable(file.Description, file.Date, file.FileName, file.Category, file.AttachmentDescription);
                    });
                    this.showDatePickerWithClassTTDate();
                });
            }
            else {
                this.timeTableService.deleteFromCMSTimeTable().then((_) => {
                    this.showDatePickerWithClassTTDate();
                });
            }
        }, (err) => {
            this.showDatePickerWithClassTTDate();
        });
    }
    showDatePickerWithClassTTDate() {
        this.eventSource = [];
        this.timeTableService.selectFromCMSTimeTable(this.category).then((data) => {
            this.fileInfoDatabase = data;
            console.log("this.fileInfoDatabase", this.fileInfoDatabase);
            this.fileInfoDatabase.map((fileInfo) => {
                let selectedDate = fileInfo.Date.split(" ")[0];
                let date = Number.parseInt(selectedDate.split("-")[0]);
                let month = Number.parseInt(selectedDate.split("-")[1]);
                let year = Number.parseInt(selectedDate.split("-")[2]);
                let startTime = new Date(year, month - 1, date + 1);
                let endTime = new Date(year, month - 1, date + 1);
                this.eventSource.push({
                    title: `${fileInfo.Description} - ${fileInfo.AttachmentDescription}`,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true,
                    desc: fileInfo.FileName,
                });
            });
        });
    }
    handleEventClicked(event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(event);
            if (this.internetService.networkConnected) {
                if (event.desc.split(".")[1] == "jpeg" ||
                    event.desc.split(".")[1] == "jpg" ||
                    event.desc.split(".")[1] == "gif" ||
                    event.desc.split(".")[1] == "png")
                    this.photoViewer.show(`${this.apiService.fileUrl}PDF/StudentTimeTableFile/${event.desc}`);
                else
                    // this.previewAnyFile.preview(
                    //   `${this.apiService.fileUrl}PDF/StudentTimeTableFile/${event.desc}`
                    // );
                    this.inAppBrowser.create(`https://docs.google.com/viewer?url=${this.apiService.fileUrl}PDF/StudentTimeTableFile/${event.desc}&embedded=true`, "_self", { location: "no" });
            }
            else
                this.toastService.createToast("Check your internet connection to proceed", 3000);
        });
    }
    doRefresh(event) {
        if (this.internetService.networkConnected) {
            this.getTimeTable(this.userId, this.branchId, this.classId, this.batchId);
        }
        else {
            this.toastService.createToast("Check internet connection to update details", 3000);
            this.showDatePickerWithClassTTDate();
        }
        setTimeout(() => {
            event.target.complete();
        }, 2000);
    }
    next() {
        this.myCal.slideNext();
    }
    back() {
        this.myCal.slidePrev();
    }
    // Selected date reange and hence title changed
    onViewTitleChanged(title) {
        this.viewTitle = title;
    }
};
ClassTimeTablePagePage.ctorParameters = () => [
    { type: _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_7__["PhotoViewer"] },
    { type: _ionic_native_preview_any_file_ngx__WEBPACK_IMPORTED_MODULE_9__["PreviewAnyFile"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"] },
    { type: _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_2__["InternetServiceService"] },
    { type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__["StorageServiceService"] },
    { type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_4__["ApiServiceService"] },
    { type: _Services_time_table_service_service__WEBPACK_IMPORTED_MODULE_5__["TimeTableServiceService"] },
    { type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_6__["ToastServiceService"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_10__["InAppBrowser"] }
];
ClassTimeTablePagePage.propDecorators = {
    myCal: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: [ionic2_calendar__WEBPACK_IMPORTED_MODULE_11__["CalendarComponent"],] }]
};
ClassTimeTablePagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-class-time-table-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./class-time-table-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/class-time-table-page/class-time-table-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./class-time-table-page.page.scss */ "./src/app/pages/class-time-table-page/class-time-table-page.page.scss")).default]
    })
], ClassTimeTablePagePage);



/***/ })

}]);
//# sourceMappingURL=class-time-table-page-class-time-table-page-module-es2015.js.map