(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Pages-pdf-list-page-pdf-list-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/pdf-list-page/pdf-list-page.page.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/pdf-list-page/pdf-list-page.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesPdfListPagePdfListPagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar\n  color=\"primary\"\n  mode=\"ios\"\n  *ngIf=\"storageService.instituteName.length > 0\"\n>\n  <ion-title>{{storageService.instituteName}}</ion-title>\n</ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-icon\n    style=\"padding-left: 10px; font-size: 6vw\"\n    name=\"arrow-back\"\n    slot=\"start\"\n    (click)=\"onBackClick()\"\n  ></ion-icon>\n    <ion-title>{{type}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content\n      pullingIcon=\"chevron-down-circle-outline\"\n      pullingText=\"Pull to refresh\"\n      refreshingSpinner=\"circles\"\n      refreshingText=\"Refreshing...\"\n    >\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-item>\n    <ion-select\n      [(ngModel)]=\"selectedSubject\"\n      interface=\"popover\"\n      (ionChange)=\"onSelectChange()\"\n    >\n      <ion-select-option\n        *ngFor=\"let sub of subjectDetails\"\n        [value]=\"sub.subjectId\"\n        >{{sub.subjectName}}</ion-select-option\n      >\n    </ion-select>\n  </ion-item>\n\n  <ion-card\n    class=\"ion-margin ion-padding\"\n    *ngFor=\"let file of fileInfo\"\n    (click)=\"onFileClick(file.FileName)\"\n  >\n    <ion-card-subtitle> {{file.FileName}} </ion-card-subtitle>\n  </ion-card>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/Pages/pdf-list-page/pdf-list-page-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/Pages/pdf-list-page/pdf-list-page-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: PdfListPagePageRoutingModule */

    /***/
    function srcAppPagesPdfListPagePdfListPageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PdfListPagePageRoutingModule", function () {
        return PdfListPagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _pdf_list_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./pdf-list-page.page */
      "./src/app/Pages/pdf-list-page/pdf-list-page.page.ts");

      var routes = [{
        path: '',
        component: _pdf_list_page_page__WEBPACK_IMPORTED_MODULE_3__["PdfListPagePage"]
      }];

      var PdfListPagePageRoutingModule = function PdfListPagePageRoutingModule() {
        _classCallCheck(this, PdfListPagePageRoutingModule);
      };

      PdfListPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], PdfListPagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/Pages/pdf-list-page/pdf-list-page.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/Pages/pdf-list-page/pdf-list-page.module.ts ***!
      \*************************************************************/

    /*! exports provided: PdfListPagePageModule */

    /***/
    function srcAppPagesPdfListPagePdfListPageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PdfListPagePageModule", function () {
        return PdfListPagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _pdf_list_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./pdf-list-page-routing.module */
      "./src/app/Pages/pdf-list-page/pdf-list-page-routing.module.ts");
      /* harmony import */


      var _pdf_list_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./pdf-list-page.page */
      "./src/app/Pages/pdf-list-page/pdf-list-page.page.ts");

      var PdfListPagePageModule = function PdfListPagePageModule() {
        _classCallCheck(this, PdfListPagePageModule);
      };

      PdfListPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _pdf_list_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["PdfListPagePageRoutingModule"]],
        declarations: [_pdf_list_page_page__WEBPACK_IMPORTED_MODULE_6__["PdfListPagePage"]]
      })], PdfListPagePageModule);
      /***/
    },

    /***/
    "./src/app/Pages/pdf-list-page/pdf-list-page.page.scss":
    /*!*************************************************************!*\
      !*** ./src/app/Pages/pdf-list-page/pdf-list-page.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesPdfListPagePdfListPagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL1BhZ2VzL3BkZi1saXN0LXBhZ2UvcGRmLWxpc3QtcGFnZS5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/Pages/pdf-list-page/pdf-list-page.page.ts":
    /*!***********************************************************!*\
      !*** ./src/app/Pages/pdf-list-page/pdf-list-page.page.ts ***!
      \***********************************************************/

    /*! exports provided: PdfListPagePage */

    /***/
    function srcAppPagesPdfListPagePdfListPagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PdfListPagePage", function () {
        return PdfListPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../../Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");
      /* harmony import */


      var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./../../Services/api-service.service */
      "./src/app/Services/api-service.service.ts");
      /* harmony import */


      var _ionic_native_preview_any_file_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/preview-any-file/ngx */
      "./node_modules/@ionic-native/preview-any-file/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/in-app-browser/ngx */
      "./node_modules/@ionic-native/in-app-browser/__ivy_ngcc__/ngx/index.js");

      var PdfListPagePage = /*#__PURE__*/function () {
        function PdfListPagePage(previewAnyFile, activatedRoute, storageService, apiService, navController, inAppBrowser) {
          _classCallCheck(this, PdfListPagePage);

          this.previewAnyFile = previewAnyFile;
          this.activatedRoute = activatedRoute;
          this.storageService = storageService;
          this.apiService = apiService;
          this.navController = navController;
          this.inAppBrowser = inAppBrowser;
          this.type = "";
          this.subjects = [];
          this.selectedSubjects = [];
          this.subjectDetails = [];
          this.fileInfo = [];
          this.selectedSubject = "0";
          this.branchId = "";
          this.classId = "";
          this.pdfType = "";
          this.pdfTypeUrl = "";
        }

        _createClass(PdfListPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.type = this.activatedRoute.snapshot.params["type"];
            this.branchId = this.storageService.userDetails.branchId;
            this.classId = this.storageService.userDetails.classId;
            this.pdfType = this.storageService.pdftype;
            this.pdfTypeUrl = this.storageService.pdftypeget;
            this.subjects = this.storageService.userDetails.subjectName.split(",");
            this.selectedSubjects = this.storageService.userDetails.selectedSubjects.split(",");
            this.subjectDetails = [];

            for (var i = 0; i < this.subjects.length; i++) {
              this.subjectDetails.push({
                subjectId: this.selectedSubjects[i],
                subjectName: this.subjects[i]
              });
            }

            this.selectedSubject = this.subjectDetails[0].subjectId;
            this.getPdfListSubjectWise(this.branchId, this.classId, this.selectedSubject, this.pdfType);
          }
        }, {
          key: "onSelectChange",
          value: function onSelectChange() {
            this.getPdfListSubjectWise(this.branchId, this.classId, this.selectedSubject, this.pdfType);
          }
        }, {
          key: "getPdfListSubjectWise",
          value: function getPdfListSubjectWise(branchId, classId, selectedId, pdfType) {
            var _this = this;

            this.apiService.getPdfListSubjectWise(branchId, classId, selectedId, pdfType).then(function (data) {
              _this.fileInfo = JSON.parse(data.data).d;
              console.log(_this.fileInfo);
            });
          }
        }, {
          key: "onFileClick",
          value: function onFileClick(fileName) {
            // this.previewAnyFile
            //   .preview(
            //     `${this.apiService.fileUrl}StudentAppPDF/${this.pdfTypeUrl}/${fileName}`
            //   )
            //   .then(
            //     (data) => console.log("open successfull", data),
            //     (err) => console.log("err", err)
            //   );
            this.inAppBrowser.create("https://docs.google.com/viewer?url=".concat(this.apiService.fileUrl, "StudentAppPDF/").concat(this.pdfTypeUrl, "/").concat(fileName, "&embedded=true"), "_self", {
              location: "no"
            });
          }
        }, {
          key: "doRefresh",
          value: function doRefresh(event) {
            this.getPdfListSubjectWise(this.branchId, this.classId, this.selectedSubject, this.pdfType);
            setTimeout(function () {
              event.target.complete();
            }, 2000);
          }
        }, {
          key: "onBackClick",
          value: function onBackClick() {
            this.navController.back();
          }
        }]);

        return PdfListPagePage;
      }();

      PdfListPagePage.ctorParameters = function () {
        return [{
          type: _ionic_native_preview_any_file_ngx__WEBPACK_IMPORTED_MODULE_5__["PreviewAnyFile"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__["StorageServiceService"]
        }, {
          type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_4__["ApiServiceService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"]
        }, {
          type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_7__["InAppBrowser"]
        }];
      };

      PdfListPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-pdf-list-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./pdf-list-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/pdf-list-page/pdf-list-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./pdf-list-page.page.scss */
        "./src/app/Pages/pdf-list-page/pdf-list-page.page.scss"))["default"]]
      })], PdfListPagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=Pages-pdf-list-page-pdf-list-page-module-es5.js.map