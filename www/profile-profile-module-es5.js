(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-profile-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/profile/profile.page.html":
    /*!***************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/profile/profile.page.html ***!
      \***************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesProfileProfilePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <ion-toolbar\n  color=\"primary\"\n  mode=\"ios\"\n  *ngIf=\"storageService.instituteName.length > 0\"\n>\n  <ion-title>{{storageService.instituteName}}</ion-title>\n</ion-toolbar>\n  <div class=\"div-profile ion-no-padding\">\n<ion-buttons slot=\"start\">\n <ion-menu-button color=\"light\"></ion-menu-button>\n</ion-buttons>\n    <ion-title>Profile</ion-title>\n    <ion-card class=\"profile\">\n      <ion-item lines=\"none\" class=\"ion-no-margin ion-no-padding\">\n        <!-- <ion-avatar slot=\"start\">\n          <img\n            [src]=\"\"\n          />\n        </ion-avatar> -->\n        <ion-label>\n          <h3 class=\"h3\">\n           {{profileInfo.studentName}}\n          </h3>\n          <p class=\"p\">\n            Contact: {{profileInfo.studentContact}}\n          </p>\n        </ion-label>\n      </ion-item>\n    </ion-card>\n  </div>\n  <ion-card class=\"ion-no-padding user-info\">\n    <ion-item class=\"ion-no-padding ion-padding-start ion-padding-end\">\n      <ion-label class=\"heading\">Email: </ion-label>\n      <ion-label slot=\"end\" class=\"info ion-text-end ion-text-wrap\">{{profileInfo.email}}</ion-label>\n    </ion-item>\n\n    <!-- <ion-item class=\"ion-no-padding ion-padding-start ion-padding-end\">\n      <ion-label class=\"heading\">Date of Birth: </ion-label>\n      <ion-label slot=\"end\" class=\"info ion-text-end ion-text-wrap\">{{userInfo.birthDate}}</ion-label>\n    </ion-item> -->\n\n    <!-- <ion-item class=\"ion-no-padding ion-padding-start ion-padding-end\">\n      <ion-label class=\"heading\">Gender:</ion-label>\n      <ion-label slot=\"end\" class=\"info ion-text-end ion-text-wrap\">{{userInfo.gender}}</ion-label>\n    </ion-item> -->\n\n    <ion-item class=\"ion-no-padding ion-padding-start ion-padding-end\">\n      <ion-label class=\"heading\">Parent's Contact: </ion-label>\n      <ion-label slot=\"end\" class=\"info ion-text-end ion-text-wrap\">{{profileInfo.parentContact}}</ion-label>\n    </ion-item>\n\n    <ion-item class=\"ion-no-padding ion-padding-start ion-padding-end\">\n      <ion-label class=\"heading\">Class:</ion-label>\n      <ion-label slot=\"end\" class=\"info ion-text-end ion-text-wrap\">{{profileInfo.className}}</ion-label>\n    </ion-item>\n\n    <ion-item class=\"ion-no-padding ion-padding-start ion-padding-end\">\n      <ion-label class=\"heading\">Subjects:</ion-label>\n      <ion-label slot=\"end\" class=\"info ion-text-end ion-text-wrap\">{{profileInfo.subjectName}}</ion-label>\n    </ion-item>\n\n    <ion-item class=\"ion-no-padding ion-padding-start ion-padding-end\">\n      <ion-label class=\"heading\">Branch Name:</ion-label>\n      <ion-label slot=\"end\" class=\"info ion-text-end ion-text-wrap\">{{profileInfo.branchName}}</ion-label>\n    </ion-item>\n\n    <ion-item class=\"ion-no-padding ion-padding-start ion-padding-end\">\n      <ion-label class=\"heading\">Batch Name</ion-label>\n      <ion-label slot=\"end\" class=\"info ion-text-end ion-text-wrap\">{{profileInfo.batchName}}</ion-label>\n    </ion-item>\n  </ion-card>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/profile/profile-routing.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/pages/profile/profile-routing.module.ts ***!
      \*********************************************************/

    /*! exports provided: ProfilePageRoutingModule */

    /***/
    function srcAppPagesProfileProfileRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProfilePageRoutingModule", function () {
        return ProfilePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./profile.page */
      "./src/app/pages/profile/profile.page.ts");

      var routes = [{
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_3__["ProfilePage"]
      }];

      var ProfilePageRoutingModule = function ProfilePageRoutingModule() {
        _classCallCheck(this, ProfilePageRoutingModule);
      };

      ProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ProfilePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/profile/profile.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/pages/profile/profile.module.ts ***!
      \*************************************************/

    /*! exports provided: ProfilePageModule */

    /***/
    function srcAppPagesProfileProfileModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function () {
        return ProfilePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./profile-routing.module */
      "./src/app/pages/profile/profile-routing.module.ts");
      /* harmony import */


      var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./profile.page */
      "./src/app/pages/profile/profile.page.ts");

      var ProfilePageModule = function ProfilePageModule() {
        _classCallCheck(this, ProfilePageModule);
      };

      ProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProfilePageRoutingModule"]],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
      })], ProfilePageModule);
      /***/
    },

    /***/
    "./src/app/pages/profile/profile.page.scss":
    /*!*************************************************!*\
      !*** ./src/app/pages/profile/profile.page.scss ***!
      \*************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesProfileProfilePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: rgba(0, 0, 0, 0.04);\n}\n\n.div-profile {\n  margin: 0px;\n  position: relative;\n  top: 0%;\n  background: var(--ion-color-primary);\n  width: 100%;\n  height: 25vw;\n  border-bottom-left-radius: 50px;\n  border-bottom-right-radius: 50px;\n}\n\n.div-profile ion-title {\n  color: white;\n  position: absolute;\n  top: 15%;\n  left: 11%;\n}\n\n.div-profile .profile {\n  --background: white;\n  top: 2vw;\n  height: 19vw;\n  border-radius: 20px;\n  width: 80%;\n  margin-left: 10%;\n  padding: 3vw;\n}\n\n.div-profile ion-avatar {\n  margin: 0;\n  height: 11vw;\n  width: 11vw;\n}\n\n.div-profile ion-label {\n  margin: 0;\n  margin-left: 30px;\n}\n\n.div-profile .h3 {\n  margin-bottom: 2vw;\n  font-weight: 500;\n  font-size: 4vw;\n}\n\n.div-profile .p {\n  font-size: 3.5vw;\n  font-weight: 400;\n  color: black;\n}\n\n.user-info {\n  position: absolute;\n  top: 48vw;\n  width: 90%;\n  margin-left: 5%;\n  border-radius: 20px;\n}\n\n.user-info .heading {\n  margin: 0;\n  font-size: 4vw;\n  font-weight: 500;\n}\n\n.user-info .info {\n  margin: 0;\n  margin-top: 10px;\n  font-size: 3.5vw;\n  font-weight: 400;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZmlsZS9wcm9maWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlDQUFBO0FBQ0o7O0FBRUU7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0Esb0NBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0VBQ0EsZ0NBQUE7QUFDSjs7QUFFRTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FBQ0o7O0FBR0U7RUFDRSxtQkFBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBQUo7O0FBR0U7RUFDRSxTQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFBSjs7QUFHRTtFQUNJLFNBQUE7RUFDQSxpQkFBQTtBQUFOOztBQUdFO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFBSjs7QUFHRTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBQUo7O0FBR0U7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FBQUo7O0FBR0U7RUFDRSxTQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FBQUo7O0FBR0U7RUFDRSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBQUoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9wcm9maWxlL3Byb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMDQpO1xyXG4gIH1cclxuICBcclxuICAuZGl2LXByb2ZpbGUge1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDAlO1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDI1dnc7XHJcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA1MHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDUwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5kaXYtcHJvZmlsZSBpb24tdGl0bGUge1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAxNSU7XHJcbiAgICBsZWZ0OiAxMSU7XHJcbiAgXHJcbiAgfVxyXG4gIFxyXG4gIC5kaXYtcHJvZmlsZSAucHJvZmlsZXtcclxuICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICB0b3A6IDJ2dztcclxuICAgIGhlaWdodDogMTl2dztcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwJTtcclxuICAgIHBhZGRpbmc6IDN2dztcclxuICB9XHJcbiAgXHJcbiAgLmRpdi1wcm9maWxlIGlvbi1hdmF0YXIge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgaGVpZ2h0OiAxMXZ3O1xyXG4gICAgd2lkdGg6IDExdnc7XHJcbiAgfVxyXG4gIFxyXG4gIC5kaXYtcHJvZmlsZSBpb24tbGFiZWwge1xyXG4gICAgICBtYXJnaW46IDA7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xyXG4gIH1cclxuICBcclxuICAuZGl2LXByb2ZpbGUgLmgzIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDJ2dztcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBmb250LXNpemU6IDR2dztcclxuICB9XHJcbiAgXHJcbiAgLmRpdi1wcm9maWxlIC5wIHtcclxuICAgIGZvbnQtc2l6ZTogMy41dnc7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gIH1cclxuICBcclxuICAudXNlci1pbmZvIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNDh2dztcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIH1cclxuICBcclxuICAudXNlci1pbmZvIC5oZWFkaW5nIHtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIGZvbnQtc2l6ZTogNHZ3O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB9XHJcbiAgXHJcbiAgLnVzZXItaW5mbyAuaW5mbyB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgZm9udC1zaXplOiAzLjV2dztcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgfSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/profile/profile.page.ts":
    /*!***********************************************!*\
      !*** ./src/app/pages/profile/profile.page.ts ***!
      \***********************************************/

    /*! exports provided: ProfilePage */

    /***/
    function srcAppPagesProfileProfilePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProfilePage", function () {
        return ProfilePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../../Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");

      var ProfilePage = /*#__PURE__*/function () {
        function ProfilePage(storageService) {
          _classCallCheck(this, ProfilePage);

          this.storageService = storageService;
          this.profileInfo = {
            batchName: this.storageService.userDetails.batchName,
            branchName: this.storageService.userDetails.branchName,
            className: this.storageService.userDetails.className,
            email: this.storageService.userDetails.email,
            parentContact: this.storageService.userDetails.parentContact,
            studentContact: this.storageService.userDetails.studentContact,
            studentName: this.storageService.userDetails.studentName,
            subjectName: this.storageService.userDetails.subjectName
          };
        }

        _createClass(ProfilePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ProfilePage;
      }();

      ProfilePage.ctorParameters = function () {
        return [{
          type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__["StorageServiceService"]
        }];
      };

      ProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-profile",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./profile.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/profile/profile.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./profile.page.scss */
        "./src/app/pages/profile/profile.page.scss"))["default"]]
      })], ProfilePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=profile-profile-module-es5.js.map