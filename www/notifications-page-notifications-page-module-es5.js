(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notifications-page-notifications-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/notifications-page/notifications-page.page.html":
    /*!*************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/notifications-page/notifications-page.page.html ***!
      \*************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesNotificationsPageNotificationsPagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar\n    color=\"primary\"\n    mode=\"ios\"\n    *ngIf=\"storageService.instituteName.length > 0\"\n  >\n    <ion-title>{{storageService.instituteName}}</ion-title>\n  </ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons class=\"menuButton\" slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Notifications</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content\n      pullingIcon=\"chevron-down-circle-outline\"\n      pullingText=\"Pull to refresh\"\n      refreshingSpinner=\"circles\"\n      refreshingText=\"Refreshing...\"\n    >\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-card\n    class=\"ion-margin ion-padding\"\n    *ngFor=\"let notice of noticeDetails\"\n    (click)=\"onNotificationClick(notice.ID)\"\n  >\n    <ion-item\n      class=\"ion-no-margin ion-no-padding\"\n      lines=\"none\"\n      [ngStyle]=\"{'font-weight': notice.NStatus == 'Unread' ? 'bold' : '400'}\"\n    >\n      <ion-label>{{notice.NBody}}</ion-label>\n      <ion-label slot=\"end\" class=\"ion-text-right\">{{notice.NDate}}</ion-label>\n    </ion-item>\n  </ion-card>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/Services/toast-service.service.ts":
    /*!***************************************************!*\
      !*** ./src/app/Services/toast-service.service.ts ***!
      \***************************************************/

    /*! exports provided: ToastServiceService */

    /***/
    function srcAppServicesToastServiceServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ToastServiceService", function () {
        return ToastServiceService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var ToastServiceService = /*#__PURE__*/function () {
        function ToastServiceService(toastController) {
          _classCallCheck(this, ToastServiceService);

          this.toastController = toastController;
        }

        _createClass(ToastServiceService, [{
          key: "createToast",
          value: function createToast(message, duration) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        animated: true,
                        duration: duration,
                        position: "bottom",
                        message: message
                      });

                    case 2:
                      toast = _context.sent;
                      _context.next = 5;
                      return toast.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return ToastServiceService;
      }();

      ToastServiceService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      ToastServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ToastServiceService);
      /***/
    },

    /***/
    "./src/app/pages/notifications-page/notifications-page-routing.module.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/pages/notifications-page/notifications-page-routing.module.ts ***!
      \*******************************************************************************/

    /*! exports provided: NotificationsPagePageRoutingModule */

    /***/
    function srcAppPagesNotificationsPageNotificationsPageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationsPagePageRoutingModule", function () {
        return NotificationsPagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _notifications_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./notifications-page.page */
      "./src/app/pages/notifications-page/notifications-page.page.ts");

      var routes = [{
        path: '',
        component: _notifications_page_page__WEBPACK_IMPORTED_MODULE_3__["NotificationsPagePage"]
      }];

      var NotificationsPagePageRoutingModule = function NotificationsPagePageRoutingModule() {
        _classCallCheck(this, NotificationsPagePageRoutingModule);
      };

      NotificationsPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], NotificationsPagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/notifications-page/notifications-page.module.ts":
    /*!***********************************************************************!*\
      !*** ./src/app/pages/notifications-page/notifications-page.module.ts ***!
      \***********************************************************************/

    /*! exports provided: NotificationsPagePageModule */

    /***/
    function srcAppPagesNotificationsPageNotificationsPageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationsPagePageModule", function () {
        return NotificationsPagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _notifications_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./notifications-page-routing.module */
      "./src/app/pages/notifications-page/notifications-page-routing.module.ts");
      /* harmony import */


      var _notifications_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./notifications-page.page */
      "./src/app/pages/notifications-page/notifications-page.page.ts");

      var NotificationsPagePageModule = function NotificationsPagePageModule() {
        _classCallCheck(this, NotificationsPagePageModule);
      };

      NotificationsPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _notifications_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["NotificationsPagePageRoutingModule"]],
        declarations: [_notifications_page_page__WEBPACK_IMPORTED_MODULE_6__["NotificationsPagePage"]]
      })], NotificationsPagePageModule);
      /***/
    },

    /***/
    "./src/app/pages/notifications-page/notifications-page.page.scss":
    /*!***********************************************************************!*\
      !*** ./src/app/pages/notifications-page/notifications-page.page.scss ***!
      \***********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesNotificationsPageNotificationsPagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL25vdGlmaWNhdGlvbnMtcGFnZS9ub3RpZmljYXRpb25zLXBhZ2UucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/pages/notifications-page/notifications-page.page.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/notifications-page/notifications-page.page.ts ***!
      \*********************************************************************/

    /*! exports provided: NotificationsPagePage */

    /***/
    function srcAppPagesNotificationsPageNotificationsPagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationsPagePage", function () {
        return NotificationsPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _Services_notification_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../../Services/notification-service.service */
      "./src/app/Services/notification-service.service.ts");
      /* harmony import */


      var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../../Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");
      /* harmony import */


      var src_app_Services_toast_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/Services/toast-service.service */
      "./src/app/Services/toast-service.service.ts");
      /* harmony import */


      var src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/Services/api-service.service */
      "./src/app/Services/api-service.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var NotificationsPagePage = /*#__PURE__*/function () {
        function NotificationsPagePage(router, notificationService, storageService, apiServiceService, toastService) {
          _classCallCheck(this, NotificationsPagePage);

          this.router = router;
          this.notificationService = notificationService;
          this.storageService = storageService;
          this.apiServiceService = apiServiceService;
          this.toastService = toastService;
          this.sid = "";
          this.noticeDetails = [];
        }

        _createClass(NotificationsPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            // this.storageService.testNotificationsCount = 0;
            // this.storageService.generalNotificationsCount = 0;
            // this.notificationService.getNotificationCount();
            // console.log(
            //   this.storageService.testNotificationsCount,
            //   this.storageService.generalNotificationsCount
            // );
            this.sid = this.storageService.userDetails.sId;
            this.getNotices();
          }
        }, {
          key: "getNotices",
          value: function getNotices() {
            var _this = this;

            this.notificationService.getCMSNotificationMessage(this.sid).then(function (data) {
              _this.noticeDetails = data;
              console.log("noticeDetails", _this.noticeDetails);
            });
          }
        }, {
          key: "doRefresh",
          value: function doRefresh(event) {
            this.getLastNotification(event);
          }
        }, {
          key: "getLastNotification",
          value: function getLastNotification(event) {
            var _this2 = this;

            this.sid = this.storageService.userDetails.sId;
            this.notificationService.getLastNotification().then(function (data) {
              _this2.lastNotification = data;
              if (_this2.lastNotification.ArrengeTestId == null) _this2.lastNotification.ArrengeTestId = _this2.lastNotification.ROnlineTestNotificationId;

              _this2.notificationService.selectFromCMSNotificationMessage().then(function (data) {
                if (data != "") _this2.lastNotification.ROnlineNotificationId = data;

                _this2.getNotificationsOnline(_this2.lastNotification.branchId, _this2.lastNotification.classId, _this2.lastNotification.BatchId, _this2.lastNotification.ROnlineNotificationId, _this2.sid, _this2.lastNotification.RPdfUploadId, _this2.lastNotification.ArrengeTestId, _this2.lastNotification.MaxOfflineTestPaperId, _this2.lastNotification.MaxOfflineTestStudentMarksId, _this2.lastNotification.userId, _this2.lastNotification.selectedSubjects, _this2.lastNotification.studentName, event);
              });
            });
          }
        }, {
          key: "getNotificationsOnline",
          value: function getNotificationsOnline(branchId, classId, selectedBatches, RNotificationId, SId, pdfUploadId, arrengeTestId, MaxOfflineTestPaperId, MaxOfflineTestStudentMarksId, userId, selectedSubjects, studentName, event) {
            var _this3 = this;

            if (arrengeTestId == "" || arrengeTestId == null || arrengeTestId == undefined) console.log("test is null");
            this.apiServiceService.getNotificationsOnline(branchId, classId, selectedBatches, RNotificationId, pdfUploadId, arrengeTestId, MaxOfflineTestPaperId, MaxOfflineTestStudentMarksId, userId, selectedSubjects, this.storageService.userDetails.clientId, studentName, "norecived", this.storageService.tokenId).then(function (result) {
              // console.log("success")
              var data = JSON.parse(result.data);
              console.log("getNotificationsOnline", data);

              if (data.d.length > 0) {
                var dataLength = 0;

                for (var i = 0; i < data.d.length; i++) {
                  var NotificationId = data.d[i].NotificationId;
                  var Category = data.d[i].Category;
                  var Message = data.d[i].Message;
                  console.log(data.d[i].Message);
                  var NDateOnline = data.d[i].Date.split(" ")[0];
                  var NTimeOnline = data.d[i].Date.split(" ")[1];
                  dataLength = i + 1;

                  _this3.apiServiceService.getNotificationsOnline(branchId, classId, selectedBatches, RNotificationId, pdfUploadId, NotificationId, MaxOfflineTestPaperId, MaxOfflineTestStudentMarksId, userId, selectedSubjects, _this3.storageService.userDetails.clientId, studentName, "received", _this3.storageService.tokenId);

                  _this3.exists(NotificationId, Category, Message, NDateOnline, NTimeOnline, SId, event);
                } // if (dataLength == data.d.length) {
                //   // localStorage.setItem("connectionStatus", "0");
                //   this.getNotificationCount();
                // }

              } // else if (length == 0) {
              //   this.getNotificationCount();
              // }
              else {
                  event.target.complete();

                  _this3.toastService.createToast("You recived all your notices", 3000);
                }
            }, function (err) {
              return console.log("Server Error", err);
            });
          }
        }, {
          key: "exists",
          value: function exists(NotificationId, Category, Message, NDateOnline, NTimeOnline, SId, event) {
            var _this4 = this;

            if (Category == "Notice" || Category == "PDF") {
              this.notificationService.insertIntoCMSNotificationMessage(Message, NDateOnline, NTimeOnline, SId, NotificationId, Category).then(function (_) {
                return _this4.notificationService.getCMSNotificationMessage(_this4.sid).then(function (data) {
                  _this4.noticeDetails = data;
                  event.target.complete();
                  console.log("noticeDetails", _this4.noticeDetails);
                });
              });
            }
          }
        }, {
          key: "onNotificationClick",
          value: function onNotificationClick(notificationId) {
            var _this5 = this;

            this.noticeDetails[this.noticeDetails.length - notificationId].NStatus = "Read";
            this.notificationService.updateCMSNotificationMessage(notificationId).then(function (_) {
              return _this5.router.navigate(["/notification-detail-page", notificationId]);
            });
            console.log('notification', this.noticeDetails);
          }
        }]);

        return NotificationsPagePage;
      }();

      NotificationsPagePage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
        }, {
          type: _Services_notification_service_service__WEBPACK_IMPORTED_MODULE_2__["NotificationServiceService"]
        }, {
          type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__["StorageServiceService"]
        }, {
          type: src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_5__["ApiServiceService"]
        }, {
          type: src_app_Services_toast_service_service__WEBPACK_IMPORTED_MODULE_4__["ToastServiceService"]
        }];
      };

      NotificationsPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-notifications-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./notifications-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/notifications-page/notifications-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./notifications-page.page.scss */
        "./src/app/pages/notifications-page/notifications-page.page.scss"))["default"]]
      })], NotificationsPagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=notifications-page-notifications-page-module-es5.js.map