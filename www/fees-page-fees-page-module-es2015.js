(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["fees-page-fees-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fees-page/fees-page.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fees-page/fees-page.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar\n  color=\"primary\"\n  mode=\"ios\"\n  *ngIf=\"storageService.instituteName.length > 0\"\n>\n  <ion-title>{{storageService.instituteName}}</ion-title>\n</ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons class=\"menuButton\" slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Fees</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content\n      pullingIcon=\"chevron-down-circle-outline\"\n      pullingText=\"Pull to refresh\"\n      refreshingSpinner=\"circles\"\n      refreshingText=\"Refreshing...\"\n    >\n    </ion-refresher-content>\n  </ion-refresher>\n  <table id=\"tblfeesDetails\">\n    <td colspan=\"3\" color=\"primary\" class=\"tableName\">Fees Status</td>\n    <tr>\n      <th>Total Fees</th>\n      <th>Paid</th>\n      <th>Remaining</th>\n    </tr>\n    <tr>\n      <td>{{feesDetails.totalFees | currency: 'INR'}}</td>\n      <td>{{feesDetails.paidFees | currency: 'INR'}}</td>\n      <td>{{feesDetails.remainingFees | currency: 'INR'}}</td>\n    </tr>\n  </table>\n\n  <!-- <ion-item class=\"ion-no-margin ion-no-padding\" color=\"primary\">\n    <ion-label>Last Fees Paid Transactions</ion-label>\n  </ion-item> -->\n  <table id=\"tblfeesDetails\">\n    <td colspan=\"3\" color=\"primary\" class=\"tableName\">\n      Last Fees Paid Transactions\n    </td>\n    <tr>\n      <th>Paid Date</th>\n      <th>Amount</th>\n    </tr>\n    <tr *ngFor=\"let feesDetails of feesHistoryDetails\">\n      <td>{{feesDetails.paidDate}}</td>\n      <td>{{feesDetails.paidFees | currency: 'INR'}}</td>\n    </tr>\n  </table>\n\n  <canvas #pieChart></canvas>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/fees-page/fees-page-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/fees-page/fees-page-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: FeesPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeesPagePageRoutingModule", function() { return FeesPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _fees_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fees-page.page */ "./src/app/pages/fees-page/fees-page.page.ts");




const routes = [
    {
        path: '',
        component: _fees_page_page__WEBPACK_IMPORTED_MODULE_3__["FeesPagePage"]
    }
];
let FeesPagePageRoutingModule = class FeesPagePageRoutingModule {
};
FeesPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], FeesPagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/fees-page/fees-page.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/fees-page/fees-page.module.ts ***!
  \*****************************************************/
/*! exports provided: FeesPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeesPagePageModule", function() { return FeesPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _fees_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./fees-page-routing.module */ "./src/app/pages/fees-page/fees-page-routing.module.ts");
/* harmony import */ var _fees_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fees-page.page */ "./src/app/pages/fees-page/fees-page.page.ts");







let FeesPagePageModule = class FeesPagePageModule {
};
FeesPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _fees_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["FeesPagePageRoutingModule"]
        ],
        declarations: [_fees_page_page__WEBPACK_IMPORTED_MODULE_6__["FeesPagePage"]]
    })
], FeesPagePageModule);



/***/ }),

/***/ "./src/app/pages/fees-page/fees-page.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/fees-page/fees-page.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#tblfeesDetails th,\ntd {\n  text-align: center;\n  padding: 4px;\n  border: 1px solid #ccc;\n}\n\n#tblfeesDetails {\n  height: auto;\n  padding: 1px;\n  margin-top: 5px;\n  overflow: scroll;\n  border: 1px solid #ccc;\n  font-size: 4vw;\n  width: 100%;\n}\n\n#tblfeesDetails th {\n  border: 1px solid #ccc;\n}\n\n.tableName {\n  color: white;\n  background: #3880ff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZmVlcy1wYWdlL2ZlZXMtcGFnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBRUUsa0JBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7QUFDRjs7QUFFQTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtBQUNGOztBQUNBO0VBQ0Usc0JBQUE7QUFFRjs7QUFDQTtFQUNJLFlBQUE7RUFDQSxtQkFBQTtBQUVKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZmVlcy1wYWdlL2ZlZXMtcGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjdGJsZmVlc0RldGFpbHMgdGgsXHJcbnRkIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcGFkZGluZzogNHB4O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbn1cclxuXHJcbiN0YmxmZWVzRGV0YWlscyB7XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG4gIHBhZGRpbmc6IDFweDtcclxuICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgb3ZlcmZsb3c6IHNjcm9sbDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gIGZvbnQtc2l6ZTogNHZ3O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcbiN0YmxmZWVzRGV0YWlscyB0aCB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxufVxyXG5cclxuLnRhYmxlTmFtZSB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMzg4MGZmO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/fees-page/fees-page.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/fees-page/fees-page.page.ts ***!
  \***************************************************/
/*! exports provided: FeesPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeesPagePage", function() { return FeesPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! chart.js */ "./node_modules/chart.js/dist/Chart.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../Services/api-service.service */ "./src/app/Services/api-service.service.ts");
/* harmony import */ var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");
/* harmony import */ var _Services_fees_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../Services/fees-service.service */ "./src/app/Services/fees-service.service.ts");
/* harmony import */ var _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../Services/internet-service.service */ "./src/app/Services/internet-service.service.ts");
/* harmony import */ var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../../Services/toast-service.service */ "./src/app/Services/toast-service.service.ts");








let FeesPagePage = class FeesPagePage {
    constructor(apiService, storageService, feesService, internetService, toastService) {
        this.apiService = apiService;
        this.storageService = storageService;
        this.feesService = feesService;
        this.internetService = internetService;
        this.toastService = toastService;
        this.userId = "";
        this.feesHistoryDetails = [];
        this.feesDetails = {
            paidFees: "",
            remainingFees: "",
            totalFees: "",
        };
    }
    ngOnInit() {
        this.userId = this.storageService.userDetails.userId;
        if (this.internetService.networkConnected)
            this.getFeesData(this.userId);
        else {
            this.getStudentFeesData();
            this.getStudentFeesHistory();
            this.toastService.createToast("Check internet connection to update details", 2000);
        }
    }
    getFeesData(userId) {
        this.apiService.getFeesData(userId).then((result) => {
            // console.log(JSON.parse(result.data));
            let data = JSON.parse(result.data);
            var length = data.d.length;
            // console.log('length', length)
            if (length > 0) {
                this.feesService.deleteFromCMSFeesInfo().then((_) => {
                    for (var i = 0; i < data.d.length; i++) {
                        var total_fees = data.d[i].total_fee;
                        var paid_fees = data.d[i].paid_fee;
                        var rem_fees = parseInt(total_fees) - parseInt(paid_fees);
                        this.feesService
                            .insertIntoCMSFeesInfo(total_fees, paid_fees, rem_fees)
                            .then((_) => this.getFeesHistory(userId));
                    }
                });
            }
            else if (length == 0) {
                this.getFeesHistory(userId);
            }
        }, (err) => {
            this.getFeesHistory(userId);
        });
    }
    getFeesHistory(userId) {
        this.apiService.getFeesHistory(userId).then((result) => {
            let data = JSON.parse(result.data);
            // console.log('history',data);
            var length = data.d.length;
            if (length > 0) {
                var dataLength = 0;
                this.feesService.deleteFromCMSFeesHistory().then((_) => {
                    for (var i = 0; i < data.d.length; i++) {
                        var paid_fees = data.d[i].payment;
                        var paid_date = data.d[i].paid_date;
                        dataLength = i + 1;
                        this.feesService.insertIntoCMSFeesHistory(paid_fees, paid_date);
                    }
                    if (dataLength == data.d.length) {
                        this.getStudentFeesData();
                        this.getStudentFeesHistory();
                    }
                });
            }
            else if (length == 0) {
                this.getStudentFeesData();
                this.getStudentFeesHistory();
            }
        }, (err) => {
            this.getStudentFeesData();
            this.getStudentFeesHistory();
        });
    }
    getStudentFeesData() {
        this.feesService.getStudentFeesData().then((data) => {
            this.feesDetails = data;
            console.log(this.feesDetails);
            this.pie = new chart_js__WEBPACK_IMPORTED_MODULE_2__["Chart"](this.pieChart1.nativeElement, {
                type: "pie",
                data: {
                    labels: ["Paid", "Remaining"],
                    datasets: [
                        {
                            label: "Fees Details",
                            data: [this.feesDetails.paidFees, this.feesDetails.remainingFees],
                            backgroundColor: ["#2dd36f", "#eb445a"],
                        },
                    ],
                },
                options: {
                    tooltips: {
                        enabled: true,
                    },
                },
            });
        });
    }
    getStudentFeesHistory() {
        this.feesService.getStudentFeesHistory().then((data) => {
            this.feesHistoryDetails = data;
            console.log(this.feesHistoryDetails);
        });
    }
    doRefresh(event) {
        if (this.internetService.networkConnected)
            this.getFeesData(this.userId);
        else {
            this.getStudentFeesData();
            this.getStudentFeesHistory();
            this.toastService.createToast("Check internet connection to update details", 3000);
        }
        setTimeout(() => {
            event.target.complete();
        }, 2000);
    }
};
FeesPagePage.ctorParameters = () => [
    { type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_3__["ApiServiceService"] },
    { type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_4__["StorageServiceService"] },
    { type: _Services_fees_service_service__WEBPACK_IMPORTED_MODULE_5__["FeesServiceService"] },
    { type: _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_6__["InternetServiceService"] },
    { type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_7__["ToastServiceService"] }
];
FeesPagePage.propDecorators = {
    pieChart1: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ["pieChart",] }]
};
FeesPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-fees-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./fees-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fees-page/fees-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./fees-page.page.scss */ "./src/app/pages/fees-page/fees-page.page.scss")).default]
    })
], FeesPagePage);



/***/ })

}]);
//# sourceMappingURL=fees-page-fees-page-module-es2015.js.map