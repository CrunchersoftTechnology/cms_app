(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pdf-page-pdf-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pdf-page/pdf-page.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pdf-page/pdf-page.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar\n    color=\"primary\"\n    mode=\"ios\"\n    *ngIf=\"storageService.instituteName.length > 0\"\n  >\n    <ion-title>{{storageService.instituteName}}</ion-title>\n  </ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons class=\"menuButton\" slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Study Material</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card\n    class=\"ion-margin ion-padding\"\n    (click)=\"onPDFCategoryClick('GetUploadTextbooks', 'UploadTextbooksPDF', 'Text Books')\"\n  >\n    <ion-card-subtitle>Text Books</ion-card-subtitle>\n  </ion-card>\n  <ion-card\n    class=\"ion-margin ion-padding\"\n    (click)=\"onPDFCategoryClick('GetUploadReferencebooks', 'UploadReferencebooksPDF', 'Reference Books')\"\n  >\n    <ion-card-subtitle>Reference Books</ion-card-subtitle>\n  </ion-card>\n  <ion-card\n    class=\"ion-margin ion-padding\"\n    (click)=\"onPDFCategoryClick('GetNotes', 'UploadNotesPDF','Notes')\"\n  >\n    <ion-card-subtitle>Notes</ion-card-subtitle>\n  </ion-card>\n  <!-- <ion-card\n    class=\"ion-margin ion-padding\"\n    (click)=\"onPDFCategoryClick('GetAssignments', 'UploadAssignmentsPDF', 'Assignments')\"\n  >\n    <ion-card-subtitle>Assignments</ion-card-subtitle>\n  </ion-card> -->\n  <ion-card\n    class=\"ion-margin ion-padding\"\n    (click)=\"onPDFCategoryClick('GetUploadQuestionpapers', 'UploadQuestionpapersPDF', 'Question Papers')\"\n  >\n    <ion-card-subtitle>Question Papers</ion-card-subtitle>\n  </ion-card>\n  <ion-card\n    class=\"ion-margin ion-padding\"\n    (click)=\"onPDFCategoryClick('GetUploadPracticepapers', 'UploadPracticepapersPDF', 'Practice Papers')\"\n  >\n    <ion-card-subtitle>Practice Papers</ion-card-subtitle>\n  </ion-card>\n  <ion-card\n    class=\"ion-margin ion-padding\"\n    (click)=\"onPDFCategoryClick('GetUploadInbuiltquestionbanks', 'UploadInbuiltquestionbankPDF', 'Inbuilt Question Bank')\"\n  >\n    <ion-card-subtitle>Inbuilt Question Bank</ion-card-subtitle>\n  </ion-card>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/pdf-page/pdf-page-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/pdf-page/pdf-page-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: PdfPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PdfPagePageRoutingModule", function() { return PdfPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _pdf_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pdf-page.page */ "./src/app/pages/pdf-page/pdf-page.page.ts");




const routes = [
    {
        path: '',
        component: _pdf_page_page__WEBPACK_IMPORTED_MODULE_3__["PdfPagePage"]
    }
];
let PdfPagePageRoutingModule = class PdfPagePageRoutingModule {
};
PdfPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PdfPagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/pdf-page/pdf-page.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/pdf-page/pdf-page.module.ts ***!
  \***************************************************/
/*! exports provided: PdfPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PdfPagePageModule", function() { return PdfPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _pdf_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pdf-page-routing.module */ "./src/app/pages/pdf-page/pdf-page-routing.module.ts");
/* harmony import */ var _pdf_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pdf-page.page */ "./src/app/pages/pdf-page/pdf-page.page.ts");







let PdfPagePageModule = class PdfPagePageModule {
};
PdfPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _pdf_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["PdfPagePageRoutingModule"]
        ],
        declarations: [_pdf_page_page__WEBPACK_IMPORTED_MODULE_6__["PdfPagePage"]]
    })
], PdfPagePageModule);



/***/ }),

/***/ "./src/app/pages/pdf-page/pdf-page.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/pdf-page/pdf-page.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BkZi1wYWdlL3BkZi1wYWdlLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/pdf-page/pdf-page.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/pdf-page/pdf-page.page.ts ***!
  \*************************************************/
/*! exports provided: PdfPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PdfPagePage", function() { return PdfPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");




let PdfPagePage = class PdfPagePage {
    constructor(router, storageService) {
        this.router = router;
        this.storageService = storageService;
    }
    ngOnInit() { }
    onPDFCategoryClick(pdfType, pdfTypeGet, type) {
        this.storageService.pdftype = pdfType;
        this.storageService.pdftypeget = pdfTypeGet;
        this.router.navigate(['/pdf-list-page', type]);
    }
};
PdfPagePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__["StorageServiceService"] }
];
PdfPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-pdf-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./pdf-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pdf-page/pdf-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./pdf-page.page.scss */ "./src/app/pages/pdf-page/pdf-page.page.scss")).default]
    })
], PdfPagePage);



/***/ })

}]);
//# sourceMappingURL=pdf-page-pdf-page-module-es2015.js.map