(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Pages-login-page-login-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/login-page/login-page.page.html":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/login-page/login-page.page.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesLoginPageLoginPagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Login</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-item>\n    <ion-input placeholder=\"Enter Email or Phone\" [(ngModel)]=\"loginCredentials\"></ion-input>\n  </ion-item>\n  <ion-button (click)=\"onLoginClick()\" expand=\"block\" fill=\"solid\" color=\"primary\"> Login </ion-button>\n\n  {{error}}\n</ion-content> -->\n\n<div class=\"login\">\n  <div heading-column-lr>\n    <h1 big-heading>Login</h1>\n    <!-- <h2 small-heading (click)=\"onRegisterClick()\">REGISTER</h2> -->\n    <p class=\"welcome\">Welcome to OEK Student App</p>\n  </div>\n\n  <form name=\"loginForm\" #loginForm=\"ngForm\">\n    <ion-item transparent>\n      <!-- <ion-icon icon-small name=\"person\" color=\"primary\"></ion-icon> -->\n      <ion-input\n        name=\"emailId\"\n        [(ngModel)]=\"loginCredentials\"\n        #emailId=\"ngModel\"\n        email\n        required\n        type=\"text\"\n        placeholder=\"Email or Phone\"\n      ></ion-input>\n    </ion-item>\n    <!-- <div *ngIf=\"emailId.invalid && (emailId.dirty || emailId.touched)\">\n      <ion-text color=\"danger\" *ngIf=\"emailId.errors.required\"\n        >Enter email or phone number</ion-text\n      >\n    </div> -->\n    <div *ngIf=\"showErrorMessage\">\n      <ion-text color=\"danger\">User not registered</ion-text>\n    </div>\n    <div *ngIf=\"showValidationMessage\">\n      <ion-text color=\"danger\">Logout from other device</ion-text>\n    </div>\n\n    <!-- <ion-label class=\"forgot-password\" (click)=\"onForgotPass()\"\n      >Forgot Password</ion-label\n    > -->\n    <div style=\"height: 30px\"></div>\n\n    <ion-button\n      color=\"primary\"\n      class=\"ion-button-class\"\n      size=\"large\"\n      expand=\"block\"\n      fill=\"solid\"\n      shape=\"round\"\n      (click)=\"onLoginWithPass()\"\n    >\n      Login With Password\n    </ion-button>\n\n    <ion-button\n      color=\"primary\"\n      class=\"ion-button-class\"\n      size=\"large\"\n      expand=\"block\"\n      fill=\"solid\"\n      shape=\"round\"\n      (click)=\"onLoginWithOTP()\"\n    >\n      Login With Code\n    </ion-button>\n  </form>\n</div>\n\n\n<ion-label class=\"version\"> Version 1.0 </ion-label>\n";
      /***/
    },

    /***/
    "./src/app/Pages/login-page/login-page-routing.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/Pages/login-page/login-page-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: LoginPagePageRoutingModule */

    /***/
    function srcAppPagesLoginPageLoginPageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPagePageRoutingModule", function () {
        return LoginPagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login-page.page */
      "./src/app/Pages/login-page/login-page.page.ts");

      var routes = [{
        path: '',
        component: _login_page_page__WEBPACK_IMPORTED_MODULE_3__["LoginPagePage"]
      }];

      var LoginPagePageRoutingModule = function LoginPagePageRoutingModule() {
        _classCallCheck(this, LoginPagePageRoutingModule);
      };

      LoginPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/Pages/login-page/login-page.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/Pages/login-page/login-page.module.ts ***!
      \*******************************************************/

    /*! exports provided: LoginPagePageModule */

    /***/
    function srcAppPagesLoginPageLoginPageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPagePageModule", function () {
        return LoginPagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-page-routing.module */
      "./src/app/Pages/login-page/login-page-routing.module.ts");
      /* harmony import */


      var _login_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login-page.page */
      "./src/app/Pages/login-page/login-page.page.ts");

      var LoginPagePageModule = function LoginPagePageModule() {
        _classCallCheck(this, LoginPagePageModule);
      };

      LoginPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPagePageRoutingModule"]],
        declarations: [_login_page_page__WEBPACK_IMPORTED_MODULE_6__["LoginPagePage"]]
      })], LoginPagePageModule);
      /***/
    },

    /***/
    "./src/app/Pages/login-page/login-page.page.scss":
    /*!*******************************************************!*\
      !*** ./src/app/Pages/login-page/login-page.page.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesLoginPageLoginPagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".login {\n  position: absolute;\n  left: 50%;\n  top: 40%;\n  transform: translate(-49%, -49%);\n  width: 90%;\n}\n\n.registration {\n  position: absolute;\n  top: 2%;\n  left: 5%;\n  width: 90%;\n}\n\n.login-header {\n  background: var(--ion-color-primary);\n}\n\n.logo-image {\n  display: block;\n  width: 120px;\n  margin: 0 auto;\n  text-align: center;\n}\n\n[heading-column-lr] {\n  display: flex;\n  align-items: center;\n  margin-bottom: 25vw;\n}\n\n[big-heading] {\n  font-size: 12vw !important;\n  color: var(--ion-color-dark);\n}\n\n[small-heading] {\n  margin-left: auto;\n  padding-right: 40px;\n  font-size: 15px;\n}\n\n[transparent] {\n  background: transparent;\n}\n\nion-input {\n  font-size: 4.5vw;\n  font-weight: 400;\n}\n\n[icon-small] {\n  width: 30px;\n}\n\nion-item {\n  --highlight-color-focused: transparent;\n  --highlight-height: 0;\n  --highlight-color-invalid: #000;\n  --highlight-color-valid: #ddd;\n  --min-height: 65px;\n  --padding-start: 0;\n}\n\nion-label {\n  color: #000;\n  opacity: 0.8;\n  font-size: 13px;\n  display: block;\n}\n\n.forgot-password {\n  color: var(--ion-color-primary);\n  font-size: 13px;\n  display: block;\n  text-align: right;\n  padding: 10px;\n}\n\n.link-button {\n  color: var(--ion-color-primary);\n}\n\n.demo-login {\n  color: var(--ion-color-primary);\n  margin-left: 80px;\n}\n\n.chbox {\n  margin-right: 5px;\n}\n\n.label {\n  margin-top: 10px;\n  position: absolute;\n  left: 30%;\n}\n\n.label-text {\n  font-size: 5vw;\n  font-weight: bold;\n  text-decoration: underline;\n}\n\n.ion-button-class {\n  margin-top: 6vw;\n  width: 98%;\n  font-size: 4vw;\n}\n\n.welcome {\n  position: absolute;\n  top: 15%;\n  font-size: 4.5vw;\n  color: gray;\n  font-weight: 400;\n}\n\n.version {\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  bottom: 4%;\n  font-size: 4vw;\n  color: gray;\n  font-weight: 300;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvUGFnZXMvbG9naW4tcGFnZS9sb2dpbi1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFLQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFFQSxnQ0FBQTtFQUNBLFVBQUE7QUFKSjs7QUFPQTtFQUNJLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0FBSko7O0FBT0E7RUFDSSxvQ0FBQTtBQUpKOztBQU9BO0VBQ0ksY0FBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUFKSjs7QUFPQTtFQUNJLGFBQUE7RUFFQSxtQkFBQTtFQUNBLG1CQUFBO0FBSko7O0FBT0E7RUFDSSwwQkFBQTtFQUNBLDRCQUFBO0FBSko7O0FBT0E7RUFDSSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQUpKOztBQU9BO0VBQ0ksdUJBQUE7QUFKSjs7QUFPQTtFQUNJLGdCQUFBO0VBQ0EsZ0JBQUE7QUFKSjs7QUFPQTtFQUNJLFdBQUE7QUFKSjs7QUFPQTtFQUNJLHNDQUFBO0VBQ0EscUJBQUE7RUFDQSwrQkFBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQUpKOztBQU9BO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQUpKOztBQU9BO0VBQ0ksK0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtBQUpKOztBQU9BO0VBQ0ksK0JBQUE7QUFKSjs7QUFPQTtFQUNJLCtCQUFBO0VBQ0EsaUJBQUE7QUFKSjs7QUFPQTtFQUNJLGlCQUFBO0FBSko7O0FBT0E7RUFDSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtBQUpKOztBQU9BO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0VBQ0EsMEJBQUE7QUFKSjs7QUFPQTtFQUNJLGVBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtBQUpKOztBQU9BO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUFKSjs7QUFPQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0EsVUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUFKSiIsImZpbGUiOiJzcmMvYXBwL1BhZ2VzL2xvZ2luLXBhZ2UvbG9naW4tcGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyAuY2hib3gge1xyXG4vLyAgICAgbWFyZ2luOiAzcHg7XHJcbi8vICAgICBwYWRkaW5nOiAwcHg7XHJcbi8vICB9XHJcblxyXG4ubG9naW4ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgdG9wOiA0MCU7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKC00OSUsIC00OSUpO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTQ5JSwgLTQ5JSk7XHJcbiAgICB3aWR0aDogOTAlXHJcbn1cclxuXHJcbi5yZWdpc3RyYXRpb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAyJTtcclxuICAgIGxlZnQ6IDUlO1xyXG4gICAgd2lkdGg6IDkwJVxyXG59XHJcblxyXG4ubG9naW4taGVhZGVyIHtcclxuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxufVxyXG5cclxuLmxvZ28taW1hZ2Uge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuW2hlYWRpbmctY29sdW1uLWxyXSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyNXZ3O1xyXG59XHJcblxyXG5bYmlnLWhlYWRpbmddIHtcclxuICAgIGZvbnQtc2l6ZTogMTJ2dyAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYXJrKTtcclxufVxyXG5cclxuW3NtYWxsLWhlYWRpbmddIHtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgcGFkZGluZy1yaWdodDogNDBweDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG5cclxuW3RyYW5zcGFyZW50XSB7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxuaW9uLWlucHV0IHtcclxuICAgIGZvbnQtc2l6ZTogNC41dnc7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG5baWNvbi1zbWFsbF0ge1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbn1cclxuXHJcbmlvbi1pdGVtIHtcclxuICAgIC0taGlnaGxpZ2h0LWNvbG9yLWZvY3VzZWQ6IHRyYW5zcGFyZW50O1xyXG4gICAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwO1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItaW52YWxpZDogIzAwMDtcclxuICAgIC0taGlnaGxpZ2h0LWNvbG9yLXZhbGlkOiAjZGRkO1xyXG4gICAgLS1taW4taGVpZ2h0OiA2NXB4O1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xyXG59XHJcblxyXG5pb24tbGFiZWwge1xyXG4gICAgY29sb3I6ICMwMDA7XHJcbiAgICBvcGFjaXR5OiAuODtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uZm9yZ290LXBhc3N3b3JkIHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgcGFkZGluZzogMTBweFxyXG59XHJcblxyXG4ubGluay1idXR0b24ge1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KVxyXG59XHJcblxyXG4uZGVtby1sb2dpbiB7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDgwcHg7XHJcbn1cclxuXHJcbi5jaGJveCB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxufVxyXG5cclxuLmxhYmVsIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiAzMCU7XHJcbn1cclxuXHJcbi5sYWJlbC10ZXh0IHtcclxuICAgIGZvbnQtc2l6ZTogNXZ3O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxufVxyXG5cclxuLmlvbi1idXR0b24tY2xhc3Mge1xyXG4gICAgbWFyZ2luLXRvcDogNnZ3O1xyXG4gICAgd2lkdGg6IDk4JTtcclxuICAgIGZvbnQtc2l6ZTogNHZ3XHJcbn1cclxuXHJcbi53ZWxjb21lIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMTUlO1xyXG4gICAgZm9udC1zaXplOiA0LjV2dztcclxuICAgIGNvbG9yOiBncmF5O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxufVxyXG5cclxuLnZlcnNpb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICBib3R0b206IDQlO1xyXG4gICAgZm9udC1zaXplOiA0dnc7XHJcbiAgICBjb2xvcjogZ3JheTtcclxuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/Pages/login-page/login-page.page.ts":
    /*!*****************************************************!*\
      !*** ./src/app/Pages/login-page/login-page.page.ts ***!
      \*****************************************************/

    /*! exports provided: LoginPagePage */

    /***/
    function srcAppPagesLoginPageLoginPagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPagePage", function () {
        return LoginPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./../../Services/api-service.service */
      "./src/app/Services/api-service.service.ts");
      /* harmony import */


      var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/http/ngx */
      "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/device/ngx */
      "./node_modules/@ionic-native/device/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _Services_alert_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./../../Services/alert-service.service */
      "./src/app/Services/alert-service.service.ts");
      /* harmony import */


      var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./../../Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");
      /* harmony import */


      var _Services_user_details_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./../../Services/user-details.service */
      "./src/app/Services/user-details.service.ts");
      /* harmony import */


      var _Services_database_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./../../Services/database.service */
      "./src/app/Services/database.service.ts");
      /* harmony import */


      var src_app_Services_internet_service_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! src/app/Services/internet-service.service */
      "./src/app/Services/internet-service.service.ts");

      var LoginPagePage = /*#__PURE__*/function () {
        function LoginPagePage(router, http, nativeHttp, storage, device, apiService, alertService, storageService, userDetailsService, databaseService, internetService) {
          _classCallCheck(this, LoginPagePage);

          this.router = router;
          this.http = http;
          this.nativeHttp = nativeHttp;
          this.storage = storage;
          this.device = device;
          this.apiService = apiService;
          this.alertService = alertService;
          this.storageService = storageService;
          this.userDetailsService = userDetailsService;
          this.databaseService = databaseService;
          this.internetService = internetService;
          this.loginCredentials = "";
          this.error = "";
          this.showErrorMessage = false;
          this.showValidationMessage = false;
          this.deviceId = "";
        }

        _createClass(LoginPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.deviceId = this.device.uuid;
            this.storageService.deviceId = this.deviceId;
          }
        }, {
          key: "onLoginWithOTP",
          value: function onLoginWithOTP() {
            var _this = this;

            if (this.internetService.networkConnected) {
              this.apiService.login(this.loginCredentials).then(function (data) {
                _this.userInfo = JSON.parse(data.data).d[0];
                console.log("log in details", _this.userInfo);
                _this.storageService.userInfo = JSON.parse(data.data).d[0];

                if (_this.userInfo.resultCode == "0") {
                  _this.showErrorMessage = true;
                } else if (_this.userInfo.DeviceId == "" || _this.userInfo.DeviceId == _this.deviceId) {
                  _this.validationForOTP();
                } else if (_this.userInfo.LoginStatus == "0" || _this.userInfo.LoginStatus == "loggedOut") {
                  _this.validationForOTP();
                } else {
                  _this.showValidationMessage = true;
                }
              });
            }
          }
        }, {
          key: "validationForOTP",
          value: function validationForOTP() {
            var _this2 = this;

            this.loginCredentials = "";
            this.showErrorMessage = false;
            this.showValidationMessage = false;
            this.storageService.forgotPassword = false;
            this.router.navigateByUrl("/otp-page");
            this.databaseService.getDatabaseState().subscribe(function (ready) {
              if (ready) {
                _this2.userDetailsService.insertIntoCMSLogStatus();
              }
            });
          }
        }, {
          key: "onLoginWithPass",
          value: function onLoginWithPass() {
            var _this3 = this;

            if (this.internetService.networkConnected) {
              this.apiService.login(this.loginCredentials).then(function (data) {
                console.log("log in details", JSON.parse(data.data).d[0]);
                _this3.userInfo = JSON.parse(data.data).d[0];
                _this3.storageService.userInfo = JSON.parse(data.data).d[0];

                if (_this3.userInfo.resultCode == "0") {
                  _this3.showErrorMessage = true;
                } else {
                  _this3.loginCredentials = "";
                  console.log(_this3.userInfo);

                  if (_this3.userInfo.AppPassword == "") {
                    if (_this3.userInfo.DeviceId == "" || _this3.userInfo.DeviceId == _this3.deviceId) {
                      _this3.validationForForgotPassword();
                    } else if (_this3.userInfo.LoginStatus == "0" || _this3.userInfo.LoginStatus == "loggedOut") {
                      _this3.validationForForgotPassword();
                    } else {
                      _this3.showValidationMessage = true;
                    }
                  } else {
                    if (_this3.userInfo.DeviceId == "" || _this3.userInfo.DeviceId == _this3.deviceId) {
                      _this3.validationForPassword();
                    } else if (_this3.userInfo.LoginStatus == "0" || _this3.userInfo.LoginStatus == "loggedOut") {
                      _this3.validationForPassword();
                    } else {
                      _this3.showValidationMessage = true;
                    }
                  }
                }
              }, function (err) {
                return console.log(err);
              });
            } else {
              console.log("in else");
              this.alertService.createAlert("Check your internet connection before proceeding to login");
            }
          }
        }, {
          key: "validationForPassword",
          value: function validationForPassword() {
            var _this4 = this;

            this.router.navigateByUrl("/password-page");
            this.showErrorMessage = false;
            this.showValidationMessage = false;
            this.storageService.firstTimeLogin = false;
            this.databaseService.getDatabaseState().subscribe(function (ready) {
              if (ready) {
                _this4.userDetailsService.insertIntoCMSLogStatus();
              }
            });
          }
        }, {
          key: "validationForForgotPassword",
          value: function validationForForgotPassword() {
            var _this5 = this;

            this.router.navigateByUrl("/forgot-password-page");
            this.showErrorMessage = false;
            this.showValidationMessage = false;
            this.storageService.firstTimeLogin = true;
            this.databaseService.getDatabaseState().subscribe(function (ready) {
              if (ready) {
                _this5.userDetailsService.insertIntoCMSLogStatus();
              }
            });
          }
        }, {
          key: "validateUser",
          value: function validateUser() {}
        }]);

        return LoginPagePage;
      }();

      LoginPagePage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
        }, {
          type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_5__["HTTP"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"]
        }, {
          type: _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_6__["Device"]
        }, {
          type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_4__["ApiServiceService"]
        }, {
          type: _Services_alert_service_service__WEBPACK_IMPORTED_MODULE_8__["AlertServiceService"]
        }, {
          type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_9__["StorageServiceService"]
        }, {
          type: _Services_user_details_service__WEBPACK_IMPORTED_MODULE_10__["UserDetailsService"]
        }, {
          type: _Services_database_service__WEBPACK_IMPORTED_MODULE_11__["DatabaseService"]
        }, {
          type: src_app_Services_internet_service_service__WEBPACK_IMPORTED_MODULE_12__["InternetServiceService"]
        }];
      };

      LoginPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-login-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/login-page/login-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login-page.page.scss */
        "./src/app/Pages/login-page/login-page.page.scss"))["default"]]
      })], LoginPagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=Pages-login-page-login-page-module-es5.js.map