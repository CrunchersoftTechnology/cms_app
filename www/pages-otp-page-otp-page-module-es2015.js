(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-otp-page-otp-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/otp-page/otp-page.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/otp-page/otp-page.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-button (click)=\"onBackClick()\" fill=\"clear\" class=\"back-button\">\n  <ion-icon slot=\"icon-only\" name=\"chevron-back-outline\"></ion-icon>\n</ion-button>\n<div class=\"login\">\n  <div heading-column-lr *ngIf=\"forgotPassword\">\n    <h1 big-heading>Code</h1>\n    <p class=\"sub-line\">Authentication</p>\n    <!-- <h2 small-heading (click)=\"onRegisterClick()\">REGISTER</h2> -->\n    <p class=\"welcome\">Please enter security code</p>\n  </div>\n\n  <div heading-column-lr *ngIf=\"!forgotPassword\">\n    <h1 big-heading>Login</h1>\n    <p class=\"sub-line\">with Code</p>\n    <!-- <h2 small-heading (click)=\"onRegisterClick()\">REGISTER</h2> -->\n    <p class=\"welcome\">Please enter security code is {{securityCode}}</p>\n  </div>\n\n  <ion-label *ngIf=\"showSecurityCode\" color=\"danger\">Your security code is {{securityCode}}</ion-label>\n\n  <form name=\"loginForm\" #loginForm=\"ngForm\">\n    <ion-grid fixed class=\"ion-no-margin ion-no-padding\">\n      <ion-row>\n        <ion-col size=\"3\">\n          <ion-item transparent class=\"otp-field\">\n            <!-- <ion-icon icon-small name=\"person\" color=\"primary\"></ion-icon> -->\n            <ion-input\n              #otpField1\n              name=\"otp1\"\n              required\n              type=\"text\"\n              maxlength=\"1\"\n              [(ngModel)]=\"otp1\"\n              (keyup)=\"move(otpField2, $event)\"\n            ></ion-input> </ion-item\n        ></ion-col>\n        <ion-col size=\"3\">\n          <ion-item transparent class=\"otp-field\">\n            <!-- <ion-icon icon-small name=\"person\" color=\"primary\"></ion-icon> -->\n            <ion-input\n              #otpField2\n              name=\"otp2\"\n              required\n              type=\"text\"\n              maxlength=\"1\"\n              [(ngModel)]=\"otp2\"\n              (keyup)=\"move(otpField3, $event)\"\n            ></ion-input> </ion-item\n        ></ion-col>\n        <ion-col size=\"3\">\n          <ion-item transparent class=\"otp-field\">\n            <!-- <ion-icon icon-small name=\"person\" color=\"primary\"></ion-icon> -->\n            <ion-input\n              #otpField3\n              name=\"otp3\"\n              required\n              type=\"text\"\n              maxlength=\"1\"\n              [(ngModel)]=\"otp3\"\n              (keyup)=\"move(otpField4, $event)\"\n            ></ion-input> </ion-item\n        ></ion-col>\n        <ion-col size=\"3\">\n          <ion-item transparent class=\"otp-field\">\n            <!-- <ion-icon icon-small name=\"person\" color=\"primary\"></ion-icon> -->\n            <ion-input\n              #otpField4\n              name=\"otp4\"\n              required\n              type=\"text\"\n              maxlength=\"1\"\n              [(ngModel)]=\"otp4\"\n              (keyup)=\"move(otpField4, $event)\"\n            ></ion-input> </ion-item\n        ></ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <!-- <div *ngIf=\"emailId.invalid && (emailId.dirty || emailId.touched)\">\n      <ion-text color=\"danger\" *ngIf=\"emailId.errors.required\"\n        >Enter password</ion-text\n      >\n    </div> -->\n    <div *ngIf=\"showErrorMessage\">\n      <ion-text color=\"danger\">Invalid Code</ion-text>\n    </div>\n\n    <!-- <ion-label class=\"forgot-password\" (click)=\"onForgotPass()\"\n      >Forgot Password</ion-label\n    > -->\n    <div style=\"height: 30px\"></div>\n\n    <ion-button\n      *ngIf=\"forgotPassword\"\n      color=\"primary\"\n      class=\"ion-button-class\"\n      size=\"large\"\n      expand=\"block\"\n      fill=\"solid\"\n      shape=\"round\"\n      (click)=\"onContinueClick()\"\n    >\n      Continue\n    </ion-button>\n\n    <ion-button\n      *ngIf=\"!forgotPassword\"\n      color=\"primary\"\n      class=\"ion-button-class\"\n      size=\"large\"\n      expand=\"block\"\n      fill=\"solid\"\n      shape=\"round\"\n      (click)=\"onContinueClick1()\"\n    >\n      Continue\n    </ion-button>\n  </form>\n</div>\n\n<ion-label class=\"version\"> Version 1.0 </ion-label>\n");

/***/ }),

/***/ "./src/app/Services/toast-service.service.ts":
/*!***************************************************!*\
  !*** ./src/app/Services/toast-service.service.ts ***!
  \***************************************************/
/*! exports provided: ToastServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastServiceService", function() { return ToastServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let ToastServiceService = class ToastServiceService {
    constructor(toastController) {
        this.toastController = toastController;
    }
    createToast(message, duration) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let toast = yield this.toastController.create({
                animated: true,
                duration: duration,
                position: "bottom",
                message: message
            });
            yield toast.present();
        });
    }
};
ToastServiceService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
ToastServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ToastServiceService);



/***/ }),

/***/ "./src/app/pages/otp-page/otp-page-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/otp-page/otp-page-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: OtpPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpPagePageRoutingModule", function() { return OtpPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _otp_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./otp-page.page */ "./src/app/pages/otp-page/otp-page.page.ts");




const routes = [
    {
        path: '',
        component: _otp_page_page__WEBPACK_IMPORTED_MODULE_3__["OtpPagePage"]
    }
];
let OtpPagePageRoutingModule = class OtpPagePageRoutingModule {
};
OtpPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OtpPagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/otp-page/otp-page.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/otp-page/otp-page.module.ts ***!
  \***************************************************/
/*! exports provided: OtpPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpPagePageModule", function() { return OtpPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _otp_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./otp-page-routing.module */ "./src/app/pages/otp-page/otp-page-routing.module.ts");
/* harmony import */ var _otp_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./otp-page.page */ "./src/app/pages/otp-page/otp-page.page.ts");







let OtpPagePageModule = class OtpPagePageModule {
};
OtpPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _otp_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["OtpPagePageRoutingModule"]
        ],
        declarations: [_otp_page_page__WEBPACK_IMPORTED_MODULE_6__["OtpPagePage"]]
    })
], OtpPagePageModule);



/***/ }),

/***/ "./src/app/pages/otp-page/otp-page.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/otp-page/otp-page.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".login {\n  position: absolute;\n  left: 50%;\n  top: 40%;\n  transform: translate(-49%, -49%);\n  width: 90%;\n}\n\n.registration {\n  position: absolute;\n  top: 2%;\n  left: 5%;\n  width: 90%;\n}\n\n.login-header {\n  background: var(--ion-color-primary);\n}\n\n.logo-image {\n  display: block;\n  width: 120px;\n  margin: 0 auto;\n  text-align: center;\n}\n\n[heading-column-lr] {\n  display: flex;\n  align-items: center;\n  margin-bottom: 37vw;\n}\n\n[big-heading] {\n  font-size: 12vw !important;\n  color: var(--ion-color-dark);\n}\n\n[small-heading] {\n  margin-left: auto;\n  padding-right: 40px;\n  font-size: 15px;\n}\n\n[transparent] {\n  background: transparent;\n}\n\nion-input {\n  font-size: 8vw;\n  font-weight: 500;\n  text-align: center;\n}\n\n[icon-small] {\n  width: 30px;\n}\n\nion-item {\n  --highlight-color-focused: transparent;\n  --highlight-height: 0;\n  --highlight-color-invalid: #000;\n  --highlight-color-valid: #ddd;\n  --min-height: 65px;\n  --padding-start: 0;\n}\n\nion-label {\n  color: #000;\n  opacity: 0.8;\n  font-size: 13px;\n  display: block;\n}\n\n.forgot-password {\n  color: var(--ion-color-primary);\n  font-size: 13px;\n  display: block;\n  text-align: right;\n  padding: 10px;\n}\n\n.link-button {\n  color: var(--ion-color-primary);\n}\n\n.demo-login {\n  color: var(--ion-color-primary);\n  margin-left: 80px;\n}\n\n.chbox {\n  margin-right: 5px;\n}\n\n.label {\n  margin-top: 10px;\n  position: absolute;\n  left: 30%;\n}\n\n.label-text {\n  font-size: 5vw;\n  font-weight: bold;\n  text-decoration: underline;\n}\n\n.ion-button-class {\n  margin-top: 6vw;\n  width: 98%;\n  font-size: 4vw;\n}\n\n.welcome {\n  position: absolute;\n  top: 30%;\n  font-size: 4.5vw;\n  color: gray;\n  font-weight: 400;\n}\n\n.version {\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  bottom: 4%;\n  font-size: 4vw;\n  color: gray;\n  font-weight: 300;\n}\n\n.forgot-pass {\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  bottom: 15%;\n  font-size: 4vw;\n  color: black;\n  font-weight: 400;\n}\n\n.sub-line {\n  margin: 0;\n  font-weight: 500;\n  font-size: 12vw;\n  position: absolute;\n  top: 19%;\n}\n\n.otp-field {\n  margin-left: 1vw;\n  margin-right: 1vw;\n}\n\n.back-button {\n  position: absolute;\n  top: 7%;\n  left: -3%;\n  font-size: 6vw;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb3RwLXBhZ2Uvb3RwLXBhZ2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUVBLGdDQUFBO0VBQ0EsVUFBQTtBQUpKOztBQU9BO0VBQ0ksa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7QUFKSjs7QUFPQTtFQUNJLG9DQUFBO0FBSko7O0FBT0E7RUFDSSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQUpKOztBQU9BO0VBQ0ksYUFBQTtFQUVBLG1CQUFBO0VBQ0EsbUJBQUE7QUFKSjs7QUFPQTtFQUNJLDBCQUFBO0VBQ0EsNEJBQUE7QUFKSjs7QUFPQTtFQUNJLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBSko7O0FBT0E7RUFDSSx1QkFBQTtBQUpKOztBQU9BO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFKSjs7QUFPQTtFQUNJLFdBQUE7QUFKSjs7QUFPQTtFQUNJLHNDQUFBO0VBQ0EscUJBQUE7RUFDQSwrQkFBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQUpKOztBQU9BO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQUpKOztBQU9BO0VBQ0ksK0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtBQUpKOztBQU9BO0VBQ0ksK0JBQUE7QUFKSjs7QUFPQTtFQUNJLCtCQUFBO0VBQ0EsaUJBQUE7QUFKSjs7QUFPQTtFQUNJLGlCQUFBO0FBSko7O0FBT0E7RUFDSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtBQUpKOztBQU9BO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0VBQ0EsMEJBQUE7QUFKSjs7QUFPQTtFQUNJLGVBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtBQUpKOztBQU9BO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUFKSjs7QUFPQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0EsVUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUFKSjs7QUFPQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFKSjs7QUFPQTtFQUNJLFNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7QUFKSjs7QUFPQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7QUFKSjs7QUFPQTtFQUNJLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtBQUpKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvb3RwLXBhZ2Uvb3RwLXBhZ2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gLmNoYm94IHtcclxuLy8gICAgIG1hcmdpbjogM3B4O1xyXG4vLyAgICAgcGFkZGluZzogMHB4O1xyXG4vLyAgfVxyXG5cclxuLmxvZ2luIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHRvcDogNDAlO1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNDklLCAtNDklKTtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC00OSUsIC00OSUpO1xyXG4gICAgd2lkdGg6IDkwJVxyXG59XHJcblxyXG4ucmVnaXN0cmF0aW9uIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMiU7XHJcbiAgICBsZWZ0OiA1JTtcclxuICAgIHdpZHRoOiA5MCVcclxufVxyXG5cclxuLmxvZ2luLWhlYWRlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbn1cclxuXHJcbi5sb2dvLWltYWdlIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbltoZWFkaW5nLWNvbHVtbi1scl0ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzd2dztcclxufVxyXG5cclxuW2JpZy1oZWFkaW5nXSB7XHJcbiAgICBmb250LXNpemU6IDEydncgIWltcG9ydGFudDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyayk7XHJcbn1cclxuXHJcbltzbWFsbC1oZWFkaW5nXSB7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIHBhZGRpbmctcmlnaHQ6IDQwcHg7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbn1cclxuXHJcblt0cmFuc3BhcmVudF0ge1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbn1cclxuXHJcbmlvbi1pbnB1dCB7XHJcbiAgICBmb250LXNpemU6IDh2dztcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbltpY29uLXNtYWxsXSB7XHJcbiAgICB3aWR0aDogMzBweDtcclxufVxyXG5cclxuaW9uLWl0ZW0ge1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XHJcbiAgICAtLWhpZ2hsaWdodC1jb2xvci1pbnZhbGlkOiAjMDAwO1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6ICNkZGQ7XHJcbiAgICAtLW1pbi1oZWlnaHQ6IDY1cHg7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XHJcbn1cclxuXHJcbmlvbi1sYWJlbCB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIG9wYWNpdHk6IC44O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5mb3Jnb3QtcGFzc3dvcmQge1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBwYWRkaW5nOiAxMHB4XHJcbn1cclxuXHJcbi5saW5rLWJ1dHRvbiB7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpXHJcbn1cclxuXHJcbi5kZW1vLWxvZ2luIHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICBtYXJnaW4tbGVmdDogODBweDtcclxufVxyXG5cclxuLmNoYm94IHtcclxuICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG59XHJcblxyXG4ubGFiZWwge1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDMwJTtcclxufVxyXG5cclxuLmxhYmVsLXRleHQge1xyXG4gICAgZm9udC1zaXplOiA1dnc7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG59XHJcblxyXG4uaW9uLWJ1dHRvbi1jbGFzcyB7XHJcbiAgICBtYXJnaW4tdG9wOiA2dnc7XHJcbiAgICB3aWR0aDogOTglO1xyXG4gICAgZm9udC1zaXplOiA0dndcclxufVxyXG5cclxuLndlbGNvbWUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMCU7XHJcbiAgICBmb250LXNpemU6IDQuNXZ3O1xyXG4gICAgY29sb3I6IGdyYXk7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG4udmVyc2lvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIGJvdHRvbTogNCU7XHJcbiAgICBmb250LXNpemU6IDR2dztcclxuICAgIGNvbG9yOiBncmF5O1xyXG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcclxufVxyXG5cclxuLmZvcmdvdC1wYXNzIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgYm90dG9tOiAxNSU7XHJcbiAgICBmb250LXNpemU6IDR2dztcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxuXHJcbi5zdWItbGluZSB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgZm9udC1zaXplOiAxMnZ3O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAxOSU7XHJcbn1cclxuXHJcbi5vdHAtZmllbGQge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDF2dztcclxuICAgIG1hcmdpbi1yaWdodDogMXZ3O1xyXG59XHJcblxyXG4uYmFjay1idXR0b24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA3JTtcclxuICAgIGxlZnQ6IC0zJTtcclxuICAgIGZvbnQtc2l6ZTogNnZ3O1xyXG4gICAgbWFyZ2luOiAwO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/pages/otp-page/otp-page.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/otp-page/otp-page.page.ts ***!
  \*************************************************/
/*! exports provided: OtpPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpPagePage", function() { return OtpPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");
/* harmony import */ var src_app_Services_otp_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/otp-service.service */ "./src/app/Services/otp-service.service.ts");
/* harmony import */ var src_app_Services_user_details_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/user-details.service */ "./src/app/Services/user-details.service.ts");
/* harmony import */ var src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/Services/api-service.service */ "./src/app/Services/api-service.service.ts");
/* harmony import */ var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../Services/toast-service.service */ "./src/app/Services/toast-service.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_Services_alert_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/Services/alert-service.service */ "./src/app/Services/alert-service.service.ts");









let OtpPagePage = class OtpPagePage {
    constructor(navController, alertController, storageService, otpService, userDetailsService, apiService, toastService, alertService) {
        this.navController = navController;
        this.alertController = alertController;
        this.storageService = storageService;
        this.otpService = otpService;
        this.userDetailsService = userDetailsService;
        this.apiService = apiService;
        this.toastService = toastService;
        this.alertService = alertService;
        this.showErrorMessage = false;
        this.showSecurityCode = false;
    }
    ngOnInit() {
        this.forgotPassword = this.storageService.forgotPassword;
        if (!this.forgotPassword) {
            if (this.storageService.userInfo.SecurityCode == 0) {
                this.securityCode = Math.floor(1000 + Math.random() * 9000);
                console.log(this.securityCode);
                this.apiService
                    .setSecurityCode(this.storageService.userInfo.ClientId, this.storageService.userInfo.UserId, this.securityCode)
                    .then((_) => {
                    // this.createAlert(`Your security code id ${this.securityCode}`);
                    this.showSecurityCode = true;
                    setTimeout(() => {
                        this.otpField1.setFocus();
                    }, 500);
                });
            }
            else {
                this.securityCode = this.storageService.userInfo.SecurityCode;
            }
        }
        else if (this.forgotPassword) {
            this.securityCode = Math.floor(1000 + Math.random() * 9000);
            console.log("security code", this.securityCode);
            this.toastService
                .createToast("Sending OTP check your mail...", 3000)
                .then((_) => {
                this.apiService
                    .sendOTPThroughMail(this.storageService.userInfo.Email, this.securityCode)
                    .then((data) => {
                    console.log("data", data.data);
                    this.toastService
                        .createToast("OTP sent successfully", 3000)
                        .then((_) => {
                        setTimeout(() => {
                            this.otpField1.setFocus();
                        }, 500);
                    });
                }, (err) => this.toastService.createToast("Failed to send OTP", 3000));
            });
        }
        // console.log(this.number);
        // console.log(this.storageService.userInfo.StudentContact);
        // this.otpService.sendOtp(
        //   this.storageService.userInfo.StudentContact,
        //   this.randomNumber
        // );
    }
    // ionViewDidEnter() {
    //   console.log("view enter", this.otpField1);
    //   // this.otpField1.nativeElement.focus();
    // }
    // ionViewDidLoad() {
    //   console.log("view load", this.otpField1);
    // }
    onContinueClick() {
        let enteredOTP = Number.parseInt(`${this.otp1}${this.otp2}${this.otp3}${this.otp4}`);
        console.log(enteredOTP);
        if (enteredOTP == this.securityCode) {
            this.apiService
                .createPassword(this.storageService.userInfo.UserId, this.storageService.password)
                .then((data) => {
                console.log(JSON.parse(data.data).d[0]);
                if (JSON.parse(data.data).d[0] == null) {
                    this.toastService.createToast("Password changed successfully", 3000);
                    this.userDetailsService.getStudentAccess(this.storageService.userInfo.UserId);
                    // this.router.navigateByUrl("/home-page");
                }
            });
        }
        else
            this.showErrorMessage = true;
    }
    onContinueClick1() {
        let enteredOTP = Number.parseInt(`${this.otp1}${this.otp2}${this.otp3}${this.otp4}`);
        console.log(enteredOTP);
        if (enteredOTP == this.securityCode) {
            this.userDetailsService.getStudentAccess(this.storageService.userInfo.UserId);
        }
        else
            this.showErrorMessage = true;
    }
    move(next, event) {
        console.log("key", event.key);
        if (event.key != "Backspace")
            next.setFocus();
        // let length = from.length;
        // if(length == 1) {
        //   to.setFocus();
        // }
    }
    createAlert(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                animated: true,
                backdropDismiss: false,
                subHeader: message,
                buttons: [
                    {
                        text: "Ok",
                        role: "cancel",
                    },
                ],
                cssClass: "alert-title",
            });
            yield alert.present();
            alert.onDidDismiss().then((_) => {
                setTimeout(() => {
                    this.otpField1.setFocus();
                }, 500);
            });
        });
    }
    onBackClick() {
        this.navController.back();
    }
};
OtpPagePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: src_app_Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__["StorageServiceService"] },
    { type: src_app_Services_otp_service_service__WEBPACK_IMPORTED_MODULE_3__["OtpServiceService"] },
    { type: src_app_Services_user_details_service__WEBPACK_IMPORTED_MODULE_4__["UserDetailsService"] },
    { type: src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_5__["ApiServiceService"] },
    { type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_6__["ToastServiceService"] },
    { type: src_app_Services_alert_service_service__WEBPACK_IMPORTED_MODULE_8__["AlertServiceService"] }
];
OtpPagePage.propDecorators = {
    otpField1: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ["otpField1",] }]
};
OtpPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-otp-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./otp-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/otp-page/otp-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./otp-page.page.scss */ "./src/app/pages/otp-page/otp-page.page.scss")).default]
    })
], OtpPagePage);



/***/ })

}]);
//# sourceMappingURL=pages-otp-page-otp-page-module-es2015.js.map