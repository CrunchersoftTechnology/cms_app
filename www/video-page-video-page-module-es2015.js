(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["video-page-video-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/video-page/video-page.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/video-page/video-page.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar\n  color=\"primary\"\n  mode=\"ios\"\n  *ngIf=\"storageService.instituteName.length > 0\"\n>\n  <ion-title>{{storageService.instituteName}}</ion-title>\n</ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons class=\"menuButton\" slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Videos</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content\n      pullingIcon=\"chevron-down-circle-outline\"\n      pullingText=\"Pull to refresh\"\n      refreshingSpinner=\"circles\"\n      refreshingText=\"Refreshing...\"\n    >\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-item>\n    <ion-select\n      [(ngModel)]=\"selectedSubject\"\n      interface=\"popover\"\n      (ionChange)=\"onSelectChange()\"\n    >\n      <ion-select-option\n        *ngFor=\"let sub of subjectDetails\"\n        [value]=\"sub.subjectId\"\n        >{{sub.subjectName}}</ion-select-option\n      >\n    </ion-select>\n  </ion-item>\n\n  <ion-card\n    class=\"ion-margin ion-padding\"\n    *ngFor=\"let video of videoInfo\"\n    (click)=\"onVideoClick(video.Link)\"\n  >\n    <ion-item lines=\"none\" class=\"ion-no-margin ion-no-padding\">\n      <ion-thumbnail slot=\"start\">\n        <ion-img\n          [src]=\"'http://test.onlineexamkatta.com/Images/VideoCourseLinkImage/' + video.FileName\"\n        ></ion-img>\n      </ion-thumbnail>\n      <div>\n        {{video.Description}}\n      \n   \n  <br/>\n        <span class=\"vid-discri\">{{video.AttachmentDescription}}</span></div\n      >\n      \n    </ion-item>\n  </ion-card>\n</ion-content>\n\n<!-- <ion-button (click)=\"onClick('SsrYSeg371s')\" expand=\"block\" fill=\"clear\" shape=\"round\">\n  Open youtube\n</ion-button> -->\n\n<!-- <head>\n  <script src=\"https://cdn.plyr.io/3.6.2/plyr.js\"></script>\n  <link rel=\"stylesheet\" href=\"https://cdn.plyr.io/3.6.2/plyr.css\" />\n</head>\n\n<body>\n PLYR\n  <div class=\"plyr__video-embed\" id=\"player\">\n    <iframe\n      width=\"853\"\n      height=\"480\"\n      src=\"https://www.youtube.com/watch?v=SsrYSeg371s&feature=youtu.be?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1\"\n      allowfullscreen\n      allowtransparency\n      allow=\"autoplay\"\n    ></iframe>\n  </div>\n\n  <script>\n    const controls = [\n      \"play-large\", // The large play button in the center\n      //'restart', // Restart playback\n      \"rewind\", // Rewind by the seek time (default 10 seconds)\n      \"play\", // Play/pause playback\n      \"fast-forward\", // Fast forward by the seek time (default 10 seconds)\n      \"progress\", // The progress bar and scrubber for playback and buffering\n      \"current-time\", // The current time of playback\n      \"duration\", // The full duration of the media\n      \"mute\", // Toggle mute\n      \"volume\", // Volume control\n\n      \"fullscreen\", // Toggle fullscreen\n    ];\n\n    const player = new Plyr(\"#player\", {\n      controls,\n      settings: [\"quality\"],\n      quality: {\n        default: \"1080\",\n      },\n    });\n    var player = new Plyr(\"#player\", { controls });\n  </script>\n</body> -->\n");

/***/ }),

/***/ "./src/app/pages/video-page/video-page-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/video-page/video-page-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: VideoPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoPagePageRoutingModule", function() { return VideoPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _video_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./video-page.page */ "./src/app/pages/video-page/video-page.page.ts");




const routes = [
    {
        path: '',
        component: _video_page_page__WEBPACK_IMPORTED_MODULE_3__["VideoPagePage"]
    }
];
let VideoPagePageRoutingModule = class VideoPagePageRoutingModule {
};
VideoPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], VideoPagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/video-page/video-page.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/video-page/video-page.module.ts ***!
  \*******************************************************/
/*! exports provided: VideoPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoPagePageModule", function() { return VideoPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _video_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./video-page-routing.module */ "./src/app/pages/video-page/video-page-routing.module.ts");
/* harmony import */ var _video_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./video-page.page */ "./src/app/pages/video-page/video-page.page.ts");







let VideoPagePageModule = class VideoPagePageModule {
};
VideoPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _video_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["VideoPagePageRoutingModule"]
        ],
        declarations: [_video_page_page__WEBPACK_IMPORTED_MODULE_6__["VideoPagePage"]]
    })
], VideoPagePageModule);



/***/ }),

/***/ "./src/app/pages/video-page/video-page.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/video-page/video-page.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".vid-discri {\n  color: darkgray;\n  font-size: 13px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdmlkZW8tcGFnZS92aWRlby1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7RUFDQSxlQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy92aWRlby1wYWdlL3ZpZGVvLXBhZ2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnZpZC1kaXNjcml7XHJcbiAgICBjb2xvcjogZGFya2dyYXk7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/video-page/video-page.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/video-page/video-page.page.ts ***!
  \*****************************************************/
/*! exports provided: VideoPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoPagePage", function() { return VideoPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/streaming-media/ngx */ "./node_modules/@ionic-native/streaming-media/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");
/* harmony import */ var src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/api-service.service */ "./src/app/Services/api-service.service.ts");
/* harmony import */ var _ionic_native_app_availability_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/app-availability/ngx */ "./node_modules/@ionic-native/app-availability/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_youtube_video_player_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/youtube-video-player/ngx */ "./node_modules/@ionic-native/youtube-video-player/__ivy_ngcc__/ngx/index.js");








let VideoPagePage = class VideoPagePage {
    // apiKey = "AIzaSyB1IxCfQeRjZOYn744r3Zdl3w0grnITHz8"
    constructor(storageService, apiService, streamingMedia, youtube, appAvailability, inAppBrowser) {
        this.storageService = storageService;
        this.apiService = apiService;
        this.streamingMedia = streamingMedia;
        this.youtube = youtube;
        this.appAvailability = appAvailability;
        this.inAppBrowser = inAppBrowser;
        this.subjects = [];
        this.selectedSubjects = [];
        this.subjectDetails = [];
        this.videoInfo = [];
        this.selectedSubject = "0";
        this.branchId = "";
        this.classId = "";
    }
    ngOnInit() { }
    ionViewDidEnter() {
        this.branchId = this.storageService.userDetails.branchId;
        this.classId = this.storageService.userDetails.classId;
        this.subjects = this.storageService.userDetails.subjectName.split(",");
        this.selectedSubjects = this.storageService.userDetails.selectedSubjects.split(",");
        this.subjectDetails = [];
        for (let i = 0; i < this.subjects.length; i++) {
            this.subjectDetails.push({
                subjectId: this.selectedSubjects[i],
                subjectName: this.subjects[i],
            });
        }
        this.selectedSubject = this.subjectDetails[0].subjectId;
        this.getVideoList(this.branchId, this.classId, this.selectedSubject);
    }
    onSelectChange() {
        this.getVideoList(this.branchId, this.classId, this.selectedSubject);
    }
    getVideoList(branchId, classId, subjectId) {
        this.apiService.getVideo(branchId, classId, subjectId).then((data) => {
            console.log(JSON.parse(data.data));
            this.videoInfo = JSON.parse(data.data).d;
        });
    }
    onVideoClick(vidoeUrl) {
        // let options: StreamingVideoOptions = {
        //   orientation: "landscape",
        //   controls: true,
        //   successCallback: () => console.log("successfully played"),
        //   errorCallback: (e) => console.log("Error", e),
        // };
        // this.streamingMedia.playVideo(link, options);
        // this.youtube.openVideo(link);
        const browser = this.inAppBrowser.create(vidoeUrl, "_system");
    }
    doRefresh(event) {
        this.getVideoList(this.branchId, this.classId, this.selectedSubject);
        setTimeout(() => {
            event.target.complete();
        }, 2000);
    }
};
VideoPagePage.ctorParameters = () => [
    { type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__["StorageServiceService"] },
    { type: src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_4__["ApiServiceService"] },
    { type: _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_2__["StreamingMedia"] },
    { type: _ionic_native_youtube_video_player_ngx__WEBPACK_IMPORTED_MODULE_7__["YoutubeVideoPlayer"] },
    { type: _ionic_native_app_availability_ngx__WEBPACK_IMPORTED_MODULE_5__["AppAvailability"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__["InAppBrowser"] }
];
VideoPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-video-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./video-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/video-page/video-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./video-page.page.scss */ "./src/app/pages/video-page/video-page.page.scss")).default]
    })
], VideoPagePage);



/***/ })

}]);
//# sourceMappingURL=video-page-video-page-module-es2015.js.map