(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-page-home-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-page/home-page.page.html":
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-page/home-page.page.html ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesHomePageHomePagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons class=\"menuButton\" slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Home</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-label class=\"welcome\" color=\"primary\">{{welcomeText.welcomeText}}</ion-label>\n\n   <img class=\"image\" src=\"{{welcomeText.welcomeImg}}\" />\n   \n  <!--<ion-label class=\"heading\" color=\"primary\">P K Foundation's</ion-label>\n  <ion-label class=\"sub-heading\" color=\"primary\"\n    >JEE, NEET, MHT-CET Preparatory Course</ion-label\n  > -->\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/home-page/home-page-routing.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/home-page/home-page-routing.module.ts ***!
      \*************************************************************/

    /*! exports provided: HomePagePageRoutingModule */

    /***/
    function srcAppPagesHomePageHomePageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePagePageRoutingModule", function () {
        return HomePagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _home_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./home-page.page */
      "./src/app/pages/home-page/home-page.page.ts");

      var routes = [{
        path: '',
        component: _home_page_page__WEBPACK_IMPORTED_MODULE_3__["HomePagePage"]
      }];

      var HomePagePageRoutingModule = function HomePagePageRoutingModule() {
        _classCallCheck(this, HomePagePageRoutingModule);
      };

      HomePagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HomePagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/home-page/home-page.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/home-page/home-page.module.ts ***!
      \*****************************************************/

    /*! exports provided: HomePagePageModule */

    /***/
    function srcAppPagesHomePageHomePageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePagePageModule", function () {
        return HomePagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _home_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./home-page-routing.module */
      "./src/app/pages/home-page/home-page-routing.module.ts");
      /* harmony import */


      var _home_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./home-page.page */
      "./src/app/pages/home-page/home-page.page.ts");

      var HomePagePageModule = function HomePagePageModule() {
        _classCallCheck(this, HomePagePageModule);
      };

      HomePagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _home_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePagePageRoutingModule"]],
        declarations: [_home_page_page__WEBPACK_IMPORTED_MODULE_6__["HomePagePage"]]
      })], HomePagePageModule);
      /***/
    },

    /***/
    "./src/app/pages/home-page/home-page.page.scss":
    /*!*****************************************************!*\
      !*** ./src/app/pages/home-page/home-page.page.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesHomePageHomePagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".welcome {\n  position: absolute;\n  bottom: 20%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  font-size: 8vw;\n  font-weight: 30;\n  font-family: serif;\n}\n\n.image {\n  position: absolute;\n  left: 50%;\n  top: 27%;\n  transform: translatex(-50%);\n  height: 60vw;\n}\n\n.heading {\n  position: absolute;\n  top: 62%;\n  left: 50%;\n  transform: translatex(-50%);\n  font-size: 8vw;\n  font-weight: bold;\n  font-family: serif;\n  width: 100%;\n  text-align: center;\n}\n\n.sub-heading {\n  position: absolute;\n  top: 67%;\n  left: 50%;\n  transform: translatex(-50%);\n  font-size: 5vw;\n  font-family: serif;\n  width: 100%;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1wYWdlL2hvbWUtcGFnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsMkJBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsMkJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLDJCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lLXBhZ2UvaG9tZS1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53ZWxjb21lIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJvdHRvbTogMjAlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICBmb250LXNpemU6IDh2dztcclxuICAgIGZvbnQtd2VpZ2h0OiAzMDtcclxuICAgIGZvbnQtZmFtaWx5OiBzZXJpZjtcclxufVxyXG5cclxuLmltYWdlIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHRvcDogMjclO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGV4KC01MCUpO1xyXG4gICAgaGVpZ2h0OiA2MHZ3O1xyXG59IFxyXG5cclxuLmhlYWRpbmcge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA2MiU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXgoLTUwJSk7XHJcbiAgICBmb250LXNpemU6IDh2dztcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1mYW1pbHk6IHNlcmlmO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zdWItaGVhZGluZyB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDY3JTtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRleCgtNTAlKTtcclxuICAgIGZvbnQtc2l6ZTogNXZ3O1xyXG4gICAgZm9udC1mYW1pbHk6IHNlcmlmO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/home-page/home-page.page.ts":
    /*!***************************************************!*\
      !*** ./src/app/pages/home-page/home-page.page.ts ***!
      \***************************************************/

    /*! exports provided: HomePagePage */

    /***/
    function srcAppPagesHomePageHomePagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePagePage", function () {
        return HomePagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/app/Services/api-service.service */
      "./src/app/Services/api-service.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var src_app_Services_internet_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/Services/internet-service.service */
      "./src/app/Services/internet-service.service.ts");
      /* harmony import */


      var src_app_Services_storage_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _Services_loading_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./../../Services/loading-service.service */
      "./src/app/Services/loading-service.service.ts");
      /* harmony import */


      var _Services_database_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./../../Services/database.service */
      "./src/app/Services/database.service.ts");
      /* harmony import */


      var _Services_user_details_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./../../Services/user-details.service */
      "./src/app/Services/user-details.service.ts");
      /* harmony import */


      var _Services_attendance_service_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./../../Services/attendance-service.service */
      "./src/app/Services/attendance-service.service.ts");
      /* harmony import */


      var _Services_fees_service_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./../../Services/fees-service.service */
      "./src/app/Services/fees-service.service.ts");
      /* harmony import */


      var src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! src/app/Services/notification-service.service */
      "./src/app/Services/notification-service.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @ionic-native/status-bar/ngx */
      "./node_modules/@ionic-native/status-bar/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _Services_save_test_info_service_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! ./../../Services/save-test-info-service.service */
      "./src/app/Services/save-test-info-service.service.ts");
      /* harmony import */


      var _Services_offline_result_service_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! ./../../Services/offline-result-service.service */
      "./src/app/Services/offline-result-service.service.ts");
      /* harmony import */


      var _Services_daily_practice_paper_service_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! ./../../Services/daily-practice-paper-service.service */
      "./src/app/Services/daily-practice-paper-service.service.ts");
      /* harmony import */


      var _Services_time_table_service_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! ./../../Services/time-table-service.service */
      "./src/app/Services/time-table-service.service.ts");

      var HomePagePage = /*#__PURE__*/function () {
        function HomePagePage(router, storage, statusBar, loadingController, menuController, alertController, storageService, loadingService, databaseService, userDetailsService, attendanceService, feesService, notificationService, internetService, apiServiceService, saveTestInfoServiceService, offlineService, dailyPracticePaperService, timeTableService, apiService) {
          _classCallCheck(this, HomePagePage);

          this.router = router;
          this.storage = storage;
          this.statusBar = statusBar;
          this.loadingController = loadingController;
          this.menuController = menuController;
          this.alertController = alertController;
          this.storageService = storageService;
          this.loadingService = loadingService;
          this.databaseService = databaseService;
          this.userDetailsService = userDetailsService;
          this.attendanceService = attendanceService;
          this.feesService = feesService;
          this.notificationService = notificationService;
          this.internetService = internetService;
          this.apiServiceService = apiServiceService;
          this.saveTestInfoServiceService = saveTestInfoServiceService;
          this.offlineService = offlineService;
          this.dailyPracticePaperService = dailyPracticePaperService;
          this.timeTableService = timeTableService;
          this.apiService = apiService;
          this.welcomeText = {
            welcomeText: "",
            welcomeImg: ""
          };
        }

        _createClass(HomePagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.databaseService.getDatabaseState().subscribe(function (ready) {
              if (ready) _this.userDetailsService.selectFromCMSRegistrationDetails().then(function (data) {
                _this.storageService.userDetails = data;
                console.log("user details home page", _this.storageService.userDetails);

                _this.apiService.getAboutUsDetails().then(function (data) {
                  console.log(JSON.parse(data.data).d[0]);
                  var abotUsData = JSON.parse(data.data).d[0];
                  console.log("umesh......." + abotUsData.WelcomeText);
                  _this.welcomeText.welcomeText = abotUsData.WelcomeText;
                  _this.welcomeText.welcomeImg = _this.apiService.fileUrl + "pdf/HomePageImage/" + abotUsData.FileName;
                });

                _this.apiService.checkActiveStatus(_this.storageService.userDetails.userId).then(function (data) {
                  console.log("Activation Status", JSON.parse(data.data).d[0].IsActive);

                  if (JSON.parse(data.data).d[0].IsActive == "True") {
                    _this.saveTestInfoServiceService.getSavedTestsCount().then(function (data) {
                      if (data == 0) {
                        // this.loadingService.createLoading('Fetching your tests...')
                        setTimeout(function (_) {
                          return _this.userDetailsService.getSavedTests(_this.storageService.userInfo.UserId, _this.storageService.userInfo.ClientId);
                        }, 3000);
                      }
                    });
                  } else {
                    _this.onAccoutDeactivated();
                  }
                });
              });
            });
            this.storage.get("token").then(function (token) {
              return _this.storageService.tokenId = token;
            });
          }
        }, {
          key: "onAccoutDeactivated",
          value: function onAccoutDeactivated() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this2 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.alertController.create({
                        header: "Accout Deactivated",
                        subHeader: "Your accout is deactivated",
                        cssClass: "alert-title",
                        buttons: [{
                          text: "Ok",
                          handler: function handler() {
                            _this2.handleLogout();
                          }
                        }]
                      });

                    case 2:
                      alert = _context.sent;
                      _context.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "onLoginWithOTP",
          value: function onLoginWithOTP() {
            if (this.internetService.networkConnected) {
              this.apiService.getAboutUsDetails().then(function (data) {
                console.log("umesh.......");
                console.log(JSON.parse(data.data));
              });
            }
          }
        }, {
          key: "handleLogout",
          value: function handleLogout() {
            var _this3 = this;

            this.loadingService.createLoading("Loging Out...").then(function (_) {
              _this3.databaseService.getDatabaseState().subscribe(function (ready) {
                if (ready) _this3.userDetailsService.deleteFromCMSLogStatus().then(function (_) {
                  return _this3.userDetailsService.deleteFromCMSRegistrationDetails().then(function (_) {
                    return _this3.attendanceService.deleteFromCMSAttendance().then(function (_) {
                      return _this3.attendanceService.deleteFromCMSAttendanceCount().then(function (_) {
                        return _this3.feesService.deleteFromCMSFeesHistory().then(function (_) {
                          return _this3.feesService.deleteFromCMSFeesInfo().then(function (_) {
                            return _this3.notificationService.deleteFromCMSNotificationId().then(function (_) {
                              return _this3.notificationService.deleteFromCMSNotificationMessage().then(function (_) {
                                return _this3.notificationService.deleteFromCMSNotificationMessageTest().then(function (_) {
                                  return _this3.notificationService.deleteFromNotification().then(function (_) {
                                    return _this3.notificationService.deleteFromOnlineTestNotifications().then(function (_) {
                                      return _this3.saveTestInfoServiceService.deleteFromSaveTestInfo().then(function (_) {
                                        return _this3.offlineService.deleteFromCMSResults().then(function (_) {
                                          return _this3.dailyPracticePaperService.deleteFromCMSDailyPracticePaper().then(function (_) {
                                            return _this3.timeTableService.deleteFromCMSTimeTable().then(function (_) {
                                              _this3.storage.ready().then(function (ready) {
                                                if (ready) _this3.storage.set("userLoggedIn", false);
                                              });

                                              _this3.apiServiceService.setToken(_this3.storageService.userDetails.userId, "");

                                              _this3.apiServiceService.removeSecurityCode(_this3.storageService.userDetails.clientId, _this3.storageService.userDetails.userId).then(function (_) {
                                                return _this3.loadingController.dismiss().then(function (_) {
                                                  return _this3.router.navigateByUrl("/login-page").then(function () {
                                                    _this3.statusBar.backgroundColorByHexString("#ffffff");

                                                    _this3.statusBar.styleDefault();
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          }
        }]);

        return HomePagePage;
      }();

      HomePagePage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_12__["Router"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_14__["Storage"]
        }, {
          type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_13__["StatusBar"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]
        }, {
          type: src_app_Services_storage_service_service__WEBPACK_IMPORTED_MODULE_4__["StorageServiceService"]
        }, {
          type: _Services_loading_service_service__WEBPACK_IMPORTED_MODULE_6__["LoadingServiceService"]
        }, {
          type: _Services_database_service__WEBPACK_IMPORTED_MODULE_7__["DatabaseService"]
        }, {
          type: _Services_user_details_service__WEBPACK_IMPORTED_MODULE_8__["UserDetailsService"]
        }, {
          type: _Services_attendance_service_service__WEBPACK_IMPORTED_MODULE_9__["AttendanceServiceService"]
        }, {
          type: _Services_fees_service_service__WEBPACK_IMPORTED_MODULE_10__["FeesServiceService"]
        }, {
          type: src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_11__["NotificationServiceService"]
        }, {
          type: src_app_Services_internet_service_service__WEBPACK_IMPORTED_MODULE_3__["InternetServiceService"]
        }, {
          type: src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_1__["ApiServiceService"]
        }, {
          type: _Services_save_test_info_service_service__WEBPACK_IMPORTED_MODULE_15__["SaveTestInfoServiceService"]
        }, {
          type: _Services_offline_result_service_service__WEBPACK_IMPORTED_MODULE_16__["OfflineResultServiceService"]
        }, {
          type: _Services_daily_practice_paper_service_service__WEBPACK_IMPORTED_MODULE_17__["DailyPracticePaperServiceService"]
        }, {
          type: _Services_time_table_service_service__WEBPACK_IMPORTED_MODULE_18__["TimeTableServiceService"]
        }, {
          type: src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_1__["ApiServiceService"]
        }];
      };

      HomePagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: "app-home-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./home-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-page/home-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./home-page.page.scss */
        "./src/app/pages/home-page/home-page.page.scss"))["default"]]
      })], HomePagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=home-page-home-page-module-es5.js.map