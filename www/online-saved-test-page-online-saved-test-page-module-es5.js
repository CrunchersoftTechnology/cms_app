(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["online-saved-test-page-online-saved-test-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-saved-test-page/online-saved-test-page.page.html":
    /*!*********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-saved-test-page/online-saved-test-page.page.html ***!
      \*********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesOnlineSavedTestPageOnlineSavedTestPagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar\n  color=\"primary\"\n  mode=\"ios\"\n  *ngIf=\"storageService.instituteName.length > 0\"\n>\n  <ion-title>{{storageService.instituteName}}</ion-title>\n</ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons class=\"menuButton\" slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Saved Tests</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content style=\"--background: rgba(0, 0, 0, 0.04)\">\n  <ion-text *ngIf=\"testDetails.length == 0\" color=\"danger\" class=\"text-info\">\n    You haven't solved any test yet...!!!\n  </ion-text>\n\n  <ion-card\n    class=\"card\"\n    #testCard\n    *ngFor=\"let testDetail of testDetails\"\n    (click)=\"onTestClick()\"\n    [routerLink]=\"['/online-test-page', testDetail.testId]\"\n  >\n    <ion-card-content class=\"ion-no-padding\">\n      <ion-grid fixed class=\"ion-no-padding\">\n        <ion-row>\n          <ion-col size=\"2\">\n            <ion-item\n              class=\"ion-no-margin ion-no-padding icon-item-start\"\n              lines=\"none\"\n            >\n              <ion-icon\n                class=\"subject-logo-start\"\n                name=\"calculator\"\n                color=\"primary\"\n              ></ion-icon>\n            </ion-item>\n          </ion-col>\n          <ion-col size=\"8\">\n            <ion-item\n              class=\"ion-no-margin ion-no-padding ion-padding-end testId-item\"\n            >\n              <ion-label class=\"ion-no-margin-bottom testId\">\n                {{testDetail.subjectName}}\n              </ion-label>\n            </ion-item>\n            <ion-item class=\"ion-no-margin ion-no-padding\" lines=\"none\">\n              <ion-label class=\"test-info\">\n                <ion-text class=\"date\">\n                  {{testDetail.testDate.slice(0, 2) + '-' +\n                  testDetail.testDate.slice(2, 4) + '-' +\n                  testDetail.testDate.slice(4)}}\n                </ion-text>\n                <ion-text class=\"status\" color=\"success\"> Completed </ion-text>\n\n                <ion-text\n                  class=\"score\"\n                  *ngIf=\"testDetail.testSubmitted == 'true'\"\n                >\n                  {{testDetail.obtainedMarks}}/{{testDetail.totalMarks}}\n                </ion-text>\n              </ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col size=\"2\">\n            <ion-item\n              class=\"ion-no-margin ion-no-padding icon-item-end\"\n              lines=\"none\"\n            >\n              <ion-icon\n                class=\"subject-logo-end\"\n                name=\"checkmark-circle\"\n                color=\"success\"\n              ></ion-icon>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/online-saved-test-page/online-saved-test-page-routing.module.ts":
    /*!***************************************************************************************!*\
      !*** ./src/app/pages/online-saved-test-page/online-saved-test-page-routing.module.ts ***!
      \***************************************************************************************/

    /*! exports provided: OnlineSavedTestPagePageRoutingModule */

    /***/
    function srcAppPagesOnlineSavedTestPageOnlineSavedTestPageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OnlineSavedTestPagePageRoutingModule", function () {
        return OnlineSavedTestPagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _online_saved_test_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./online-saved-test-page.page */
      "./src/app/pages/online-saved-test-page/online-saved-test-page.page.ts");

      var routes = [{
        path: '',
        component: _online_saved_test_page_page__WEBPACK_IMPORTED_MODULE_3__["OnlineSavedTestPagePage"]
      }];

      var OnlineSavedTestPagePageRoutingModule = function OnlineSavedTestPagePageRoutingModule() {
        _classCallCheck(this, OnlineSavedTestPagePageRoutingModule);
      };

      OnlineSavedTestPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], OnlineSavedTestPagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/online-saved-test-page/online-saved-test-page.module.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/pages/online-saved-test-page/online-saved-test-page.module.ts ***!
      \*******************************************************************************/

    /*! exports provided: OnlineSavedTestPagePageModule */

    /***/
    function srcAppPagesOnlineSavedTestPageOnlineSavedTestPageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OnlineSavedTestPagePageModule", function () {
        return OnlineSavedTestPagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _online_saved_test_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./online-saved-test-page-routing.module */
      "./src/app/pages/online-saved-test-page/online-saved-test-page-routing.module.ts");
      /* harmony import */


      var _online_saved_test_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./online-saved-test-page.page */
      "./src/app/pages/online-saved-test-page/online-saved-test-page.page.ts");

      var OnlineSavedTestPagePageModule = function OnlineSavedTestPagePageModule() {
        _classCallCheck(this, OnlineSavedTestPagePageModule);
      };

      OnlineSavedTestPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _online_saved_test_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["OnlineSavedTestPagePageRoutingModule"]],
        declarations: [_online_saved_test_page_page__WEBPACK_IMPORTED_MODULE_6__["OnlineSavedTestPagePage"]]
      })], OnlineSavedTestPagePageModule);
      /***/
    },

    /***/
    "./src/app/pages/online-saved-test-page/online-saved-test-page.page.scss":
    /*!*******************************************************************************!*\
      !*** ./src/app/pages/online-saved-test-page/online-saved-test-page.page.scss ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesOnlineSavedTestPageOnlineSavedTestPagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".card {\n  margin-left: 0;\n  margin-right: 0;\n  border-radius: 10px;\n}\n\n.icon-item-start {\n  margin: 0px;\n  margin-top: 24px;\n  margin-left: 15px;\n}\n\n.subject-logo-start {\n  font-size: 30px;\n}\n\n.icon-item-end {\n  margin: 0px;\n  margin-top: 24px;\n  margin-left: 10px;\n}\n\n.subject-logo-end {\n  font-size: 30px;\n}\n\n.testId-item {\n  padding: 0;\n}\n\n.testId {\n  margin-bottom: 0;\n  font-weight: bold;\n}\n\n.test-info {\n  margin: 0;\n  font-size: 14px;\n}\n\n.status {\n  margin: 4px;\n}\n\n.date {\n  margin: 4px;\n  margin-left: 0;\n}\n\n.score {\n  margin: 4px;\n  font-weight: bold;\n}\n\n.text-info {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb25saW5lLXNhdmVkLXRlc3QtcGFnZS9vbmxpbmUtc2F2ZWQtdGVzdC1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtBQUNKOztBQUdBO0VBQ0ksVUFBQTtBQUFKOztBQUdBO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtBQUFKOztBQUdBO0VBQ0ksU0FBQTtFQUNBLGVBQUE7QUFBSjs7QUFHQTtFQUNJLFdBQUE7QUFBSjs7QUFHQTtFQUNJLFdBQUE7RUFDQSxjQUFBO0FBQUo7O0FBR0E7RUFDSSxXQUFBO0VBQ0EsaUJBQUE7QUFBSjs7QUFHQTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtBQUFKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvb25saW5lLXNhdmVkLXRlc3QtcGFnZS9vbmxpbmUtc2F2ZWQtdGVzdC1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG5cclxuLmljb24taXRlbS1zdGFydCB7XHJcbiAgICBtYXJnaW46IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDI0cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMTVweDtcclxufVxyXG5cclxuLnN1YmplY3QtbG9nby1zdGFydCB7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbn1cclxuXHJcbi5pY29uLWl0ZW0tZW5kIHtcclxuICAgIG1hcmdpbjogMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMjRweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG59XHJcblxyXG4uc3ViamVjdC1sb2dvLWVuZCB7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbn1cclxuXHJcblxyXG4udGVzdElkLWl0ZW0ge1xyXG4gICAgcGFkZGluZzogMDtcclxufVxyXG5cclxuLnRlc3RJZCB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwOyBcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4udGVzdC1pbmZvIHtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG5cclxuLnN0YXR1cyB7XHJcbiAgICBtYXJnaW46IDRweFxyXG59XHJcblxyXG4uZGF0ZSB7XHJcbiAgICBtYXJnaW46IDRweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG59XHJcblxyXG4uc2NvcmUge1xyXG4gICAgbWFyZ2luOiA0cHg7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuLnRleHQtaW5mbyB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDUwJTtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG59Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/online-saved-test-page/online-saved-test-page.page.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/pages/online-saved-test-page/online-saved-test-page.page.ts ***!
      \*****************************************************************************/

    /*! exports provided: OnlineSavedTestPagePage */

    /***/
    function srcAppPagesOnlineSavedTestPageOnlineSavedTestPagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OnlineSavedTestPagePage", function () {
        return OnlineSavedTestPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../../Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");
      /* harmony import */


      var _Services_save_test_info_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../../Services/save-test-info-service.service */
      "./src/app/Services/save-test-info-service.service.ts");

      var OnlineSavedTestPagePage = /*#__PURE__*/function () {
        function OnlineSavedTestPagePage(storageService, saveTestInfoService) {
          _classCallCheck(this, OnlineSavedTestPagePage);

          this.storageService = storageService;
          this.saveTestInfoService = saveTestInfoService;
          this.testDetails = [];
        }

        _createClass(OnlineSavedTestPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.getTest();
          }
        }, {
          key: "getTest",
          value: function getTest() {
            var _this = this;

            this.testDetails = []; // console.log('in get test page')

            this.saveTestInfoService.getTest().subscribe(function (data) {
              _this.testDetails = data; // console.log('test in page', this.testDetails);
            });
          }
        }, {
          key: "onTestClick",
          value: function onTestClick() {
            this.storageService.page = "saved-test";
          }
        }]);

        return OnlineSavedTestPagePage;
      }();

      OnlineSavedTestPagePage.ctorParameters = function () {
        return [{
          type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__["StorageServiceService"]
        }, {
          type: _Services_save_test_info_service_service__WEBPACK_IMPORTED_MODULE_3__["SaveTestInfoServiceService"]
        }];
      };

      OnlineSavedTestPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-online-saved-test-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./online-saved-test-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/online-saved-test-page/online-saved-test-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./online-saved-test-page.page.scss */
        "./src/app/pages/online-saved-test-page/online-saved-test-page.page.scss"))["default"]]
      })], OnlineSavedTestPagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=online-saved-test-page-online-saved-test-page-module-es5.js.map