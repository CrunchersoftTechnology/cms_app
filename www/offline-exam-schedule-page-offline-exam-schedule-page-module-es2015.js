(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["offline-exam-schedule-page-offline-exam-schedule-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.html ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar\n    color=\"primary\"\n    mode=\"ios\"\n    *ngIf=\"storageService.instituteName.length > 0\"\n  >\n    <ion-title>{{storageService.instituteName}}</ion-title>\n  </ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons class=\"menuButton\" slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Offline Exam Schedule</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n\n\n<ion-content>\n\n  <ion-card\n    *ngFor=\"let notification of offlineTestData\"\n    class=\"ion-padding\"\n   \n  >\n    <ion-card-title>{{notification.Title}}</ion-card-title>\n    <ion-card-content>\n      \n      \n        <ion-label class=\"lab\">Date : </ion-label><ion-label style=\"margin: 4%;\">{{notification.TestDate}}</ion-label> \n        <ion-label class=\"lab\">In</ion-label> <ion-label style=\"margin: 2%;\">{{dateConvert(notification.InTime)}}</ion-label>\n        \n      \n        <ion-label class=\"lab\">Out</ion-label> <ion-label style=\"margin: 2%;\">{{dateConvert(notification.OutTime)}}</ion-label><br/>\n        \n        <ion-label class=\"lab\">Subject : </ion-label>  <ion-label>{{notification.SubjectId}}</ion-label>\n        <ion-label  style=\"margin: 4%;\"> <ion-label class=\"lab\">Total Marks : </ion-label><ion-label >{{notification.TotalMark}}</ion-label>\n      </ion-label>\n    \n      </ion-card-content>\n  </ion-card>\n</ion-content>\n\n\n\n<!-- <ion-content> {{apiData}} </ion-content> -->\n");

/***/ }),

/***/ "./src/app/Services/offline-exam-schedule-service.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/Services/offline-exam-schedule-service.service.ts ***!
  \*******************************************************************/
/*! exports provided: OfflineExamScheduleServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfflineExamScheduleServiceService", function() { return OfflineExamScheduleServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./database.service */ "./src/app/Services/database.service.ts");



let OfflineExamScheduleServiceService = class OfflineExamScheduleServiceService {
    constructor(databaseService) {
        this.databaseService = databaseService;
    }
    insertIntoDatabase(testDetails) {
        return this.databaseService
            .getDataBase()
            .executeSql(`insert into OfflineExamSchedule(NotificationId, SubjectId, InTime, OutTime, TotalMark, Title, TestDate, Category) values (?,?,?,?,?,?,?,?)`, [
            testDetails.NotificationId,
            testDetails.SubjectId,
            testDetails.InTime,
            testDetails.OutTime,
            testDetails.TotalMark,
            testDetails.Title,
            testDetails.TestDate,
            testDetails.Category,
        ]);
    }
    createTable() {
        return this.databaseService
            .getDataBase()
            .executeSql(`create table OfflineExamSchedule (NotificationId text, SubjectId integer, InTime text, OutTime text, TotalMark integer, Title text, TestDate text, Category text)`, []);
    }
    getTestDetails() {
        return this.databaseService
            .getDataBase()
            .executeSql(`select * from OfflineExamSchedule`, [])
            .then((data) => {
            let testDetails = [];
            for (let i = 0; i < data.rows.length; i++) {
                testDetails.push({
                    Category: data.rows.item(i).Category,
                    InTime: data.rows.item(i).InTime,
                    NotificationId: data.rows.item(i).NotificationId,
                    OutTime: data.rows.item(i).OutTime,
                    SubjectId: data.rows.item(i).SubjectId,
                    TestDate: data.rows.item(i).TestDate,
                    Title: data.rows.item(i).Title,
                    TotalMark: data.rows.item(i).TotalMark,
                });
            }
            return testDetails;
        });
    }
    checkTableExists() {
        return this.databaseService
            .getDataBase()
            .executeSql(`SELECT name FROM sqlite_master WHERE type='table' AND name='OfflineExamSchedule'`, [])
            .then((data) => {
            if (data.rows.length > 0)
                return true;
            else
                return false;
        });
    }
};
OfflineExamScheduleServiceService.ctorParameters = () => [
    { type: _database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"] }
];
OfflineExamScheduleServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    })
], OfflineExamScheduleServiceService);



/***/ }),

/***/ "./src/app/Services/toast-service.service.ts":
/*!***************************************************!*\
  !*** ./src/app/Services/toast-service.service.ts ***!
  \***************************************************/
/*! exports provided: ToastServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastServiceService", function() { return ToastServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let ToastServiceService = class ToastServiceService {
    constructor(toastController) {
        this.toastController = toastController;
    }
    createToast(message, duration) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let toast = yield this.toastController.create({
                animated: true,
                duration: duration,
                position: "bottom",
                message: message
            });
            yield toast.present();
        });
    }
};
ToastServiceService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
ToastServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ToastServiceService);



/***/ }),

/***/ "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page-routing.module.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page-routing.module.ts ***!
  \***********************************************************************************************/
/*! exports provided: OfflineExamSchedulePagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfflineExamSchedulePagePageRoutingModule", function() { return OfflineExamSchedulePagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _offline_exam_schedule_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./offline-exam-schedule-page.page */ "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.ts");




const routes = [
    {
        path: '',
        component: _offline_exam_schedule_page_page__WEBPACK_IMPORTED_MODULE_3__["OfflineExamSchedulePagePage"]
    }
];
let OfflineExamSchedulePagePageRoutingModule = class OfflineExamSchedulePagePageRoutingModule {
};
OfflineExamSchedulePagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OfflineExamSchedulePagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.module.ts ***!
  \***************************************************************************************/
/*! exports provided: OfflineExamSchedulePagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfflineExamSchedulePagePageModule", function() { return OfflineExamSchedulePagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _offline_exam_schedule_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./offline-exam-schedule-page-routing.module */ "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page-routing.module.ts");
/* harmony import */ var _offline_exam_schedule_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./offline-exam-schedule-page.page */ "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.ts");







let OfflineExamSchedulePagePageModule = class OfflineExamSchedulePagePageModule {
};
OfflineExamSchedulePagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _offline_exam_schedule_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["OfflineExamSchedulePagePageRoutingModule"]
        ],
        declarations: [_offline_exam_schedule_page_page__WEBPACK_IMPORTED_MODULE_6__["OfflineExamSchedulePagePage"]]
    })
], OfflineExamSchedulePagePageModule);



/***/ }),

/***/ "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.scss ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".lab {\n  font-weight: bold;\n  color: darkgrey;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb2ZmbGluZS1leGFtLXNjaGVkdWxlLXBhZ2Uvb2ZmbGluZS1leGFtLXNjaGVkdWxlLXBhZ2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksaUJBQUE7RUFDQSxlQUFBO0FBQUoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9vZmZsaW5lLWV4YW0tc2NoZWR1bGUtcGFnZS9vZmZsaW5lLWV4YW0tc2NoZWR1bGUtcGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubGFie1xyXG5cclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6IGRhcmtncmV5O1xyXG5cclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.ts ***!
  \*************************************************************************************/
/*! exports provided: OfflineExamSchedulePagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfflineExamSchedulePagePage", function() { return OfflineExamSchedulePagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");
/* harmony import */ var src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/api-service.service */ "./src/app/Services/api-service.service.ts");
/* harmony import */ var _Services_offline_exam_schedule_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../Services/offline-exam-schedule-service.service */ "./src/app/Services/offline-exam-schedule-service.service.ts");
/* harmony import */ var _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../Services/internet-service.service */ "./src/app/Services/internet-service.service.ts");
/* harmony import */ var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../Services/toast-service.service */ "./src/app/Services/toast-service.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");








let OfflineExamSchedulePagePage = class OfflineExamSchedulePagePage {
    constructor(storage, storageService, apiService, offlineExamScheduleService, internetService, toastService) {
        this.storage = storage;
        this.storageService = storageService;
        this.apiService = apiService;
        this.offlineExamScheduleService = offlineExamScheduleService;
        this.internetService = internetService;
        this.toastService = toastService;
        this.classId = "";
        this.branchId = "";
        this.clientId = "";
        this.selectedBatches = "";
        this.selectedSubjects = "";
        this.offlineTestData = [];
    }
    ngOnInit() {
        this.userDetails = {
            classId: this.storageService.userDetails.classId,
            clientId: this.storageService.userDetails.clientId,
            branchId: this.storageService.userDetails.branchId,
            selectedBatches: this.storageService.userDetails.batchId,
            selectedSubjects: this.storageService.userDetails.selectedSubjects,
        };
        this.offlineExamScheduleService.checkTableExists().then((exists) => {
            console.log("exists", exists);
            if (exists == true) {
                this.saveTestData();
            }
            else {
                this.offlineExamScheduleService.createTable().then((_) => {
                    this.saveTestData();
                });
            }
        });
    }
    saveTestData() {
        if (this.internetService.networkConnected) {
            this.storage.get("offlineTestScheduleId").then((data) => {
                console.log("offlineTestScheduleId", data);
                if (data) {
                    this.getOfflineTestSchedule(this.userDetails, data);
                }
                else {
                    console.log("umesh...............");
                    this.getOfflineTestSchedule(this.userDetails, "0");
                }
            });
        }
        else {
            this.getOfflineTestScheduleDatabase();
            this.toastService.createToast("Check internet connection to update details", 3000);
        }
    }
    getOfflineTestSchedule(userDetails, notificationId) {
        this.apiService
            .getCMSOfflineTestNotification(userDetails, notificationId)
            .then((result) => {
            console.log("Offline Test Detailssssssss", JSON.parse(result.data).d);
            let data = JSON.parse(result.data);
            let length = data.d.length;
            if (length > 0) {
                let offlineTestData;
                let iteration = 0;
                for (let i = 0; i < length; i++) {
                    iteration += 1;
                    offlineTestData = {
                        Category: data.d[i].Category,
                        InTime: data.d[i].InTime,
                        NotificationId: data.d[i].NotificationId,
                        OutTime: data.d[i].OutTime,
                        SubjectId: data.d[i].SubjectId,
                        TestDate: data.d[i].TestDate,
                        Title: data.d[i].Title,
                        TotalMark: data.d[i].TotalMark,
                    };
                    this.offlineExamScheduleService
                        .insertIntoDatabase(offlineTestData)
                        .then((_) => {
                        console.log("Added sucessfully", offlineTestData);
                        if (iteration == length)
                            this.getOfflineTestScheduleDatabase();
                    });
                    // .catch((err) => {
                    //   console.log("error", err);
                    //   this.offlineExamScheduleService.createTable().then((_) =>
                    //     this.offlineExamScheduleService
                    //       .insertIntoDatabase(offlineTestData)
                    //       .then((_) => {
                    //         console.log("Added sucessfully 1", offlineTestData);
                    //         if (iteration == length)
                    //           this.getOfflineTestScheduleDatabase();
                    //       })
                    //   );
                    // });
                }
            }
            else {
                this.getOfflineTestScheduleDatabase();
            }
        });
    }
    getOfflineTestScheduleDatabase() {
        this.offlineExamScheduleService
            .getTestDetails()
            .then((data) => {
            this.offlineTestData = data;
            if (this.offlineTestData.length > 0) {
                this.storage.set("offlineTestScheduleId", this.offlineTestData[this.offlineTestData.length - 1].NotificationId);
                console.log("offline Test Data", this.offlineTestData, "offlineTestScheduleId", this.offlineTestData[this.offlineTestData.length - 1].NotificationId, this.offlineTestData[this.offlineTestData.length - 1].InTime);
            }
        });
    }
    dateConvert(date) {
        console.log("umesh test datessss", date);
        let start = 0;
        let inDateString = "";
        for (let i = 0; i < date.length; i++) {
            if (date[i] == '(') {
                start = 1;
                continue;
            }
            else if (date[i] == ')') {
                break;
            }
            if (start == 1) {
                inDateString = inDateString + date[i];
            }
        }
        let d = new Date(parseInt(inDateString));
        let hours = d.getHours();
        let seconds = d.getSeconds();
        let finalHours = 0;
        let ampm = "";
        if (hours > 12) {
            ampm = "PM";
            finalHours = hours / 12;
        }
        else {
            ampm = "AM";
        }
        let dateS = finalHours.toString().split(".")[0] + " : " + seconds.toString() + " " + ampm;
        return dateS;
    }
};
OfflineExamSchedulePagePage.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__["StorageServiceService"] },
    { type: src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_3__["ApiServiceService"] },
    { type: _Services_offline_exam_schedule_service_service__WEBPACK_IMPORTED_MODULE_4__["OfflineExamScheduleServiceService"] },
    { type: _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_5__["InternetServiceService"] },
    { type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_6__["ToastServiceService"] }
];
OfflineExamSchedulePagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-offline-exam-schedule-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./offline-exam-schedule-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./offline-exam-schedule-page.page.scss */ "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.scss")).default]
    })
], OfflineExamSchedulePagePage);



/***/ })

}]);
//# sourceMappingURL=offline-exam-schedule-page-offline-exam-schedule-page-module-es2015.js.map