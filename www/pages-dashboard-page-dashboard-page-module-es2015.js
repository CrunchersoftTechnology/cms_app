(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dashboard-page-dashboard-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard-page/dashboard-page.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard-page/dashboard-page.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-menu contentId=\"content\" class=\"menu\">\n  <ion-content>\n    <div class=\"menu-wrapper\">\n      <!-- <div class=\"avatar-wrapper\">\n      <ion-avatar class=\"md hydrated avatar\">\n        <img\n          [src]=\"storageService.userInfo.imageData\"\n        />\n      </ion-avatar>\n    </div> -->\n\n      <ion-icon class=\"close-icon\" name=\"close\" (click)=\"onClose()\"></ion-icon>\n\n      <div>\n        <ion-label class=\"ion-text-wrap user-name\"\n          >{{storageService.userDetails.studentName}}</ion-label\n        >\n      </div>\n\n      <ion-list class=\"list\">\n        <ion-menu-toggle auto-hide=\"false\" *ngFor=\"let page of pages\">\n          <ion-item\n            (click)=\"onClick(page.title)\"\n            class=\"list-items\"\n            [routerLink]=\"page.url\"\n            routerDirection=\"root\"\n          >\n            <ion-icon\n              class=\"list-icon\"\n              slot=\"start\"\n              color=\"light\"\n              [name]=\"selectedPath == page.url ? page.icon : page.icon + '-outline'\"\n            ></ion-icon>\n            <ion-text> {{page.title}} </ion-text>\n          </ion-item>\n        </ion-menu-toggle>\n      </ion-list>\n    </div>\n    <!-- <ion-item class=\"menu-button\">    \n      <ion-button (click)=\"onLogout()\" expand=\"block\" fill=\"clear\" color=\"light\">\n        <ion-icon slot=\"start\" name=\"log-out\"></ion-icon>\n        logout\n      </ion-button>\n    </ion-item> -->\n  </ion-content>\n</ion-menu>\n\n<ion-router-outlet id=\"content\" main></ion-router-outlet>\n\n<ion-tabs>\n  <ion-tab-bar slot=\"bottom\" class=\"tab-bar\">\n    <ion-tab-button tab=\"home-page\" class=\"tab\">\n      <ion-icon name=\"home-sharp\"></ion-icon>\n      <ion-label>Home</ion-label>\n    </ion-tab-button>\n    <ion-tab-button tab=\"fees-page\" class=\"tab\">\n      <ion-icon name=\"clipboard\"></ion-icon>\n      <ion-label>Fees</ion-label>\n    </ion-tab-button>\n    <ion-tab-button tab=\"live-test-page\" class=\"tab\">\n      <ion-icon name=\"newspaper-sharp\"></ion-icon>\n      <ion-label>Live Test</ion-label>\n    </ion-tab-button>\n    <ion-tab-button tab=\"profile\" class=\"tab\">\n      <ion-icon name=\"person\"></ion-icon>\n      <ion-label>Profile</ion-label>\n    </ion-tab-button>\n    <ion-tab-button tab=\"notifications-page\" class=\"tab\">\n      <ion-icon name=\"browsers\"></ion-icon>\n      <ion-label>Notification</ion-label>\n    </ion-tab-button>\n    <!-- <ion-tab-button tab=\"\" class=\"tab\">\n      <ion-icon name=\"notifications-sharp\"></ion-icon>\n      <ion-label>Notification</ion-label>\n    </ion-tab-button> -->\n  </ion-tab-bar>\n</ion-tabs>\n");

/***/ }),

/***/ "./src/app/Services/attendance-service.service.ts":
/*!********************************************************!*\
  !*** ./src/app/Services/attendance-service.service.ts ***!
  \********************************************************/
/*! exports provided: AttendanceServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttendanceServiceService", function() { return AttendanceServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./database.service */ "./src/app/Services/database.service.ts");



let AttendanceServiceService = class AttendanceServiceService {
    constructor(databaseService) {
        this.databaseService = databaseService;
    }
    deleteFromCMSAttendance() {
        return this.databaseService
            .getDataBase()
            .executeSql("DELETE FROM CMSAttendance", []);
    }
    insertIntoCMSAttendance(adate, attStatus, SubjectName, BatchName, InTime, OutTime, MonthOfDate) {
        return this.databaseService
            .getDataBase()
            .executeSql("INSERT INTO CMSAttendance(ADate, status, subjectName, batchName, inTime, outTime, monthOfDate) values(?,?,?,?,?,?,?)", [adate, attStatus, SubjectName, BatchName, InTime, OutTime, MonthOfDate]);
        // .then((_) =>
        //   this.databaseService
        //     .getDataBase()
        //     .executeSql("select * from CMSAttendance", [])
        //     .then((data) => console.log("CMSAttendance", data))
        // );
    }
    deleteFromCMSAttendanceCount() {
        return this.databaseService
            .getDataBase()
            .executeSql("DELETE FROM CMSAttendanceCount", []);
    }
    insertIntoCMSAttendanceCount(dataLength, monthNm, totalCount, presentCount) {
        return this.databaseService
            .getDataBase()
            .executeSql("INSERT INTO CMSAttendanceCount(id, monthName, totalCount, presentCount) values(?,?,?,?)", [dataLength, monthNm, totalCount, presentCount])
            .then((_) => this.databaseService
            .getDataBase()
            .executeSql("select * from CMSAttendanceCount", [])
            .then((data) => console.log("CMSAttendanceCount", data)));
    }
    getAttendance() {
        return this.databaseService
            .getDataBase()
            .executeSql("SELECT * FROM CMSAttendance ORDER BY ADate DESC", [])
            .then((data) => {
            let attendanceData = [];
            for (let i = 0; i < data.rows.length; i++) {
                attendanceData.push({
                    date: data.rows.item(i).ADate,
                    status: data.rows.item(i).status,
                    subjectName: data.rows.item(i).subjectName,
                    inTime: data.rows.item(i).inTime,
                    outTime: data.rows.item(i).outTime,
                    batchName: data.rows.item(i).batchName,
                    monthOfDate: data.rows.item(i).monthOfDate,
                });
            }
            return attendanceData;
        });
    }
    getDistinctSubjects() {
        return this.databaseService
            .getDataBase()
            .executeSql("SELECT DISTINCT subjectName FROM CMSAttendance", [])
            .then((data) => {
            let subjects = [];
            if (data.rows.length > 0) {
                for (let i = 0; i < data.rows.length; i++) {
                    subjects.push(data.rows.item(0).subjectName);
                }
            }
            return subjects;
        });
    }
};
AttendanceServiceService.ctorParameters = () => [
    { type: _database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"] }
];
AttendanceServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    })
], AttendanceServiceService);



/***/ }),

/***/ "./src/app/Services/daily-practice-paper-service.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/Services/daily-practice-paper-service.service.ts ***!
  \******************************************************************/
/*! exports provided: DailyPracticePaperServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DailyPracticePaperServiceService", function() { return DailyPracticePaperServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./database.service */ "./src/app/Services/database.service.ts");



let DailyPracticePaperServiceService = class DailyPracticePaperServiceService {
    constructor(databaseService) {
        this.databaseService = databaseService;
    }
    insertIntoCMSDailyPracticePaper(description, date, fileName, attachmentDescription) {
        this.databaseService
            .getDataBase()
            .executeSql(`INSERT INTO CMSDailyPracticePaper(Description, Date, FileName, AttachmentDescription) values(?,?,?,?)`, [description, date, fileName, attachmentDescription]);
    }
    selectFromCMSDailyPracticePapere() {
        let fileInfo = [];
        return this.databaseService
            .getDataBase()
            .executeSql(`SELECT * FROM CMSDailyPracticePaper ORDER BY ID DESC`, [])
            .then((data) => {
            for (let i = 0; i < data.rows.length; i++) {
                fileInfo.push({
                    AttachmentDescription: data.rows.item(i).AttachmentDescription,
                    Date: data.rows.item(i).Date,
                    Description: data.rows.item(i).Description,
                    FileName: data.rows.item(i).FileName,
                });
            }
            return fileInfo;
        });
    }
    deleteFromCMSDailyPracticePaper() {
        return this.databaseService
            .getDataBase()
            .executeSql(`DELETE FROM CMSDailyPracticePaper`, []);
    }
};
DailyPracticePaperServiceService.ctorParameters = () => [
    { type: _database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"] }
];
DailyPracticePaperServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], DailyPracticePaperServiceService);



/***/ }),

/***/ "./src/app/Services/fees-service.service.ts":
/*!**************************************************!*\
  !*** ./src/app/Services/fees-service.service.ts ***!
  \**************************************************/
/*! exports provided: FeesServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeesServiceService", function() { return FeesServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./database.service */ "./src/app/Services/database.service.ts");



let FeesServiceService = class FeesServiceService {
    constructor(databaseService) {
        this.databaseService = databaseService;
    }
    deleteFromCMSFeesInfo() {
        return this.databaseService
            .getDataBase()
            .executeSql(`DELETE FROM CMSFeesInfo`, []);
    }
    insertIntoCMSFeesInfo(totalFees, paidFees, remainingFees) {
        return this.databaseService
            .getDataBase()
            .executeSql(`INSERT INTO CMSFeesInfo(totalFees, paidFees, remFees) values(?,?,?)`, [totalFees, paidFees, remainingFees]);
        // .then((_) =>
        //   this.databaseService
        //     .getDataBase()
        //     .executeSql(`Select * from CMSFeesInfo`, [])
        //     .then((data) => console.log(data))
        // );
    }
    deleteFromCMSFeesHistory() {
        return this.databaseService
            .getDataBase()
            .executeSql(`DELETE FROM CMSFeesHistory`, []);
    }
    insertIntoCMSFeesHistory(paidFees, paidDate) {
        return this.databaseService
            .getDataBase()
            .executeSql(`INSERT INTO CMSFeesHistory(paidFees, paidDate) values(?,?)`, [paidFees, paidDate]);
        // .then((_) =>
        //   this.databaseService
        //     .getDataBase()
        //     .executeSql(`Select * from CMSFeesHistory`, [])
        //     .then((data) => console.log(data))
        // );
    }
    getStudentFeesHistory() {
        return this.databaseService
            .getDataBase()
            .executeSql(`SELECT paidFees, paidDate FROM CMSFeesHistory`, [])
            .then((data) => {
            let feesDetails = [];
            for (let i = 0; i < data.rows.length; i++) {
                feesDetails.push({
                    paidDate: data.rows.item(i).paidDate,
                    paidFees: data.rows.item(i).paidFees,
                });
            }
            return feesDetails;
        });
    }
    getStudentFeesData() {
        return this.databaseService
            .getDataBase()
            .executeSql(`SELECT totalFees, paidFees, remFees FROM CMSFeesInfo`, [])
            .then((data) => {
            let feesDetails;
            if (data.rows.length > 0) {
                for (let i = 0; i < data.rows.length; i++) {
                    feesDetails = {
                        paidFees: data.rows.item(i).paidFees,
                        remainingFees: data.rows.item(i).remFees,
                        totalFees: data.rows.item(i).totalFees,
                    };
                }
            }
            else {
                feesDetails = {
                    paidFees: "-",
                    remainingFees: "-",
                    totalFees: "-",
                };
            }
            return feesDetails;
        });
    }
};
FeesServiceService.ctorParameters = () => [
    { type: _database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"] }
];
FeesServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    })
], FeesServiceService);



/***/ }),

/***/ "./src/app/Services/offline-result-service.service.ts":
/*!************************************************************!*\
  !*** ./src/app/Services/offline-result-service.service.ts ***!
  \************************************************************/
/*! exports provided: OfflineResultServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfflineResultServiceService", function() { return OfflineResultServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./database.service */ "./src/app/Services/database.service.ts");



let OfflineResultServiceService = class OfflineResultServiceService {
    constructor(databaseService) {
        this.databaseService = databaseService;
    }
    insertIntoCMSResults(SId, ObtainedMarks, TotalMarks, Timing, Duration, Title, Percentage, TestDate, Category, RStatus) {
        return this.databaseService
            .getDataBase()
            .executeSql(`INSERT INTO CMSResults(SId, ObtainedMarks, TotalMarks, Timing, Duration, Title, Percentage, TestDate, Category, Status) values (?,?,?,?,?,?,?,?,?,?)`, [
            SId,
            ObtainedMarks,
            TotalMarks,
            Timing,
            Duration,
            Title,
            Percentage,
            TestDate,
            Category,
            RStatus,
        ]);
        // .then((_) =>
        //   this.databaseService
        //     .getDataBase()
        //     .executeSql(`select * from CMSResults`, [])
        //     .then((data) => console.log("offline result database", data))
        // );
    }
    deleteFromCMSResults() {
        return this.databaseService
            .getDataBase()
            .executeSql(`delete from CMSResults`, []);
    }
};
OfflineResultServiceService.ctorParameters = () => [
    { type: _database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"] }
];
OfflineResultServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    })
], OfflineResultServiceService);



/***/ }),

/***/ "./src/app/Services/time-table-service.service.ts":
/*!********************************************************!*\
  !*** ./src/app/Services/time-table-service.service.ts ***!
  \********************************************************/
/*! exports provided: TimeTableServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeTableServiceService", function() { return TimeTableServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./database.service */ "./src/app/Services/database.service.ts");



let TimeTableServiceService = class TimeTableServiceService {
    constructor(databaseService) {
        this.databaseService = databaseService;
    }
    insertIntoCMSTimeTable(description, date, fileName, category, attachmentDescription) {
        this.databaseService
            .getDataBase()
            .executeSql(`INSERT INTO CMSTimeTable(Description, Date, FileName, Category, AttachmentDescription) values(?,?,?,?,?)`, [description, date, fileName, category, attachmentDescription]);
    }
    selectFromCMSTimeTable(category) {
        let fileInfo = [];
        return this.databaseService
            .getDataBase()
            .executeSql(`SELECT * FROM CMSTimeTable WHERE Category=? ORDER BY ID DESC`, [category])
            .then((data) => {
            for (let i = 0; i < data.rows.length; i++) {
                fileInfo.push({
                    AttachmentDescription: data.rows.item(i).AttachmentDescription,
                    Category: data.rows.item(i).Category,
                    Date: data.rows.item(i).Date,
                    Description: data.rows.item(i).Description,
                    FileName: data.rows.item(i).FileName,
                });
            }
            return fileInfo;
        });
    }
    deleteFromCMSTimeTable() {
        return this.databaseService
            .getDataBase()
            .executeSql(`DELETE FROM CMSTimeTable`, []);
    }
};
TimeTableServiceService.ctorParameters = () => [
    { type: _database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"] }
];
TimeTableServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    })
], TimeTableServiceService);



/***/ }),

/***/ "./src/app/pages/dashboard-page/dashboard-page-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/dashboard-page/dashboard-page-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: DashboardPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPagePageRoutingModule", function() { return DashboardPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _dashboard_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard-page.page */ "./src/app/pages/dashboard-page/dashboard-page.page.ts");




const routes = [
    {
        path: "",
        component: _dashboard_page_page__WEBPACK_IMPORTED_MODULE_3__["DashboardPagePage"],
        children: [
            {
                path: "home-page",
                loadChildren: () => __webpack_require__.e(/*! import() | home-page-home-page-module */ "home-page-home-page-module").then(__webpack_require__.bind(null, /*! ../home-page/home-page.module */ "./src/app/pages/home-page/home-page.module.ts")).then((m) => m.HomePagePageModule),
            },
            {
                path: "fees-page",
                loadChildren: () => Promise.all(/*! import() | fees-page-fees-page-module */[__webpack_require__.e("default~Pages-online-test-page-online-test-page-module~attendance-page-attendance-page-module~fees-p~d85ed61c"), __webpack_require__.e("fees-page-fees-page-module")]).then(__webpack_require__.bind(null, /*! ../fees-page/fees-page.module */ "./src/app/pages/fees-page/fees-page.module.ts")).then((m) => m.FeesPagePageModule),
            },
            {
                path: "notifications-page",
                loadChildren: () => __webpack_require__.e(/*! import() | notifications-page-notifications-page-module */ "notifications-page-notifications-page-module").then(__webpack_require__.bind(null, /*! ../notifications-page/notifications-page.module */ "./src/app/pages/notifications-page/notifications-page.module.ts")).then((m) => m.NotificationsPagePageModule),
            },
            {
                path: "profile",
                loadChildren: () => __webpack_require__.e(/*! import() | profile-profile-module */ "profile-profile-module").then(__webpack_require__.bind(null, /*! ../profile/profile.module */ "./src/app/pages/profile/profile.module.ts")).then((m) => m.ProfilePageModule),
            },
            {
                path: "attendance-page",
                loadChildren: () => Promise.all(/*! import() | attendance-page-attendance-page-module */[__webpack_require__.e("default~Pages-online-test-page-online-test-page-module~attendance-page-attendance-page-module~fees-p~d85ed61c"), __webpack_require__.e("attendance-page-attendance-page-module")]).then(__webpack_require__.bind(null, /*! ../attendance-page/attendance-page.module */ "./src/app/pages/attendance-page/attendance-page.module.ts")).then((m) => m.AttendancePagePageModule),
            },
            {
                path: "online-saved-test-page",
                loadChildren: () => __webpack_require__.e(/*! import() | online-saved-test-page-online-saved-test-page-module */ "online-saved-test-page-online-saved-test-page-module").then(__webpack_require__.bind(null, /*! ../online-saved-test-page/online-saved-test-page.module */ "./src/app/pages/online-saved-test-page/online-saved-test-page.module.ts")).then((m) => m.OnlineSavedTestPagePageModule),
            },
            {
                path: "offline-result-page",
                loadChildren: () => __webpack_require__.e(/*! import() | offline-result-page-offline-result-page-module */ "offline-result-page-offline-result-page-module").then(__webpack_require__.bind(null, /*! ../offline-result-page/offline-result-page.module */ "./src/app/pages/offline-result-page/offline-result-page.module.ts")).then((m) => m.OfflineResultPagePageModule),
            },
            {
                path: "class-time-table-page/:id",
                loadChildren: () => __webpack_require__.e(/*! import() | class-time-table-page-class-time-table-page-module */ "class-time-table-page-class-time-table-page-module").then(__webpack_require__.bind(null, /*! ../class-time-table-page/class-time-table-page.module */ "./src/app/pages/class-time-table-page/class-time-table-page.module.ts")).then((m) => m.ClassTimeTablePagePageModule),
            },
            {
                path: "daily-practice-paper-page",
                loadChildren: () => __webpack_require__.e(/*! import() | daily-practice-paper-page-daily-practice-paper-page-module */ "daily-practice-paper-page-daily-practice-paper-page-module").then(__webpack_require__.bind(null, /*! ../daily-practice-paper-page/daily-practice-paper-page.module */ "./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.module.ts")).then((m) => m.DailyPracticePaperPagePageModule),
            },
            {
                path: "video-page",
                loadChildren: () => __webpack_require__.e(/*! import() | video-page-video-page-module */ "video-page-video-page-module").then(__webpack_require__.bind(null, /*! ../video-page/video-page.module */ "./src/app/pages/video-page/video-page.module.ts")).then((m) => m.VideoPagePageModule),
            },
            {
                path: "pdf-page",
                loadChildren: () => __webpack_require__.e(/*! import() | pdf-page-pdf-page-module */ "pdf-page-pdf-page-module").then(__webpack_require__.bind(null, /*! ../pdf-page/pdf-page.module */ "./src/app/pages/pdf-page/pdf-page.module.ts")).then((m) => m.PdfPagePageModule),
            },
            {
                path: "about-us-page",
                loadChildren: () => __webpack_require__.e(/*! import() | about-us-page-about-us-page-module */ "about-us-page-about-us-page-module").then(__webpack_require__.bind(null, /*! ../about-us-page/about-us-page.module */ "./src/app/pages/about-us-page/about-us-page.module.ts")).then((m) => m.AboutUsPagePageModule),
            },
            {
                path: "live-test-page",
                loadChildren: () => Promise.all(/*! import() | live-test-page-live-test-page-module */[__webpack_require__.e("default~Pages-online-test-list-page-online-test-list-page-module~live-test-page-live-test-page-module"), __webpack_require__.e("live-test-page-live-test-page-module")]).then(__webpack_require__.bind(null, /*! ../live-test-page/live-test-page.module */ "./src/app/pages/live-test-page/live-test-page.module.ts")).then((m) => m.LiveTestPagePageModule),
            },
            {
                path: "offline-exam-schedule-page",
                loadChildren: () => __webpack_require__.e(/*! import() | offline-exam-schedule-page-offline-exam-schedule-page-module */ "offline-exam-schedule-page-offline-exam-schedule-page-module").then(__webpack_require__.bind(null, /*! ../offline-exam-schedule-page/offline-exam-schedule-page.module */ "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.module.ts")).then((m) => m.OfflineExamSchedulePagePageModule),
            },
        ],
    },
];
let DashboardPagePageRoutingModule = class DashboardPagePageRoutingModule {
};
DashboardPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DashboardPagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/dashboard-page/dashboard-page.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/dashboard-page/dashboard-page.module.ts ***!
  \***************************************************************/
/*! exports provided: DashboardPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPagePageModule", function() { return DashboardPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _dashboard_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard-page-routing.module */ "./src/app/pages/dashboard-page/dashboard-page-routing.module.ts");
/* harmony import */ var _dashboard_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard-page.page */ "./src/app/pages/dashboard-page/dashboard-page.page.ts");







let DashboardPagePageModule = class DashboardPagePageModule {
};
DashboardPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _dashboard_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["DashboardPagePageRoutingModule"]
        ],
        declarations: [_dashboard_page_page__WEBPACK_IMPORTED_MODULE_6__["DashboardPagePage"]]
    })
], DashboardPagePageModule);



/***/ }),

/***/ "./src/app/pages/dashboard-page/dashboard-page.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/dashboard-page/dashboard-page.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".menu-wrapper {\n  background: #0d4fc3;\n  height: 100%;\n}\n\n.list {\n  position: absolute;\n  top: 40vw;\n  width: 100%;\n  /* height: 121vw; */\n  /* border-radius: 30px; */\n  background-color: var(--ion-color-primary);\n  overflow-x: scroll;\n}\n\n.list-items {\n  --background: var(--ion-color-primary);\n  --color: white;\n  height: 9.5vw;\n  font-size: 3.5vw;\n  width: 85%;\n  margin-left: 25px;\n}\n\n.list-icon {\n  font-size: 6vw;\n}\n\n.avatar-wrapper {\n  position: relative;\n  left: 10vw;\n  top: 5vw;\n  height: 16vw;\n  width: 16vw;\n  border-radius: 100%;\n}\n\n.avatar {\n  position: absolute;\n  top: 23%;\n  left: 23%;\n  height: 55%;\n  width: 55%;\n}\n\n.close-icon {\n  position: absolute;\n  top: 5%;\n  right: 10%;\n  color: white;\n  font-size: 25px;\n}\n\n.user-name {\n  position: absolute;\n  top: 26vw;\n  left: 12vw;\n  color: white;\n  font-size: 20px;\n  font-weight: bold;\n}\n\n.tab-bar {\n  --border: 0px;\n  --background: rgba(0, 0, 0, 0.02);\n}\n\n.menu-button {\n  --background: var(--ion-color-primary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGFzaGJvYXJkLXBhZ2UvZGFzaGJvYXJkLXBhZ2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLDBDQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFHQTtFQUNJLHNDQUFBO0VBQ0EsY0FBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtBQUFKOztBQUdBO0VBQ0ksY0FBQTtBQUFKOztBQUdBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7QUFBSjs7QUFHQTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBQUFKOztBQUdBO0VBQ0ksa0JBQUE7RUFDQSxPQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBQUo7O0FBR0E7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQUFKOztBQUdBO0VBQ0ksYUFBQTtFQUNBLGlDQUFBO0FBQUo7O0FBR0E7RUFDSSxzQ0FBQTtBQUFKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGFzaGJvYXJkLXBhZ2UvZGFzaGJvYXJkLXBhZ2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1lbnUtd3JhcHBlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMGQ0ZmMzO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4ubGlzdCB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDQwdnc7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIC8qIGhlaWdodDogMTIxdnc7ICovXHJcbiAgICAvKiBib3JkZXItcmFkaXVzOiAzMHB4OyAqL1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgb3ZlcmZsb3cteDogc2Nyb2xsO1xyXG5cclxufVxyXG5cclxuLmxpc3QtaXRlbXMge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICAtLWNvbG9yOiB3aGl0ZTtcclxuICAgIGhlaWdodDogOS41dnc7XHJcbiAgICBmb250LXNpemU6IDMuNXZ3O1xyXG4gICAgd2lkdGg6IDg1JTtcclxuICAgIG1hcmdpbi1sZWZ0OiAyNXB4O1xyXG59XHJcblxyXG4ubGlzdC1pY29uIHtcclxuICAgIGZvbnQtc2l6ZTogNnZ3O1xyXG59XHJcblxyXG4uYXZhdGFyLXdyYXBwZXIge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbGVmdDogMTB2dztcclxuICAgIHRvcDogNXZ3O1xyXG4gICAgaGVpZ2h0OiAxNnZ3O1xyXG4gICAgd2lkdGg6IDE2dnc7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG59XHJcblxyXG4uYXZhdGFyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMjMlO1xyXG4gICAgbGVmdDogMjMlO1xyXG4gICAgaGVpZ2h0OiA1NSU7XHJcbiAgICB3aWR0aDogNTUlO1xyXG59XHJcblxyXG4uY2xvc2UtaWNvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDUlO1xyXG4gICAgcmlnaHQ6IDEwJTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxufVxyXG5cclxuLnVzZXItbmFtZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDI2dnc7XHJcbiAgICBsZWZ0OiAxMnZ3O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbi50YWItYmFyIHtcclxuICAgIC0tYm9yZGVyOiAwcHg7XHJcbiAgICAtLWJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4wMik7XHJcbn1cclxuXHJcbi5tZW51LWJ1dHRvbiB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/dashboard-page/dashboard-page.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/dashboard-page/dashboard-page.page.ts ***!
  \*************************************************************/
/*! exports provided: DashboardPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPagePage", function() { return DashboardPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var src_app_Services_storage_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _Services_loading_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../../Services/loading-service.service */ "./src/app/Services/loading-service.service.ts");
/* harmony import */ var _Services_database_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./../../Services/database.service */ "./src/app/Services/database.service.ts");
/* harmony import */ var _Services_user_details_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./../../Services/user-details.service */ "./src/app/Services/user-details.service.ts");
/* harmony import */ var _Services_attendance_service_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./../../Services/attendance-service.service */ "./src/app/Services/attendance-service.service.ts");
/* harmony import */ var _Services_fees_service_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./../../Services/fees-service.service */ "./src/app/Services/fees-service.service.ts");
/* harmony import */ var src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/Services/notification-service.service */ "./src/app/Services/notification-service.service.ts");
/* harmony import */ var src_app_Services_internet_service_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/app/Services/internet-service.service */ "./src/app/Services/internet-service.service.ts");
/* harmony import */ var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./../../Services/api-service.service */ "./src/app/Services/api-service.service.ts");
/* harmony import */ var _Services_save_test_info_service_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./../../Services/save-test-info-service.service */ "./src/app/Services/save-test-info-service.service.ts");
/* harmony import */ var _Services_offline_result_service_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./../../Services/offline-result-service.service */ "./src/app/Services/offline-result-service.service.ts");
/* harmony import */ var _Services_daily_practice_paper_service_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./../../Services/daily-practice-paper-service.service */ "./src/app/Services/daily-practice-paper-service.service.ts");
/* harmony import */ var _Services_time_table_service_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./../../Services/time-table-service.service */ "./src/app/Services/time-table-service.service.ts");



















let DashboardPagePage = class DashboardPagePage {
    constructor(router, storage, statusBar, loadingController, menuController, alertController, storageService, loadingService, databaseService, userDetailsService, attendanceService, feesService, notificationService, internetService, apiServiceService, saveTestInfoServiceService, offlineService, dailyPracticePaperService, timeTableService, apiService) {
        this.router = router;
        this.storage = storage;
        this.statusBar = statusBar;
        this.loadingController = loadingController;
        this.menuController = menuController;
        this.alertController = alertController;
        this.storageService = storageService;
        this.loadingService = loadingService;
        this.databaseService = databaseService;
        this.userDetailsService = userDetailsService;
        this.attendanceService = attendanceService;
        this.feesService = feesService;
        this.notificationService = notificationService;
        this.internetService = internetService;
        this.apiServiceService = apiServiceService;
        this.saveTestInfoServiceService = saveTestInfoServiceService;
        this.offlineService = offlineService;
        this.dailyPracticePaperService = dailyPracticePaperService;
        this.timeTableService = timeTableService;
        this.apiService = apiService;
        this.pages = [
            {
                title: "HOME",
                url: "/dashboard-page/home-page",
                icon: "home",
            },
            {
                title: "PROFILE",
                url: "/dashboard-page/profile",
                icon: "person",
            },
            {
                title: "ATTENDANCE",
                url: "/dashboard-page/attendance-page",
                icon: "clipboard",
            },
            {
                title: "FEES",
                url: "/dashboard-page/fees-page",
                icon: "newspaper",
            },
            {
                title: "LIVE TESTS",
                url: "/dashboard-page/live-test-page",
                icon: "newspaper",
            },
            {
                title: "OFFLINE EXAM SCHEDULE",
                url: "/dashboard-page/offline-exam-schedule-page",
                icon: "newspaper",
            },
            {
                title: "NOTIFICATIONS",
                url: "/dashboard-page/notifications-page",
                icon: "medal",
            },
            {
                title: "ONLINE RESULT",
                url: "/dashboard-page/online-saved-test-page",
                icon: "browsers",
            },
            {
                title: "OFFINE RESULT",
                url: "/dashboard-page/offline-result-page",
                icon: "copy",
            },
            {
                title: "STUDY MATERIAL",
                url: "/dashboard-page/pdf-page",
                icon: "bookmark",
            },
            {
                title: "EXAM TIMETABLE",
                url: "/dashboard-page/class-time-table-page/2",
                icon: "bar-chart",
            },
            {
                title: "EXPERT LECTURE TIMETABLE",
                url: "/dashboard-page/class-time-table-page/1",
                icon: "notifications",
            },
            {
                title: "DPP",
                url: "/dashboard-page/daily-practice-paper-page",
                icon: "desktop",
            },
            {
                title: "VIDEOS",
                url: "/dashboard-page/video-page",
                icon: "settings",
            },
            {
                title: "ABOUT US",
                url: "/dashboard-page/about-us-page",
                icon: "desktop",
            },
            {
                title: "LOGOUT",
                url: "",
                icon: "log-out",
            },
        ];
        this.selectedPath = "";
        this.notificationsCount = 0;
        this.router.events.subscribe((event) => {
            this.selectedPath = event.url;
        });
    }
    ngOnInit() {
        // console.log("user info", this.storageService.userInfo.UserId);
        // this.databaseService.getDatabaseState().subscribe((ready) => {
        //   if (ready)
        //     this.userDetailsService
        //       .selectFromCMSRegistrationDetails()
        //       .then((data: UserDetails) => {
        //         this.storageService.userDetails = data;
        //         console.log(
        //           "user details home page",
        //           this.storageService.userDetails
        //         );
        //         this.apiService
        //           .checkActiveStatus(this.storageService.userDetails.userId)
        //           .then((data) => {
        //             console.log(
        //               "Activation Status",
        //               JSON.parse(data.data).d[0].IsActive
        //             );
        //             if (JSON.parse(data.data).d[0].IsActive == "True") {
        //               this.saveTestInfoServiceService
        //                 .getSavedTestsCount()
        //                 .then((data) => {
        //                   if (data == 0) {
        //                     // this.loadingService.createLoading('Fetching your tests...')
        //                     setTimeout(
        //                       (_) =>
        //                         this.userDetailsService.getSavedTests(
        //                           this.storageService.userInfo.UserId,
        //                           this.storageService.userInfo.ClientId
        //                         ),
        //                       3000
        //                     );
        //                   }
        //                 });
        //             } else {
        //               this.onAccoutDeactivated();
        //             }
        //           });
        //       });
        // });
        // this.storage
        //   .get("token")
        //   .then((token) => (this.storageService.tokenId = token));
    }
    onClose() {
        this.menuController.close();
    }
    onAccoutDeactivated() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: "Accout Deactivated",
                subHeader: "Your accout is deactivated",
                cssClass: "alert-title",
                buttons: [
                    {
                        text: "Ok",
                        handler: () => {
                            this.handleLogout();
                        },
                    },
                ],
            });
            yield alert.present();
        });
    }
    onLogout() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.menuController.toggle().then(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const alert = yield this.alertController.create({
                    header: "Logout",
                    subHeader: "Do you want to logout ?",
                    cssClass: "alert-title",
                    buttons: [
                        {
                            text: "Cancel",
                            role: "cancel",
                        },
                        {
                            text: "Logout",
                            handler: () => {
                                this.handleLogout();
                            },
                        },
                    ],
                });
                yield alert.present();
            }));
        });
    }
    handleLogout() {
        this.loadingService.createLoading("Loging Out...").then((_) => {
            this.databaseService.getDatabaseState().subscribe((ready) => {
                if (ready)
                    this.userDetailsService.deleteFromCMSLogStatus().then((_) => this.userDetailsService
                        .deleteFromCMSRegistrationDetails()
                        .then((_) => this.attendanceService.deleteFromCMSAttendance().then((_) => this.attendanceService
                        .deleteFromCMSAttendanceCount()
                        .then((_) => this.feesService.deleteFromCMSFeesHistory().then((_) => this.feesService.deleteFromCMSFeesInfo().then((_) => this.notificationService
                        .deleteFromCMSNotificationId()
                        .then((_) => this.notificationService
                        .deleteFromCMSNotificationMessage()
                        .then((_) => this.notificationService
                        .deleteFromCMSNotificationMessageTest()
                        .then((_) => this.notificationService
                        .deleteFromNotification()
                        .then((_) => this.notificationService
                        .deleteFromOnlineTestNotifications()
                        .then((_) => this.saveTestInfoServiceService
                        .deleteFromSaveTestInfo()
                        .then((_) => this.offlineService
                        .deleteFromCMSResults()
                        .then((_) => this.dailyPracticePaperService
                        .deleteFromCMSDailyPracticePaper()
                        .then((_) => this.timeTableService
                        .deleteFromCMSTimeTable()
                        .then((_) => {
                        this.storage
                            .ready()
                            .then((ready) => {
                            if (ready)
                                this.storage.set("userLoggedIn", false);
                        });
                        this.apiServiceService.setToken(this
                            .storageService
                            .userDetails
                            .userId, "");
                        this.apiServiceService
                            .removeSecurityCode(this
                            .storageService
                            .userDetails
                            .clientId, this
                            .storageService
                            .userDetails
                            .userId)
                            .then((_) => this.loadingController
                            .dismiss()
                            .then((_) => this.router
                            .navigateByUrl("/login-page")
                            .then(() => {
                            this.statusBar.backgroundColorByHexString("#ffffff");
                            this.statusBar.styleDefault();
                        })));
                    })))))))))))))));
            });
        });
    }
    onClick(title) {
        if (title == "LOGOUT") {
            this.menuController.close();
            this.onLogout();
        }
    }
};
DashboardPagePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_3__["StatusBar"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: src_app_Services_storage_service_service__WEBPACK_IMPORTED_MODULE_5__["StorageServiceService"] },
    { type: _Services_loading_service_service__WEBPACK_IMPORTED_MODULE_7__["LoadingServiceService"] },
    { type: _Services_database_service__WEBPACK_IMPORTED_MODULE_8__["DatabaseService"] },
    { type: _Services_user_details_service__WEBPACK_IMPORTED_MODULE_9__["UserDetailsService"] },
    { type: _Services_attendance_service_service__WEBPACK_IMPORTED_MODULE_10__["AttendanceServiceService"] },
    { type: _Services_fees_service_service__WEBPACK_IMPORTED_MODULE_11__["FeesServiceService"] },
    { type: src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_12__["NotificationServiceService"] },
    { type: src_app_Services_internet_service_service__WEBPACK_IMPORTED_MODULE_13__["InternetServiceService"] },
    { type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_14__["ApiServiceService"] },
    { type: _Services_save_test_info_service_service__WEBPACK_IMPORTED_MODULE_15__["SaveTestInfoServiceService"] },
    { type: _Services_offline_result_service_service__WEBPACK_IMPORTED_MODULE_16__["OfflineResultServiceService"] },
    { type: _Services_daily_practice_paper_service_service__WEBPACK_IMPORTED_MODULE_17__["DailyPracticePaperServiceService"] },
    { type: _Services_time_table_service_service__WEBPACK_IMPORTED_MODULE_18__["TimeTableServiceService"] },
    { type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_14__["ApiServiceService"] }
];
DashboardPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-dashboard-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./dashboard-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard-page/dashboard-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./dashboard-page.page.scss */ "./src/app/pages/dashboard-page/dashboard-page.page.scss")).default]
    })
], DashboardPagePage);



/***/ })

}]);
//# sourceMappingURL=pages-dashboard-page-dashboard-page-module-es2015.js.map