(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["offline-exam-schedule-page-offline-exam-schedule-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.html":
    /*!*****************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.html ***!
      \*****************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesOfflineExamSchedulePageOfflineExamSchedulePagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar\n    color=\"primary\"\n    mode=\"ios\"\n    *ngIf=\"storageService.instituteName.length > 0\"\n  >\n    <ion-title>{{storageService.instituteName}}</ion-title>\n  </ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons class=\"menuButton\" slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Offline Exam Schedule</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n\n\n<ion-content>\n\n  <ion-card\n    *ngFor=\"let notification of offlineTestData\"\n    class=\"ion-padding\"\n   \n  >\n    <ion-card-title>{{notification.Title}}</ion-card-title>\n    <ion-card-content>\n      \n      \n        <ion-label class=\"lab\">Date : </ion-label><ion-label style=\"margin: 4%;\">{{notification.TestDate}}</ion-label> \n        <ion-label class=\"lab\">In</ion-label> <ion-label style=\"margin: 2%;\">{{dateConvert(notification.InTime)}}</ion-label>\n        \n      \n        <ion-label class=\"lab\">Out</ion-label> <ion-label style=\"margin: 2%;\">{{dateConvert(notification.OutTime)}}</ion-label><br/>\n        \n        <ion-label class=\"lab\">Subject : </ion-label>  <ion-label>{{notification.SubjectId}}</ion-label>\n        <ion-label  style=\"margin: 4%;\"> <ion-label class=\"lab\">Total Marks : </ion-label><ion-label >{{notification.TotalMark}}</ion-label>\n      </ion-label>\n    \n      </ion-card-content>\n  </ion-card>\n</ion-content>\n\n\n\n<!-- <ion-content> {{apiData}} </ion-content> -->\n";
      /***/
    },

    /***/
    "./src/app/Services/offline-exam-schedule-service.service.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/Services/offline-exam-schedule-service.service.ts ***!
      \*******************************************************************/

    /*! exports provided: OfflineExamScheduleServiceService */

    /***/
    function srcAppServicesOfflineExamScheduleServiceServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OfflineExamScheduleServiceService", function () {
        return OfflineExamScheduleServiceService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./database.service */
      "./src/app/Services/database.service.ts");

      var OfflineExamScheduleServiceService = /*#__PURE__*/function () {
        function OfflineExamScheduleServiceService(databaseService) {
          _classCallCheck(this, OfflineExamScheduleServiceService);

          this.databaseService = databaseService;
        }

        _createClass(OfflineExamScheduleServiceService, [{
          key: "insertIntoDatabase",
          value: function insertIntoDatabase(testDetails) {
            return this.databaseService.getDataBase().executeSql("insert into OfflineExamSchedule(NotificationId, SubjectId, InTime, OutTime, TotalMark, Title, TestDate, Category) values (?,?,?,?,?,?,?,?)", [testDetails.NotificationId, testDetails.SubjectId, testDetails.InTime, testDetails.OutTime, testDetails.TotalMark, testDetails.Title, testDetails.TestDate, testDetails.Category]);
          }
        }, {
          key: "createTable",
          value: function createTable() {
            return this.databaseService.getDataBase().executeSql("create table OfflineExamSchedule (NotificationId text, SubjectId integer, InTime text, OutTime text, TotalMark integer, Title text, TestDate text, Category text)", []);
          }
        }, {
          key: "getTestDetails",
          value: function getTestDetails() {
            return this.databaseService.getDataBase().executeSql("select * from OfflineExamSchedule", []).then(function (data) {
              var testDetails = [];

              for (var i = 0; i < data.rows.length; i++) {
                testDetails.push({
                  Category: data.rows.item(i).Category,
                  InTime: data.rows.item(i).InTime,
                  NotificationId: data.rows.item(i).NotificationId,
                  OutTime: data.rows.item(i).OutTime,
                  SubjectId: data.rows.item(i).SubjectId,
                  TestDate: data.rows.item(i).TestDate,
                  Title: data.rows.item(i).Title,
                  TotalMark: data.rows.item(i).TotalMark
                });
              }

              return testDetails;
            });
          }
        }, {
          key: "checkTableExists",
          value: function checkTableExists() {
            return this.databaseService.getDataBase().executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='OfflineExamSchedule'", []).then(function (data) {
              if (data.rows.length > 0) return true;else return false;
            });
          }
        }]);

        return OfflineExamScheduleServiceService;
      }();

      OfflineExamScheduleServiceService.ctorParameters = function () {
        return [{
          type: _database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"]
        }];
      };

      OfflineExamScheduleServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], OfflineExamScheduleServiceService);
      /***/
    },

    /***/
    "./src/app/Services/toast-service.service.ts":
    /*!***************************************************!*\
      !*** ./src/app/Services/toast-service.service.ts ***!
      \***************************************************/

    /*! exports provided: ToastServiceService */

    /***/
    function srcAppServicesToastServiceServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ToastServiceService", function () {
        return ToastServiceService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var ToastServiceService = /*#__PURE__*/function () {
        function ToastServiceService(toastController) {
          _classCallCheck(this, ToastServiceService);

          this.toastController = toastController;
        }

        _createClass(ToastServiceService, [{
          key: "createToast",
          value: function createToast(message, duration) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        animated: true,
                        duration: duration,
                        position: "bottom",
                        message: message
                      });

                    case 2:
                      toast = _context.sent;
                      _context.next = 5;
                      return toast.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return ToastServiceService;
      }();

      ToastServiceService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      ToastServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ToastServiceService);
      /***/
    },

    /***/
    "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page-routing.module.ts":
    /*!***********************************************************************************************!*\
      !*** ./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page-routing.module.ts ***!
      \***********************************************************************************************/

    /*! exports provided: OfflineExamSchedulePagePageRoutingModule */

    /***/
    function srcAppPagesOfflineExamSchedulePageOfflineExamSchedulePageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OfflineExamSchedulePagePageRoutingModule", function () {
        return OfflineExamSchedulePagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _offline_exam_schedule_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./offline-exam-schedule-page.page */
      "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.ts");

      var routes = [{
        path: '',
        component: _offline_exam_schedule_page_page__WEBPACK_IMPORTED_MODULE_3__["OfflineExamSchedulePagePage"]
      }];

      var OfflineExamSchedulePagePageRoutingModule = function OfflineExamSchedulePagePageRoutingModule() {
        _classCallCheck(this, OfflineExamSchedulePagePageRoutingModule);
      };

      OfflineExamSchedulePagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], OfflineExamSchedulePagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.module.ts":
    /*!***************************************************************************************!*\
      !*** ./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.module.ts ***!
      \***************************************************************************************/

    /*! exports provided: OfflineExamSchedulePagePageModule */

    /***/
    function srcAppPagesOfflineExamSchedulePageOfflineExamSchedulePageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OfflineExamSchedulePagePageModule", function () {
        return OfflineExamSchedulePagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _offline_exam_schedule_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./offline-exam-schedule-page-routing.module */
      "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page-routing.module.ts");
      /* harmony import */


      var _offline_exam_schedule_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./offline-exam-schedule-page.page */
      "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.ts");

      var OfflineExamSchedulePagePageModule = function OfflineExamSchedulePagePageModule() {
        _classCallCheck(this, OfflineExamSchedulePagePageModule);
      };

      OfflineExamSchedulePagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _offline_exam_schedule_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["OfflineExamSchedulePagePageRoutingModule"]],
        declarations: [_offline_exam_schedule_page_page__WEBPACK_IMPORTED_MODULE_6__["OfflineExamSchedulePagePage"]]
      })], OfflineExamSchedulePagePageModule);
      /***/
    },

    /***/
    "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.scss":
    /*!***************************************************************************************!*\
      !*** ./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.scss ***!
      \***************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesOfflineExamSchedulePageOfflineExamSchedulePagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".lab {\n  font-weight: bold;\n  color: darkgrey;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb2ZmbGluZS1leGFtLXNjaGVkdWxlLXBhZ2Uvb2ZmbGluZS1leGFtLXNjaGVkdWxlLXBhZ2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksaUJBQUE7RUFDQSxlQUFBO0FBQUoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9vZmZsaW5lLWV4YW0tc2NoZWR1bGUtcGFnZS9vZmZsaW5lLWV4YW0tc2NoZWR1bGUtcGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubGFie1xyXG5cclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6IGRhcmtncmV5O1xyXG5cclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.ts":
    /*!*************************************************************************************!*\
      !*** ./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.ts ***!
      \*************************************************************************************/

    /*! exports provided: OfflineExamSchedulePagePage */

    /***/
    function srcAppPagesOfflineExamSchedulePageOfflineExamSchedulePagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OfflineExamSchedulePagePage", function () {
        return OfflineExamSchedulePagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../../Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");
      /* harmony import */


      var src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/Services/api-service.service */
      "./src/app/Services/api-service.service.ts");
      /* harmony import */


      var _Services_offline_exam_schedule_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./../../Services/offline-exam-schedule-service.service */
      "./src/app/Services/offline-exam-schedule-service.service.ts");
      /* harmony import */


      var _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./../../Services/internet-service.service */
      "./src/app/Services/internet-service.service.ts");
      /* harmony import */


      var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./../../Services/toast-service.service */
      "./src/app/Services/toast-service.service.ts");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");

      var OfflineExamSchedulePagePage = /*#__PURE__*/function () {
        function OfflineExamSchedulePagePage(storage, storageService, apiService, offlineExamScheduleService, internetService, toastService) {
          _classCallCheck(this, OfflineExamSchedulePagePage);

          this.storage = storage;
          this.storageService = storageService;
          this.apiService = apiService;
          this.offlineExamScheduleService = offlineExamScheduleService;
          this.internetService = internetService;
          this.toastService = toastService;
          this.classId = "";
          this.branchId = "";
          this.clientId = "";
          this.selectedBatches = "";
          this.selectedSubjects = "";
          this.offlineTestData = [];
        }

        _createClass(OfflineExamSchedulePagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.userDetails = {
              classId: this.storageService.userDetails.classId,
              clientId: this.storageService.userDetails.clientId,
              branchId: this.storageService.userDetails.branchId,
              selectedBatches: this.storageService.userDetails.batchId,
              selectedSubjects: this.storageService.userDetails.selectedSubjects
            };
            this.offlineExamScheduleService.checkTableExists().then(function (exists) {
              console.log("exists", exists);

              if (exists == true) {
                _this.saveTestData();
              } else {
                _this.offlineExamScheduleService.createTable().then(function (_) {
                  _this.saveTestData();
                });
              }
            });
          }
        }, {
          key: "saveTestData",
          value: function saveTestData() {
            var _this2 = this;

            if (this.internetService.networkConnected) {
              this.storage.get("offlineTestScheduleId").then(function (data) {
                console.log("offlineTestScheduleId", data);

                if (data) {
                  _this2.getOfflineTestSchedule(_this2.userDetails, data);
                } else {
                  console.log("umesh...............");

                  _this2.getOfflineTestSchedule(_this2.userDetails, "0");
                }
              });
            } else {
              this.getOfflineTestScheduleDatabase();
              this.toastService.createToast("Check internet connection to update details", 3000);
            }
          }
        }, {
          key: "getOfflineTestSchedule",
          value: function getOfflineTestSchedule(userDetails, notificationId) {
            var _this3 = this;

            this.apiService.getCMSOfflineTestNotification(userDetails, notificationId).then(function (result) {
              console.log("Offline Test Detailssssssss", JSON.parse(result.data).d);
              var data = JSON.parse(result.data);
              var length = data.d.length;

              if (length > 0) {
                (function () {
                  var offlineTestData;
                  var iteration = 0;

                  for (var i = 0; i < length; i++) {
                    iteration += 1;
                    offlineTestData = {
                      Category: data.d[i].Category,
                      InTime: data.d[i].InTime,
                      NotificationId: data.d[i].NotificationId,
                      OutTime: data.d[i].OutTime,
                      SubjectId: data.d[i].SubjectId,
                      TestDate: data.d[i].TestDate,
                      Title: data.d[i].Title,
                      TotalMark: data.d[i].TotalMark
                    };

                    _this3.offlineExamScheduleService.insertIntoDatabase(offlineTestData).then(function (_) {
                      console.log("Added sucessfully", offlineTestData);
                      if (iteration == length) _this3.getOfflineTestScheduleDatabase();
                    }); // .catch((err) => {
                    //   console.log("error", err);
                    //   this.offlineExamScheduleService.createTable().then((_) =>
                    //     this.offlineExamScheduleService
                    //       .insertIntoDatabase(offlineTestData)
                    //       .then((_) => {
                    //         console.log("Added sucessfully 1", offlineTestData);
                    //         if (iteration == length)
                    //           this.getOfflineTestScheduleDatabase();
                    //       })
                    //   );
                    // });

                  }
                })();
              } else {
                _this3.getOfflineTestScheduleDatabase();
              }
            });
          }
        }, {
          key: "getOfflineTestScheduleDatabase",
          value: function getOfflineTestScheduleDatabase() {
            var _this4 = this;

            this.offlineExamScheduleService.getTestDetails().then(function (data) {
              _this4.offlineTestData = data;

              if (_this4.offlineTestData.length > 0) {
                _this4.storage.set("offlineTestScheduleId", _this4.offlineTestData[_this4.offlineTestData.length - 1].NotificationId);

                console.log("offline Test Data", _this4.offlineTestData, "offlineTestScheduleId", _this4.offlineTestData[_this4.offlineTestData.length - 1].NotificationId, _this4.offlineTestData[_this4.offlineTestData.length - 1].InTime);
              }
            });
          }
        }, {
          key: "dateConvert",
          value: function dateConvert(date) {
            console.log("umesh test datessss", date);
            var start = 0;
            var inDateString = "";

            for (var i = 0; i < date.length; i++) {
              if (date[i] == '(') {
                start = 1;
                continue;
              } else if (date[i] == ')') {
                break;
              }

              if (start == 1) {
                inDateString = inDateString + date[i];
              }
            }

            var d = new Date(parseInt(inDateString));
            var hours = d.getHours();
            var seconds = d.getSeconds();
            var finalHours = 0;
            var ampm = "";

            if (hours > 12) {
              ampm = "PM";
              finalHours = hours / 12;
            } else {
              ampm = "AM";
            }

            var dateS = finalHours.toString().split(".")[0] + " : " + seconds.toString() + " " + ampm;
            return dateS;
          }
        }]);

        return OfflineExamSchedulePagePage;
      }();

      OfflineExamSchedulePagePage.ctorParameters = function () {
        return [{
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"]
        }, {
          type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__["StorageServiceService"]
        }, {
          type: src_app_Services_api_service_service__WEBPACK_IMPORTED_MODULE_3__["ApiServiceService"]
        }, {
          type: _Services_offline_exam_schedule_service_service__WEBPACK_IMPORTED_MODULE_4__["OfflineExamScheduleServiceService"]
        }, {
          type: _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_5__["InternetServiceService"]
        }, {
          type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_6__["ToastServiceService"]
        }];
      };

      OfflineExamSchedulePagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-offline-exam-schedule-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./offline-exam-schedule-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./offline-exam-schedule-page.page.scss */
        "./src/app/pages/offline-exam-schedule-page/offline-exam-schedule-page.page.scss"))["default"]]
      })], OfflineExamSchedulePagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=offline-exam-schedule-page-offline-exam-schedule-page-module-es5.js.map