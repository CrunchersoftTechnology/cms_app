(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["demo-page-demo-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/demo-page/demo-page.page.html":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/demo-page/demo-page.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppDemoPageDemoPagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>demo-page</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/demo-page/demo-page-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/demo-page/demo-page-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: DemoPagePageRoutingModule */

    /***/
    function srcAppDemoPageDemoPageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DemoPagePageRoutingModule", function () {
        return DemoPagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _demo_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./demo-page.page */
      "./src/app/demo-page/demo-page.page.ts");

      var routes = [{
        path: '',
        component: _demo_page_page__WEBPACK_IMPORTED_MODULE_3__["DemoPagePage"]
      }];

      var DemoPagePageRoutingModule = function DemoPagePageRoutingModule() {
        _classCallCheck(this, DemoPagePageRoutingModule);
      };

      DemoPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], DemoPagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/demo-page/demo-page.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/demo-page/demo-page.module.ts ***!
      \***********************************************/

    /*! exports provided: DemoPagePageModule */

    /***/
    function srcAppDemoPageDemoPageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DemoPagePageModule", function () {
        return DemoPagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _demo_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./demo-page-routing.module */
      "./src/app/demo-page/demo-page-routing.module.ts");
      /* harmony import */


      var _demo_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./demo-page.page */
      "./src/app/demo-page/demo-page.page.ts");

      var DemoPagePageModule = function DemoPagePageModule() {
        _classCallCheck(this, DemoPagePageModule);
      };

      DemoPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _demo_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["DemoPagePageRoutingModule"]],
        declarations: [_demo_page_page__WEBPACK_IMPORTED_MODULE_6__["DemoPagePage"]]
      })], DemoPagePageModule);
      /***/
    },

    /***/
    "./src/app/demo-page/demo-page.page.scss":
    /*!***********************************************!*\
      !*** ./src/app/demo-page/demo-page.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function srcAppDemoPageDemoPagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RlbW8tcGFnZS9kZW1vLXBhZ2UucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/demo-page/demo-page.page.ts":
    /*!*********************************************!*\
      !*** ./src/app/demo-page/demo-page.page.ts ***!
      \*********************************************/

    /*! exports provided: DemoPagePage */

    /***/
    function srcAppDemoPageDemoPagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DemoPagePage", function () {
        return DemoPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var DemoPagePage = /*#__PURE__*/function () {
        function DemoPagePage() {
          _classCallCheck(this, DemoPagePage);
        }

        _createClass(DemoPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return DemoPagePage;
      }();

      DemoPagePage.ctorParameters = function () {
        return [];
      };

      DemoPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-demo-page',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./demo-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/demo-page/demo-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./demo-page.page.scss */
        "./src/app/demo-page/demo-page.page.scss"))["default"]]
      })], DemoPagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=demo-page-demo-page-module-es5.js.map