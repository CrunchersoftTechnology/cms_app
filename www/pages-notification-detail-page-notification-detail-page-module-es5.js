(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-notification-detail-page-notification-detail-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/notification-detail-page/notification-detail-page.page.html":
    /*!*************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/notification-detail-page/notification-detail-page.page.html ***!
      \*************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesNotificationDetailPageNotificationDetailPagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar\n    color=\"primary\"\n    mode=\"ios\"\n    *ngIf=\"storageService.instituteName.length > 0\"\n  >\n    <ion-title>{{storageService.instituteName}}</ion-title>\n  </ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-icon\n      style=\"padding-left: 10px; font-size: 6vw\"\n      name=\"arrow-back\"\n      slot=\"start\"\n      (click)=\"onBackClick()\"\n    ></ion-icon>\n    <ion-title>{{notificationDetail.NDate}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content class=\"ion-padding\"> {{notificationDetail.NBody}} </ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/notification-detail-page/notification-detail-page-routing.module.ts":
    /*!*******************************************************************************************!*\
      !*** ./src/app/pages/notification-detail-page/notification-detail-page-routing.module.ts ***!
      \*******************************************************************************************/

    /*! exports provided: NotificationDetailPagePageRoutingModule */

    /***/
    function srcAppPagesNotificationDetailPageNotificationDetailPageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationDetailPagePageRoutingModule", function () {
        return NotificationDetailPagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _notification_detail_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./notification-detail-page.page */
      "./src/app/pages/notification-detail-page/notification-detail-page.page.ts");

      var routes = [{
        path: '',
        component: _notification_detail_page_page__WEBPACK_IMPORTED_MODULE_3__["NotificationDetailPagePage"]
      }];

      var NotificationDetailPagePageRoutingModule = function NotificationDetailPagePageRoutingModule() {
        _classCallCheck(this, NotificationDetailPagePageRoutingModule);
      };

      NotificationDetailPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], NotificationDetailPagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/notification-detail-page/notification-detail-page.module.ts":
    /*!***********************************************************************************!*\
      !*** ./src/app/pages/notification-detail-page/notification-detail-page.module.ts ***!
      \***********************************************************************************/

    /*! exports provided: NotificationDetailPagePageModule */

    /***/
    function srcAppPagesNotificationDetailPageNotificationDetailPageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationDetailPagePageModule", function () {
        return NotificationDetailPagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _notification_detail_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./notification-detail-page-routing.module */
      "./src/app/pages/notification-detail-page/notification-detail-page-routing.module.ts");
      /* harmony import */


      var _notification_detail_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./notification-detail-page.page */
      "./src/app/pages/notification-detail-page/notification-detail-page.page.ts");

      var NotificationDetailPagePageModule = function NotificationDetailPagePageModule() {
        _classCallCheck(this, NotificationDetailPagePageModule);
      };

      NotificationDetailPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _notification_detail_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["NotificationDetailPagePageRoutingModule"]],
        declarations: [_notification_detail_page_page__WEBPACK_IMPORTED_MODULE_6__["NotificationDetailPagePage"]]
      })], NotificationDetailPagePageModule);
      /***/
    },

    /***/
    "./src/app/pages/notification-detail-page/notification-detail-page.page.scss":
    /*!***********************************************************************************!*\
      !*** ./src/app/pages/notification-detail-page/notification-detail-page.page.scss ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesNotificationDetailPageNotificationDetailPagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL25vdGlmaWNhdGlvbi1kZXRhaWwtcGFnZS9ub3RpZmljYXRpb24tZGV0YWlsLXBhZ2UucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/pages/notification-detail-page/notification-detail-page.page.ts":
    /*!*********************************************************************************!*\
      !*** ./src/app/pages/notification-detail-page/notification-detail-page.page.ts ***!
      \*********************************************************************************/

    /*! exports provided: NotificationDetailPagePage */

    /***/
    function srcAppPagesNotificationDetailPageNotificationDetailPagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationDetailPagePage", function () {
        return NotificationDetailPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var src_app_Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/Services/notification-service.service */
      "./src/app/Services/notification-service.service.ts");

      var NotificationDetailPagePage = /*#__PURE__*/function () {
        function NotificationDetailPagePage(navController, activatedRoute, storageService, notificationService) {
          _classCallCheck(this, NotificationDetailPagePage);

          this.navController = navController;
          this.activatedRoute = activatedRoute;
          this.storageService = storageService;
          this.notificationService = notificationService;
        }

        _createClass(NotificationDetailPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.notificationDetail = {
              Category: '',
              ID: 0,
              NBody: '',
              NDate: '',
              NOnlineNotificationId: '',
              NStatus: '',
              NTime: '',
              sId: ''
            };
            this.notificationId = this.activatedRoute.snapshot.params["id"];
            this.notificationService.getCMSNotification(this.notificationId).then(function (data) {
              return _this.notificationDetail = data;
            });
          }
        }, {
          key: "onBackClick",
          value: function onBackClick() {
            this.navController.back();
          }
        }]);

        return NotificationDetailPagePage;
      }();

      NotificationDetailPagePage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: src_app_Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__["StorageServiceService"]
        }, {
          type: src_app_Services_notification_service_service__WEBPACK_IMPORTED_MODULE_5__["NotificationServiceService"]
        }];
      };

      NotificationDetailPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-notification-detail-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./notification-detail-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/notification-detail-page/notification-detail-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./notification-detail-page.page.scss */
        "./src/app/pages/notification-detail-page/notification-detail-page.page.scss"))["default"]]
      })], NotificationDetailPagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-notification-detail-page-notification-detail-page-module-es5.js.map