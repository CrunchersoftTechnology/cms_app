(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Pages-password-page-password-page-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/password-page/password-page.page.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/password-page/password-page.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesPasswordPagePasswordPagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Login</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-item>\n    <ion-input placeholder=\"Enter Password\" [(ngModel)]=\"password\"></ion-input>\n  </ion-item>\n  <ion-button\n    (click)=\"onLoginClick()\"\n    expand=\"block\"\n    fill=\"solid\"\n    color=\"primary\"\n  >\n    Login\n  </ion-button>\n\n  <ion-text color=\"primary\" (click)=\"onForgotPass()\">\n    Forgot Password\n  </ion-text>\n\n  <ion-item>\n    <ion-input placeholder=\"Enter OTP\" [(ngModel)]=\"enteredOTP\"></ion-input>\n  </ion-item>\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button\n          (click)=\"onSendOTP()\"\n          expand=\"block\"\n          fill=\"solid\"\n          color=\"primary\"\n        >\n          Send OTP\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-button\n          (click)=\"onLoginWithOTP()\"\n          expand=\"block\"\n          fill=\"solid\"\n          color=\"primary\"\n        >\n          Login with OTP\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content> -->\n\n<!-- <ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Login</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-item>\n    <ion-input placeholder=\"Enter Email or Phone\" [(ngModel)]=\"loginCredentials\"></ion-input>\n  </ion-item>\n  <ion-button (click)=\"onLoginClick()\" expand=\"block\" fill=\"solid\" color=\"primary\"> Login </ion-button>\n\n  {{error}}\n</ion-content> -->\n\n<ion-button (click)=\"onBackClick()\" fill=\"clear\" class=\"back-button\">\n  <ion-icon slot=\"icon-only\" name=\"chevron-back-outline\"></ion-icon>\n</ion-button>\n\n<div class=\"login\">\n  <div heading-column-lr>\n    <h1 big-heading>Enter</h1>\n    <p class=\"sub-line\">Password</p>\n    <!-- <h2 small-heading (click)=\"onRegisterClick()\">REGISTER</h2> -->\n    <p class=\"welcome\">Please enter correct password to login</p>\n  </div>\n\n  <form name=\"loginForm\" #loginForm=\"ngForm\">\n    <ion-item transparent>\n      <!-- <ion-icon icon-small name=\"person\" color=\"primary\"></ion-icon> -->\n      <ion-input\n        name=\"emailId\"\n        [(ngModel)]=\"password\"\n        #emailId=\"ngModel\"\n        email\n        required\n        [type]=\"inputType\"\n        placeholder=\"Enter Password\"\n      ></ion-input>\n      <ion-icon\n        [name]=\"iconName\"\n        slot=\"end\"\n        (click)=\"changeInputType()\"\n      ></ion-icon>\n    </ion-item>\n    <!-- <div *ngIf=\"emailId.invalid && (emailId.dirty || emailId.touched)\">\n      <ion-text color=\"danger\" *ngIf=\"emailId.errors.required\"\n        >Enter password</ion-text\n      >\n    </div> -->\n    <div *ngIf=\"showErrorMessage\">\n      <ion-text color=\"danger\">Wrong password</ion-text>\n    </div>\n\n    <!-- <ion-label class=\"forgot-password\" (click)=\"onForgotPass()\"\n      >Forgot Password</ion-label\n    > -->\n    <div style=\"height: 30px\"></div>\n\n    <ion-button\n      color=\"primary\"\n      class=\"ion-button-class\"\n      size=\"large\"\n      expand=\"block\"\n      fill=\"solid\"\n      shape=\"round\"\n      (click)=\"onLogin()\"\n    >\n      Login\n    </ion-button>\n  </form>\n</div>\n\n<ion-label class=\"forgot-pass\" (click)=\"onForgotPassClick()\"\n  >Forgot password ?</ion-label\n>\n\n<ion-label class=\"version\"> Version 1.0 </ion-label>\n";
      /***/
    },

    /***/
    "./src/app/Pages/password-page/password-page-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/Pages/password-page/password-page-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: PasswordPagePageRoutingModule */

    /***/
    function srcAppPagesPasswordPagePasswordPageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PasswordPagePageRoutingModule", function () {
        return PasswordPagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _password_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./password-page.page */
      "./src/app/Pages/password-page/password-page.page.ts");

      var routes = [{
        path: '',
        component: _password_page_page__WEBPACK_IMPORTED_MODULE_3__["PasswordPagePage"]
      }];

      var PasswordPagePageRoutingModule = function PasswordPagePageRoutingModule() {
        _classCallCheck(this, PasswordPagePageRoutingModule);
      };

      PasswordPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], PasswordPagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/Pages/password-page/password-page.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/Pages/password-page/password-page.module.ts ***!
      \*************************************************************/

    /*! exports provided: PasswordPagePageModule */

    /***/
    function srcAppPagesPasswordPagePasswordPageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PasswordPagePageModule", function () {
        return PasswordPagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _password_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./password-page-routing.module */
      "./src/app/Pages/password-page/password-page-routing.module.ts");
      /* harmony import */


      var _password_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./password-page.page */
      "./src/app/Pages/password-page/password-page.page.ts");

      var PasswordPagePageModule = function PasswordPagePageModule() {
        _classCallCheck(this, PasswordPagePageModule);
      };

      PasswordPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _password_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["PasswordPagePageRoutingModule"]],
        declarations: [_password_page_page__WEBPACK_IMPORTED_MODULE_6__["PasswordPagePage"]]
      })], PasswordPagePageModule);
      /***/
    },

    /***/
    "./src/app/Pages/password-page/password-page.page.scss":
    /*!*************************************************************!*\
      !*** ./src/app/Pages/password-page/password-page.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesPasswordPagePasswordPagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".login {\n  position: absolute;\n  left: 50%;\n  top: 40%;\n  transform: translate(-49%, -49%);\n  width: 90%;\n}\n\n.registration {\n  position: absolute;\n  top: 2%;\n  left: 5%;\n  width: 90%;\n}\n\n.login-header {\n  background: var(--ion-color-primary);\n}\n\n.logo-image {\n  display: block;\n  width: 120px;\n  margin: 0 auto;\n  text-align: center;\n}\n\n[heading-column-lr] {\n  display: flex;\n  align-items: center;\n  margin-bottom: 37vw;\n}\n\n[big-heading] {\n  font-size: 12vw !important;\n  color: var(--ion-color-dark);\n}\n\n[small-heading] {\n  margin-left: auto;\n  padding-right: 40px;\n  font-size: 15px;\n}\n\n[transparent] {\n  background: transparent;\n}\n\nion-input {\n  font-size: 4.5vw;\n  font-weight: 400;\n}\n\n[icon-small] {\n  width: 30px;\n}\n\nion-item {\n  --highlight-color-focused: transparent;\n  --highlight-height: 0;\n  --highlight-color-invalid: #000;\n  --highlight-color-valid: #ddd;\n  --min-height: 65px;\n  --padding-start: 0;\n}\n\nion-label {\n  color: #000;\n  opacity: 0.8;\n  font-size: 13px;\n  display: block;\n}\n\n.forgot-password {\n  color: var(--ion-color-primary);\n  font-size: 13px;\n  display: block;\n  text-align: right;\n  padding: 10px;\n}\n\n.link-button {\n  color: var(--ion-color-primary);\n}\n\n.demo-login {\n  color: var(--ion-color-primary);\n  margin-left: 80px;\n}\n\n.chbox {\n  margin-right: 5px;\n}\n\n.label {\n  margin-top: 10px;\n  position: absolute;\n  left: 30%;\n}\n\n.label-text {\n  font-size: 5vw;\n  font-weight: bold;\n  text-decoration: underline;\n}\n\n.ion-button-class {\n  margin-top: 6vw;\n  width: 98%;\n  font-size: 4vw;\n}\n\n.welcome {\n  position: absolute;\n  top: 30%;\n  font-size: 4.5vw;\n  color: gray;\n  font-weight: 400;\n}\n\n.version {\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  bottom: 4%;\n  font-size: 4vw;\n  color: gray;\n  font-weight: 300;\n}\n\n.forgot-pass {\n  position: absolute;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  bottom: 15%;\n  font-size: 4vw;\n  color: black;\n  font-weight: 400;\n}\n\n.sub-line {\n  margin: 0;\n  font-weight: 500;\n  font-size: 12vw;\n  position: absolute;\n  top: 19%;\n}\n\n.back-button {\n  position: absolute;\n  top: 7%;\n  left: -3%;\n  font-size: 6vw;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvUGFnZXMvcGFzc3dvcmQtcGFnZS9wYXNzd29yZC1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFLQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFFQSxnQ0FBQTtFQUNBLFVBQUE7QUFKSjs7QUFPQTtFQUNJLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0FBSko7O0FBT0E7RUFDSSxvQ0FBQTtBQUpKOztBQU9BO0VBQ0ksY0FBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUFKSjs7QUFPQTtFQUNJLGFBQUE7RUFFQSxtQkFBQTtFQUNBLG1CQUFBO0FBSko7O0FBT0E7RUFDSSwwQkFBQTtFQUNBLDRCQUFBO0FBSko7O0FBT0E7RUFDSSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQUpKOztBQU9BO0VBQ0ksdUJBQUE7QUFKSjs7QUFPQTtFQUNJLGdCQUFBO0VBQ0EsZ0JBQUE7QUFKSjs7QUFPQTtFQUNJLFdBQUE7QUFKSjs7QUFPQTtFQUNJLHNDQUFBO0VBQ0EscUJBQUE7RUFDQSwrQkFBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQUpKOztBQU9BO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQUpKOztBQU9BO0VBQ0ksK0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtBQUpKOztBQU9BO0VBQ0ksK0JBQUE7QUFKSjs7QUFPQTtFQUNJLCtCQUFBO0VBQ0EsaUJBQUE7QUFKSjs7QUFPQTtFQUNJLGlCQUFBO0FBSko7O0FBT0E7RUFDSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtBQUpKOztBQU9BO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0VBQ0EsMEJBQUE7QUFKSjs7QUFPQTtFQUNJLGVBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtBQUpKOztBQU9BO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUFKSjs7QUFPQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0EsVUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUFKSjs7QUFPQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFKSjs7QUFPQTtFQUNJLFNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7QUFKSjs7QUFPQTtFQUNJLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtBQUpKIiwiZmlsZSI6InNyYy9hcHAvUGFnZXMvcGFzc3dvcmQtcGFnZS9wYXNzd29yZC1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIC5jaGJveCB7XHJcbi8vICAgICBtYXJnaW46IDNweDtcclxuLy8gICAgIHBhZGRpbmc6IDBweDtcclxuLy8gIH1cclxuXHJcbi5sb2dpbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0b3A6IDQwJTtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTQ5JSwgLTQ5JSk7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNDklLCAtNDklKTtcclxuICAgIHdpZHRoOiA5MCVcclxufVxyXG5cclxuLnJlZ2lzdHJhdGlvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDIlO1xyXG4gICAgbGVmdDogNSU7XHJcbiAgICB3aWR0aDogOTAlXHJcbn1cclxuXHJcbi5sb2dpbi1oZWFkZXIge1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG59XHJcblxyXG4ubG9nby1pbWFnZSB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHdpZHRoOiAxMjBweDtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG5baGVhZGluZy1jb2x1bW4tbHJdIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIG1hcmdpbi1ib3R0b206IDM3dnc7XHJcbn1cclxuXHJcbltiaWctaGVhZGluZ10ge1xyXG4gICAgZm9udC1zaXplOiAxMnZ3ICFpbXBvcnRhbnQ7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmspO1xyXG59XHJcblxyXG5bc21hbGwtaGVhZGluZ10ge1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiA0MHB4O1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG59XHJcblxyXG5bdHJhbnNwYXJlbnRdIHtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG5pb24taW5wdXQge1xyXG4gICAgZm9udC1zaXplOiA0LjV2dztcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxuXHJcbltpY29uLXNtYWxsXSB7XHJcbiAgICB3aWR0aDogMzBweDtcclxufVxyXG5cclxuaW9uLWl0ZW0ge1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XHJcbiAgICAtLWhpZ2hsaWdodC1jb2xvci1pbnZhbGlkOiAjMDAwO1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6ICNkZGQ7XHJcbiAgICAtLW1pbi1oZWlnaHQ6IDY1cHg7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XHJcbn1cclxuXHJcbmlvbi1sYWJlbCB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIG9wYWNpdHk6IC44O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5mb3Jnb3QtcGFzc3dvcmQge1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBwYWRkaW5nOiAxMHB4XHJcbn1cclxuXHJcbi5saW5rLWJ1dHRvbiB7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpXHJcbn1cclxuXHJcbi5kZW1vLWxvZ2luIHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICBtYXJnaW4tbGVmdDogODBweDtcclxufVxyXG5cclxuLmNoYm94IHtcclxuICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG59XHJcblxyXG4ubGFiZWwge1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDMwJTtcclxufVxyXG5cclxuLmxhYmVsLXRleHQge1xyXG4gICAgZm9udC1zaXplOiA1dnc7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG59XHJcblxyXG4uaW9uLWJ1dHRvbi1jbGFzcyB7XHJcbiAgICBtYXJnaW4tdG9wOiA2dnc7XHJcbiAgICB3aWR0aDogOTglO1xyXG4gICAgZm9udC1zaXplOiA0dndcclxufVxyXG5cclxuLndlbGNvbWUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAzMCU7XHJcbiAgICBmb250LXNpemU6IDQuNXZ3O1xyXG4gICAgY29sb3I6IGdyYXk7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG4udmVyc2lvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIGJvdHRvbTogNCU7XHJcbiAgICBmb250LXNpemU6IDR2dztcclxuICAgIGNvbG9yOiBncmF5O1xyXG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcclxufVxyXG5cclxuLmZvcmdvdC1wYXNzIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgYm90dG9tOiAxNSU7XHJcbiAgICBmb250LXNpemU6IDR2dztcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxuXHJcbi5zdWItbGluZSB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgZm9udC1zaXplOiAxMnZ3O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAxOSU7XHJcbn1cclxuXHJcbi5iYWNrLWJ1dHRvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDclO1xyXG4gICAgbGVmdDogLTMlO1xyXG4gICAgZm9udC1zaXplOiA2dnc7XHJcbiAgICBtYXJnaW46IDA7XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/Pages/password-page/password-page.page.ts":
    /*!***********************************************************!*\
      !*** ./src/app/Pages/password-page/password-page.page.ts ***!
      \***********************************************************/

    /*! exports provided: PasswordPagePage */

    /***/
    function srcAppPagesPasswordPagePasswordPagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PasswordPagePage", function () {
        return PasswordPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./../../Services/storage-service.service */
      "./src/app/Services/storage-service.service.ts");
      /* harmony import */


      var _Services_alert_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./../../Services/alert-service.service */
      "./src/app/Services/alert-service.service.ts");
      /* harmony import */


      var _Services_otp_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./../../Services/otp-service.service */
      "./src/app/Services/otp-service.service.ts");
      /* harmony import */


      var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./../../Services/api-service.service */
      "./src/app/Services/api-service.service.ts");
      /* harmony import */


      var src_app_Services_user_details_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/Services/user-details.service */
      "./src/app/Services/user-details.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var PasswordPagePage = /*#__PURE__*/function () {
        function PasswordPagePage(router, storage, navController, storageService, alertService, otpService, apiService, userDetailsService) {
          _classCallCheck(this, PasswordPagePage);

          this.router = router;
          this.storage = storage;
          this.navController = navController;
          this.storageService = storageService;
          this.alertService = alertService;
          this.otpService = otpService;
          this.apiService = apiService;
          this.userDetailsService = userDetailsService;
          this.password = "";
          this.enteredOTP = "";
          this.inputType = "password";
          this.iconName = "eye";
          this.showErrorMessage = false;
        }

        _createClass(PasswordPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "onLogin",
          value: function onLogin() {
            if (this.password == this.storageService.userInfo.AppPassword) {
              this.password = "";
              this.userDetailsService.getStudentAccess(this.storageService.userInfo.UserId);
              this.showErrorMessage = false;
            } else this.showErrorMessage = true;
          }
        }, {
          key: "onForgotPassClick",
          value: function onForgotPassClick() {
            this.router.navigateByUrl("/forgot-password-page");
            this.showErrorMessage = false;
          }
        }, {
          key: "changeInputType",
          value: function changeInputType() {
            if (this.inputType == "password") {
              this.inputType = "text";
              this.iconName = "eye-off";
            } else {
              this.inputType = "password";
              this.iconName = "eye";
            }
          }
        }, {
          key: "onBackClick",
          value: function onBackClick() {
            this.navController.back();
          }
        }]);

        return PasswordPagePage;
      }();

      PasswordPagePage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["NavController"]
        }, {
          type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_4__["StorageServiceService"]
        }, {
          type: _Services_alert_service_service__WEBPACK_IMPORTED_MODULE_5__["AlertServiceService"]
        }, {
          type: _Services_otp_service_service__WEBPACK_IMPORTED_MODULE_6__["OtpServiceService"]
        }, {
          type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_7__["ApiServiceService"]
        }, {
          type: src_app_Services_user_details_service__WEBPACK_IMPORTED_MODULE_8__["UserDetailsService"]
        }];
      };

      PasswordPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-password-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./password-page.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/password-page/password-page.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./password-page.page.scss */
        "./src/app/Pages/password-page/password-page.page.scss"))["default"]]
      })], PasswordPagePage);
      /***/
    },

    /***/
    "./src/app/Services/toast-service.service.ts":
    /*!***************************************************!*\
      !*** ./src/app/Services/toast-service.service.ts ***!
      \***************************************************/

    /*! exports provided: ToastServiceService */

    /***/
    function srcAppServicesToastServiceServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ToastServiceService", function () {
        return ToastServiceService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var ToastServiceService = /*#__PURE__*/function () {
        function ToastServiceService(toastController) {
          _classCallCheck(this, ToastServiceService);

          this.toastController = toastController;
        }

        _createClass(ToastServiceService, [{
          key: "createToast",
          value: function createToast(message, duration) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        animated: true,
                        duration: duration,
                        position: "bottom",
                        message: message
                      });

                    case 2:
                      toast = _context.sent;
                      _context.next = 5;
                      return toast.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return ToastServiceService;
      }();

      ToastServiceService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      ToastServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ToastServiceService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=Pages-password-page-password-page-module-es5.js.map