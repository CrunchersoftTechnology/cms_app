(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["daily-practice-paper-page-daily-practice-paper-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.page.html":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.page.html ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar\n  color=\"primary\"\n  mode=\"ios\"\n  *ngIf=\"storageService.instituteName.length > 0\"\n>\n  <ion-title>{{storageService.instituteName}}</ion-title>\n</ion-toolbar>\n  <ion-toolbar color=\"primary\" mode=\"ios\">\n    <ion-buttons class=\"menuButton\" slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Daily Practice Paper</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content\n      pullingIcon=\"chevron-down-circle-outline\"\n      pullingText=\"Pull to refresh\"\n      refreshingSpinner=\"circles\"\n      refreshingText=\"Refreshing...\"\n    >\n    </ion-refresher-content>\n  </ion-refresher>\n  <app-calender-component\n    *ngIf=\"eventSource.length > 0; else elseBlock\"\n    [eventSource]=\"eventSource\"\n    (onEventClicked)=\"handleEventClicked($event)\"\n  ></app-calender-component\n>\n<ng-template class=\"ion-no-margin ion-no-padding\" #elseBlock>\n  <ion-row>\n    <ion-col size=\"2\">\n      <ion-button fill=\"clear\" (click)=\"back()\">\n        <ion-icon name=\"arrow-back\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </ion-col>\n\n    <ion-col size=\"8\" class=\"ion-text-center\">\n      <h5>{{ viewTitle }}</h5>\n    </ion-col>\n\n    <ion-col size=\"2\">\n      <ion-button fill=\"clear\" (click)=\"next()\">\n        <ion-icon name=\"arrow-forward\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </ion-col>\n  </ion-row>\n\n  <calendar\n    [eventSource]=\"eventSource\"\n    [calendarMode]=\"calendar.mode\"\n    [currentDate]=\"calendar.currentDate\"\n    (onTitleChanged)=\"onViewTitleChanged($event)\"\n    startHour=\"6\"\n    endHour=\"20\"\n    step=\"30\"\n    startingDayWeek=\"1\"\n  >\n  </calendar>\n</ng-template>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/Services/toast-service.service.ts":
/*!***************************************************!*\
  !*** ./src/app/Services/toast-service.service.ts ***!
  \***************************************************/
/*! exports provided: ToastServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastServiceService", function() { return ToastServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let ToastServiceService = class ToastServiceService {
    constructor(toastController) {
        this.toastController = toastController;
    }
    createToast(message, duration) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let toast = yield this.toastController.create({
                animated: true,
                duration: duration,
                position: "bottom",
                message: message
            });
            yield toast.present();
        });
    }
};
ToastServiceService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
ToastServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ToastServiceService);



/***/ }),

/***/ "./src/app/pages/daily-practice-paper-page/daily-practice-paper-page-routing.module.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/daily-practice-paper-page/daily-practice-paper-page-routing.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: DailyPracticePaperPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DailyPracticePaperPagePageRoutingModule", function() { return DailyPracticePaperPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _daily_practice_paper_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./daily-practice-paper-page.page */ "./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.page.ts");




const routes = [
    {
        path: '',
        component: _daily_practice_paper_page_page__WEBPACK_IMPORTED_MODULE_3__["DailyPracticePaperPagePage"]
    }
];
let DailyPracticePaperPagePageRoutingModule = class DailyPracticePaperPagePageRoutingModule {
};
DailyPracticePaperPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DailyPracticePaperPagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.module.ts ***!
  \*************************************************************************************/
/*! exports provided: DailyPracticePaperPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DailyPracticePaperPagePageModule", function() { return DailyPracticePaperPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _daily_practice_paper_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./daily-practice-paper-page-routing.module */ "./src/app/pages/daily-practice-paper-page/daily-practice-paper-page-routing.module.ts");
/* harmony import */ var _daily_practice_paper_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./daily-practice-paper-page.page */ "./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.page.ts");
/* harmony import */ var src_app_Components_calender_component_calender_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/Components/calender-component/calender-component.module */ "./src/app/Components/calender-component/calender-component.module.ts");
/* harmony import */ var ionic2_calendar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ionic2-calendar */ "./node_modules/ionic2-calendar/__ivy_ngcc__/fesm2015/ionic2-calendar.js");









let DailyPracticePaperPagePageModule = class DailyPracticePaperPagePageModule {
};
DailyPracticePaperPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _daily_practice_paper_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["DailyPracticePaperPagePageRoutingModule"],
            ionic2_calendar__WEBPACK_IMPORTED_MODULE_8__["NgCalendarModule"],
            src_app_Components_calender_component_calender_component_module__WEBPACK_IMPORTED_MODULE_7__["CalenderComponentModule"]
        ],
        declarations: [_daily_practice_paper_page_page__WEBPACK_IMPORTED_MODULE_6__["DailyPracticePaperPagePage"]],
    })
], DailyPracticePaperPagePageModule);



/***/ }),

/***/ "./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.page.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.page.scss ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ::ng-deep .monthview-eventdetail-timecolumn {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGFpbHktcHJhY3RpY2UtcGFwZXItcGFnZS9kYWlseS1wcmFjdGljZS1wYXBlci1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGFBQUE7QUFBUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RhaWx5LXByYWN0aWNlLXBhcGVyLXBhZ2UvZGFpbHktcHJhY3RpY2UtcGFwZXItcGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCA6Om5nLWRlZXAge1xyXG4gICAgLm1vbnRodmlldy1ldmVudGRldGFpbC10aW1lY29sdW1uIHtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgfVxyXG59Il19 */");

/***/ }),

/***/ "./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.page.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.page.ts ***!
  \***********************************************************************************/
/*! exports provided: DailyPracticePaperPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DailyPracticePaperPagePage", function() { return DailyPracticePaperPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../Services/internet-service.service */ "./src/app/Services/internet-service.service.ts");
/* harmony import */ var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");
/* harmony import */ var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../Services/api-service.service */ "./src/app/Services/api-service.service.ts");
/* harmony import */ var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../Services/toast-service.service */ "./src/app/Services/toast-service.service.ts");
/* harmony import */ var _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/photo-viewer/ngx */ "./node_modules/@ionic-native/photo-viewer/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _Services_daily_practice_paper_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../../Services/daily-practice-paper-service.service */ "./src/app/Services/daily-practice-paper-service.service.ts");
/* harmony import */ var _ionic_native_preview_any_file_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/preview-any-file/ngx */ "./node_modules/@ionic-native/preview-any-file/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var ionic2_calendar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ionic2-calendar */ "./node_modules/ionic2-calendar/__ivy_ngcc__/fesm2015/ionic2-calendar.js");











let DailyPracticePaperPagePage = class DailyPracticePaperPagePage {
    constructor(photoViewer, previewAnyFile, internetService, storageService, apiService, dailyPracticePaperService, toastService, inAppBrowser) {
        this.photoViewer = photoViewer;
        this.previewAnyFile = previewAnyFile;
        this.internetService = internetService;
        this.storageService = storageService;
        this.apiService = apiService;
        this.dailyPracticePaperService = dailyPracticePaperService;
        this.toastService = toastService;
        this.inAppBrowser = inAppBrowser;
        this.eventSource = [];
        this.fileInfo = [];
        this.fileInfoDatabase = [];
        this.userId = "";
        this.branchId = "";
        this.classId = "";
        this.batchId = "";
        this.calendar = {
            mode: "month",
            currentDate: new Date(),
        };
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.userId = this.storageService.userDetails.userId;
        this.branchId = this.storageService.userDetails.branchId;
        this.classId = this.storageService.userDetails.classId;
        this.batchId = this.storageService.userDetails.batchId;
        if (this.internetService.networkConnected) {
            this.getDailyPracticePaper(this.userId, this.branchId, this.classId, this.batchId);
        }
        else {
            this.toastService.createToast("Check internet connection to update details", 3000);
            this.showDatePickerWithDppDate();
        }
    }
    getDailyPracticePaper(userId, branchId, classId, batchId) {
        this.apiService.getDailyPracticePaper(branchId, classId, batchId).then((result) => {
            let data = JSON.parse(result.data);
            let length = data.d.length;
            if (length > 0) {
                this.dailyPracticePaperService
                    .deleteFromCMSDailyPracticePaper()
                    .then((_) => {
                    this.fileInfo = data.d;
                    console.log(this.fileInfo);
                    this.fileInfo.map((file) => {
                        this.dailyPracticePaperService.insertIntoCMSDailyPracticePaper(file.Description, file.Date, file.FileName, file.AttachmentDescription);
                    });
                    this.showDatePickerWithDppDate();
                });
            }
            else {
                this.dailyPracticePaperService
                    .deleteFromCMSDailyPracticePaper()
                    .then((_) => {
                    this.showDatePickerWithDppDate();
                });
            }
        }, (err) => {
            this.showDatePickerWithDppDate();
        });
    }
    showDatePickerWithDppDate() {
        this.eventSource = [];
        this.dailyPracticePaperService
            .selectFromCMSDailyPracticePapere()
            .then((data) => {
            this.fileInfoDatabase = data;
            // console.log("this.fileInfoDatabase", this.fileInfoDatabase);
            this.fileInfoDatabase.map((fileInfo) => {
                let selectedDate = fileInfo.Date.split(" ")[0];
                let date = Number.parseInt(selectedDate.split("-")[0]);
                let month = Number.parseInt(selectedDate.split("-")[1]);
                let year = Number.parseInt(selectedDate.split("-")[2]);
                let startTime = new Date(year, month - 1, date + 1);
                let endTime = new Date(year, month - 1, date + 1);
                this.eventSource.push({
                    title: `${fileInfo.Description} - ${fileInfo.AttachmentDescription}`,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true,
                    desc: fileInfo.FileName,
                });
            });
        });
    }
    handleEventClicked(event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(event);
            if (this.internetService.networkConnected) {
                if (event.desc.split(".")[1] == "jpeg" ||
                    event.desc.split(".")[1] == "jpg" ||
                    event.desc.split(".")[1] == "gif" ||
                    event.desc.split(".")[1] == "png")
                    this.photoViewer.show(`${this.apiService.fileUrl}PDF/DailyPracticePaperFile/${event.desc}`);
                else
                    // this.previewAnyFile.preview(
                    //   `${this.apiService.fileUrl}PDF/DailyPracticePaperFile/${event.desc}`
                    // );
                    this.inAppBrowser.create(`https://docs.google.com/viewer?url=${this.apiService.fileUrl}PDF/DailyPracticePaperFile/${event.desc}&embedded=true`, "_self", { location: "no" });
            }
            else
                this.toastService.createToast("Check your internet connection to proceed", 3000);
        });
    }
    doRefresh(event) {
        if (this.internetService.networkConnected) {
            this.getDailyPracticePaper(this.userId, this.branchId, this.classId, this.batchId);
        }
        else {
            this.toastService.createToast("Check internet connection to update details", 3000);
            this.showDatePickerWithDppDate();
        }
        setTimeout(() => {
            event.target.complete();
        }, 2000);
    }
    next() {
        this.myCal.slideNext();
    }
    back() {
        this.myCal.slidePrev();
    }
    // Selected date reange and hence title changed
    onViewTitleChanged(title) {
        this.viewTitle = title;
    }
};
DailyPracticePaperPagePage.ctorParameters = () => [
    { type: _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_6__["PhotoViewer"] },
    { type: _ionic_native_preview_any_file_ngx__WEBPACK_IMPORTED_MODULE_8__["PreviewAnyFile"] },
    { type: _Services_internet_service_service__WEBPACK_IMPORTED_MODULE_2__["InternetServiceService"] },
    { type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_3__["StorageServiceService"] },
    { type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_4__["ApiServiceService"] },
    { type: _Services_daily_practice_paper_service_service__WEBPACK_IMPORTED_MODULE_7__["DailyPracticePaperServiceService"] },
    { type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_5__["ToastServiceService"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_9__["InAppBrowser"] }
];
DailyPracticePaperPagePage.propDecorators = {
    myCal: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: [ionic2_calendar__WEBPACK_IMPORTED_MODULE_10__["CalendarComponent"],] }]
};
DailyPracticePaperPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-daily-practice-paper-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./daily-practice-paper-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./daily-practice-paper-page.page.scss */ "./src/app/pages/daily-practice-paper-page/daily-practice-paper-page.page.scss")).default]
    })
], DailyPracticePaperPagePage);



/***/ })

}]);
//# sourceMappingURL=daily-practice-paper-page-daily-practice-paper-page-module-es2015.js.map