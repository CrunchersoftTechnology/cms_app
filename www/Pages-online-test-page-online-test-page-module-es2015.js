(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Pages-online-test-page-online-test-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/online-test-page/online-test-page.page.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/online-test-page/online-test-page.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [hidden]=\"testView == false\">\n  <ion-toolbar class=\"toolbar\" color=\"primary\">\n    <ion-icon\n      *ngIf=\"testSubmitted == true || page == 'bookmark'\"\n      slot=\"start\"\n      class=\"ion-margin-start\"\n      (click)=\"onBackClick()\"\n      name=\"arrow-back-outline\"\n      size=\"large\"\n    >\n    </ion-icon>\n    <ion-label style=\"font-size: 20px\" class=\"ion-margin-start\">\n      {{index + 1}}/{{questionsList.length}}\n    </ion-label>\n    <ion-item\n      [hidden]=\"testSubmitted == true || page == 'bookmark'\"\n      lines=\"none\"\n      slot=\"end\"\n      class=\"ion-margin-end ion-no-margin-bottom ion-no-padding\"\n      color=\"primary\"\n    >\n      <ion-label class=\"ion-margin-start\" style=\"font-size: 20px\">\n        <ion-text color=\"light\" id=\"timer\">00:00</ion-text>\n      </ion-label>\n    </ion-item>\n    <ion-item\n      *ngIf=\"page == 'saved-test' && testSubmitted == true\"\n      lines=\"none\"\n      slot=\"end\"\n      class=\"ion-margin-end ion-no-margin-bottom ion-no-padding\"\n      color=\"primary\"\n      style=\"width: 100px\"\n    >\n      <ion-label class=\"ion-no-margin\">\n        <ion-text color=\"light\">{{testGivenDate}}</ion-text>\n      </ion-label>\n    </ion-item>\n  </ion-toolbar>\n</ion-header>\n<ion-item\n  class=\"ion-no-margin-top header-item\"\n  color=\"primary\"\n  [hidden]=\"testView == false\"\n>\n  <!-- <ion-label\n    *ngIf=\"(page == 'unit-test' || page == 'paused-test' || page == 'question-papers') && testSubmitted == false\"\n    class=\"ion-margin-start\"\n    (click)=\"testInfo()\"\n    >{{testType}}</ion-label\n  > -->\n  <ion-label\n    *ngIf=\"(page == 'saved-test' || page == 'unit-test') && testSubmitted == true\"\n    >{{questionStatus}}</ion-label\n  >\n\n  <!-- <ion-icon\n    slot=\"end\"\n    [name]=\"bookmarkIcon\"\n    size=\"default\"\n    *ngIf=\"(page == 'unit-test' || page == 'saved-test')\"\n    (click)=\"bookmark()\"\n  ></ion-icon> -->\n  <ion-icon\n    slot=\"end\"\n    name=\"list-circle-outline\"\n    class=\"ion-margin-end\"\n    size=\"default\"\n    *ngIf=\"testSubmitted==false\"\n    (click)=\"go()\"\n  ></ion-icon>\n</ion-item>\n\n<ion-content class=\"ion-no-padding\" [hidden]=\"testView == false\">\n  <div class=\"marking\" *ngIf=\"page != 'saved-test'\">\n    <ion-text color=\"success\" *ngIf=\"testDetails.TestType == '1'\">\n      {{ testDetails.NeetCorrect }}.0\n    </ion-text>\n    <ion-text\n      color=\"success\"\n      *ngIf=\"testDetails.TestType == '2' || testDetails.TestType == '4'\"\n    >\n      {{ showInputBox ? testDetails.JeeNewCorrect : testDetails.JeeCorrect}}.0\n    </ion-text>\n    <ion-text\n      color=\"success\"\n      *ngIf=\"testDetails.TestType == '3' || testDetails.TestType == '5'\"\n    >\n      {{ SubjectTestId == 1 || SubjectTestId == 2 || SubjectTestId == 4 ? \"1\" :\n      \"2\"}}.0\n    </ion-text>\n    ,\n    <ion-text color=\"danger\" *ngIf=\"testDetails.TestType == '1'\">\n      -{{testDetails.NeetInCorrect}}.0\n    </ion-text>\n    <ion-text\n      color=\"danger\"\n      *ngIf=\"testDetails.TestType == '2' || testDetails.TestType == '4'\"\n    >\n      -{{showInputBox ? testDetails.JeeNewInCorrect :\n      testDetails.JeeInCorrect}}.0\n    </ion-text>\n    <ion-text\n      color=\"danger\"\n      *ngIf=\"testDetails.TestType == '3' || testDetails.TestType == '5'\"\n    >\n      0.0\n    </ion-text>\n  </div>\n\n  <!-- <div class=\"marking\" *ngIf=\"page == 'saved-test'\">\n    <ion-text color=\"success\" *ngIf=\"testType == '1'\">\n      {{ testDetails.NeetCorrect }}.0\n    </ion-text>\n    <ion-text\n      color=\"success\"\n      *ngIf=\"testDetails.TestType == '2' || testDetails.TestType == '4'\"\n    >\n      {{ showInputBox ? testDetails.JeeNewCorrect : testDetails.JeeCorrect}}.0\n    </ion-text>\n    <ion-text color=\"success\" *ngIf=\"testDetails.TestType == '3'\">\n      {{ SubjectTestId == 1 || SubjectTestId == 2 || SubjectTestId == 4 ? \"1\" :\n      \"2\"}}.0\n    </ion-text>\n    ,\n    <ion-text color=\"danger\" *ngIf=\"testDetails.TestType == '1'\">\n      -{{testDetails.NeetInCorrect}}.0\n    </ion-text>\n    <ion-text\n      color=\"danger\"\n      *ngIf=\"testDetails.TestType == '2' || testDetails.TestType == '4'\"\n    >\n      -{{showInputBox ? testDetails.JeeNewInCorrect :\n      testDetails.JeeInCorrect}}.0\n    </ion-text>\n    <ion-text color=\"danger\" *ngIf=\"testDetails.TestType == '3'\">\n      0.0\n    </ion-text>\n  </div> -->\n  <ion-item lines=\"none\" class=\"ion-no-padding ion-no-margin\">\n    <ion-label\n      class=\"ion-text-wrap ion-margin-start ion-margin-end ion-no-padding\"\n      style=\"overflow-x: scroll\"\n    >\n      <ion-text id=\"question\"></ion-text>\n    </ion-label>\n  </ion-item>\n\n  <ion-item\n    style=\"display: block\"\n    lines=\"none\"\n    class=\"ion-no-padding ion-no-margin ion-margin-start\"\n    [hidden]=\"questionImage\"\n  >\n    <img id=\"queImg\" [src]=\"questionImagePath\" />\n  </ion-item>\n\n  <ion-item class=\"ion-no-padding ion-no-margin\" *ngIf=\"iconCount == 1\">\n    <ion-icon class=\"ion-margin-start\" name=\"star\" color=\"warning\"></ion-icon>\n  </ion-item>\n\n  <ion-item class=\"ion-no-padding ion-no-margin\" *ngIf=\"iconCount == 2\">\n    <ion-icon class=\"ion-margin-start\" name=\"star\" color=\"warning\"></ion-icon>\n    <ion-icon name=\"star\" color=\"warning\"></ion-icon>\n  </ion-item>\n\n  <ion-item class=\"ion-no-padding ion-no-margin\" *ngIf=\"iconCount == 3\">\n    <ion-icon class=\"ion-margin-start\" name=\"star\" color=\"warning\"></ion-icon>\n    <ion-icon name=\"star\" color=\"warning\"></ion-icon>\n    <ion-icon name=\"star\" color=\"warning\"></ion-icon>\n  </ion-item>\n\n  <div [hidden]=\"showInputBox\">\n    <ion-item\n      (click)=\"clickOpt1()\"\n      class=\"ion-no-padding\"\n      [style]=\"option == 'A' ? '--border-color: var(--ion-color-success);' : (inCorrOption == 'A' ? '--border-color: var(--ion-color-danger);' : '')\"\n      [hidden]=\"!optionImage\"\n    >\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"1.5\">\n            <div\n              [style]=\"option == 'A' ? 'background-color: var(--ion-color-success);' : (inCorrOption == 'A' ? 'background-color: var(--ion-color-danger);' : 'background-color: var(--ion-color-primary);')\"\n              class=\"options ion-no-margin ion-no-padding ion-text-center\"\n              lines=\"none\"\n            >\n              <ion-text color=\"light\"> A </ion-text>\n            </div>\n          </ion-col>\n          <ion-col size=\"10.5\">\n            <ion-label\n              class=\"ion-text-wrap ion-padding-start\"\n              style=\"overflow-x: scroll\"\n            >\n              <ion-text\n                class=\"opt\"\n                id=\"opt1\"\n                style=\"overflow-x: scroll\"\n              ></ion-text>\n            </ion-label>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n    <ion-item\n      (click)=\"clickOpt2()\"\n      class=\"ion-no-padding\"\n      [style]=\"option == 'B' ? '--border-color: var(--ion-color-success);' : (inCorrOption == 'B' ? '--border-color: var(--ion-color-danger);' : '')\"\n      [hidden]=\"!optionImage\"\n    >\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"1.5\">\n            <div\n              [style]=\"option == 'B' ? 'background-color: var(--ion-color-success);' : (inCorrOption == 'B' ? 'background-color: var(--ion-color-danger);' : 'background-color: var(--ion-color-primary);')\"\n              class=\"options ion-no-margin ion-no-padding ion-text-center\"\n              lines=\"none\"\n            >\n              <ion-text color=\"light\"> B </ion-text>\n            </div>\n          </ion-col>\n          <ion-col size=\"10.5\">\n            <ion-label\n              class=\"ion-text-wrap ion-padding-start\"\n              style=\"overflow-x: scroll\"\n            >\n              <ion-text\n                class=\"opt\"\n                id=\"opt2\"\n                style=\"overflow-x: scroll\"\n              ></ion-text>\n            </ion-label>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n    <ion-item\n      (click)=\"clickOpt3()\"\n      class=\"ion-no-padding\"\n      [style]=\"option == 'C' ? '--border-color: var(--ion-color-success);' : (inCorrOption == 'C' ? '--border-color: var(--ion-color-danger);' : '')\"\n      [hidden]=\"!optionImage\"\n    >\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"1.5\">\n            <div\n              [style]=\"option == 'C' ? 'background-color: var(--ion-color-success);' : (inCorrOption == 'C' ? 'background-color: var(--ion-color-danger);' : 'background-color: var(--ion-color-primary);')\"\n              class=\"options ion-no-margin ion-no-padding ion-text-center\"\n              lines=\"none\"\n            >\n              <ion-text color=\"light\"> C </ion-text>\n            </div>\n          </ion-col>\n          <ion-col size=\"10.5\">\n            <ion-label\n              class=\"ion-text-wrap ion-padding-start\"\n              style=\"overflow-x: scroll\"\n            >\n              <ion-text\n                class=\"opt\"\n                id=\"opt3\"\n                style=\"overflow-x: scroll\"\n              ></ion-text>\n            </ion-label>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n    <ion-item\n      (click)=\"clickOpt4()\"\n      class=\"ion-no-padding\"\n      [style]=\"option == 'D' ? '--border-color: var(--ion-color-success);' : (inCorrOption == 'D' ? '--border-color: var(--ion-color-danger);' : '')\"\n      [hidden]=\"!optionImage\"\n    >\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"1.5\">\n            <div\n              [style]=\"option == 'D' ? 'background-color: var(--ion-color-success);' : (inCorrOption == 'D' ? 'background-color: var(--ion-color-danger);' : 'background-color: var(--ion-color-primary);')\"\n              class=\"options ion-no-margin ion-no-padding ion-text-center\"\n              lines=\"none\"\n            >\n              <ion-text color=\"light\"> D </ion-text>\n            </div>\n          </ion-col>\n          <ion-col size=\"10.5\">\n            <ion-label\n              class=\"ion-text-wrap ion-padding-start\"\n              style=\"overflow-x: scroll\"\n            >\n              <ion-text\n                class=\"opt\"\n                id=\"opt4\"\n                style=\"overflow-x: scroll\"\n              ></ion-text>\n            </ion-label>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n\n    <ion-item\n      style=\"display: block\"\n      lines=\"none\"\n      class=\"ion-no-padding ion-no-margin ion-margin-start\"\n      [hidden]=\"optionImage\"\n    >\n      <img id=\"queImg\" [src]=\"optionImagePath\" />\n    </ion-item>\n\n    <ion-grid [hidden]=\"optionImage\">\n      <ion-row>\n        <ion-col push=\"0.75\" size=\"3\">\n          <div\n            (click)=\"clickOpt1()\"\n            [style]=\"option == 'A' ? 'background-color: var(--ion-color-success);' : (inCorrOption == 'A' ? 'background-color: var(--ion-color-danger);' : 'background-color: var(--ion-color-primary);')\"\n            class=\"options1 ion-no-margin ion-no-padding ion-text-center\"\n            lines=\"none\"\n          >\n            <ion-text color=\"light\"> 1 </ion-text>\n          </div>\n        </ion-col>\n        <ion-col push=\"0.75\" size=\"3\">\n          <div\n            (click)=\"clickOpt2()\"\n            [style]=\"option == 'B' ? 'background-color: var(--ion-color-success);' : (inCorrOption == 'B' ? 'background-color: var(--ion-color-danger);' : 'background-color: var(--ion-color-primary);')\"\n            class=\"options1 ion-no-margin ion-no-padding ion-text-center\"\n            lines=\"none\"\n          >\n            <ion-text color=\"light\"> 2 </ion-text>\n          </div>\n        </ion-col>\n        <ion-col push=\"0.75\" size=\"3\">\n          <div\n            (click)=\"clickOpt3()\"\n            [style]=\"option == 'C' ? 'background-color: var(--ion-color-success);' : (inCorrOption == 'C' ? 'background-color: var(--ion-color-danger);' : 'background-color: var(--ion-color-primary);')\"\n            class=\"options1 ion-no-margin ion-no-padding ion-text-center\"\n            lines=\"none\"\n          >\n            <ion-text color=\"light\"> 3 </ion-text>\n          </div>\n        </ion-col>\n        <ion-col push=\"0.75\" size=\"3\">\n          <div\n            (click)=\"clickOpt4()\"\n            [style]=\"option == 'D' ? 'background-color: var(--ion-color-success);' : (inCorrOption == 'D' ? 'background-color: var(--ion-color-danger);' : 'background-color: var(--ion-color-primary);')\"\n            class=\"options1 ion-no-margin ion-no-padding ion-text-center\"\n            lines=\"none\"\n          >\n            <ion-text color=\"light\"> 4 </ion-text>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <div [hidden]=\"!showInputBox\">\n    <ion-grid class=\"ion-no-padding ion-no-margin\">\n      <ion-row>\n        <ion-col size=\"7\">\n          <ion-item>\n            <ion-label class=\"input-label\">Answer:</ion-label>\n            <ion-input\n              [(ngModel)]=\"answer\"\n              (ionChange)=\"onAnswerChange()\"\n              class=\"input\"\n              [disabled]=\"testSubmitted\"\n            ></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"5\">\n          <ion-item>\n            <ion-label class=\"input-label\">Unit:</ion-label>\n            <ion-input\n              [(ngModel)]=\"unit\"\n              (ionChange)=\"onUnitChange()\"\n              class=\"input\"\n              [disabled]=\"testSubmitted || answer.length == 0\"\n            ></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <ion-item\n    *ngIf=\"testSubmitted == true || page == 'bookmark'\"\n    class=\"ion-no-padding ion-no-margin\"\n    lines=\"none\"\n  >\n    <ion-label class=\"ion-text-center\">\n      <ion-text color=\"primary\"> SOLUTION </ion-text>\n    </ion-label>\n  </ion-item>\n\n  <ion-item lines=\"none\">\n    <ion-label\n      style=\"overflow-x: scroll\"\n      class=\"ion-text-wrap\"\n      id=\"hint\"\n    ></ion-label>\n  </ion-item>\n\n  <ion-item\n    style=\"display: block\"\n    lines=\"none\"\n    class=\"ion-no-padding ion-no-margin ion-margin-start ion-margin-bottom\"\n    [hidden]=\"hintImage\"\n  >\n    <img id=\"queImg\" [src]=\"hintImagePath\" />\n  </ion-item>\n</ion-content>\n\n<!-- Result Section -->\n\n<ion-content\n  [hidden]=\"testView == true\"\n  style=\"--background: rgba(0, 0, 0, 0.2)\"\n  class=\"ion-no-padding\"\n>\n  <div class=\"div ion-no-padding\">\n    <ion-icon\n      class=\"div-icon\"\n      name=\"close\"\n      (click)=\"exitTest()\"\n      size=\"large\"\n      color=\"light\"\n    ></ion-icon>\n    <!-- <ion-avatar class=\"avatar\">\n      <img [src]=\"storageServiceService.userInfo.imageData\" />\n    </ion-avatar> -->\n\n    <ion-card class=\"div-card\">\n      <ion-card-content class=\"ion-no-padding\">\n        <ion-item class=\"ion-no-padding item-margin\">\n          <ion-label\n            class=\"ion-text-center\"\n            style=\"font-weight: bold; font-size: 4vw\"\n          >\n            {{storageService.userDetails.studentName}}\n          </ion-label>\n        </ion-item>\n        <ion-item class=\"ion-no-margin ion-no-padding\" lines=\"none\">\n          <ion-label\n            style=\"font-weight: bold; font-size: 4vw\"\n            class=\"ion-text-center\"\n          >\n            {{testTitle}}\n          </ion-label>\n        </ion-item>\n\n        <div color=\"primary\" class=\"score-card\">\n          <div class=\"inner-div\" style=\"width: 100%\">\n            <ion-label\n              class=\"ion-text-center\"\n              style=\"font-weight: bold; font-size: 4vw; margin-top: 2vw\"\n            >\n              <ion-text color=\"light\"> SCORE </ion-text>\n            </ion-label>\n          </div>\n\n          <div class=\"inner-div\" style=\"width: 100%\">\n            <ion-label\n              class=\"ion-text-center\"\n              style=\"font-weight: bold; font-size: 6vw\"\n            >\n              <ion-text color=\"light\"> {{totalMarks}} </ion-text>\n            </ion-label>\n          </div>\n          <div class=\"inner-div\" style=\"width: 100%\">\n            <ion-label\n              class=\"ion-text-center\"\n              style=\"font-weight: bold; font-size: 4vw; margin-bottom: 2vw\"\n            >\n              <ion-text color=\"light\"> Out of {{outOfMarks}} </ion-text>\n            </ion-label>\n          </div>\n        </div>\n        <ion-item class=\"ion-no-padding\">\n          <ion-label\n            class=\"ion-text-center\"\n            style=\"font-weight: bold; font-size: 4vw\"\n            >Time Taken: {{totalMins}} min, {{totalSecs}} sec\n          </ion-label>\n        </ion-item>\n        <!-- <ion-item class=\"ion-no-padding\" lines=\"none\">\n          <ion-label\n            class=\"ion-text-center\"\n            style=\"font-weight: bold; font-size: 4vw\"\n            >Correct: {{corrMarks}} marks\n          </ion-label>\n          <ion-label\n            class=\"ion-text-center\"\n            style=\"font-weight: bold; font-size: 4vw\"\n            >Incorrect: -{{inCorrMarks}} marks\n          </ion-label>\n        </ion-item> -->\n      </ion-card-content>\n    </ion-card>\n\n    <div class=\"div2\">\n      <ion-card class=\"div2-card ion-margin\">\n        <ion-card-content class=\"ion-no-padding ion-padding-bottom\">\n          <ion-item class=\"ion-no-padding\">\n            <ion-label class=\"ion-text-center\">\n              <ion-text color=\"success\"> Correct: {{correctPer}}% </ion-text>\n            </ion-label>\n            <ion-label class=\"ion-text-center\">\n              <ion-text color=\"danger\"> Incorrect: {{inCorrectPer}}% </ion-text>\n            </ion-label>\n            <ion-label class=\"ion-text-center\">\n              <ion-text color=\"primary\"> Skipped: {{skippedPer}}% </ion-text>\n            </ion-label>\n          </ion-item>\n          <canvas #pieChart1></canvas>\n          <ion-item class=\"ion-no-padding\">\n            <ion-icon\n              class=\"ion-margin-start\"\n              slot=\"start\"\n              name=\"checkmark\"\n              color=\"success\"\n            ></ion-icon>\n            <ion-label style=\"font-weight: bold\"> Correct </ion-label>\n            <ion-badge color=\"success\">{{correctQuestions}}</ion-badge>\n          </ion-item>\n          <ion-item class=\"ion-no-padding\">\n            <ion-icon\n              class=\"ion-margin-start\"\n              name=\"close-circle\"\n              color=\"danger\"\n              slot=\"start\"\n            ></ion-icon>\n            <ion-label style=\"font-weight: bold\"> Incorrect </ion-label>\n            <ion-badge color=\"danger\">{{inCorrectQuestions}}</ion-badge>\n          </ion-item>\n          <ion-item class=\"ion-no-padding\" lines=\"none\">\n            <ion-icon\n              class=\"ion-margin-start\"\n              name=\"chevron-forward-circle\"\n              color=\"primary\"\n              slot=\"start\"\n            ></ion-icon>\n            <ion-label style=\"font-weight: bold\"> Skipped </ion-label>\n            <ion-badge color=\"primary\">{{skippedQuestions}}</ion-badge>\n          </ion-item>\n        </ion-card-content>\n      </ion-card>\n\n      <ion-card class=\"div2-card ion-margin\">\n        <ion-card-content class=\"ion-no-padding ion-padding-bottom\">\n          <ion-item class=\"ion-no-padding\">\n            <ion-label class=\"ion-text-center\">\n              <ion-text color=\"success\"> Accuracy: {{accuracy}}% </ion-text>\n            </ion-label>\n          </ion-item>\n          <canvas #pieChart2></canvas>\n        </ion-card-content>\n      </ion-card>\n\n      <ion-card class=\"div2-card ion-margin-start ion-margin-end\">\n        <ion-card-content class=\"ion-no-padding ion-padding-bottom\">\n          <ion-item class=\"ion-no-padding\">\n            <ion-label class=\"ion-text-center\">\n              <ion-text color=\"primary\" color=\"success\">\n                Score: {{score}}%\n              </ion-text>\n            </ion-label>\n          </ion-item>\n          <canvas #pieChart3></canvas>\n        </ion-card-content>\n      </ion-card>\n\n      <ion-card class=\"div2-card ion-margin\">\n        <ion-card-content class=\"ion-no-padding ion-padding-bottom\">\n          <ion-item class=\"ion-no-padding\">\n            <ion-label class=\"ion-text-center\">Question Analysis</ion-label>\n          </ion-item>\n          <ion-item class=\"ion-no-padding\" lines=\"none\">\n            <ion-label class=\"ion-text-center\">\n              <ion-text color=\"primary\" color=\"success\">\n                Correct({{correctQuestions}})\n              </ion-text>\n            </ion-label>\n            <ion-label class=\"ion-text-center\">\n              <ion-text color=\"primary\" color=\"danger\">\n                Incorrect({{inCorrectQuestions}})\n              </ion-text>\n            </ion-label>\n            <ion-label class=\"ion-text-center\">\n              <ion-text color=\"primary\" color=\"primary\">\n                Skipped({{skippedQuestions}})\n              </ion-text>\n            </ion-label>\n          </ion-item>\n          <ion-grid>\n            <ion-row>\n              <ion-col\n                size=\"2.4\"\n                *ngFor=\"let question of questionsList; let i=index\"\n              >\n                <ion-card\n                  [color]=\"question.userAns == question.Answer || (question.userNumericalAns == question.Numerical_Answer && question.userUnit == question.Unit) ? 'success' : ((question.QuestionType !=3 && question.userAns == '' || question.userAns == 'Unsolved') || (question.QuestionType == 3 && question.userNumericalAns == '') ? 'primary' : 'danger')\"\n                  class=\"card\"\n                  (click)=\"selectQuestion(i)\"\n                >\n                  <ion-label class=\"ion-text-center\" style=\"height: auto\"\n                    >{{i + 1}}</ion-label\n                  >\n                </ion-card>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-card-content>\n      </ion-card>\n\n      <ion-card class=\"div2-card ion-margin ion-padding\">\n        <ion-button\n          (click)=\"solution()\"\n          expand=\"block\"\n          fill=\"solid\"\n          color=\"primary\"\n        >\n          SOLUTION\n        </ion-button>\n      </ion-card>\n    </div>\n  </div>\n</ion-content>\n\n<ion-toolbar\n  class=\"footer-item ion-no-margin-bottom\"\n  color=\"primary\"\n  lines=\"none\"\n  [hidden]=\"testView == false\"\n>\n  <ion-grid class=\"ion-no-margin ion-no-padding\">\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-toolbar color=\"primary\">\n          <ion-icon\n            class=\"ion-margin-start\"\n            slot=\"start\"\n            name=\"chevron-back-circle-outline\"\n            size=\"large\"\n            (click)=\"previous()\"\n          >\n          </ion-icon>\n          <ion-button\n            *ngIf=\"testSubmitted == false && (page == 'unit-test')\"\n            color=\"light\"\n            class=\"toolbar-btn ion-text-wrap\"\n            (click)=\"clear()\"\n            fill=\"outline\"\n            expand=\"block\"\n            [disabled]=\"selectionCleared == false\"\n          >\n            Clear Selection\n          </ion-button>\n          <ion-icon\n            class=\"ion-margin-end\"\n            slot=\"end\"\n            name=\"chevron-forward-circle-outline\"\n            size=\"large\"\n            (click)=\"next()\"\n          >\n          </ion-icon>\n        </ion-toolbar>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-button\n          class=\"ion-no-margin-top\"\n          expand=\"block\"\n          fill=\"solid\"\n          color=\"primary\"\n          class=\"footer-btn\"\n          size=\"default\"\n          (click)=\"onSubmit()\"\n          *ngIf=\"testSubmitted==false && (page == 'unit-test')\"\n        >\n          Submit Test\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-toolbar>\n");

/***/ }),

/***/ "./src/app/Pages/online-test-page/online-test-page-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/Pages/online-test-page/online-test-page-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: OnlineTestPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnlineTestPagePageRoutingModule", function() { return OnlineTestPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _online_test_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./online-test-page.page */ "./src/app/Pages/online-test-page/online-test-page.page.ts");




const routes = [
    {
        path: '',
        component: _online_test_page_page__WEBPACK_IMPORTED_MODULE_3__["OnlineTestPagePage"]
    }
];
let OnlineTestPagePageRoutingModule = class OnlineTestPagePageRoutingModule {
};
OnlineTestPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OnlineTestPagePageRoutingModule);



/***/ }),

/***/ "./src/app/Pages/online-test-page/online-test-page.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/Pages/online-test-page/online-test-page.module.ts ***!
  \*******************************************************************/
/*! exports provided: OnlineTestPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnlineTestPagePageModule", function() { return OnlineTestPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _online_test_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./online-test-page-routing.module */ "./src/app/Pages/online-test-page/online-test-page-routing.module.ts");
/* harmony import */ var _online_test_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./online-test-page.page */ "./src/app/Pages/online-test-page/online-test-page.page.ts");







let OnlineTestPagePageModule = class OnlineTestPagePageModule {
};
OnlineTestPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _online_test_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["OnlineTestPagePageRoutingModule"]
        ],
        declarations: [_online_test_page_page__WEBPACK_IMPORTED_MODULE_6__["OnlineTestPagePage"]]
    })
], OnlineTestPagePageModule);



/***/ }),

/***/ "./src/app/Pages/online-test-page/online-test-page.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/Pages/online-test-page/online-test-page.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header-item {\n  border-bottom-left-radius: 25px;\n  border-bottom-right-radius: 25px;\n}\n\n.footer-item {\n  border-top-left-radius: 25px;\n  border-top-right-radius: 25px;\n  margin-bottom: 0px;\n}\n\n.options {\n  padding: 0px;\n  position: relative;\n  height: 45px;\n  width: 45px;\n  border-radius: 45px;\n}\n\n.options1 {\n  padding: 0px;\n  position: relative;\n  height: 45px;\n  width: 45px;\n  border-radius: 45px;\n}\n\n.icon {\n  margin: 2px;\n  border: 2px solid white;\n  border-radius: 100%;\n}\n\n.options ion-text {\n  margin: 0;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n\n.options1 ion-text {\n  margin: 0;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n\nion-radio .radio-icon {\n  display: none;\n}\n\n.footer-btn {\n  height: 40px;\n  margin: 0;\n}\n\n.toolbar-btn {\n  height: 40px;\n  margin: 0;\n  padding: 0;\n  margin-left: 50px;\n  margin-right: 50px;\n  font-size: 13px;\n}\n\n.opt {\n  margin: 0;\n  position: absolute;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n.toolbar {\n  --border-width: 0px;\n  --border-style: none;\n}\n\n.card {\n  height: 50px;\n  width: 50px;\n  border-radius: 100%;\n}\n\n.card ion-label {\n  margin: 0;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  color: white;\n  font-size: medium;\n}\n\n.div {\n  margin: 0px;\n  position: relative;\n  top: 0%;\n  background: var(--ion-color-primary);\n  width: 100%;\n  height: 60vw;\n  border-bottom-right-radius: 50%;\n  border-bottom-left-radius: 50%;\n}\n\n.div2 {\n  margin: 0px;\n  width: 100%;\n}\n\n.div-icon {\n  position: absolute;\n  top: 2%;\n  left: 2%;\n}\n\n.div-card {\n  position: relative;\n  top: 19vw;\n  margin: 0;\n  margin-right: 15px;\n  margin-left: 15px;\n  margin-bottom: 22vw;\n  border-radius: 15px;\n}\n\n.div2-card {\n  border-radius: 15px;\n}\n\n.avatar {\n  position: absolute;\n  top: 18%;\n  left: 43%;\n  z-index: 1;\n  height: 15vw;\n  width: 15vw;\n}\n\n.score-card {\n  height: 25vw;\n  width: 35vw;\n  background-color: var(--ion-color-primary);\n  margin-left: auto;\n  margin-right: auto;\n  display: flex;\n  flex-wrap: wrap;\n  justify-content: center;\n  align-content: space-between;\n}\n\n.inner-div {\n  display: flex;\n  justify-content: center;\n}\n\n.item-margin {\n  margin-top: 8vw;\n}\n\n.input-label {\n  margin-bottom: 0;\n  font-size: 3.5vw;\n}\n\n.input {\n  --padding-bottom: 0px;\n  font-size: 3.5vw;\n}\n\n.marking {\n  text-align: center;\n  margin-top: 5px;\n  margin-left: 77vw;\n  background: lightgray;\n  width: 65px;\n  padding: 5px;\n  border-radius: 10px;\n  font-size: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvUGFnZXMvb25saW5lLXRlc3QtcGFnZS9vbmxpbmUtdGVzdC1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFpQ0E7RUFDSSwrQkFBQTtFQUNBLGdDQUFBO0FBaENKOztBQXVDQTtFQUNJLDRCQUFBO0VBQ0EsNkJBQUE7RUFDQSxrQkFBQTtBQXBDSjs7QUF1Q0E7RUFDSSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FBcENKOztBQXVDQTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7QUFwQ0o7O0FBdUNBO0VBQ0ksV0FBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFwQ0o7O0FBK0NBO0VBQ0ksU0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtBQTVDSjs7QUErQ0E7RUFDSSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0FBNUNKOztBQWdEQTtFQUNJLGFBQUE7QUE3Q0o7O0FBaURBO0VBQ0ksWUFBQTtFQUNBLFNBQUE7QUE5Q0o7O0FBaURBO0VBQ0ksWUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUE5Q0o7O0FBMkRBO0VBQ0ksU0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLDJCQUFBO0FBeERKOztBQTJEQTtFQUNJLG1CQUFBO0VBQ0Esb0JBQUE7QUF4REo7O0FBMkRBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQXhESjs7QUEyREE7RUFDSSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBeERKOztBQTJEQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxvQ0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsK0JBQUE7RUFDQSw4QkFBQTtBQXhESjs7QUEyREE7RUFDSSxXQUFBO0VBQ0EsV0FBQTtBQXhESjs7QUEyREE7RUFDSSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0FBeERKOztBQTJEQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUdBLFNBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQTFESjs7QUE2REE7RUFDSSxtQkFBQTtBQTFESjs7QUE2REE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBMURKOztBQTZEQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsMENBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtFQUNBLDRCQUFBO0FBMURKOztBQTZEQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBQTFESjs7QUE2REE7RUFDSSxlQUFBO0FBMURKOztBQTZEQTtFQUNJLGdCQUFBO0VBQ0EsZ0JBQUE7QUExREo7O0FBNkRBO0VBQ0kscUJBQUE7RUFDQSxnQkFBQTtBQTFESjs7QUE2REE7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUExREoiLCJmaWxlIjoic3JjL2FwcC9QYWdlcy9vbmxpbmUtdGVzdC1wYWdlL29ubGluZS10ZXN0LXBhZ2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gLnF1ZU5vIHtcclxuLy8gICAgIHBhZGRpbmc6IDBweDtcclxuLy8gICAgIGhlaWdodDogNDVweDsgXHJcbi8vICAgICB3aWR0aDogNDVweDtcclxuLy8gICAgIG1hcmdpbjogMDtcclxuLy8gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuLy8gICAgIHRvcDogNTAlO1xyXG4vLyAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xyXG4vLyB9XHJcblxyXG4vLyAucXVlTm8tbGFiZWwge1xyXG4vLyAgICAgLy8gd2lkdGg6IDMwcHg7XHJcbi8vICAgICAvLyBoZWlnaHQ6IDMwcHg7XHJcbi8vICAgICAvLyBwYWRkaW5nOiAwcHg7XHJcbi8vICAgICAvLyBib3JkZXI6IDJweCBzb2xpZCB3aGl0ZTtcclxuLy8gICAgIC8vIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbi8vICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgXHJcbi8vIH1cclxuXHJcblxyXG4vLyAucXVlTm8ge1xyXG4gICBcclxuLy8gICAgIHBhZGRpbmc6IDVweDtcclxuLy8gICAgIGJvcmRlcjogMnB4IHNvbGlkIHdoaXRlO1xyXG4vLyAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuLy8gfVxyXG5cclxuXHJcbi8vIC50b3RhbFF1ZSB7XHJcbi8vICAgICBmb250LXNpemU6IDIwcHg7XHJcbi8vIH1cclxuXHJcbi5oZWFkZXItaXRlbSB7XHJcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAyNXB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDI1cHg7XHJcbn1cclxuXHJcbi8vIC50b29sYmFyIHtcclxuLy8gICAgIG1hcmdpbi10b3A6IDBweDtcclxuLy8gfVxyXG5cclxuLmZvb3Rlci1pdGVtIHtcclxuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDI1cHg7XHJcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMjVweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxufVxyXG5cclxuLm9wdGlvbnMge1xyXG4gICAgcGFkZGluZzogMHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiA0NXB4OyBcclxuICAgIHdpZHRoOiA0NXB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNDVweDtcclxufVxyXG5cclxuLm9wdGlvbnMxIHtcclxuICAgIHBhZGRpbmc6IDBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGhlaWdodDogNDVweDsgXHJcbiAgICB3aWR0aDogNDVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDQ1cHg7XHJcbn1cclxuXHJcbi5pY29uIHtcclxuICAgIG1hcmdpbjogMnB4O1xyXG4gICAgYm9yZGVyOiAycHggc29saWQgd2hpdGU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG59XHJcblxyXG4vLyAucXVlTm8gaW9uLXRleHQge1xyXG4vLyAgICAgbWFyZ2luOiAwO1xyXG4vLyAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4vLyAgICAgdG9wOiA1MCU7XHJcbi8vICAgICBsZWZ0OiA0NSU7XHJcbi8vICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuLy8gfVxyXG5cclxuLm9wdGlvbnMgaW9uLXRleHQge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKVxyXG59XHJcblxyXG4ub3B0aW9uczEgaW9uLXRleHQge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKVxyXG59XHJcblxyXG5cclxuaW9uLXJhZGlvIC5yYWRpby1pY29uIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcblxyXG4uZm9vdGVyLWJ0biB7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBtYXJnaW46IDA7XHJcbn1cclxuXHJcbi50b29sYmFyLWJ0biB7IFxyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIG1hcmdpbi1sZWZ0OiA1MHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcblxyXG4vLyAub3B0aW9uLWNvbnRhaW5lcntcclxuLy8gICAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLXN1Y2Nlc3MpO1xyXG4vLyAgICBtYXJnaW46IDVweFxyXG4vLyB9XHJcblxyXG4vLyAuYm9yZGVyLWJvdHRvbSB7XHJcbi8vICAgICBib3JkZXItYm90dG9tOiAwLjVweCBzb2xpZCBibGFjaztcclxuLy8gICAgIG1hcmdpbjogNXB4XHJcbi8vIH1cclxuXHJcbi5vcHQge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSlcclxufVxyXG5cclxuLnRvb2xiYXIge1xyXG4gICAgLS1ib3JkZXItd2lkdGg6IDBweDtcclxuICAgIC0tYm9yZGVyLXN0eWxlOiBub25lO1xyXG59XHJcblxyXG4uY2FyZCB7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbn1cclxuXHJcbi5jYXJkIGlvbi1sYWJlbCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDUwJTtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiBtZWRpdW07XHJcbn1cclxuXHJcbi5kaXYge1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDAlO1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDYwdnc7XHJcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogNTAlO1xyXG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNTAlO1xyXG59XHJcblxyXG4uZGl2MiB7XHJcbiAgICBtYXJnaW46IDBweDtcclxuICAgIHdpZHRoOiAxMDAlXHJcbn1cclxuXHJcbi5kaXYtaWNvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDIlO1xyXG4gICAgbGVmdDogMiVcclxufVxyXG5cclxuLmRpdi1jYXJkIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogMTl2dztcclxuICAgIC8vIGhlaWdodDogMTM1JTtcclxuICAgIC8vIHdpZHRoOiA5MyU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDE1cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDIydnc7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG59XHJcblxyXG4uZGl2Mi1jYXJkIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbn1cclxuXHJcbi5hdmF0YXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAxOCU7XHJcbiAgICBsZWZ0OiA0MyU7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgaGVpZ2h0OiAxNXZ3O1xyXG4gICAgd2lkdGg6IDE1dnc7XHJcbn1cclxuXHJcbi5zY29yZS1jYXJkIHtcclxuICAgIGhlaWdodDogMjV2dztcclxuICAgIHdpZHRoOiAzNXZ3O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcblxyXG4uaW5uZXItZGl2IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLml0ZW0tbWFyZ2luIHtcclxuICAgIG1hcmdpbi10b3A6IDh2dztcclxufVxyXG5cclxuLmlucHV0LWxhYmVsIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICBmb250LXNpemU6IDMuNXZ3O1xyXG59XHJcblxyXG4uaW5wdXQge1xyXG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gICAgZm9udC1zaXplOiAzLjV2dztcclxufVxyXG5cclxuLm1hcmtpbmcge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDc3dnc7XHJcbiAgICBiYWNrZ3JvdW5kOiBsaWdodGdyYXk7XHJcbiAgICB3aWR0aDogNjVweDtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/Pages/online-test-page/online-test-page.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/Pages/online-test-page/online-test-page.page.ts ***!
  \*****************************************************************/
/*! exports provided: OnlineTestPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnlineTestPagePage", function() { return OnlineTestPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../Services/storage-service.service */ "./src/app/Services/storage-service.service.ts");
/* harmony import */ var _Services_alert_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../Services/alert-service.service */ "./src/app/Services/alert-service.service.ts");
/* harmony import */ var _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../Services/toast-service.service */ "./src/app/Services/toast-service.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! chart.js */ "./node_modules/chart.js/dist/Chart.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _Components_question_panel_question_panel_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../Components/question-panel/question-panel.page */ "./src/app/Components/question-panel/question-panel.page.ts");
/* harmony import */ var _Services_api_service_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./../../Services/api-service.service */ "./src/app/Services/api-service.service.ts");
/* harmony import */ var _Services_save_test_info_service_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./../../Services/save-test-info-service.service */ "./src/app/Services/save-test-info-service.service.ts");












let OnlineTestPagePage = class OnlineTestPagePage {
    constructor(platform, router, activatedRoute, popoverController, alertController, navController, storage, storageService, alertService, toastService, apiService, saveTestInfoService) {
        this.platform = platform;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.popoverController = popoverController;
        this.alertController = alertController;
        this.navController = navController;
        this.storage = storage;
        this.storageService = storageService;
        this.alertService = alertService;
        this.toastService = toastService;
        this.apiService = apiService;
        this.saveTestInfoService = saveTestInfoService;
        this.questionInfo = [];
        this.questionsList = [];
        this.questionCount = 0;
        this.index = 0;
        this.testSubmitted = false;
        this.testView = true;
        this.page = "";
        this.bookmarkIcon = "bookmark-outline";
        this.questionImage = true;
        this.questionImagePath = "";
        this.optionImage = true;
        this.optionImagePath = "";
        this.hintImage = true;
        this.hintImagePath = "";
        this.iconCount = 0;
        this.showInputBox = false;
        this.answer = "";
        this.unit = "";
        this.selectionCleared = false;
        this.inGo = false;
        this.testTitle = "";
        this.count = 0;
        this.totalQuestions = [];
        this.secs = 0;
        this.totalMarks = 0;
        this.outOfMarks = 0;
        this.corrMarks = 0;
        this.inCorrMarks = 0;
        this.correctQuestions = 0;
        this.inCorrectQuestions = 0;
        this.skippedQuestions = 0;
        this.selectedOptionArr = [];
        this.testType = "";
        this.platform.backButton.subscribeWithPriority(0, (processNextHandler) => {
            // console.log("in test back");
            if (this.router.url.startsWith("/online-test-page")) {
                if (this.page == "unit-test" || this.page == "saved-test") {
                    if (this.testView == true && this.testSubmitted == false) {
                    }
                    else if (this.testView == true && this.testSubmitted == true) {
                        this.testView = false;
                    }
                    else if (this.testView == false && this.testSubmitted == true) {
                        this.navController.back();
                    }
                }
                else if (this.page == "bookmark") {
                    if (this.inGo == true) {
                        popoverController.dismiss();
                    }
                    else {
                        this.navController.back();
                    }
                }
                this.popoverController.dismiss();
            }
            else {
                processNextHandler();
            }
            // else if(this.router.url.startsWith('/dashboard')){
            //   navigator["app"].exitApp();
            // }
        });
    }
    ngOnInit() {
        this.selectedOption = {
            questionNo: null,
            option: "",
            userNumericalAns: "",
            userUnit: "",
        };
        this.page = this.storageService.page;
        if (this.page == "unit-test") {
            this.testDetails = this.storageService.notification;
            this.testTitle = this.testDetails.Title;
            this.questionInfo = JSON.parse(this.testDetails.TestObj).QuestionDetails;
            this.questionInfo.map((question) => {
                this.questionsList.push({
                    Answer: question.Answer,
                    ChapterId: question.ChapterId,
                    ChapterName: question.ChapterName,
                    ClassId: question.ClassId,
                    ClassName: question.ClassName,
                    Hint: question.Hint,
                    HintImageFile: question.HintImageFile,
                    HintImagePath: question.HintImagePath,
                    IsHintAsImage: question.IsHintAsImage,
                    IsOptionAsImage: question.IsOptionAsImage,
                    IsQuestionAsImage: question.IsQuestionAsImage,
                    Numerical_Answer: question.Numerical_Answer,
                    Option1: question.Option1,
                    Option2: question.Option2,
                    Option3: question.Option3,
                    Option4: question.Option4,
                    OptionImageFile: question.OptionImageFile,
                    OptionImagePath: question.OptionImagePath,
                    QuestionId: question.QuestionId,
                    QuestionImageFile: question.QuestionImageFile,
                    QuestionImagePath: question.QuestionImagePath,
                    QuestionInfo: question.QuestionInfo,
                    QuestionLevel: question.QuestionLevel,
                    QuestionType: question.QuestionType,
                    QuestionYear: question.QuestionYear,
                    SubjectName: question.SubjectName,
                    SubjectTestId: question.SubjectTestId,
                    Unit: question.Unit,
                    userAns: "",
                    userNumericalAns: "",
                    userUnit: "",
                });
            });
            console.log(this.questionsList);
            this.questionCount = this.questionsList.length;
            this.showTest();
            this.onReady();
        }
        else if (this.page == "saved-test") {
            this.testView = false;
            this.testSubmitted = true;
            this.testId = parseInt(this.activatedRoute.snapshot.paramMap.get("id"));
            this.saveTestInfoService
                .getTestDetailsForTest(this.testId)
                .then((data) => {
                this.savedTestDetails = data;
                console.log("Saved test", this.savedTestDetails);
                this.testTitle = this.savedTestDetails.subjectName;
                this.totalMarks = this.savedTestDetails.obtainedMarks;
                this.outOfMarks = this.savedTestDetails.totalMarks;
                this.totalMins = this.savedTestDetails.timeTakenMin;
                this.totalSecs = this.savedTestDetails.timeTakenSec;
                this.correctQuestions = this.savedTestDetails.corrQueCount;
                this.inCorrectQuestions = this.savedTestDetails.inCorrQueCount;
                this.skippedQuestions =
                    this.savedTestDetails.totalQue -
                        (this.correctQuestions + this.inCorrectQuestions);
                this.testGivenDate =
                    this.savedTestDetails.testDate.slice(0, 2) +
                        "-" +
                        this.savedTestDetails.testDate.slice(2, 4) +
                        "-" +
                        this.savedTestDetails.testDate.slice(4);
                this.questionsList = JSON.parse(this.savedTestDetails.questions);
                this.questionCount = this.questionsList.length;
                this.testType = this.savedTestDetails.testType;
                this.createPieChart();
            });
        }
    }
    next() {
        if (this.index < this.questionCount - 1) {
            this.index = this.index + 1;
            this.showTest();
            if (this.questionsList[this.index].QuestionType != 3) {
                if ((this.page == "unit-test" ||
                    this.page == "paused-test" ||
                    this.page == "question-papers") &&
                    this.testSubmitted == false) {
                    this.funSelectedOption();
                }
                else {
                    this.inCorrOption = "";
                    this.option = this.questionsList[this.index].Answer;
                    if (this.questionsList[this.index].Answer !=
                        this.questionsList[this.index].userAns) {
                        this.inCorrOption = this.questionsList[this.index].userAns;
                    }
                    if ((this.page == "saved-test" ||
                        this.page == "unit-test" ||
                        this.page == "paused-test" ||
                        this.page == "question-papers") &&
                        this.testSubmitted == true) {
                        this.getQuestionStatus();
                    }
                }
            }
            else {
                this.selectionCleared = false;
                this.answer = this.questionsList[this.index].userNumericalAns;
                this.unit = this.questionsList[this.index].userUnit;
                if ((this.page == "saved-test" ||
                    this.page == "unit-test" ||
                    this.page == "paused-test") &&
                    this.testSubmitted == true) {
                    this.getQuestionStatus();
                }
            }
        }
        else {
            this.alertService.createAlert("You are on the last question");
        }
    }
    previous() {
        if (this.index > 0) {
            this.index = this.index - 1;
            this.showTest();
            if (this.questionsList[this.index].QuestionType != 3) {
                if ((this.page == "unit-test" ||
                    this.page == "paused-test" ||
                    this.page == "question-papers") &&
                    this.testSubmitted == false) {
                    this.funSelectedOption();
                }
                else {
                    this.inCorrOption = "";
                    this.option = this.questionsList[this.index].Answer;
                    if (this.questionsList[this.index].Answer !=
                        this.questionsList[this.index].userAns) {
                        this.inCorrOption = this.questionsList[this.index].userAns;
                    }
                    if ((this.page == "saved-test" ||
                        this.page == "unit-test" ||
                        this.page == "paused-test" ||
                        this.page == "question-papers") &&
                        this.testSubmitted == true) {
                        this.getQuestionStatus();
                    }
                }
            }
            else {
                this.selectionCleared = false;
                this.answer = this.questionsList[this.index].userNumericalAns;
                this.unit = this.questionsList[this.index].userUnit;
                if ((this.page == "saved-test" ||
                    this.page == "unit-test" ||
                    this.page == "paused-test") &&
                    this.testSubmitted == true) {
                    this.getQuestionStatus();
                }
            }
        }
        else {
            this.alertService.createAlert("You are on the first question");
        }
    }
    funSelectedOption() {
        // this.hintShown = false;
        this.option = "";
        // document.getElementById('hint').innerHTML = ''
        this.selectedOption = this.selectedOptionArr.find(({ questionNo }) => questionNo === this.questionsList[this.index].QuestionId);
        // console.log("selectedOptions", this.selectedOption);
        if (this.selectedOption == null) {
            this.option = "";
            this.selectionCleared = false;
        }
        else {
            this.option = this.selectedOption.option;
            this.selectionCleared = true;
        }
        // this.selectedOptionArr.find(({questionNo}) => questionNo == this.index).option;
        // console.log(this.selectedOptionArr.find(({questionNo}) => questionNo == this.index).option);
        // console.log(this.questionsList[this.index].ANSWER);
    }
    getQuestionStatus() {
        if (this.questionsList[this.index].QuestionType != 3) {
            if (this.questionsList[this.index].userAns == "" ||
                this.questionsList[this.index].userAns == "Unsolved") {
                this.questionStatus = "SKIPPED";
            }
            else if (this.questionsList[this.index].Answer ==
                this.questionsList[this.index].userAns) {
                this.questionStatus = "CORRRECT";
            }
            else {
                this.questionStatus = "INCORRECT";
            }
        }
        else if (this.questionsList[this.index].QuestionType == 3) {
            if (this.questionsList[this.index].userNumericalAns == "") {
                this.questionStatus = "SKIPPED";
            }
            else if (this.questionsList[this.index].userNumericalAns ==
                this.questionsList[this.index].Numerical_Answer &&
                this.questionsList[this.index].userUnit ==
                    this.questionsList[this.index].Unit) {
                this.questionStatus = "CORRRECT";
            }
            else {
                this.questionStatus = "INCORRECT";
            }
        }
    }
    showTest() {
        this.iconCount = this.questionsList[this.index].QuestionLevel;
        // if (
        //   this.bookmarkQustions.find(
        //     ({ questionId }) =>
        //       questionId == this.questionsList[this.index].QUESTION_ID
        //   ) == null
        // ) {
        //   this.bookmarkIcon = "bookmark-outline";
        //   // console.log("in show test if");
        // } else {
        //   this.bookmarkIcon = "bookmark";
        //   // console.log("in show test else");
        // }
        document.getElementById("question").innerHTML =
            "$" + this.questionsList[this.index].QuestionInfo + "$";
        document.getElementById("opt1").innerHTML =
            "$" + this.questionsList[this.index].Option1 + "$";
        document.getElementById("opt2").innerHTML =
            "$" + this.questionsList[this.index].Option2 + "$";
        document.getElementById("opt3").innerHTML =
            "$" + this.questionsList[this.index].Option3 + "$";
        document.getElementById("opt4").innerHTML =
            "$" + this.questionsList[this.index].Option4 + "$";
        eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
        // if(this.questionsList[this.index].QUESTION_YEAR != '') {
        //   document.getElementById('prevAsk').innerHTML = this.questionsList[this.index].QUESTION_YEAR;
        // }
        if (this.page == "bookmark") {
            document.getElementById("hint").innerHTML =
                "$" + this.questionsList[this.index].Hint + "$";
            eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
            this.option = this.questionsList[this.index].Answer;
            this.answer = this.questionsList[this.index].Numerical_Answer;
            this.unit = this.questionsList[this.index].Unit;
        }
        if (this.testSubmitted == true) {
            document.getElementById("hint").innerHTML =
                "$" + this.questionsList[this.index].Hint + "$";
            eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
        }
        this.questionsList[this.index].QuestionType == 3
            ? (this.showInputBox = true)
            : (this.showInputBox = false);
        this.SubjectTestId = this.questionsList[this.index].SubjectTestId;
        if (this.questionsList[this.index].IsQuestionAsImage == 1) {
            this.questionImage = false;
            this.questionImagePath = `assets/${this.questionsList[this.index].QuestionImagePath}`;
        }
        else {
            this.questionImage = true;
            this.questionImagePath = "";
        }
        if (this.questionsList[this.index].IsOptionAsImage == 1) {
            this.optionImage = false;
            this.optionImagePath = `assets/${this.questionsList[this.index].OptionImagePath}`;
        }
        else {
            this.optionImage = true;
            this.optionImagePath = "";
        }
        if (this.testSubmitted == true &&
            this.questionsList[this.index].IsHintAsImage == 1) {
            this.hintImage = false;
            this.hintImagePath = `assets/${this.questionsList[this.index].HintImagePath}`;
        }
        else {
            this.hintImage = true;
            this.hintImagePath = "";
        }
    }
    onReady() {
        this.secs = Number.parseInt(this.testDetails.Duration) * 60;
        this.stopClockInterval = setInterval((_) => this.countTime(), 1000);
    }
    countTime() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.secs--;
            this.fmins = Math.floor(this.secs / 60);
            this.fsecs = Math.floor(this.secs % 60);
            if (this.fmins <= 9) {
                this.fmins = "0" + this.fmins;
            }
            if (this.fsecs <= 9) {
                this.fsecs = "0" + this.fsecs;
            }
            if (this.secs >= 0) {
                document.getElementById("timer").innerHTML =
                    this.fmins + ":" + this.fsecs;
            }
            else {
                clearTimeout(this.stopClockInterval);
                const alert = yield this.alertController.create({
                    subHeader: "Your test time exceeds",
                    cssClass: "alert-title",
                    animated: true,
                    buttons: [
                        {
                            text: "Ok",
                            handler: () => {
                                this.popoverController.dismiss();
                                this.submit();
                            },
                        },
                    ],
                    backdropDismiss: false,
                });
                yield alert.present();
            }
        });
    }
    clickOpt1() {
        if (this.testSubmitted == false) {
            if ((this.page == "unit-test" ||
                this.page == "paused-test" ||
                this.page == "question-papers") &&
                this.selectionCleared == false) {
                this.selectOption("A");
            }
            else if (this.page == "unit-test" ||
                this.page == "paused-test" ||
                this.page == "question-papers") {
                this.toastService.createToast("you cannot select another option", 1000);
            }
        }
    }
    clickOpt2() {
        if (this.testSubmitted == false) {
            if ((this.page == "unit-test" ||
                this.page == "paused-test" ||
                this.page == "question-papers") &&
                this.selectionCleared == false) {
                this.selectOption("B");
            }
            else if (this.page == "unit-test" ||
                this.page == "paused-test" ||
                this.page == "question-papers") {
                this.toastService.createToast("you cannot select another option", 1000);
            }
        }
    }
    clickOpt3() {
        if (this.testSubmitted == false) {
            if ((this.page == "unit-test" ||
                this.page == "paused-test" ||
                this.page == "question-papers") &&
                this.selectionCleared == false) {
                this.selectOption("C");
            }
            else if (this.page == "unit-test" ||
                this.page == "paused-test" ||
                this.page == "question-papers") {
                this.toastService.createToast("you cannot select another option", 1000);
            }
        }
    }
    clickOpt4() {
        if (this.testSubmitted == false) {
            if ((this.page == "unit-test" ||
                this.page == "paused-test" ||
                this.page == "question-papers") &&
                this.selectionCleared == false) {
                this.selectOption("D");
            }
            else if (this.page == "unit-test" ||
                this.page == "paused-test" ||
                this.page == "question-papers") {
                this.toastService.createToast("you cannot select another option", 1000);
            }
        }
    }
    selectOption(option) {
        this.selectedOption = this.selectedOptionArr.find(({ questionNo }) => questionNo === this.questionsList[this.index].QuestionId);
        // console.log("selected in opt1", this.selectedOption);
        if (this.selectedOptionArr.includes(this.selectedOption)) {
            // console.log("in opt4 if");
            this.selectedOptionArr.splice(this.selectedOptionArr.indexOf(this.selectedOption), 1);
            this.questionsList[this.index].userAns = "";
        }
        this.selectedOption = {
            questionNo: this.questionsList[this.index].QuestionId,
            option: option,
            userNumericalAns: "",
            userUnit: "",
        };
        this.option = option;
        this.questionsList[this.index].userAns = option;
        // console.log("user ans", this.questionsList[this.index].userAns);
        this.selectedOptionArr.push(this.selectedOption);
        this.selectionCleared = true;
        // console.log("selectedArr", this.selectedOptionArr);
    }
    onBackClick() {
        if (this.page == "unit-test" || this.page == "saved-test")
            if (this.testSubmitted == true)
                this.testView = false;
        // console.log("test view", this.testView);
        // } else if (this.page == "bookmark") {
        //   this.navController.back();
        // }
    }
    bookmark() { }
    addRemoveBookmark() { }
    go() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.inGo = true;
            if (this.count == 0) {
                this.totalQuestions = [];
                let index = 0;
                this.questionsList.map((questions) => {
                    this.totalQuestions.push({
                        index: index,
                        questionNo: questions.QuestionId,
                        questionType: questions.QuestionType,
                    });
                    index++;
                });
                this.count++;
            }
            const modal = yield this.popoverController.create({
                component: _Components_question_panel_question_panel_page__WEBPACK_IMPORTED_MODULE_9__["QuestionPanelPage"],
                backdropDismiss: false,
                animated: true,
                cssClass: "myPopOver3",
                componentProps: {
                    totalQuestions: this.totalQuestions,
                    answeredQuestions: this.selectedOptionArr,
                    index: this.index,
                },
            });
            yield modal.present();
            modal.onDidDismiss().then((_) => {
                this.inGo = false;
                this.storage.ready().then((ready) => {
                    if (ready) {
                        this.storage.get("index").then((index) => {
                            // console.log("index in modal", index);
                            if (index == null) {
                            }
                            else
                                this.index = index;
                            this.showTest();
                            this.storage.remove("index");
                            if (this.questionsList[this.index].QuestionType != 3) {
                                if (this.page == "unit-test" ||
                                    this.page == "paused-test" ||
                                    this.page == "question-papers") {
                                    this.funSelectedOption();
                                }
                            }
                            else {
                                this.selectionCleared = false;
                                this.answer = this.questionsList[this.index].userNumericalAns;
                                this.unit = this.questionsList[this.index].userUnit;
                            }
                        });
                    }
                });
            });
        });
    }
    clear() {
        if (this.questionsList[this.index].QuestionType != 3) {
            this.selectionCleared = false;
            this.option = "";
            this.questionsList[this.index].userAns = "";
            this.selectedOption = this.selectedOptionArr.find(({ questionNo }) => questionNo === this.questionsList[this.index].QuestionId);
            // console.log("selected in opt1", this.selectedOption);
            if (this.selectedOptionArr.includes(this.selectedOption)) {
                // console.log("in opt4 if");
                this.selectedOptionArr.splice(this.selectedOptionArr.indexOf(this.selectedOption), 1);
            }
        }
        else if (this.questionsList[this.index].QuestionType == 3) {
            this.answer = "";
            this.unit = "";
        }
    }
    onAnswerChange() {
        this.questionsList[this.index].userNumericalAns = this.answer;
        this.selectedOption = this.selectedOptionArr.find(({ questionNo }) => questionNo === this.questionsList[this.index].QuestionId);
        if (this.answer.length == 0) {
            this.unit = "";
            if (this.selectedOptionArr.includes(this.selectedOption)) {
                this.selectedOptionArr.splice(this.selectedOptionArr.indexOf(this.selectedOption), 1);
            }
        }
        else {
            if (this.selectedOptionArr.includes(this.selectedOption)) {
                this.selectedOptionArr[this.selectedOptionArr.indexOf(this.selectedOption)].userNumericalAns = this.answer;
            }
            else {
                this.selectedOptionArr.push({
                    questionNo: this.questionsList[this.index].QuestionId,
                    option: "",
                    userNumericalAns: this.answer,
                    userUnit: this.unit,
                });
            }
        }
    }
    onUnitChange() {
        this.questionsList[this.index].userUnit = this.unit;
        this.selectedOption = this.selectedOptionArr.find(({ questionNo }) => questionNo === this.questionsList[this.index].QuestionId);
        if (this.unit.length == 0 && this.answer.length != 0) {
            if (this.selectedOptionArr.includes(this.selectedOption)) {
                this.selectedOptionArr[this.selectedOptionArr.indexOf(this.selectedOption)].userUnit = "";
            }
        }
        else if (this.unit.length > 0 && this.answer.length > 0) {
            if (this.selectedOptionArr.includes(this.selectedOption)) {
                this.selectedOptionArr[this.selectedOptionArr.indexOf(this.selectedOption)].userUnit = this.unit;
            }
        }
    }
    onSubmit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                subHeader: "Do you want to submit test ?",
                cssClass: "alert-title",
                animated: true,
                backdropDismiss: false,
                buttons: [
                    {
                        text: "Cancel",
                        role: "cancel",
                    },
                    {
                        text: "Confirm",
                        handler: () => {
                            clearTimeout(this.stopClockInterval);
                            this.submit();
                        },
                    },
                ],
            });
            yield alert.present();
        });
    }
    submit() {
        console.log("After Submit", this.questionsList);
        console.log("Selected option array", this.selectedOptionArr);
        this.testView = false;
        this.testSubmitted = true;
        if (this.testDetails.TestType == "1")
            this.outOfMarks =
                this.questionsList.length *
                    Number.parseInt(this.testDetails.NeetCorrect);
        else if (this.testDetails.TestType == "2" ||
            this.testDetails.TestType == "4") {
            this.questionsList.map((question) => {
                if (question.QuestionType == 3)
                    this.outOfMarks += Number.parseInt(this.testDetails.JeeNewCorrect);
                else
                    this.outOfMarks += Number.parseInt(this.testDetails.JeeCorrect);
            });
        }
        else if (this.testDetails.TestType == "3" ||
            this.testDetails.TestType == "5") {
            this.questionsList.map((question) => {
                if (question.SubjectTestId == 1 ||
                    question.SubjectTestId == 2 ||
                    question.SubjectTestId == 4)
                    this.outOfMarks += 1;
                else
                    this.outOfMarks += 2;
            });
        }
        // this.corrMarks = this.settings.correctMarks;
        // this.inCorrMarks = this.settings.inCorrectMarks;
        this.getMarks();
        this.getTime();
        this.saveTest();
        this.sendTestDetails();
    }
    getMarks() {
        if (this.testDetails.TestType == "1") {
            this.questionsList.map((question) => {
                if (question.userAns != "") {
                    if (question.Answer == question.userAns) {
                        this.totalMarks += Number.parseInt(this.testDetails.NeetCorrect);
                        this.correctQuestions++;
                    }
                    else {
                        this.totalMarks -= Number.parseInt(this.testDetails.NeetInCorrect);
                        this.inCorrectQuestions++;
                    }
                }
            });
        }
        else if (this.testDetails.TestType == "2" ||
            this.testDetails.TestType == "4") {
            this.questionsList.map((question) => {
                if (question.QuestionType == 3) {
                    if (question.userNumericalAns != "") {
                        if (question.userNumericalAns == question.Numerical_Answer &&
                            question.userUnit == question.Unit) {
                            this.totalMarks += Number.parseInt(this.testDetails.JeeNewCorrect);
                            this.correctQuestions++;
                        }
                        else {
                            this.totalMarks -= Number.parseInt(this.testDetails.JeeNewInCorrect);
                            this.inCorrectQuestions++;
                        }
                    }
                }
                else {
                    if (question.userAns != "") {
                        if (question.Answer == question.userAns) {
                            this.totalMarks += Number.parseInt(this.testDetails.JeeCorrect);
                            this.correctQuestions++;
                        }
                        else {
                            this.totalMarks -= Number.parseInt(this.testDetails.JeeInCorrect);
                            this.inCorrectQuestions++;
                        }
                    }
                }
            });
        }
        else if (this.testDetails.TestType == "3" ||
            this.testDetails.TestType == "5") {
            this.questionsList.map((question) => {
                if (question.userAns != "") {
                    if (question.Answer == question.userAns) {
                        if (question.SubjectTestId == 1 ||
                            question.SubjectTestId == 2 ||
                            question.SubjectTestId == 4)
                            this.totalMarks += 1;
                        else
                            this.totalMarks += 2;
                        this.correctQuestions++;
                    }
                    else
                        this.inCorrectQuestions++;
                }
            });
        }
        this.skippedQuestions =
            this.questionsList.length -
                (this.correctQuestions + this.inCorrectQuestions);
        // console.log("skipped", this.skippedQuestions);
        // console.log("correct", this.correctQuestions);
        // console.log("incorrect", this.inCorrectQuestions);
        // console.log("total marks", this.totalMarks);
        // console.log("out of marks", this.outOfMarks);
        this.createPieChart();
    }
    getTime() {
        if (this.secs < 0) {
            this.totalMins = Number.parseInt(this.testDetails.Duration);
            this.totalSecs = "00";
        }
        else {
            let min = Number.parseInt(this.testDetails.Duration) - 1;
            let sec = 60;
            this.totalMins = min - this.fmins;
            this.totalSecs = sec - this.fsecs;
            if (this.totalSecs == 60) {
                this.totalMins = this.totalMins + 1;
                this.totalSecs = 0;
            }
            if (this.totalMins < 10) {
                this.totalMins = "0" + this.totalMins;
            }
            if (this.totalSecs < 10) {
                this.totalSecs = "0" + this.totalSecs;
            }
        }
    }
    saveTest() {
        let day = new Date().getDate().toString();
        if (day.length == 1)
            day = "0" + day;
        let month = (new Date().getMonth() + 1).toString();
        if (month.length == 1)
            month = "0" + month;
        let year = new Date().getFullYear().toString();
        let submitDate = day + month + year;
        this.savedTestDetails = {
            testDate: submitDate,
            subjectName: this.testDetails.Title,
            totalQue: this.questionsList.length,
            obtainedMarks: this.totalMarks,
            totalMarks: this.outOfMarks,
            totalTime: Number.parseInt(this.testDetails.Duration),
            attQueCount: this.correctQuestions + this.inCorrectQuestions,
            nonAttQueCount: this.skippedQuestions,
            corrQueCount: this.correctQuestions,
            inCorrQueCount: this.inCorrectQuestions,
            timeTakenMin: this.totalMins,
            timeTakenSec: this.totalSecs,
            testType: this.testDetails.TestType,
            questions: JSON.stringify(this.questionsList),
        };
        console.log("save test obj", this.savedTestDetails);
        this.saveTestInfoService.insertIntoTable(this.savedTestDetails);
        // let savedTest = { ...this.savedTestDetails };
        this.apiService
            .APPPostResultOfOnlineTest(this.savedTestDetails, this.storageService.userDetails.userId, this.storageService.userDetails.clientId)
            .then((data) => console.log("savedTestApi", JSON.parse(data.data)));
    }
    sendTestDetails() {
        let questionInfo = [];
        this.questionsList.map((question) => {
            if (question.QuestionType != 3) {
                questionInfo.push({
                    QuestionId: question.QuestionId.toString(),
                    CorrectAnswer: question.Answer,
                    StudentAnswer: question.userAns != "" ? question.userAns : "Unsolved",
                    Status: question.Answer == question.userAns
                        ? "Correct"
                        : question.userAns == ""
                            ? "Unsolved"
                            : "Incorrect",
                });
            }
            else {
                questionInfo.push({
                    QuestionId: question.QuestionId.toString(),
                    CorrectAnswer: `${question.Numerical_Answer}, ${question.Unit}`,
                    StudentAnswer: question.userNumericalAns != ""
                        ? `${question.userNumericalAns}, ${question.userUnit}`
                        : "Unsolved",
                    Status: question.Numerical_Answer == question.userNumericalAns &&
                        question.Unit == question.userUnit
                        ? "Correct"
                        : question.userNumericalAns == ""
                            ? "Unsolved"
                            : "Incorrect",
                });
            }
        });
        let testDetails = {
            ArrengedTestId: this.testDetails.ArrengeTestId,
            UserId: this.storageService.userDetails.userId,
            TestPaperId: this.testDetails.TestPaperId,
            TestDate: this.testDetails.TestDate.split("-")[1] +
                "-" +
                this.testDetails.TestDate.split("-")[0] +
                "-" +
                this.testDetails.TestDate.split("-")[2],
            TimeDuration: this.testDetails.Duration,
            StartTime: this.testDetails.StartTime,
            ObtainedMarks: this.totalMarks,
            OutOfMarks: this.outOfMarks,
            Questions: questionInfo,
            BranchId: this.storageService.userDetails.branchId,
            ClientId: this.storageService.userDetails.clientId,
            timeTakenMin: this.totalMins,
            timeTakenSec: this.totalSecs,
        };
        console.log("test details", testDetails);
        this.apiService.sendTestDetails(testDetails).then((data) => console.log(data.data), (err) => console.log(err));
    }
    createPieChart() {
        if (this.page == "saved-test") {
            this.correctPer = Math.round((this.savedTestDetails.corrQueCount / this.savedTestDetails.totalQue) *
                100);
            this.inCorrectPer = Math.round((this.savedTestDetails.inCorrQueCount /
                this.savedTestDetails.totalQue) *
                100);
            this.skippedPer = 100 - (this.correctPer + this.inCorrectPer);
        }
        else if (this.page == "unit-test") {
            this.correctPer = Math.round((this.correctQuestions / this.questionsList.length) * 100);
            this.inCorrectPer = Math.round((this.inCorrectQuestions / this.questionsList.length) * 100);
            this.skippedPer = 100 - (this.correctPer + this.inCorrectPer);
        }
        this.pie1 = new chart_js__WEBPACK_IMPORTED_MODULE_6__["Chart"](this.pieChart1.nativeElement, {
            type: "pie",
            data: {
                datasets: [
                    {
                        data: [this.correctPer, this.inCorrectPer, this.skippedPer],
                        backgroundColor: ["#2dd36f", "#eb445a", "#3880ff"],
                    },
                ],
            },
            options: {
                tooltips: {
                    enabled: false,
                },
            },
        });
        if (this.page == "unit-test") {
            if (this.selectedOptionArr.length == 0) {
                this.accuracy = 0;
            }
            else {
                this.accuracy = Math.round((this.correctQuestions /
                    (this.correctQuestions + this.inCorrectQuestions)) *
                    100);
            }
        }
        else if (this.page == "saved-test") {
            if (this.savedTestDetails.attQueCount == 0) {
                this.accuracy = 0;
            }
            else {
                this.accuracy = Math.round((this.savedTestDetails.corrQueCount /
                    (this.savedTestDetails.corrQueCount +
                        this.savedTestDetails.inCorrQueCount)) *
                    100);
            }
        }
        this.pie2 = new chart_js__WEBPACK_IMPORTED_MODULE_6__["Chart"](this.pieChart2.nativeElement, {
            type: "pie",
            data: {
                datasets: [
                    {
                        data: [this.accuracy, 100 - this.accuracy],
                        backgroundColor: ["#2dd36f", "#eb445a"],
                    },
                ],
            },
            options: {
                tooltips: {
                    enabled: false,
                },
            },
        });
        if (this.page == "unit-test") {
            this.score = Math.round((this.totalMarks / this.outOfMarks) * 100);
        }
        else if (this.page == "saved-test") {
            this.score = Math.round((this.savedTestDetails.obtainedMarks /
                this.savedTestDetails.totalMarks) *
                100);
        }
        this.pie3 = new chart_js__WEBPACK_IMPORTED_MODULE_6__["Chart"](this.pieChart3.nativeElement, {
            type: "pie",
            data: {
                datasets: [
                    {
                        data: [this.score, 100 - this.score],
                        backgroundColor: ["#2dd36f", "#eb445a"],
                    },
                ],
            },
            options: {
                tooltips: {
                    enabled: false,
                },
            },
        });
    }
    solution() {
        this.inCorrOption = "";
        this.testView = true;
        this.index = 0;
        this.showTest();
        this.option = this.questionsList[0].Answer;
        if (this.questionsList[this.index].Answer !=
            this.questionsList[this.index].userAns) {
            this.inCorrOption = this.questionsList[this.index].userAns;
        }
        if ((this.page == "saved-test" || this.page == "unit-test") &&
            this.testSubmitted == true) {
            this.getQuestionStatus();
        }
    }
    selectQuestion(index) {
        this.inCorrOption = "";
        this.testView = true;
        this.index = index;
        this.showTest();
        this.answer = this.questionsList[index].userNumericalAns;
        this.unit = this.questionsList[index].userUnit;
        this.option = this.questionsList[index].Answer;
        if (this.questionsList[this.index].Answer !=
            this.questionsList[this.index].userAns) {
            this.inCorrOption = this.questionsList[this.index].userAns;
        }
        if ((this.page == "saved-test" || this.page == "unit-test") &&
            this.testSubmitted == true) {
            this.getQuestionStatus();
        }
    }
    exitTest() {
        this.navController.back();
        // if (this.page == "unit-test")
        //   this.router.navigateByUrl("/dashboard/unit-test");
        // else {
        //   this.navController.back();
        // }
    }
};
OnlineTestPagePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: _Services_storage_service_service__WEBPACK_IMPORTED_MODULE_2__["StorageServiceService"] },
    { type: _Services_alert_service_service__WEBPACK_IMPORTED_MODULE_3__["AlertServiceService"] },
    { type: _Services_toast_service_service__WEBPACK_IMPORTED_MODULE_4__["ToastServiceService"] },
    { type: _Services_api_service_service__WEBPACK_IMPORTED_MODULE_10__["ApiServiceService"] },
    { type: _Services_save_test_info_service_service__WEBPACK_IMPORTED_MODULE_11__["SaveTestInfoServiceService"] }
];
OnlineTestPagePage.propDecorators = {
    pieChart1: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ["pieChart1",] }],
    pieChart2: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ["pieChart2",] }],
    pieChart3: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ["pieChart3",] }]
};
OnlineTestPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-online-test-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./online-test-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/online-test-page/online-test-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./online-test-page.page.scss */ "./src/app/Pages/online-test-page/online-test-page.page.scss")).default]
    })
], OnlineTestPagePage);



/***/ })

}]);
//# sourceMappingURL=Pages-online-test-page-online-test-page-module-es2015.js.map